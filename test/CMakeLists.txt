# *****************************************************************************
#
# app/SL - Application Scripting Language
#
# (c) 2020, 2024, Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

include (Catch)
enable_testing ()

find_package (Catch2 REQUIRED 3)
find_package (junkbox CONFIG REQUIRED)

add_executable (tests
	component_test/parser_test.cpp
	component_test/list_expression_test.cpp
	component_test/function_application_expression_test.cpp
	component_test/global_runtime_test.cpp
	component_test/evaluation_test.cpp
	component_test/arithmetic_functions_test.cpp
	component_test/expression_algorithm_test.cpp
	component_test/expression_traits_test.cpp
	component_test/def_and_fun_test.cpp
	component_test/scanner_test.cpp
	component_test/arithmetic_parse_eval_test.cpp
	component_test/literals_parse_eval_test.cpp
	component_test/rel_and_logic_parse_eval_test.cpp
	component_test/recursion_test.cpp
	component_test/special_functions_test.cpp
	component_test/collection_functions_test.cpp
	component_test/flow_control_test.cpp
	component_test/expression_sequence_test.cpp

	parser/token_test.cpp
	parser/lexical_error_test.cpp
	parser/scanner_test.cpp
	parser/syntax_error_test.cpp

	expression/nil_expression_test.cpp
	expression/integer_literal_expression_test.cpp
	expression/list_expression_test.cpp
	expression/function_application_expression_test.cpp
	expression/map_literal_expression_test.cpp
	expression/name_expression_test.cpp
	expression/text_literal_expression_test.cpp

	runtime/global_runtime_test.cpp
	runtime/local_runtime_test.cpp
	runtime/runtime_factory_test.cpp)
	

target_link_libraries (tests PRIVATE
    appsl
    junkbox::junkbox
    Catch2::Catch2WithMain)

target_include_directories (tests PRIVATE ${PROJECT_SOURCE_DIR}/test)

catch_discover_tests (tests REPORTER junit OUTPUT_DIR ../test-report)
