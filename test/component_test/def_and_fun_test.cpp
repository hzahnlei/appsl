// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

using namespace appsl;

SCENARIO("Parsing and evaluating 'define' expressions.", "[parse, evaluate, define]")
{

	GIVEN("a fresh stack.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};

		WHEN("we evaluate (def! x 10)")
		{
			Scanner scanner{make_unique<istringstream>("(def! x 10)")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the function to return 10") { REQUIRE(actual->int_value() == 10); }

			AND_THEN("we expect x to be defined") { REQUIRE(stack->lookup("x").int_value() == 10); }
		}
	}

	GIVEN("a fresh stack.")
	{
		Global_Stack_Frame stack;

		WHEN("we evaluate (def! x)")
		{
			Scanner scanner{make_unique<istringstream>("(def! x)")};
			Parser parser{scanner};

			THEN("a 'too few arguments' error will occurr")
			{
				REQUIRE_THROWS_MATCHES(
						parser.parse()->evaluate(stack), Semantic_Error,
						test::pretty_message(
								"Semantic error at line 1, column 7: Too few "
								"arguments. Function 'def!' expects 2 arguments: "
								"'\\'name', '\\'value'. But 1 parameter given: 'x'."));
			}
		}
	}

	GIVEN("function SQ defined like so (def! SQ (fun x (* x x))))")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{make_unique<istringstream>("(def! SQ (fun x (* x x))))")};
		Parser parser{scanner};
		parser.parse()->evaluate(*stack);

		WHEN("when we apply SQ on too many parameters")
		{
			Scanner scanner{make_unique<istringstream>("(SQ 1 2 3)")};
			Parser parser{scanner};

			THEN("a 'too many arguments' error will occurr")
			{
				REQUIRE_THROWS_MATCHES(
						parser.parse()->evaluate(*stack), Semantic_Error,
						test::pretty_message(
								"Semantic error at line 1, column 9: Too many "
								"arguments. Function 'SQ' = '𝛌x.(* x x)' expects 1 "
								"argument: 'x'. But 3 parameters given: '1', '2', "
								"'3'."));
			}
		}
	}

	GIVEN("function INC defined like so (def! INC (fun x y (+ x y))))")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{make_unique<istringstream>("(def! INC (fun (x y) (+ x y))))")};
		Parser parser{scanner};
		parser.parse()->evaluate(*stack);

		WHEN("when we apply INC on too few parameters")
		{
			Scanner scanner{make_unique<istringstream>("(INC 10)")};
			Parser parser{scanner};

			THEN("a 'too few arguments' error will occurr")
			{
				REQUIRE_THROWS_MATCHES(
						parser.parse()->evaluate(*stack), Semantic_Error,
						test::pretty_message("Semantic error at line 1, column 6: Too few "
				                                     "arguments. Function 'INC' = '𝛌(x y).(+ x y)' "
				                                     "expects 2 arguments: 'x', 'y'. But 1 parameter "
				                                     "given: '10'."));
			}
		}
	}

	GIVEN("a fresh stack.")
	{
		Global_Stack_Frame stack;

		WHEN("we evaluate (def! x 10 20)")
		{
			Scanner scanner{make_unique<istringstream>("(def! x 10 20)")};
			Parser parser{scanner};

			THEN("a 'too many arguments' error will occurr")
			{
				REQUIRE_THROWS_MATCHES(parser.parse()->evaluate(stack), Semantic_Error,
				                       test::pretty_message("Semantic error at line 1, column 12: Too "
				                                            "many arguments. Function 'def!' expects 2 "
				                                            "arguments: '\\'name', '\\'value'. But 3 "
				                                            "parameters given: 'x', '10', '20'."));
			}
		}
	}

	GIVEN("a fresh stack.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};

		AND_GIVEN("we define x")
		{
			Scanner scanner{make_unique<istringstream>("(def! x 10)")};
			Parser parser{scanner};
			parser.parse()->evaluate(*stack);

			WHEN("we now try to redefine x")
			{
				Scanner scanner{make_unique<istringstream>("(def! x 20)")};
				Parser parser{scanner};

				THEN("an 'already defined' error will occurr")
				{
					REQUIRE_THROWS_MATCHES(parser.parse()->evaluate(*stack), Semantic_Error,
					                       test::pretty_message("Semantic error at line 1, column "
					                                            "7: Name 'x' already defined."));
				}
			}
		}
	}
}

SCENARIO("Using undefined names in function bodies.", "[parse, evaluate, define, fun]")
{
	GIVEN("a runtime with a function 'DIV' that calls an undefined function 'thow'")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{"(def! DIV (fun (a b) ((= b 0) then:(throw \"Division by zero.\") else:(/ a b))))"};
		Parser parser{scanner};
		parser.parse()->evaluate(*stack);

		WHEN("we call 'DIV'")
		{
			Scanner scanner{"(DIV 1 0)"};
			Parser parser{scanner};

			THEN("we expect an 'unknown function error'")
			{
				REQUIRE_THROWS_MATCHES(parser.parse()->evaluate(*stack), Semantic_Error,
				                       test::pretty_message("Semantic error at line 1, column 37: No "
				                                            "function defined for symbol 'throw'."));
			}
		}
	}
}

SCENARIO("Parsing and evaluating 'fun' expressions.", "[parse, evaluate, define]")
{
	GIVEN("a fresh stack.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};

		WHEN("we evaluate (fun x (+ x x))")
		{
			Scanner scanner{make_unique<istringstream>("(fun x (+ x x))")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the function to return 𝛌x.(+ x x)")
			{
				REQUIRE(actual->stringify() == "𝛌x.(+ x x)");
			}
		}
	}

	GIVEN("a fresh stack.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};

		WHEN("we evaluate (def! times2 (fun x (+ x x)))")
		{
			Scanner scanner{make_unique<istringstream>("(def! times2 (fun x (+ x x)))")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the function to return 𝛌x.(+ x x)")
			{
				REQUIRE(actual->stringify() == "𝛌x.(+ x x)");
			}
		}
	}

	GIVEN("times2 defined as (fun x (+ x x))")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{make_unique<istringstream>("(def! times2 (fun x (+ x x)))")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};

		WHEN("we evaluate (times2 9)")
		{
			Scanner scanner{make_unique<istringstream>("(times2 9)")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the result to be 18") { REQUIRE(actual->stringify() == "18"); }
		}
	}
}

SCENARIO("In place definition and use of functions.", "[parse, evaluate, define, function]")
{
	GIVEN("a fresh stack.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};

		WHEN("we define a function in place and immediately apply it")
		{
			Scanner scanner{make_unique<istringstream>("((fun x (+ x x)) 10)")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the result to be the application of the newly defined function")
			{
				REQUIRE(actual->stringify() == "20");
			}
		}
	}

	GIVEN("a fresh stack.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};

		WHEN("we define a function in place and immediately apply it")
		{
			Scanner scanner{make_unique<istringstream>("((def! times2 (fun x (+ x x))) 10)")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the result to be the application of the newly defined function")
			{
				REQUIRE(actual->stringify() == "20");
			}
		}
	}

	GIVEN("a fresh stack.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		WHEN("we define a function and immediately use it")
		{
			Scanner scanner{make_unique<istringstream>("((def! times2 (fun x (+ x x))) 10)")};
			Parser parser{scanner};
			parser.parse()->evaluate(*stack);

			THEN("it should be defined and usble in the outer scope as well")
			{
				Scanner scanner{make_unique<istringstream>("(times2 64)")};
				Parser parser{scanner};
				const auto actual{parser.parse()->evaluate(*stack)};

				REQUIRE(actual->stringify() == "128");
			}
		}
	}
}

SCENARIO("Error checking on function definition.", "[parse, evaluate, define, function, error]")
{
	GIVEN("a fresh runtime")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		WHEN("we define a function with duplicate argument x")
		{
			Scanner scanner{make_unique<istringstream>("(fun (x x) (+ y x))")};
			Parser parser{scanner};

			THEN("we expect an 'dublicate parameter' error")
			{
				REQUIRE_THROWS_MATCHES(parser.parse()->evaluate(*stack), Semantic_Error,
				                       test::pretty_message("Semantic error at line 1, column 9: "
				                                            "Duplicate definition of parameter 'x'."));
			}
		}
	}

	GIVEN("x and y not defined")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		WHEN("we define a function with argument x but that uses y int it's body")
		{
			Scanner scanner{make_unique<istringstream>("(fun x (+ y x))")};
			Parser parser{scanner};

			THEN("we expect an 'y not defined' error")
			{
				REQUIRE_THROWS_MATCHES(parser.parse()->evaluate(*stack), Semantic_Error,
				                       test::pretty_message("Semantic error at line 1, column 11: Name "
				                                            "'y' is undefined."));
			}
		}
	}
}

SCENARIO("Defining macro functions.", "[parse, evaluate, define, function, macro]")
{
	GIVEN("a fresh stack.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};

		WHEN("a function's parameters are quoted")
		{
			Scanner scanner{make_unique<istringstream>("(def! TRUE (fun ('then 'else) then))")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the function to return 𝛌('then 'else).then")
			{
				REQUIRE(actual->stringify() == "𝛌('then 'else).then");
			}
		}
	}

	GIVEN("macro TRUE defined such that it does not evaluate.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{make_unique<istringstream>("(def! TRUE (fun ('then 'else) then))")};
		Parser parser{scanner};
		parser.parse()->evaluate(*stack);

		WHEN("when we apply TRUE")
		{
			Scanner scanner{make_unique<istringstream>("(TRUE (+ 1 2) (+ 3 4))")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect it to return the first argument in unevaluated form")
			{
				REQUIRE(actual->stringify() == "(+ 1 2)");
			}
		}
	}

	GIVEN("macro TRUE defined such that it evaluates.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{make_unique<istringstream>("(def! TRUE (fun ('then 'else) (eval then)))")};
		Parser parser{scanner};
		parser.parse()->evaluate(*stack);

		WHEN("when we apply TRUE")
		{
			Scanner scanner{make_unique<istringstream>("(TRUE (+ 1 2) (+ 3 4))")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect it to return the first argument in evaluated form")
			{
				REQUIRE(actual->stringify() == "3");
			}
		}
	}
}

SCENARIO("Using variadic parameters", "[parse, evaluate, define, function, macro]")
{
	GIVEN("a fresh stack.")
	{
		auto stack{make_shared<Global_Stack_Frame>()};

		WHEN("we define (def! ADD (fun terms... (+ terms)))")
		{
			Scanner scanner{make_unique<istringstream>("(def! ADD (fun terms... (+ terms)))")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the function to return 𝛌terms....(+ terms)")
			{
				REQUIRE(actual->stringify() == "𝛌terms....(+ terms)");
			}
		}
	}

	GIVEN("(def! ADD (fun terms... (+ terms)))")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{make_unique<istringstream>("(def! ADD (fun [term terms...] (+ term terms)))")};
		Parser parser{scanner};
		parser.parse()->evaluate(*stack);

		WHEN("we apply ADD like so (ADD 1 2 3)")
		{
			Scanner scanner{make_unique<istringstream>("(ADD 1 2 3)")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the result to be 6") { REQUIRE(actual->stringify() == "6"); }
		}
	}

	GIVEN("a fresh runtime")
	{
		auto stack{make_shared<Global_Stack_Frame>()};

		WHEN("we try to define multiple variadic parameters")
		{
			Scanner scanner{make_unique<istringstream>(
					"(def! ADD (fun (terms... more-terms...) (+ terms more-terms)))")};
			Parser parser{scanner};

			THEN("we expect an 'not last parameter' error")
			{
				REQUIRE_THROWS_MATCHES(
						parser.parse()->evaluate(*stack), Semantic_Error,
						test::pretty_message("Semantic error at line 1, column 36: A function "
				                                     "can only have one variadic "
				                                     "parameter. And, it has to be the last parameter. "
				                                     "However, variadic parameter "
				                                     "'terms...' is followed by further parameters, "
				                                     "for example 'more-terms...'."));
			}
		}
	}
}

SCENARIO("Using quoted, variadic parameters", "[parse, evaluate, define, function, macro]")
{
	GIVEN("(def! ADD (fun 'xs... (+ xs)))")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{make_unique<istringstream>("(def! ADD (fun ('x 'xs...) (+ x xs)))")};
		Parser parser{scanner};
		parser.parse()->evaluate(*stack);

		// This works, because the literals do not need evauation
		WHEN("apply ADD like so (ADD 1 2 3 4 5 6)")
		{
			Scanner scanner{make_unique<istringstream>("(ADD 1 2 3 4 5 6)")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the function to return 21") { REQUIRE(actual->stringify() == "21"); }
		}

		WHEN("apply ADD like so (ADD (+ 1 2) 3 4 5 6)")
		{
			Scanner scanner{make_unique<istringstream>("(ADD 1 (+ 2 3) 4 5 6)")};
			Parser parser{scanner};
			THEN("we see by the exception thrown, that the arguments are actually passed unevaluated")
			{
				REQUIRE_THROWS_MATCHES(parser.parse()->evaluate(*stack), appsl::Runtime_Error,
				                       test::pretty_message("Runtime error: Sum function is about to "
				                                            "add integer numbers. "
				                                            "However, '+' is no integer number."));
			}
		}
	}
}

SCENARIO("Function returning a closure.", "[parse, evaluate, define, function, macro]")
{
	GIVEN("a fresh runtime")
	{
		auto stack{make_shared<Global_Stack_Frame>()};

		WHEN("we define a function that returns a function which refers to the outer scope")
		{
			Scanner scanner{"(def! TIMES_N (fun n (fun x (* x n))))"};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect this to work") { REQUIRE(actual->stringify() == "𝛌n.(fun x (* x n))"); }
		}
	}

	GIVEN("a function that returns a function which refers to the outer scope")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{"(def! TIMES_N (fun n (fun x (* x n))))"};
		Parser parser{scanner};
		parser.parse()->evaluate(*stack);

		WHEN("when we apply this function")
		{
			Scanner scanner{"(def! TIMES_10 (TIMES_N 10))"};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we should get the inner function with captured scope (closure)")
			{
				REQUIRE(actual->stringify() == "𝛌x.(* x n) (where n=10)");
			}
		}
	}

	GIVEN("a closure")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{"(def! TIMES_N (fun n (fun x (* x n))))\n"
		                "(def! TIMES_10 (TIMES_N 10))"};
		Parser parser{scanner};
		while (parser.has_more_to_parse())
		{
			parser.parse()->evaluate(*stack);
		}

		WHEN("apply it to some input")
		{
			Scanner scanner{"(TIMES_10 77)"};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("a correct result should be computed") { REQUIRE(actual->stringify() == "770"); }
		}
	}
}

SCENARIO("Use of named arguments.", "[named argument, name/value pair]")
{
	GIVEN("a fresh runtime with additional functions from the factory.")
	{
		auto stack{make_runtime()};

		WHEN("we evaluate ((odd? 9) then : 1 else : 2)")
		{
			Scanner scanner{make_unique<istringstream>("((odd? 9) then : 1 else : 2)")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the function to return 1") { REQUIRE(actual->stringify() == "1"); }
		}

		WHEN("we evaluate ((odd? 8) then : 1 else : 2)")
		{
			Scanner scanner{make_unique<istringstream>("((odd? 8) then : 1 else : 2)")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the function to return 2") { REQUIRE(actual->stringify() == "2"); }
		}

		WHEN("we evaluate ((odd? 8) then : 1 something-else : 2)")
		{
			Scanner scanner{make_unique<istringstream>("((odd? 8) then : 1 something-else : 2)")};
			Parser parser{scanner};

			THEN("we expect a 'name mismatch'")
			{
				REQUIRE_THROWS_MATCHES(
						parser.parse()->evaluate(*stack), Semantic_Error,
						test::pretty_message(
								"Semantic error at line 1, column 20: Function '(odd? "
								"8)' = 'nil' has a parameter with name 'else'. "
								"However, the given named argument 'something-else' = "
								"'2' does not match that name."));
			}
		}

		WHEN("we evaluate (cons lhs: 1 rhs: 2)")
		{
			Scanner scanner{make_unique<istringstream>("(cons lhs: 1 rhs: 2)")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the function to return [1 2]") { REQUIRE(actual->stringify() == "[1 2]"); }
		}

		WHEN("we evaluate (cons links: 1 rechts: 2)")
		{
			Scanner scanner{make_unique<istringstream>("(cons links: 1 rechts: 2)")};
			Parser parser{scanner};

			THEN("we expect a 'name mismatch'")
			{
				REQUIRE_THROWS_MATCHES(
						parser.parse()->evaluate(*stack), Semantic_Error,
						test::pretty_message(
								"Semantic error at line 1, column 7: Function 'cons' = "
								"'𝛌(lhs rhs).[lhs rhs]' has a parameter with name "
								"'lhs'. However, the given named argument 'links' = "
								"'1' does not match that name."));
			}
		}

		WHEN("we evaluate named arguments that are complex")
		{
			Scanner scanner{make_unique<istringstream>("(cons lhs: (* 2 3) rhs: (* 3 4))")};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect this to work and return [6 12]") { REQUIRE(actual->stringify() == "[6 12]"); }
		}
	}
}

SCENARIO("Defining a 'false' constant as function similar to the built-in 'true' function.",
         "[fast, CT, boolean, false]")
{
	GIVEN("a user defined false function")
	{
		auto stack{make_runtime()};
		Scanner scanner{"(def! FALSE (fun ('then 'else) (eval else)))"};
		Parser parser{scanner};
		parser.parse()->evaluate(*stack);
		GIVEN("a user defined ODD? function that returns true or FALSE")
		{
			Scanner scanner{make_unique<istringstream>(
					"(def! ODD? (fun n "
					"                ((odd? n) then: true else: FALSE) ))")};
			Parser parser{scanner};
			parser.parse()->evaluate(*stack);

			WHEN("we apply ODD? to 2")
			{
				Scanner scanner{R"(((ODD? 2) then: "odd" else: "even"))"};
				Parser parser{scanner};
				const auto actual{parser.parse()->evaluate(*stack)};

				THEN("we expect the answer to be 'even'")
				{
					REQUIRE(actual->stringify() == "\"even\"");
				}
			}
			WHEN("we apply ODD? to 3")
			{
				Scanner scanner{R"(((ODD? 3) then: "odd" else: "even"))"};
				Parser parser{scanner};
				const auto actual{parser.parse()->evaluate(*stack)};

				THEN("we expect the answer to be 'odd'") { REQUIRE(actual->stringify() == "\"odd\""); }
			}
		}
	}
}

TEST_CASE("Results from functions can be ignored", "[ignore, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner_given{"(def! _ 10)"};
	Parser parser_given{scanner_given};
	const auto actual_given{parser_given.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser_given.has_more_to_parse());
	REQUIRE(actual_given->stringify() == "10");
	// WHEN
	Scanner scanner_when{"_"};
	Parser parser_when{scanner_when};
	const auto actual_when{parser_when.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser_when.has_more_to_parse());
	// THEN
	CHECK(actual_when->stringify() == "nil");
}

TEST_CASE("'Let' becomes a procedural 'do' by ignoring results", "[ignore, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(let [(_ 10)" // These let "assignments" could have side-effects
	                "      (_ 20) ]"
	                "     _ )"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(actual->stringify() == "nil");
}
