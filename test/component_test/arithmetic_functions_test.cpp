// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function_application_expression.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>
#include <iostream>

using namespace appsl;

TEST_CASE("Sum function expects arguments.", "[arithmetic, sum]")
{
	Global_Stack_Frame stack;
	auto fun_name{make_unique<Name_Expression>(Token{Token::Type::NAME, "+", Token::NO_POS})};
	auto arguments{make_unique<Nil_Expression>(Token::NONE)};
	Function_Application_Expression fun_app{Token::NONE, move(fun_name), move(arguments)};
	REQUIRE_THROWS_MATCHES(
			fun_app.evaluate(stack), Semantic_Error,
			test::pretty_message("Semantic error at line 0, column 0: Too few arguments. Function '+' "
	                                     "expects 2 arguments: 'term', 'terms...'. But no parameters given."));
}

TEST_CASE("Sum function returns value of argument if only one argument given.", "[arithmetic, sum]")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	auto fun_name{make_unique<Name_Expression>(Token{Token::Type::NAME, "+", Token::NO_POS})};
	auto arguments{make_unique<List_Expression>(Token::NONE,
	                                            make_unique<Integer_Literal_Expression>(Token::NONE, 123))};
	Function_Application_Expression fun_app{Token::NONE, move(fun_name), move(arguments)};
	REQUIRE(*fun_app.evaluate(*stack) == Integer_Literal_Expression{Token::NONE, 123});
}

TEST_CASE("Sum function returns sum of all arguments.", "[arithmetic, sum]")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	auto fun_name{make_unique<Name_Expression>(Token{Token::Type::NAME, "+", Token::NO_POS})};
	auto arguments{make_unique<List_Expression>(
			Token::NONE, make_unique<Integer_Literal_Expression>(Token::NONE, 1),
			make_unique<List_Expression>(
					Token::NONE, make_unique<Integer_Literal_Expression>(Token::NONE, 2),
					make_unique<List_Expression>(
							Token::NONE,
							make_unique<Integer_Literal_Expression>(Token::NONE, 3))))};
	Function_Application_Expression fun_app{Token::NONE, move(fun_name), move(arguments)};
	REQUIRE(*fun_app.evaluate(*stack) == Integer_Literal_Expression{Token::NONE, 6});
}

TEST_CASE("Sum function cannot add lists of mixed types.", "[arithmetic, sum]")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	auto fun_name{make_unique<Name_Expression>(Token{Token::Type::NAME, "+", Token::NO_POS})};
	auto arguments{make_unique<List_Expression>(
			Token::NONE, make_unique<Real_Literal_Expression>(Token::NONE, 1.0),
			make_unique<List_Expression>(
					Token::NONE, make_unique<Integer_Literal_Expression>(Token::NONE, 2),
					make_unique<List_Expression>(
							Token::NONE,
							make_unique<Integer_Literal_Expression>(Token::NONE, 3))))};
	Function_Application_Expression fun_app{Token::NONE, move(fun_name), move(arguments)};
	REQUIRE_THROWS_MATCHES(
			fun_app.evaluate(*stack), appsl::Runtime_Error,
			test::pretty_message("Runtime error: Sum function is about to add real numbers. However, "
	                                     "'2' is no real number."));
}
