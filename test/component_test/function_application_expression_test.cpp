// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function_application_expression.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("Function_application_Expression's token can be accessed.", "[Function_application_Expression]")
{
	const Token token{Token::Type::NAME, "some-name", Token::NO_POS};
	const Function_Application_Expression expr{token, make_unique<Name_Expression>(token)};
	REQUIRE(expr.token() == token);
}

TEST_CASE("Comparison Function_application_Expression for equality works.", "[Function_application_Expression]")
{
	const Token token{Token::Type::NAME, "some-name", Token::NO_POS};
	const Function_Application_Expression expr{token, make_unique<Name_Expression>(token)};
	REQUIRE(expr.operator==(expr)); // Getting an error with C++20 for expr == expr
}

TEST_CASE("Comparison Function_application_Expression for equality works (not equal).",
          "[Function_application_Expression]")
{
	const Token token1{Token::Type::NAME, "some-name", Token::NO_POS};
	const Function_Application_Expression expr1{token1, make_unique<Name_Expression>(token1)};
	const Token token2{Token::Type::NAME, "some-other-name", Token::NO_POS};
	const Function_Application_Expression expr2{token2, make_unique<Name_Expression>(token2)};
	REQUIRE_FALSE(expr1.operator==(expr2));
}

TEST_CASE("Function_application_Expression's head can be accessed.", "[Function_application_Expression]")
{
	const Token token{Token::Type::NAME, "some-name", Token::NO_POS};
	const Function_Application_Expression expr{token, make_unique<Name_Expression>(token)};
	REQUIRE(expr.head().token() == token);
	REQUIRE(expr.tail() == *Nil_Expression::NIL);
}

TEST_CASE("Function_application_Expression's tail can be accessed.", "[Function_application_Expression]")
{
	const Token token1{Token::Type::NAME, "some-name", Token::NO_POS};
	const Token token2{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS};
	const Function_Application_Expression expr{token1, make_unique<Name_Expression>(token1),
	                                           make_unique<Integer_Literal_Expression>(token2, 123)};
	REQUIRE(expr.head().token() == token1);
	REQUIRE(expr.tail().token() == token2);
}

// Evaluation tested separately, because too complex.

TEST_CASE("Function_application_Expression is not a name.", "[Function_application_Expression]")
{
	const Token token{Token::Type::NAME, "some-name", Token::NO_POS};
	const Function_Application_Expression expr{token, make_unique<Name_Expression>(token)};
	REQUIRE_FALSE(expr.is_name());
}

TEST_CASE("Function_application_Expression's printing operator works.", "[Function_application_Expression]")
{
	const Token token1{Token::Type::NAME, "some-name", Token::NO_POS};
	const Token token2{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS};
	const Function_Application_Expression expr{token1, make_unique<Name_Expression>(token1),
	                                           make_unique<Integer_Literal_Expression>(token2, 123)};
	stringstream result;
	expr << result;
	REQUIRE(result.str() == "(EXPR type:function application (TOKEN type:NAME line:0 col:0 val:'some-name') "
	                        "head:(EXPR type:name (TOKEN type:NAME line:0 col:0 val:'some-name')) tail:(EXPR "
	                        "type:integer_literal value:123 (TOKEN type:INTEGER_LITERAL line:0 col:0 val:'123')))");
}

TEST_CASE("Expression's global printing operator works for Function_application_Expression.",
          "[Function_application_Expression]")
{
	const Token token1{Token::Type::NAME, "some-name", Token::NO_POS};
	const Token token2{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS};
	const Function_Application_Expression expr{token1, make_unique<Name_Expression>(token1),
	                                           make_unique<Integer_Literal_Expression>(token2, 123)};
	stringstream result;
	result << expr;
	REQUIRE(result.str() == "(EXPR type:function application (TOKEN type:NAME line:0 col:0 val:'some-name') "
	                        "head:(EXPR type:name (TOKEN type:NAME line:0 col:0 val:'some-name')) tail:(EXPR "
	                        "type:integer_literal value:123 (TOKEN type:INTEGER_LITERAL line:0 col:0 val:'123')))");
}
