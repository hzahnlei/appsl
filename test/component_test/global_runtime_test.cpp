// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("Expressions can be put and looked-up by name.", "[Global_Runtime_Test]")
{
	Global_Stack_Frame stack;

	const Token token{Token::Type::INTEGER_LITERAL, "4711", Token::NO_POS};
	auto expr{make_unique<Integer_Literal_Expression>(token, 4711)};
	stack.define_local_var("some-name", move(expr));
	REQUIRE(stack.lookup("some-name").token() == token);
}

TEST_CASE("Looking-up unknown names returns NIL.", "[Global_Runtime_Test]")
{
	Global_Stack_Frame stack;

	REQUIRE(stack.lookup("some-name") == *Nil_Expression::NIL);
}

TEST_CASE("Cannot redefine name.", "[Global_Runtime_Test]")
{
	Global_Stack_Frame stack;
	const Token token{Token::Type::INTEGER_LITERAL, "4711", Token::NO_POS};
	auto expr{make_shared<Integer_Literal_Expression>(token, 4711)};
	stack.define_local_var("some-name", expr);

	const Token other_token{Token::Type::INTEGER_LITERAL, "666", Token::NO_POS};
	auto other_expr{make_shared<Integer_Literal_Expression>(other_token, 666)};
	REQUIRE_THROWS_MATCHES(stack.define_local_var("some-name", other_expr), appsl::Runtime_Error,
	                       test::pretty_message("Runtime error: Name 'some-name' already defined in scope. Cannot "
	                                            "override '4711' with '666'."));
}

TEST_CASE("Look-up will read from parent if name not defined locally.", "[Global_Runtime_Test]")
{
	auto global{make_shared<Global_Stack_Frame>()};
	const Token token{Token::Type::INTEGER_LITERAL, "4711", Token::NO_POS};
	auto expr{make_unique<Integer_Literal_Expression>(token, 4711)};
	global->define_local_var("some-name", move(expr));

	auto local{global->new_child()};
	REQUIRE(local != nullptr);
	REQUIRE(local->lookup("some-name").token() == token);
}
