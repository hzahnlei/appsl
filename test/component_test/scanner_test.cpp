// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/lexical_error.hpp"
#include "appsl/parser/scanner.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

TEST_CASE("Scanner returns EOF token on empty input.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>("")};

	const Token expected{Token::Type::END_OF_FILE, "", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.current_token().is_eof());
	REQUIRE_FALSE(scanner.tokens_available());
	REQUIRE_FALSE(scanner.advance_to_next_token());
}

TEST_CASE("Scanner returns EOF token on whitespace-only input.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>(" ")};

	const Token expected{Token::Type::END_OF_FILE, "", Token::Position{1, 2}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.current_token().is_eof());
	REQUIRE_FALSE(scanner.tokens_available());
	REQUIRE_FALSE(scanner.advance_to_next_token());
}

TEST_CASE("Ignores comments, returns meaningful tokens.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>(" ;This is a comment \n"
	                                           "1234 ; A literal followed by comment\n"
	                                           "-13.74 ;; Another literal followed by a comment\n"
	                                           "      678")};

	Token expected{Token::Type::INTEGER_LITERAL, "1234", Token::Position{2, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::REAL_LITERAL, "-13.74", Token::Position{3, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::INTEGER_LITERAL, "678", Token::Position{4, 7}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Recognizes names.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>("< =< == >= > + - * /\n"
	                                           "abc Abc ABC _hello_world hello-world odd? def! (f,x) a' b'c d'''")};

	Token expected{Token::Type::NAME, "<", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "=<", Token::Position{1, 3}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "==", Token::Position{1, 6}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, ">=", Token::Position{1, 9}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, ">", Token::Position{1, 12}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "+", Token::Position{1, 14}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "-", Token::Position{1, 16}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "*", Token::Position{1, 18}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "/", Token::Position{1, 20}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "abc", Token::Position{2, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "Abc", Token::Position{2, 5}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "ABC", Token::Position{2, 9}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "_hello_world", Token::Position{2, 13}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "hello-world", Token::Position{2, 26}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "odd?", Token::Position{2, 38}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "def!", Token::Position{2, 43}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::L_PARENTHESIS, "(", Token::Position{2, 48}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "f", Token::Position{2, 49}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::COMMA, ",", Token::Position{2, 50}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "x", Token::Position{2, 51}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::R_PARENTHESIS, ")", Token::Position{2, 52}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "a'", Token::Position{2, 54}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "b'", Token::Position{2, 57}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "c", Token::Position{2, 59}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "d'''", Token::Position{2, 61}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Recognizes integer literals (decimal).", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>("1012 -5 +10 123abc")};

	Token expected{Token::Type::INTEGER_LITERAL, "1012", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::INTEGER_LITERAL, "-5", Token::Position{1, 6}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::INTEGER_LITERAL, "+10", Token::Position{1, 9}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 13}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "abc", Token::Position{1, 16}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Recognizes real literals.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>("1012.0 -5.013 +10.666 123.0abc")};

	Token expected{Token::Type::REAL_LITERAL, "1012.0", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::REAL_LITERAL, "-5.013", Token::Position{1, 8}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::REAL_LITERAL, "+10.666", Token::Position{1, 15}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::REAL_LITERAL, "123.0", Token::Position{1, 23}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "abc", Token::Position{1, 28}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Recognizes real literals only if digit before and after fullstop.", "[Scanner]")
{
	const string erroneous_source{" 123. "};
	REQUIRE_THROWS_MATCHES(make_unique<Scanner>(make_unique<istringstream>(erroneous_source)), Lexical_Error,
	                       test::pretty_message("Lexical error at line 1, column 5: Premature end of real literal. "
	                                            "Expected digits but found '.'."));
}

TEST_CASE("Recognizes text literals.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>(" \"hello\"  \"123\"  \"45.6\"  \"    \"")};

	Token expected{Token::Type::TEXT_LITERAL, "hello", Token::Position{1, 2}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::TEXT_LITERAL, "123", Token::Position{1, 11}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::TEXT_LITERAL, "45.6", Token::Position{1, 18}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::TEXT_LITERAL, "    ", Token::Position{1, 26}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Recognizes text literals (which contain escaped chars).", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>(" \"\\\\\"  \"\\\"\"  \"\\n\" ")};

	Token expected = {Token::Type::TEXT_LITERAL, "\\", Token::Position{1, 2}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::TEXT_LITERAL, "\"", Token::Position{1, 8}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::TEXT_LITERAL, "\\n", Token::Position{1, 14}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Recognizes premature end of line when scanning text literals.", "[Scanner]")
{
	const string erroneous_source{" \"hello\nworld\" "};
	REQUIRE_THROWS_MATCHES(make_unique<Scanner>(make_unique<istringstream>(erroneous_source)), Lexical_Error,
	                       test::pretty_message("Lexical error at line 1, column 7: Premature end of text literal. "
	                                            "Expected '\"' but reached end of line."));
}

TEST_CASE("Recognizes premature end of file when scanning text literals.", "[Scanner]")
{
	const string erroneous_source{" \"hello world"};
	REQUIRE_THROWS_MATCHES(
			make_unique<Scanner>(make_unique<istringstream>(erroneous_source)), Lexical_Error,
			test::pretty_message(
					"Lexical error at line 1, column 13: Premature end of text literal. Expected "
					"'\"' but reached end of file."));
}

TEST_CASE("Recognizes various special symbols.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>(": ( ) { } [ ] # #{ -> . ,")};

	Token expected{Token::Type::COLON, ":", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::L_PARENTHESIS, "(", Token::Position{1, 3}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::R_PARENTHESIS, ")", Token::Position{1, 5}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::L_CURLY_BRACE, "{", Token::Position{1, 7}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::R_CURLY_BRACE, "}", Token::Position{1, 9}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::L_BRACKET, "[", Token::Position{1, 11}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::R_BRACKET, "]", Token::Position{1, 13}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::HASH, "#", Token::Position{1, 15}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::HASH_MAP, "#{", Token::Position{1, 17}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::R_ARROW, "->", Token::Position{1, 20}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::FULL_STOP, ".", Token::Position{1, 23}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::COMMA, ",", Token::Position{1, 25}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

// Originally I was using . as cons operator. Later I wantet to use it for accessing record fields (just like in many
// other programming languages). So I made the comma to become the cons operaor. Before, the comma was just swalloed. It
// could be used to visually separate list elements without effect.
TEST_CASE("Comma is not ignored anymore.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>("a,b")};

	Token expected{Token::Type::NAME, "a", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::COMMA, ",", Token::Position{1, 2}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "b", Token::Position{1, 3}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Names of predicate functions should end with question mark.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>("odd? even? ?")};

	Token expected{Token::Type::NAME, "odd?", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "even?", Token::Position{1, 6}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "?", Token::Position{1, 12}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Names of functions with side-effects should end with exclamation mark.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>("def! set! !")};

	Token expected{Token::Type::NAME, "def!", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "set!", Token::Position{1, 6}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "!", Token::Position{1, 11}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Recognizes ellipsis.", "[Scanner]")
{
	Scanner scanner{make_unique<istringstream>("... abc...def")};

	Token expected{Token::Type::ELLIPSIS, "...", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "abc", Token::Position{1, 5}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::ELLIPSIS, "...", Token::Position{1, 8}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::NAME, "def", Token::Position{1, 11}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("A single full stop is not a half ellipsis but a full stop.", "[Scanner]")
{
	Scanner scanner{"."};

	Token expected{Token::Type::FULL_STOP, ".", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Numbers don't start with full stop-", "[Scanner]")
{
	Scanner scanner{".123"};

	Token expected{Token::Type::FULL_STOP, ".", Token::Position{1, 1}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE(scanner.advance_to_next_token());
	REQUIRE(scanner.tokens_available());

	expected = {Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}};
	REQUIRE(scanner.current_token() == expected);
	REQUIRE_FALSE(scanner.advance_to_next_token());
	REQUIRE_FALSE(scanner.tokens_available());
}

TEST_CASE("Recognizes premature end of ellipsis for two full-stops.", "[Scanner]")
{
	const string erroneous_source{".."};
	REQUIRE_THROWS_MATCHES(make_unique<Scanner>(make_unique<istringstream>(erroneous_source)), Lexical_Error,
	                       test::pretty_message("Lexical error at line 1, column 3: Premature end of ellipsis. "
	                                            "Expected '.' but reached end of file."));
}
