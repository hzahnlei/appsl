// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "test_tools/stack_frame_mock.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("Nil_Expression and Integer_Literal_Expression are not equal.", "[Integer_Literal_Expression]")
{
	const Nil_Expression nil_expr{Token::NONE};
	const Integer_Literal_Expression int_expr{Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}},
	                                          123};
	REQUIRE_FALSE(nil_expr.operator==(int_expr)); // Getting an error with C++20 for nil_expr == int_expr
	REQUIRE_FALSE(int_expr.operator==(nil_expr));
}

TEST_CASE("List_Expression's token can be accessed (none-empty list)'.", "[List_Expression]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}};
	const List_Expression expr{
			token, make_unique<Integer_Literal_Expression>(token, 123),
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "456", Token::Position{1, 5}}, 456)};
	REQUIRE(expr.token() == token);
}

TEST_CASE("Comparison List_Expression for equality works (none-empty list).", "[List_Expression]")
{
	const List_Expression expr{
			Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}},
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}}, 123),
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "456", Token::Position{1, 5}}, 456)};
	REQUIRE(expr.operator==(expr));
}

TEST_CASE("Comparison List_Expression for equality works (none-empty, none-equal list).", "[List_Expression]")
{
	const List_Expression expr1{
			Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}},
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}}, 123),
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "456", Token::Position{1, 5}}, 456)};
	const List_Expression expr2{
			Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}},
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}}, 123),
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "4568", Token::Position{1, 5}}, 4568)};
	REQUIRE_FALSE(expr1.operator==(expr2));
}

TEST_CASE("List_Expression's printing operator works (none-empty list).", "[List_Expression]")
{
	const List_Expression expr{
			Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}},
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}}, 123),
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "456", Token::Position{1, 5}}, 456)};
	stringstream result;
	expr << result;
	REQUIRE(result.str() ==
	        "(EXPR type:list (TOKEN type:INTEGER_LITERAL line:1 col:1 val:'123') head:(EXPR type:integer_literal "
	        "value:123 (TOKEN type:INTEGER_LITERAL line:1 col:1 val:'123')) tail:(EXPR type:integer_literal "
	        "value:456 (TOKEN type:INTEGER_LITERAL line:1 col:5 val:'456')))");
}

TEST_CASE("CT Expression's global printing operator works for List_Expression (none-empty list).", "[List_Expression]")
{
	const List_Expression expr{
			Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}},
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}}, 123),
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "456", Token::Position{1, 5}}, 456)};
	stringstream result;
	result << expr;
	REQUIRE(result.str() ==
	        "(EXPR type:list (TOKEN type:INTEGER_LITERAL line:1 col:1 val:'123') head:(EXPR type:integer_literal "
	        "value:123 (TOKEN type:INTEGER_LITERAL line:1 col:1 val:'123')) tail:(EXPR type:integer_literal "
	        "value:456 (TOKEN type:INTEGER_LITERAL line:1 col:5 val:'456')))");
}

TEST_CASE("CT List_Expression evaluates to itself (none-empty list).", "[List_Expression]")
{
	const auto expr{make_shared<List_Expression>(
			Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}},
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}}, 123),
			make_unique<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "456", Token::Position{1, 5}}, 456))};
	REQUIRE(*expr->evaluate(test::STACK_MOCK) == *expr);
}
