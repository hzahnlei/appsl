// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression_traits.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>
#include <iostream>

using namespace appsl;

SCENARIO("Recursion without using any recursion feature of the language."
         "Using the Y combinator to achive recursion.",
         "[recursion]")
{

	/*!
	 * This corresponds to the Clojure example found on https://rosettacode.org/wiki/Y_combinator.
	 * (def! Y (fun f ((fun x (x x)) (fun x (f (fun args... ((x x) args) ))))))
	 * (def! fac (fun f (fun n ((= n 0) 1 (* n (f (- n 1))) ))))
	 */
	GIVEN("The Y combinator and a function using it")
	{
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{"(def! Y (fun f \n"
		                "    ((fun x (x x)) \n"
		                "        (fun x (f \n"
		                "            (fun args... ((x x) args) )))))) \n"
		                "\n"
		                "(def! fac (fun f \n"
		                "    (fun n \n"
		                "        ((= n 0) \n"
		                "            1 \n"
		                "            (* n (f (- n 1))) ))))"};
		Parser parser{scanner};
		while (parser.has_more_to_parse())
		{
			parser.parse()->evaluate(*stack);
		}

		WHEN("we evaluate ((Y fac) 10)")
		{
			Scanner scanner{"((Y fac) 10)"};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the result to be an integer number") { REQUIRE(actual->is_integer()); }
			AND_THEN("we expect the result to be 3628800") { REQUIRE(actual->int_value() == 3628800); }
		}
	}

	/*!
	 * This is modeled after the Scheme example found on https://rosettacode.org/wiki/Y_combinator.
	 * (def! apply (fun (f args...) (f args)))
	 * (def! Y (fun (f) ((fun (g) (g g)) (fun (g) (f (fun a (apply (g g) a)))))))
	 * (def! fac (Y (fun (r) (fun (x) ((< x 2) 1 (* x (r (- x 1))))))))
	 */
	GIVEN("Another Y combinator and another function using it")
	{
		// Using the runtime directly, not produced by the factory.
		// Additional functions missing in this case.
		// Hence, have to define apply first.
		auto stack{make_shared<Global_Stack_Frame>()};
		Scanner scanner{"(def! apply (fun (f args...) (f args)))\n"
		                "\n"
		                "(def! Y \n"
		                "    (fun (f) \n"
		                "        ((fun (g) (g g)) \n"
		                "            (fun (g) \n"
		                "                (f (fun a (apply (g g) a))) )))) \n"
		                "\n"
		                "(def! fac \n"
		                "    (Y (fun (r) \n"
		                "        (fun (x) ((< x 2) \n"
		                "            1 \n"
		                "            (* x (r (- x 1))) )))))"};
		Parser parser{scanner};
		while (parser.has_more_to_parse())
		{
			parser.parse()->evaluate(*stack);
		}

		WHEN("we evaluate (fac 6)")
		{
			Scanner scanner{"(fac 6)"};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the result to be an integer number") { REQUIRE(actual->is_integer()); }
			AND_THEN("we expect the result to be 720") { REQUIRE(actual->int_value() == 720); }
		}
	}

	GIVEN("The Y combinator and a collatz function that uses Y")
	{
		// Using the runtime directly, not produced by the factory.
		// Additional functions missing in this case.
		// Hence, have to define apply first.
		auto stack{make_runtime()};
		Scanner scanner{"(def! Y \n"
		                "    (fun (f) \n"
		                "        ((fun (g) (g g)) \n"
		                "            (fun (g) \n"
		                "                (f (fun a \n"
		                "                    (apply (g g) a)) ))))) \n"
		                "\n"
		                "(def! COLLATZ \n"
		                "    (Y (fun (r) \n"
		                "        (fun (n) \n"
		                "            ((odd? n) \n"
		                "                ((= 1 n) \n"
		                "                    1 \n"
		                "                    (cons n (r (+ (* 3 n) 1))) )"
		                "                (cons n (r (/ n 2))) )))))"};
		Parser parser{scanner};
		while (parser.has_more_to_parse())
		{
			parser.parse()->evaluate(*stack);
		}

		WHEN("we evaluate (COLLATZ 7)")
		{
			Scanner scanner{"(COLLATZ 7)"};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the result to be a list of integer numbers")
			{
				REQUIRE(expr::is_list_of_integers(*actual));
			}
			AND_THEN("we expect the result to be [7 22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1]")
			{
				REQUIRE(actual->stringify() == "[7 22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1]");
			}
		}
	}

	//         // (def! Y (fun (f) ((fun (g) (g g)) (fun (g) (f (fun a (apply (g g) a)) )))))
	//         // (def! collatz (fun n ((odd? n) (+ (* 3 n) 1) (/ n 2) )))
	//         // (def! SEQUENCE (Y (fun r (fun ('ff nn 'c?) ((c? nn) (cons nn (r 'ff (ff nn) 'c?)) 1 )))))
	//         GIVEN("The Y combinator, a non-recurive function and a function to combine them to build sequences")
	//         {
	//                 // Using the runtime directly, not produced by the factory.
	//                 // Additional functions missing in this case.
	//                 // Hence, have to define apply first.
	//                 auto stack{make_runtime()};
	//                 Scanner scanner{"(def! Y \n"
	//                                 "    (fun (f) \n"
	//                                 "        ((fun (g) (g g)) \n"
	//                                 "            (fun (g) \n"
	//                                 "                (f (fun a \n"
	//                                 "                    (apply (g g) a)) ))))) \n"
	//                                 "\n"
	//                                 "(def! collatz \n"
	//                                 "    (fun n \n"
	//                                 "        ((odd? n) \n"
	//                                 "                (+ (* 3 n) 1) \n"
	//                                 "                (/ n 2) ))) \n"
	//                                 "\n"
	//                                 "(def! SEQUENCE \n"
	//                                 "    (Y \n"
	//                                 "        (fun r \n"
	//                                 "            (fun ('ff nn 'c?) \n"
	//                                 "                ((c? nn) \n"
	//                                 "                    (cons nn (r 'ff (ff nn) 'c?)) \n"
	//                                 "                    nn )))))"};
	//                 Parser parser{scanner};
	//                 while (parser.has_more_to_parse())
	//                 {
	//                         parser.parse()->evaluate(*stack);
	//                 }

	//                 WHEN("we evaluate (SEQUENCE collatz 7 (partial /= 1))")
	//                 {
	//                         Scanner scanner{"(SEQUENCE collatz 7 (partial /= 1))"};
	//                         Parser parser{scanner};
	//                         const auto actual{parser.parse()->evaluate(*stack)};

	//                         THEN("we expect the result to be a list of integer numbers")
	//                         {
	//                                 REQUIRE(expr::is_list_of_integers(*actual));
	//                         }
	//                         AND_THEN("we expect the result to be [7 22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1]")
	//                         {
	//                                 REQUIRE(actual->stringify() == "[7 22 11 34 17 52 26 13 40 20 10 5 16 8 4 2
	//                                 1]");
	//                         }
	//                 }
	//         }
}

SCENARIO("Recursion using recursion features of the language.", "[recursion]")
{
	GIVEN("A should-be recursive function that accidentally does not refer to itself but an nonexisting function")
	{
		auto stack{make_runtime()};
		Scanner scanner{"(def! SQ (fun x (* x x))) \n"
		                "\n"
		                "(def! SEQUENCE (fun (f n c?) \n"
		                "    ((c? n) \n"
		                "        then : (cons (f n) (SSEQQUENCE f (+ n 1) c?)) \n"
		                "        else : (f n) )))"};
		Parser parser{scanner};
		while (parser.has_more_to_parse())
		{
			parser.parse()->evaluate(*stack);
		}

		WHEN("we apply this wannabe recursive function")
		{
			Scanner scanner{"(SEQUENCE SQ 1 (fun n (< n 10)))"};
			Parser parser{scanner};
			THEN("we expect an 'unknon function' error")
			{
				REQUIRE_THROWS_MATCHES(parser.parse()->evaluate(*stack), Semantic_Error,
				                       test::pretty_message("Semantic error at line 5, column 29: No "
				                                            "function defined for "
				                                            "symbol 'SSEQQUENCE'."));
			}
		}
	}

	GIVEN("A (now really) recursive function")
	{
		auto stack{make_runtime()};
		Scanner scanner{"(def! SQ (fun x (* x x))) \n"
		                "\n"
		                "(def! SEQUENCE (fun (f n c?) \n"
		                "    ((c? n) \n"
		                "        then : (cons (f n) (SEQUENCE f (+ n 1) c?)) \n"
		                "        else : (f n) )))"};
		Parser parser{scanner};
		while (parser.has_more_to_parse())
		{
			parser.parse()->evaluate(*stack);
		}

		WHEN("we apply this recursive function")
		{
			Scanner scanner{"(SEQUENCE SQ 1 (fun n (< n 10)))"};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the result to be a list of integer numbers")
			{
				REQUIRE(expr::is_list_of_integers(*actual));
			}
			AND_THEN("we expect the result to be...")
			{
				REQUIRE(actual->stringify() == "[1 4 9 16 25 36 49 64 81 100]");
			}
		}
	}

	GIVEN("Another (now really) recursive function")
	{
		auto stack{make_runtime()};
		Scanner scanner{"(def! COLLATZ \n"
		                "    (fun n \n"
		                "        ((odd? n) \n"
		                "                (+ (* 3 n) 1) \n"
		                "                (/ n 2) ))) \n"
		                "\n"
		                "(def! SEQUENCE (fun (of-fun start-nr until) \n"
		                "    ((until start-nr) \n"
		                "        then : start-nr \n"
		                "        else : (cons start-nr (SEQUENCE of-fun (of-fun start-nr) until)) )))"};
		Parser parser{scanner};
		while (parser.has_more_to_parse())
		{
			parser.parse()->evaluate(*stack);
		}

		WHEN("we apply this recursive function")
		{
			Scanner scanner{"(SEQUENCE \n"
			                "    of-fun : COLLATZ \n"
			                "    start-nr : 27 \n"
			                "    until : (partial = 1) )"};
			Parser parser{scanner};
			const auto actual{parser.parse()->evaluate(*stack)};

			THEN("we expect the result to be a list of integer numbers")
			{
				REQUIRE(expr::is_list_of_integers(*actual));
			}
			AND_THEN("we expect the result to be...")
			{
				REQUIRE(actual->stringify() ==
				        "[27 82 41 124 62 31 94 47 142 71 214 107 322 161 484 242 121 364 182 91 274 "
				        "137 412 206 103 310 155 466 233 700 350 175 526 263 790 395 1186 593 1780 890 "
				        "445 1336 668 334 167 502 251 754 377 1132 566 283 850 425 1276 638 319 958 "
				        "479 1438 719 2158 1079 3238 1619 4858 2429 7288 3644 1822 911 2734 1367 4102 "
				        "2051 6154 3077 9232 4616 2308 1154 577 1732 866 433 1300 650 325 976 488 244 "
				        "122 61 184 92 46 23 70 35 106 53 160 80 40 20 10 5 16 8 4 2 1]");
			}
		}
	}
}

TEST_CASE("Lazy sequence", "[lazy]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(def! SQ (fun z (* z z))) \n"
	                "\n"
	                "(def! LAZY (fun (f z c?) \n"
	                "                [(f z) ((c? z) \n"
	                "                           then : nil \n"
	                "                           else : (fun () \n"
	                "                                       (LAZY f (+ z 1) c?) ))]))"};
	Parser parser{scanner};
	while (parser.has_more_to_parse())
	{
		parser.parse()->evaluate(*stack);
	}
	{
		// cppcheck-suppress shadowVariable
		Scanner scanner{"(LAZY SQ 1 (partial =< 10))"}; // Same as (LAZY SQ 1 (fun z (=< 10 z)))
		// cppcheck-suppress shadowVariable
		Parser parser{scanner};
		auto seq = parser.parse()->evaluate(*stack);
		REQUIRE(seq->head().int_value() == 1);
		seq = seq->tail().head().evaluate(*stack);
		REQUIRE(seq->head().int_value() == 4);
		seq = seq->tail().head().evaluate(*stack);
		REQUIRE(seq->head().int_value() == 9);
		seq = seq->tail().head().evaluate(*stack);
		REQUIRE(seq->head().int_value() == 16);
		seq = seq->tail().head().evaluate(*stack);
		REQUIRE(seq->head().int_value() == 25);
		seq = seq->tail().head().evaluate(*stack);
		REQUIRE(seq->head().int_value() == 36);
		seq = seq->tail().head().evaluate(*stack);
		REQUIRE(seq->head().int_value() == 49);
		seq = seq->tail().head().evaluate(*stack);
		REQUIRE(seq->head().int_value() == 64);
		seq = seq->tail().head().evaluate(*stack);
		REQUIRE(seq->head().int_value() == 81);
		seq = seq->tail().head().evaluate(*stack);
		REQUIRE(seq->head().int_value() == 100);
		seq = seq->tail().head().evaluate(*stack);
		REQUIRE(seq->head().is_nil());
	}
}

//(def! NUMBERS (fun n ((=< n 1000) then: (cons n (NUMBERS (+ n 1))) else: nil)))
// WIP
TEST_CASE("tail call recursion", "[tail, recursion]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(def! NUMBERS (fun n ((=< n 5) then:(cons n (NUMBERS (+ n 1))) else:nil)))"};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE(actual->stringify() == "𝛌n.((=< n 5) then: (cons n (NUMBERS (+ n 1))) else: nil)");
	{
		Scanner scanner{"(NUMBERS 1)"};
		Parser parser{scanner};
		const auto seq = parser.parse()->evaluate(*stack);
		REQUIRE(seq->stringify() == "[1 2 3 4 5]");
	}
}
