// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/collection_prototypes.hpp"
#include "appsl/expression/function_application_expression.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/text_literal_expression.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/parser/scanner.hpp"
#include "appsl/parser/syntax_error.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <sstream>

using namespace appsl;

TEST_CASE("Program must not start with a colon.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>(":")};
	Parser parser{scanner};
	REQUIRE_THROWS_MATCHES(parser.parse(), Syntax_Error,
	                       test::pretty_message("Syntax error at line 1, column 1: Expected atom, quote, '(', '[', "
	                                            "'#{' or ',' but found ':'."));
}

TEST_CASE("Parser returns nil expression for empty input.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("")};
	Parser parser{scanner};

	REQUIRE_FALSE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() == Nil_Expression{Token::NONE});

	REQUIRE_FALSE(parser.has_more_to_parse());
}

TEST_CASE("Parser returns integer literal if input contains only one number.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("  123  ")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Integer_Literal_Expression{Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 3}}, 123});

	REQUIRE_FALSE(parser.has_more_to_parse());
}

TEST_CASE("Multiple expressions are returned as separate expressions.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("123 456 789")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Integer_Literal_Expression{Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}}, 123});

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Integer_Literal_Expression{Token{Token::Type::INTEGER_LITERAL, "456", Token::Position{1, 5}}, 456});

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Integer_Literal_Expression{Token{Token::Type::INTEGER_LITERAL, "789", Token::Position{1, 9}}, 789});

	REQUIRE_FALSE(parser.has_more_to_parse());
}

TEST_CASE("No list elements after L bracked results in 'premature end of list'.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("[")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE_THROWS_MATCHES(
			parser.parse(), Syntax_Error,
			test::pretty_message("Syntax error at line 1, column 2: Premature end of list expression. "
	                                     "Expected expression or closing delimiter ']' but reached end of file."));
}

TEST_CASE("No R bracked results in 'premature end of list'.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("[123")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE_THROWS_MATCHES(
			parser.parse(), Syntax_Error,
			test::pretty_message("Syntax error at line 1, column 4: Premature end of list expression. "
	                                     "Expected expression or closing delimiter ']' but reached end of file."));
}

/*
 * Please note how parsing a list returns a list prototype rather than an actual list. Every expression gets evaluated
 * after parsing. Then a lisp prototype becomes a list with all elements evaluated.
 */
TEST_CASE("Parsing delimited lists works.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("[123 456 789 888]")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        List_Prototype{Token{Token::Type::L_BRACKET, "[", Token::Position{1, 1}},
	                       make_unique<Integer_Literal_Expression>(
					       Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}}, 123),
	                       make_unique<List_Prototype>(
					       Token{Token::Type::INTEGER_LITERAL, "456", Token::Position{1, 6}},
					       make_unique<Integer_Literal_Expression>(
							       Token{Token::Type::INTEGER_LITERAL, "456",
	                                                             Token::Position{1, 6}},
							       456),
					       make_unique<List_Prototype>(
							       Token{Token::Type::INTEGER_LITERAL, "789",
	                                                             Token::Position{1, 10}},
							       make_unique<Integer_Literal_Expression>(
									       Token{Token::Type::INTEGER_LITERAL,
	                                                                             "789", Token::Position{1, 10}},
									       789),
							       make_unique<List_Prototype>(
									       Token{Token::Type::INTEGER_LITERAL,
	                                                                             "888", Token::Position{1, 14}},
									       make_unique<Integer_Literal_Expression>(
											       Token{Token::Type::INTEGER_LITERAL,
	                                                                                             "888",
	                                                                                             Token::Position{1,
	                                                                                                             14}},
											       888),
									       make_unique<Nil_Expression>(Token{
											       Token::Type::R_BRACKET,
											       "]",
											       Token::Position{1,
	                                                                                                       17}}))))});

	REQUIRE_FALSE(parser.has_more_to_parse());
}

TEST_CASE("No function name/arguments after L parenthesis results in 'premature end of function application'.",
          "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("(")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE_THROWS_MATCHES(parser.parse(), Syntax_Error,
	                       test::pretty_message("Syntax error at line 1, column 2: Premature end of function "
	                                            "application or record expression. Expected expression or closing "
	                                            "delimiter ')' but reached end of file."));
}

TEST_CASE("No R parenthesis results in 'premature end of function application'.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("(sq")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE_THROWS_MATCHES(parser.parse(), Syntax_Error,
	                       test::pretty_message("Syntax error at line 1, column 3: Premature end of function "
	                                            "application or record expression. Expected expression or closing "
	                                            "delimiter ')' but reached end of file."));
}

TEST_CASE("Syntactically everything is allowed for a function name.", "[Parser]")
{
	// Semantically this is no function application because 123 is no function name.
	Scanner scanner{make_unique<istringstream>("(123)")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Function_Application_Expression{
				Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 1}},
				make_unique<Integer_Literal_Expression>(
						Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}}, 123),
				make_unique<Nil_Expression>(
						Token{Token::Type::R_PARENTHESIS, ")", Token::Position{1, 5}})});

	REQUIRE_FALSE(parser.has_more_to_parse());
}

TEST_CASE("Parsing delimited function/macro application works.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("(sum 456 789 888)")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Function_Application_Expression{
				Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 1}},
				make_unique<Name_Expression>(Token{Token::Type::NAME, "sum", Token::Position{1, 2}}),
				make_unique<Function_Application_Expression>(
						Token{Token::Type::INTEGER_LITERAL, "456", Token::Position{1, 6}},
						make_unique<Integer_Literal_Expression>(
								Token{Token::Type::INTEGER_LITERAL, "456",
	                                                              Token::Position{1, 6}},
								456),
						make_unique<Function_Application_Expression>(
								Token{Token::Type::INTEGER_LITERAL, "789",
	                                                              Token::Position{1, 10}},
								make_unique<Integer_Literal_Expression>(
										Token{Token::Type::INTEGER_LITERAL,
	                                                                              "789", Token::Position{1, 10}},
										789),
								make_unique<Function_Application_Expression>(
										Token{Token::Type::INTEGER_LITERAL,
	                                                                              "888", Token::Position{1, 14}},
										make_unique<Integer_Literal_Expression>(
												Token{Token::Type::INTEGER_LITERAL,
	                                                                                              "888",
	                                                                                              Token::Position{1,
	                                                                                                              14}},
												888),
										make_unique<Nil_Expression>(Token{
												Token::Type::R_PARENTHESIS,
												")",
												Token::Position{1,
	                                                                                                        17}}))))});

	REQUIRE_FALSE(parser.has_more_to_parse());
}

TEST_CASE("Can parse quoted atoms.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>(" '123  ")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Quoted_Expression{Token{Token::Type::QUOTE, "'", Token::Position{1, 2}},
	                          make_unique<Integer_Literal_Expression>(
						  Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 3}},
						  123)});

	REQUIRE_FALSE(parser.has_more_to_parse());
}

TEST_CASE("Can parse quoted function applications when in parenthesis.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("'(123 456)")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Quoted_Expression{Token{Token::Type::QUOTE, "'", Token::Position{1, 1}},
	                          make_unique<Function_Application_Expression>(
						  Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}},
						  make_unique<Integer_Literal_Expression>(
								  Token{Token::Type::INTEGER_LITERAL, "123",
	                                                                Token::Position{1, 2}},
								  123),
						  make_unique<Function_Application_Expression>(
								  Token{Token::Type::INTEGER_LITERAL, "456",
	                                                                Token::Position{1, 6}},
								  make_unique<Integer_Literal_Expression>(
										  Token{Token::Type::INTEGER_LITERAL,
	                                                                                "456", Token::Position{1, 6}},
										  456)))});

	REQUIRE_FALSE(parser.has_more_to_parse());
}

TEST_CASE("Function application of quoted atoms.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("('f '123)")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Function_Application_Expression{
				Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 1}},
				make_unique<Quoted_Expression>(
						Token{Token::Type::QUOTE, "'", Token::Position{1, 2}},
						make_unique<Name_Expression>(
								Token{Token::Type::NAME, "f", Token::Position{1, 3}})),
				make_unique<Function_Application_Expression>(
						Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 1}},
						make_unique<Quoted_Expression>(
								Token{Token::Type::QUOTE, "'", Token::Position{1, 5}},
								make_unique<Integer_Literal_Expression>(
										Token{Token::Type::INTEGER_LITERAL,
	                                                                              "123", Token::Position{1, 6}},
										123)))});
}

TEST_CASE("Parsing nested expressions.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("(+ (+ 1 2) (+ 3 4))")};
	Parser parser{scanner};

	// REQUIRE(parser.has_more_to_parse());

	auto plus_1_2{make_unique<Function_Application_Expression>(
			Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 4}},
			make_unique<Name_Expression>(Token{Token::Type::NAME, "+", Token::Position{1, 5}}),
			make_unique<Function_Application_Expression>(
					Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 4}},
					make_unique<Integer_Literal_Expression>(
							Token{Token::Type::INTEGER_LITERAL, "1", Token::Position{1, 7}},
							1),
					make_unique<Function_Application_Expression>(
							Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 4}},
							make_unique<Integer_Literal_Expression>(
									Token{Token::Type::INTEGER_LITERAL, "2",
	                                                                      Token::Position{1, 9}},
									2),
							make_unique<Nil_Expression>(Token{Token::Type::R_PARENTHESIS,
	                                                                                  ")",
	                                                                                  Token::Position{1, 10}}))))};

	auto plus_3_4{make_unique<Function_Application_Expression>(
			Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 1}},
			make_unique<Function_Application_Expression>(
					Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 12}},
					make_unique<Name_Expression>(
							Token{Token::Type::NAME, "+", Token::Position{1, 13}}),
					make_unique<Function_Application_Expression>(
							Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 12}},
							make_unique<Integer_Literal_Expression>(
									Token{Token::Type::INTEGER_LITERAL, "3",
	                                                                      Token::Position{1, 15}},
									3),
							make_unique<Function_Application_Expression>(
									Token{Token::Type::L_PARENTHESIS, "(",
	                                                                      Token::Position{1, 12}},
									make_unique<Integer_Literal_Expression>(
											Token{Token::Type::INTEGER_LITERAL,
	                                                                                      "4",
	                                                                                      Token::Position{1, 17}},
											4),
									make_unique<Nil_Expression>(Token{
											Token::Type::R_PARENTHESIS, ")",
											Token::Position{1, 18}})))),
			make_unique<Nil_Expression>(Token{Token::Type::R_PARENTHESIS, ")", Token::Position{1, 19}}))};

	auto outer_plus{Function_Application_Expression{
			Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 1}},
			make_unique<Name_Expression>(Token{Token::Type::NAME, "+", Token::Position{1, 2}}),
			make_unique<Function_Application_Expression>(
					Token{Token::Type::L_PARENTHESIS, "(", Token::Position{1, 1}}, move(plus_1_2),
					move(plus_3_4))}};

	REQUIRE(*parser.parse() == outer_plus);

	REQUIRE_FALSE(parser.has_more_to_parse());
}

TEST_CASE("Function applications must be terminated by ')'.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("(+ 1 2 3 4")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE_THROWS_MATCHES(parser.parse(), Syntax_Error,
	                       test::pretty_message("Syntax error at line 1, column 10: Premature end of function "
	                                            "application or record expression. Expected expression or closing "
	                                            "delimiter ')' but reached end of file."));
}

// This test case did execute from  within the IDE (VS Code) but failed on com-
// mand line (CTest).  The error message was insane. Strangely, the problem was
// caused by the test case title:
//
//     "Function applications must be terminated by ')', not ']'."
//
// This problem must have sneaked in with a newer Catch2 version!?!
TEST_CASE("Fun app must not be terminated by right square bracket.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("(+ 1 2 3 4]")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE_THROWS_MATCHES(parser.parse(), Syntax_Error,
	                       test::pretty_message("Syntax error at line 1, column 11: Expected atom, quote, '(', "
	                                            "'[', '#{' or ',' but found ']'."));
}

TEST_CASE("Failure to terminate cons expression causes syntax error", "[Parser, cons]")
{
	Scanner scanner{make_unique<istringstream>("(1 ,")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE_THROWS_MATCHES(parser.parse(), Syntax_Error,
	                       test::pretty_message("Syntax error at line 1, column 4: Premature end of cons cell. "
	                                            "Expected right hand expression."));
}

TEST_CASE("Parser recognizes text literals.", "[Parser]")
{
	Scanner scanner{make_unique<istringstream>("\"abc\" \"ABC\"")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Text_Literal_Expression{Token{Token::Type::TEXT_LITERAL, "abc", Token::Position{1, 1}}});

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(*parser.parse() ==
	        Text_Literal_Expression{Token{Token::Type::TEXT_LITERAL, "ABC", Token::Position{1, 7}}});

	REQUIRE_FALSE(parser.has_more_to_parse());
}

TEST_CASE("2 function applications can be composed.", "[Parser]")
{
	Scanner scanner{"(range 1 10) | (filter odd?)"};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(parser.parse()->stringify() == "(filter (range 1 10) odd?)");
}

TEST_CASE("3 function applications can be composed.", "[Parser]")
{
	Scanner scanner{"(range 1 10) | (filter odd?) | (map sq)"};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(parser.parse()->stringify() == "(map (filter (range 1 10) odd?) sq)");
}

TEST_CASE("4 function applications can be composed.", "[Parser]")
{
	Scanner scanner{"(range 1 10) | (filter odd?) | (filter (partial > 5)) | (map sq)"};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(parser.parse()->stringify() == "(map (filter (filter (range 1 10) odd?) (partial > 5)) sq)");
}

TEST_CASE("Another exception is required after the composition operator.", "[Parser]")
{
	Scanner scanner{"(range 1 10) |"};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE_THROWS_MATCHES(
			parser.parse(), Syntax_Error,
			test::pretty_message("Syntax error at line 1, column 14: Premature end of composed expression. "
	                                     "Expected another expression to follow composition operator '|'."));
}

TEST_CASE("Parse a map literal", "[fast]")
{
	// GIVEN
	Scanner scanner{make_unique<istringstream>("#{1 -> \"one\" 2 -> \"two\"}")};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	// WHEN
	const auto parse_result{parser.parse()};
	// THEN
	const auto stringified_parse_result{parse_result->stringify()};
	// The parser actually emits a map prototype. Its elements are not sorted reliably. Therefore the
	// stringification may vary. Only the evaluated map prototype - which is a map - has its elements sorted
	// by key therefore producing a stable stringification.
	REQUIRE(((stringified_parse_result == "#{1 -> \"one\" 2 -> \"two\"}") ||
	         (stringified_parse_result == "#{2 -> \"two\" 1 -> \"one\"}")));
}

TEST_CASE("Parse a function application with a nil argument", "[fast]")
{
	// GIVEN
	Scanner scanner{"(1 nil)"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	// WHEN
	const auto parse_result{parser.parse()};
	// THEN
	REQUIRE(parse_result->is_function_application());
	std::stringstream parse_tree{};
	parse_result->operator<<(parse_tree);
	REQUIRE(parse_tree.str() ==
	        "(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:1 val:'(') head:(EXPR "
	        "type:integer_literal value:1 (TOKEN type:INTEGER_LITERAL line:1 col:2 val:'1')) tail:(EXPR "
	        "type:function application (TOKEN type:L_PARENTHESIS line:1 col:1 val:'(') head:(EXPR type:name (TOKEN "
	        "type:NAME line:1 col:4 val:'nil')) tail:(EXPR type:nil (TOKEN type:EOF line:0 col:0 val:''))))");
	const auto stringified_parse_result{parse_result->stringify()};
	REQUIRE(stringified_parse_result == "(1 nil)");
}

// There was a bug that prevented case-expressions to work properly. Initially it looked like the parsing has gone
// wrong. But, this is not the case.
TEST_CASE("Parsing part of a case-expression.", "[parser, bug]")
{
	// GIVEN
	Scanner scanner{"(Integer? n) -> (and (>= n lower) (=< n upper))"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	// WHEN
	const auto ast{parser.parse()};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(ast->stringify() == "(Integer? n) -> (and (>= n lower) (=< n upper))");
	CHECK(ast->head().stringify() == "(Integer? n)");
	CHECK(ast->tail().stringify() == "(and (>= n lower) (=< n upper))");
}

// clang-format off
/**
 * @brief I thought the parser was unnecessarily wrapping the function name within a function application object.
 *
 * Here is the structure of the parse tree (AST) for (def! f (fun x (do (def! a 10) (* x a)))).
 * Originally I thought the inner def! was wrapped unnecessarily.
 * But the parser is actually producing AST.
 * The evaluation function needed to be fixed in order to evaluate nested function applications correctly.
 * 
 * (EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:1 val:'(')
 * 	head:(EXPR type:name (TOKEN type:NAME line:1 col:2 val:'def!'))
 * 	tail:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:1 val:'(')
 * 		head:(EXPR type:name (TOKEN type:NAME line:1 col:7 val:'f'))
 * 		tail:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:1 val:'(')

 * 			head:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:9 val:'(')
 * 				head:(EXPR type:name (TOKEN type:NAME line:1 col:10 val:'fun'))
 * 				tail:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:9 val:'(')
 * 					head:(EXPR type:name (TOKEN type:NAME line:1 col:14 val:'x'))
 * 					tail:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:9 val:'(')

 * 						head:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:16 val:'(')
 * 							head:(EXPR type:name (TOKEN type:NAME line:1 col:17 val:'do'))
 * 							tail:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:16 val:'(')

 * 								head:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:20 val:'(')
 * 									head:(EXPR type:name (TOKEN type:NAME line:1 col:21 val:'def!'))
 * 									tail:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:20 val:'(')
 * 										head:(EXPR type:name (TOKEN type:NAME line:1 col:26 val:'a'))
 * 										tail:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:20 val:'(')
 * 											head:(EXPR type:integer_literal value:10 (TOKEN type:INTEGER_LITERAL line:1 col:28 val:'10'))
 * 											tail:(EXPR type:nil (TOKEN type:EOF line:0 col:0 val:'')))))

 * 								tail:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:16 val:'(')

 * 									head:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:50 val:'(')
 * 										head:(EXPR type:name (TOKEN type:NAME line:1 col:51 val:'*'))
 * 										tail:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:50 val:'(')
 * 											head:(EXPR type:name (TOKEN type:NAME line:1 col:53 val:'x'))
 * 											tail:(EXPR type:function application (TOKEN type:L_PARENTHESIS line:1 col:50 val:'(')
 * 												head:(EXPR type:name (TOKEN type:NAME line:1 col:55 val:'a'))
 * 												tail:(EXPR type:nil (TOKEN type:EOF line:0 col:0 val:'')))))

 * 									tail:(EXPR type:nil (TOKEN type:EOF line:0 col:0 val:'')))))

 * 						tail:(EXPR type:nil (TOKEN type:EOF line:0 col:0 val:'')))))

 * 			tail:(EXPR type:nil (TOKEN type:EOF line:0 col:0 val:'')))))
 */
// clang-format on
TEST_CASE("Nested function applications (def! within do)", "[parser, bug]")
{
	// GIVEN
	Scanner scanner{"(def! f (fun x (do (def! a 10)"
	                "                   (* x a) )))"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	// WHEN
	const auto ast{parser.parse()};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(ast->tail().tail().head().tail().tail().head().tail().head().head().stringify() == "def!");
}
