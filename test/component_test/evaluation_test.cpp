// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/parser/scanner.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include "appsl/use_stl.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

TEST_CASE("Name_Expression evaluates to value assigned to it.", "[Name_Expression]")
{
	Global_Stack_Frame stack;
	stack.define_local_var("some-name",
	                       make_unique<Integer_Literal_Expression>(
					       Token{Token::Type::INTEGER_LITERAL, "666", Token::NO_POS}, 666));

	const Name_Expression expr{Token{Token::Type::NAME, "some-name", Token::NO_POS}};
	REQUIRE(*expr.evaluate(stack) ==
	        Integer_Literal_Expression{Token{Token::Type::INTEGER_LITERAL, "666", Token::NO_POS}, 666});
}

TEST_CASE("Undefined names evaluate to NIL.", "[Name_Expression]")
{
	Global_Stack_Frame stack; // some-name not defined in stack.

	const auto expr = make_shared<Name_Expression>(Token{Token::Type::NAME, "some-name", Token::NO_POS});
	REQUIRE(*expr->evaluate(stack) == *Nil_Expression::NIL);
}

/**
 * @brief Fixes a bug where variable names clashed.
 *
 * Below is a visualisation of the nested stack during execution. The "def!" function has a parameter named "name" with
 * value "name_uc". This variable ("name_uc")is about to be defined. Hence, it has no value yet (nil). But when "(UC
 * name: name)" is about to be evaluated in the course of evaluating "(def! name_uc (UC name: name ))" then the
 * interpreter looks up "name" and finds "name_uc". It looks up this as well and finds "nil".
 * The Def_Macro::evaualte function was fixed in that is does not reference its own scope, which may contain a still
 * undefined variable that eclipses an existing variable from a local variable or argument. Instead it references its
 * parant scope ("stack frame") now.
 *
 * GLOBAL RUNTIME
 *     name = "my name" <-- name from application of F
 *     value = 4711
 *         expressions = ((def! name_uc (UC name: name)) (map (range 0 4) (fun i [name_uc (+ value i)])))
 *             name = name_uc <-- name from application of def!
 *             value = (UC name: name)
 */
TEST_CASE("A bug caused a variable name clash", "[bug, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"( (def! UC (fun name (upper_case name)))                     )"
	                R"( (def! F (fun [name value]                                  )"
	                R"(              (do (def! name_uc (UC name: name ))           )"
	                R"(                  (map (range 0 4)                          )"
	                R"(                       (fun i [name_uc (+ value i)]) ) ) )) )"
	                R"( (F "my name" 4711)                                         )"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	parser.parse()->evaluate(*stack);
	REQUIRE(parser.has_more_to_parse());
	parser.parse()->evaluate(*stack);
	// WHEN
	REQUIRE(parser.has_more_to_parse());
	const auto result{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE_FALSE(parser.has_more_to_parse());
	CHECK(result->stringify() == "[\"MY NAME\" 4711]\n"
	                             "[\"MY NAME\" 4712]\n"
	                             "[\"MY NAME\" 4713]\n"
	                             "[\"MY NAME\" 4714]\n"
	                             "[\"MY NAME\" 4715]");
}
