// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

TEST_CASE("Count computes the size of collections.", "[collection, function, count]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(count [1 2 3 4 5 6])"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->is_integer());
	REQUIRE(actual->int_value() == 6);
}

TEST_CASE("The count of an empty list is 0.", "[collection, function, count]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(count [])"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->is_integer());
	REQUIRE(actual->int_value() == 0);
}

TEST_CASE("The count of a literal is 1.", "[collection, function, count]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(count 5.5)"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->is_integer());
	REQUIRE(actual->int_value() == 1);
}

TEST_CASE("Count is not variadic.", "[collection, function, count]")
{
	Global_Stack_Frame stack;
	Scanner scanner{"(count 1 2 3)"};
	Parser parser{scanner};

	REQUIRE_THROWS_MATCHES(
			parser.parse()->evaluate(stack), Semantic_Error,
			test::pretty_message(
					"Semantic error at line 1, column 12: Too many arguments. Function 'count' "
					"expects 1 argument: 'collection'. But 3 parameters given: '1', '2', '3'."));
}

TEST_CASE("Count can be called in a variadic way using apply.", "[collection, function, count, apply]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(apply count 1 2 3)"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual->is_integer());
	REQUIRE(actual->int_value() == 3);
}

TEST_CASE("Head returns the first element of a collections.", "[collection, function, head]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(head [111 222 333 444 555 666])"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->is_integer());
	REQUIRE(actual->int_value() == 111);
}

TEST_CASE("Head of an empty collections is nil.", "[collection, function, head]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(head [])"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->is_nil());
}

TEST_CASE("Head of collection with one element is that element.", "[collection, function, head]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(head [5.5])"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->is_real());
	REQUIRE(actual->real_value() == 5.5);
}

TEST_CASE("Head for a literal is literal itself.", "[collection, function, head]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(head 111)"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->is_integer());
	REQUIRE(actual->int_value() == 111);
}

TEST_CASE("Tails returns all but the first element of a collections.", "[collection, function, tail]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(tail [111 222 333 444 555 666])"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(expr::is_list_of_integers(*actual));
	REQUIRE(actual->stringify() == "[222 333 444 555 666]");
}

TEST_CASE("Tail of an empty collections is nil.", "[collection, function, head]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(tail [])"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->is_nil());
}

TEST_CASE("Tail of a collections with only one element is nil.", "[collection, function, head]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(tail [1])"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->is_nil());
}

TEST_CASE("Tail of a literal is nil.", "[collection, function, head]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(tail 5.0)"};
	Parser parser{scanner};

	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->is_nil());
}

TEST_CASE("Head and tail can be used to filter collections.", "[collection, function, head, tail]")
{

	auto stack{make_runtime()};
	Scanner scanner{"(def! FILTER \n"
	                "    (fun (coll pred?) \n"
	                "        ((= nil coll) \n" // can also use "is-nil?" here
	                "            then: nil \n"
	                "            else: ((pred? (head coll)) \n"
	                "                then: (cons (head coll) (FILTER (tail coll) pred?)) \n"
	                "                else: (FILTER (tail coll) pred?) ))))"};
	Parser parser{scanner};
	while (parser.has_more_to_parse())
	{
		parser.parse()->evaluate(*stack);
	}

	{
		Scanner scanner{"(FILTER [1 2 3 4 5 6 7 8 9 10] odd?)"};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};

		REQUIRE(expr::is_list_of_integers(*actual));
		REQUIRE(actual->stringify() == "[1 3 5 7 9]");
	}
}

TEST_CASE("Define a map", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! m #{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"})"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual->stringify() == "#{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}");
}

TEST_CASE("A map as a function", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! m #{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}) (m 2)"};
	Parser parser{scanner};
	const auto actual_def{parser.parse()->evaluate(*stack)};
	REQUIRE(actual_def->stringify() == "#{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}");
	// WHEN
	const auto actual_fun_app{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_fun_app->stringify() == "\"two\"");
}

TEST_CASE("Iterate over map (eager)", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! m #{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}) (map' m (fun k (m k)))"};
	Parser parser{scanner};
	const auto actual_def{parser.parse()->evaluate(*stack)};
	REQUIRE(actual_def->stringify() == "#{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}");
	// WHEN
	const auto actual_fun_app{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_fun_app->stringify() == "[\"one\" \"two\" \"three\"]");
}

TEST_CASE("Iterate over map (lazy)", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! m #{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}) (map m (fun k (m k)))"};
	Parser parser{scanner};
	const auto actual_def{parser.parse()->evaluate(*stack)};
	REQUIRE(actual_def->stringify() == "#{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}");
	// WHEN
	const auto actual_fun_app{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_fun_app->stringify() == "\"one\"\n\"two\"\n\"three\"");
}

TEST_CASE("Iterate over map and apply mapping (eager)", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! m #{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}) (map' m (fun k (upper_case (m k))))"};
	Parser parser{scanner};
	const auto actual_def{parser.parse()->evaluate(*stack)};
	REQUIRE(actual_def->stringify() == "#{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}");
	// WHEN
	const auto actual_fun_app{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_fun_app->stringify() == "[\"ONE\" \"TWO\" \"THREE\"]");
}

TEST_CASE("Iterate over map and apply mapping (lazy)", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! m #{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}) (map m (fun k (upper_case (m k))))"};
	Parser parser{scanner};
	const auto actual_def{parser.parse()->evaluate(*stack)};
	REQUIRE(actual_def->stringify() == "#{1 -> \"one\" 2 -> \"two\" 3 -> \"three\"}");
	// WHEN
	const auto actual_fun_app{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_fun_app->stringify() == "\"ONE\"\n\"TWO\"\n\"THREE\"");
}

TEST_CASE("Map with computed keys and values", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"#{(+ 1 2) -> \"three\" (+ 2 3) -> \"five\"}"};
	Parser parser{scanner};
	// WHEN
	const auto actual_def{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_def->stringify() == "#{3 -> \"three\" 5 -> \"five\"}");
}

TEST_CASE("Tagged map", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(tagged #{1 -> \"a\" 2 -> \"b\"} T)"};
	Parser parser{scanner};
	// WHEN
	const auto actual_def{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_def->stringify() == "(tagged #{1 -> \"a\" 2 -> \"b\"} with: T)");
}

TEST_CASE("An argument of nil is an empty list (aka no argument at all)", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(1 nil)"};
	Parser parser{scanner};
	// WHEN
	const auto actual_def{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_def->stringify() == "1");
}

TEST_CASE("Parsing and evaluating simple record literal", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(x:10 y:20)"};
	Parser parser{scanner};
	// WHEN
	const auto actual_def{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_def->stringify() == "(x: 10 y: 20)");
}

TEST_CASE("Accessing record fields by their name", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! r (x:10 y:20)) (r y)"};
	Parser parser{scanner};
	parser.parse()->evaluate(*stack);
	// WHEN
	const auto actual_field_value{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_field_value->stringify() == "20");
}

TEST_CASE("Accessing record field fails if providing unknown name", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! r (x:10 y:20)) (r z)"};
	Parser parser{scanner};
	parser.parse()->evaluate(*stack);
	REQUIRE_THROWS_MATCHES(
			// WHEN
			parser.parse()->evaluate(*stack),
			// THEN
			Semantic_Error,
			test::pretty_message("Semantic error at line 1, column 25: The fields of a record are accessed "
	                                     "by their name. This record '(x: 10 y: 20)' has no field 'z'. Valid field "
	                                     "names of this record are: 'x', 'y'."));
}

TEST_CASE("Record fields are accessed by their name", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! r (x:10 y:20)) (r 123)"};
	Parser parser{scanner};
	parser.parse()->evaluate(*stack);
	REQUIRE_THROWS_MATCHES(
			// WHEN
			parser.parse()->evaluate(*stack),
			// THEN
			Semantic_Error,
			test::pretty_message("Semantic error at line 1, column 25: The fields of a record are accessed "
	                                     "by their name. This '123' ist not a name. Valid field names of this "
	                                     "record are: 'x', 'y'."));
}

TEST_CASE("Defining a 'constructor' for a record type", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! ERROR (fun (msg cause)"
	                "                 (tagged (message: msg caused_by: cause) ERROR)))"};
	Parser parser{scanner};
	// WHEN
	const auto actual_def{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_def->stringify() == "𝛌(msg cause).(tagged (message: msg caused_by: cause) ERROR)");
}

TEST_CASE("Applying a 'constructor' for a record type", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! ERROR (fun (msg cause)"
	                "                 (tagged (message: msg caused_by: cause) ERROR)) )"
	                "(ERROR \"message\" nil)"};
	Parser parser{scanner};
	parser.parse()->evaluate(*stack);
	// WHEN
	const auto actual_application{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_application->stringify() == "(tagged (message: \"message\" caused_by: nil) with: ERROR)");
}

TEST_CASE("Counting fields of a record", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(count (x:1 y:2 z:3))"};
	Parser parser{scanner};
	// WHEN
	const auto actual_count{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_count->stringify() == "3");
}

TEST_CASE("Fields of records can be accessed using accessor syntax", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(x:1 y:2 z:3).y"};
	Parser parser{scanner};
	// WHEN
	const auto actual_count{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_count->stringify() == "2");
}

TEST_CASE("Values in maps can be accessed using accessor syntax", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"(#{"a"->123 "b"->456}."a")"};
	Parser parser{scanner};
	// WHEN
	const auto actual_count{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_count->stringify() == "123");
}

TEST_CASE("Keys in accessor syntax can be computed", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"(#{"A"->123 "B"->456}.(upper_case "b"))"};
	Parser parser{scanner};
	// WHEN
	const auto actual_count{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_count->stringify() == "456");
}

TEST_CASE("Nested maps and records can be accessed with nested accessors", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"(#{"a"->(x:1 y:2) "b"->(x:3 y:4)}."a".y)"};
	Parser parser{scanner};
	// WHEN
	const auto actual_count{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_count->stringify() == "2");
}

TEST_CASE("Detecting duplicate elements in set", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"{1 2 3 4 2 5}"};
	Parser parser{scanner};
	REQUIRE_THROWS_MATCHES(
			// WHEN
			parser.parse()->evaluate(*stack),
			// THEN
			Semantic_Error,
			test::pretty_message("Semantic error at line 1, column 10: Duplicate element '2' in set. A set "
	                                     "must be made up from unique elements. Consider using a list instead of a "
	                                     "set in case you need duplicate elements."));
}

TEST_CASE("Counting set elements", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(count {1 2 3 4 5})"};
	Parser parser{scanner};
	// WHEN
	const auto actual_count{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_count->stringify() == "5");
}

// The result of the mapping might be a value of "nil". For example in this expression
//
//     (count (map (range 1 10) odd?)),
//
// the predicate "odd?" evaluates to "true" for every --surprise-- odd number. But it evaluates to "nil" for even
// numbers. The original version of the "count" function would have stoped consuming the elements produced by the
// generator and therefore would give a wrong count. This has been fixed by giving expressions a "is iterable" property
// rather than checking for head==nil. So now, empty collections correspond with "is iterable" = "false".
TEST_CASE("The count function functions well for nil elements", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(count (map (range 1 10) odd?))"};
	Parser parser{scanner};
	// WHEN
	const auto actual_count{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_count->stringify() == "10");
}

TEST_CASE("The lazy filter generator works", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(filter (range 1 10) odd?)"};
	Parser parser{scanner};
	// WHEN
	const auto actual_count{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_count->stringify() == "1\n3\n5\n7\n9");
}

TEST_CASE("'keep' is an alias for 'filter'", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(= (keep (range 1 10) odd?) (filter (range 1 10) odd?))"};
	Parser parser{scanner};
	// WHEN
	const auto actual_count{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_count->stringify() == "true");
}

TEST_CASE("'discard' is the opposite of 'filter' (aka 'keep')", "[fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(discard (range 1 10) odd?)"};
	Parser parser{scanner};
	// WHEN
	const auto actual_count{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual_count->stringify() == "2\n4\n6\n8\n10");
}

TEST_CASE("Collection elements can be lazily enumerated", "[collection, enumerate, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"((enumerated ["a" "b" "c"]))"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual->stringify() == "[0 \"a\"]\n"
	                               "[1 \"b\"]\n"
	                               "[2 \"c\"]");
}

TEST_CASE("Collection elements can be eagerly enumerated", "[collection, enumerate, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"((enumerated' ["a" "b" "c"]))"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	// THEN
	REQUIRE(actual->stringify() == R"([[0 "a"] [1 "b"] [2 "c"]])");
}

/**
 * @brief Verifies a bug on function argument lists has been fixed.
 *
 * Originally, expressions like these have been evaluated with an surprising result:
 * > (def! f (fun x... (id x)))
 * > (f [1 2 3] [4 5 6])
 * [[1 2 3] 4 5 6] ;; Surprise, not what we expected.
 *
 * After the fix:
 * > (def! f' (fun 'x... (id x)))
 * > (f' [1 2 3] [4 5 6])
 * ([1 2 3] [4 5 6]) ;; This is what we expect.
 *
 * In Function_Application_Expression::evaluated_argument_list there was a bug that, if an arument list was actually a
 * list, the last element was not represented as a cons-cell with head=last list enement and tail=nil.
 * Function_Application_Expression::evaluated_argument_list now uses evaluated_argument_actual_list to correctly and
 * recursively evaluate actual argument lists.
 */
TEST_CASE("Argument list evaluation bug was fixed", "[collection, variadic, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner_given{"(def! f (fun x... (id x)))"};
	Parser parser_given{scanner_given};
	parser_given.parse()->evaluate(*stack);
	REQUIRE_FALSE(parser_given.has_more_to_parse());
	// WHEN
	Scanner scanner_when{"(f [1 2 3] [4 5 6])"};
	Parser parser_when{scanner_when};
	const auto ast{parser_when.parse()};
	REQUIRE(ast->stringify() == "(f [1 2 3] [4 5 6])");
	const auto actual{ast->evaluate(*stack)};
	// THEN
	CHECK_FALSE(parser_when.has_more_to_parse());
	CHECK(actual->stringify() == "[[1 2 3] [4 5 6]]");
}

TEST_CASE("There was a bug in the map generator so that an incomplete scope was used", "[fast, bug]")
{
	// ---- GIVEN ----
	auto runtime{make_runtime()};
	auto stack{runtime->new_child()};
	Scanner scanner{R"( (def! MUL (fun [x y]                                                          )"
	                R"(                [(* (head x) (head y)) (* (head (tail x)) (head (tail y)))] )) )"
	                R"( (def! BLUB (fun [u v]                                                         )"
	                R"(                 (do (def! w (* u 2))                                          )"
	                R"(                     (map (range 1 5)                                          )"
	                R"(                          (fun i (MUL x: [v i] y: [w i])) ) ) ))               )"
	                R"( (BLUB 12 13)                                                                  )"};
	Parser parser{scanner};
	REQUIRE(parser.has_more_to_parse());
	// The definition of COPY
	parser.parse()->evaluate(*stack);
	REQUIRE(parser.has_more_to_parse());
	parser.parse()->evaluate(*stack);
	REQUIRE(parser.has_more_to_parse());
	// ---- WHEN ----
	// The application of COPY
	const auto expression{parser.parse()};
	const auto result{expression->evaluate(*stack)};
	// ---- THEN ----
	CHECK(result->stringify() == "[312 1]\n"
	                             "[312 4]\n"
	                             "[312 9]\n"
	                             "[312 16]\n"
	                             "[312 25]");
	CHECK_FALSE(parser.has_more_to_parse());
}

/**
 * @brief Checks a fix to a bug where nesting eager collection functions caused an infinite loop.
 *
 * The  lazy version  (range 1 10) | (map (fun n [n (sq n)]))  executes nicely.
 * However, the eager version (range' 1 10) | (map' (fun n [n (sq n)])) is han-
 * ging up. The same goes for this eager variant, which is equivalent:
 *
 * (map' (range' 1 10) (fun n [n (sq n)]))
 *
 * Most intrestingly this (map' (range' 1 10) (fun n n,(sq n))) executes well!
 *
 * The error was within the evaluation function for nil expression, see there for details.
 */
TEST_CASE("Nested, eager collection functions infinite loop", "[collection, bug, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	// WHEN
	Scanner scanner{"(map' (range' 1 10) (fun n [n (sq n)]))"};
	Parser parser{scanner};
	const auto ast{parser.parse()};
	REQUIRE(ast->stringify() == "(map' (range' 1 10) (fun n [n (sq n)]))");
	const auto actual{ast->evaluate(*stack)};
	// THEN
	CHECK_FALSE(parser.has_more_to_parse());
	CHECK(actual->stringify() == "[[1 1] [2 4] [3 9] [4 16] [5 25] [6 36] [7 49] [8 64] [9 81] [10 100]]");
}
