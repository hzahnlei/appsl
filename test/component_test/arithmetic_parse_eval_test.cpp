// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

SCENARIO("Parsing and evaluating sum expressions.", "[parse, evaluate, define, arithmetic]")
{
	auto stack{make_shared<Global_Stack_Frame>()};

	WHEN("we evaluate (+)")
	{
		Scanner scanner{make_unique<istringstream>("(+)")};
		Parser parser{scanner};
		const auto actual{parser.parse()};
		THEN("we expect an exception")
		{
			REQUIRE_THROWS_MATCHES(actual->evaluate(*stack), Semantic_Error,
			                       test::pretty_message("Semantic error at line 1, column 2: Too few "
			                                            "arguments. Function '+' expects 2 arguments: "
			                                            "'term', 'terms...'. But no parameters given."));
		}
	}

	WHEN("we evaluate (+ 1000)")
	{
		Scanner scanner{make_unique<istringstream>("(+ 1000)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be 1000") { REQUIRE(actual->int_value() == 1000); }
	}

	WHEN("we evaluate (+ 1000 100 10 1)")
	{
		Scanner scanner{make_unique<istringstream>("(+ 1000 100 10 1)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be 1111") { REQUIRE(actual->int_value() == 1111); }
	}

	WHEN("we evaluate try sum integers and reals")
	{
		Scanner scanner{make_unique<istringstream>("(+ 123 1000.0)")};
		Parser parser{scanner};
		THEN("we expect an error")
		{
			REQUIRE_THROWS_MATCHES(
					parser.parse()->evaluate(*stack), appsl::Runtime_Error,
					test::pretty_message(
							"Runtime error: Sum function is about to add integer numbers. "
							"However, '1000' is no integer number."));
		}
	}

	WHEN("we evaluate (+ 1000.0)")
	{
		Scanner scanner{make_unique<istringstream>("(+ 1000.0)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be a real number") { REQUIRE(actual->is_real()); }
		THEN("we expect the result to be 1000.0") { REQUIRE(actual->real_value() == 1000.0); }
	}

	WHEN("we evaluate (+ 1000.0 100.0 10.0 1.0 0.1)")
	{
		Scanner scanner{make_unique<istringstream>("(+ 1000.0 100.0 10.0 1.0 0.1)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be a real number") { REQUIRE(actual->is_real()); }
		THEN("we expect the result to be 1111.1") { REQUIRE(actual->real_value() == 1111.1); }
	}

	WHEN("we evaluate try sum reals and integers")
	{
		Scanner scanner{make_unique<istringstream>("(+ 1000.0 123)")};
		Parser parser{scanner};
		THEN("we expect an error")
		{
			REQUIRE_THROWS_MATCHES(parser.parse()->evaluate(*stack), appsl::Runtime_Error,
			                       test::pretty_message("Runtime error: Sum function is about to add real "
			                                            "numbers. However, '123' is no real number."));
		}
	}
}

SCENARIO("Parsing and evaluating divsion expressions.", "[parse, evaluate, arithmetic, division]")
{
	auto stack{make_shared<Global_Stack_Frame>()};

	WHEN("we evaluate (/)")
	{
		Scanner scanner{make_unique<istringstream>("(/)")};
		Parser parser{scanner};
		THEN("we expect 'too few arguments'")
		{
			REQUIRE_THROWS_MATCHES(
					parser.parse()->evaluate(*stack), Semantic_Error,
					test::pretty_message("Semantic error at line 1, column 2: Too few arguments. "
			                                     "Function '/' expects 2 arguments: 'divisor', "
			                                     "'divisors...'. But no parameters given."));
		}
	}

	WHEN("we evaluate (/ 2.0), division acts like reciprocal on single arguments")
	{
		Scanner scanner{make_unique<istringstream>("(/ 2.0)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be a real number") { REQUIRE(actual->is_real()); }
		AND_THEN("we expect the result to be 0.5") { REQUIRE(actual->real_value() == 0.5); }
	}

	WHEN("we evaluate (/ 2, division acts like reciprocal on single arguments)")
	{
		Scanner scanner{make_unique<istringstream>("(/ 2)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be an integer number") { REQUIRE(actual->is_integer()); }
		AND_THEN("we expect the result to be 0") { REQUIRE(actual->int_value() == 0); }
	}

	WHEN("we evaluate (/ 16 2 2)")
	{
		Scanner scanner{make_unique<istringstream>("(/ 16 2 2)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be an integer number") { REQUIRE(actual->is_integer()); }
		AND_THEN("we expect the result to be 4") { REQUIRE(actual->int_value() == 4); }
	}

	WHEN("we evaluate (/ 30.0 10.0 2.0)")
	{
		Scanner scanner{make_unique<istringstream>("(/ 30.0 10.0 2.0)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be a real number") { REQUIRE(actual->is_real()); }
		AND_THEN("we expect the result to be 1.5") { REQUIRE(actual->real_value() == 1.5); }
	}
}

SCENARIO("Parsing and evaluating subtraction expressions.", "[parse, evaluate, arithmetic, subtraction]")
{
	auto stack{make_shared<Global_Stack_Frame>()};

	WHEN("we evaluate (-)")
	{
		Scanner scanner{make_unique<istringstream>("(-)")};
		Parser parser{scanner};
		THEN("we expect 'too few arguments'")
		{
			REQUIRE_THROWS_MATCHES(parser.parse()->evaluate(*stack), Semantic_Error,
			                       test::pretty_message("Semantic error at line 1, column 2: Too few "
			                                            "arguments. Function '-' expects 2 arguments: "
			                                            "'term', 'terms...'. But no parameters given."));
		}
	}

	WHEN("we evaluate (- 1.5), division acts as negation given only on argument")
	{
		Scanner scanner{make_unique<istringstream>("(- 1.5)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be a real number") { REQUIRE(actual->is_real()); }
		AND_THEN("we expect the result to be -1.5") { REQUIRE(actual->real_value() == -1.5); }
	}

	WHEN("we evaluate (- 1), division acts as negation given only on argument")
	{
		Scanner scanner{make_unique<istringstream>("(- 1)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be an integer number") { REQUIRE(actual->is_integer()); }
		AND_THEN("we expect the result to be -1") { REQUIRE(actual->int_value() == -1); }
	}

	WHEN("we evaluate (- 16 2 2)")
	{
		Scanner scanner{make_unique<istringstream>("(- 16 2 2)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be an integer number") { REQUIRE(actual->is_integer()); }
		AND_THEN("we expect the result to be 12") { REQUIRE(actual->int_value() == 12); }
	}

	WHEN("we evaluate (- 30.0 10.0 2.0)")
	{
		Scanner scanner{make_unique<istringstream>("(- 30.0 10.0 2.0)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};
		THEN("we expect the result to be a real number") { REQUIRE(actual->is_real()); }
		AND_THEN("we expect the result to be 18.0") { REQUIRE(actual->real_value() == 18.0); }
	}
}

TEST_CASE("A named function can be applied to all elements of a collection (map lazy).", "[map]")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{"(def! SQ (fun x (* x x)))"};
	Parser parser{scanner};
	parser.parse()->evaluate(*stack);
	{
		Scanner scanner{"(map [1 2 3 4 5] SQ)"};
		Parser parser{scanner};
		const auto parsed{parser.parse()};
		const auto actual{parsed->evaluate(*stack)};
		REQUIRE_FALSE(actual->head().is_nil());
		REQUIRE(actual->head().int_value() == 1);
		REQUIRE(actual->tail().head().int_value() == 4);
		REQUIRE(actual->tail().head().int_value() == 9);
		REQUIRE(actual->tail().head().int_value() == 16);
		REQUIRE(actual->tail().head().int_value() == 25);
		REQUIRE(actual->tail().head().is_nil());
		REQUIRE(actual->tail().is_nil());
	}
}

TEST_CASE("An anonymous function can be applied to all elements of a collection (map lazy).", "[map]")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{"(map [1 2 3] (fun x (* x 2))))"};
	Parser parser{scanner};
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "2\n4\n6");
}

TEST_CASE("Mappings can be nested (map lazy).", "[map]")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{"(map (map [1 2 3] (fun x (* x 2))) (fun x (+ x 1)))"};
	Parser parser{scanner};
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "3\n5\n7");
}
