// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/function/collection/range_generator.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "test_tools/stack_frame_mock.hpp"
#include <catch2/catch_test_macros.hpp>
#include <memory>

using namespace appsl;
using std::make_unique;

TEST_CASE("A Nil_Expression is no list of integers.", "[expression, traits]")
{
	REQUIRE(expr::is_list_of_integers(*Nil_Expression::NIL));
}

TEST_CASE("An Integer_Literal is a list of integers.", "[expression, traits]")
{
	const Integer_Literal_Expression int_expr{Token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}},
	                                          123};
	REQUIRE(expr::is_list_of_integers(int_expr));
}

TEST_CASE("A list of Integer_Literals is a list of integers.", "[expression, traits]")
{
	const List_Expression list{
			Token{Token::Type::L_BRACKET, "[", Token::NO_POS},
			make_unique<Integer_Literal_Expression>(Token{Token::Type::INTEGER_LITERAL, "1", Token::NO_POS},
	                                                        1),
			make_unique<List_Expression>(
					Token::NONE,
					make_unique<Integer_Literal_Expression>(
							Token{Token::Type::INTEGER_LITERAL, "2", Token::NO_POS}, 2),
					make_unique<List_Expression>(
							Token::NONE,
							make_unique<Integer_Literal_Expression>(
									Token{Token::Type::INTEGER_LITERAL, "3",
	                                                                      Token::NO_POS},
									3),
							make_unique<List_Expression>(
									Token::NONE,
									make_unique<Integer_Literal_Expression>(
											Token{Token::Type::INTEGER_LITERAL,
	                                                                                      "4", Token::NO_POS},
											4))))};

	REQUIRE(expr::is_list_of_integers(list));
}

TEST_CASE("A list of mixed types is no list of integers.", "[expression, traits]")
{
	const List_Expression list{
			Token{Token::Type::L_BRACKET, "[", Token::NO_POS},
			make_unique<Integer_Literal_Expression>(Token{Token::Type::INTEGER_LITERAL, "1", Token::NO_POS},
	                                                        1),
			make_unique<List_Expression>(
					Token::NONE,
					make_unique<Integer_Literal_Expression>(
							Token{Token::Type::INTEGER_LITERAL, "2", Token::NO_POS}, 2),
					make_unique<List_Expression>(
							Token::NONE,
							make_unique<Name_Expression>(Token{Token::Type::NAME, "name",
	                                                                                   Token::NO_POS}),
							make_unique<List_Expression>(
									Token::NONE,
									make_unique<Integer_Literal_Expression>(
											Token{Token::Type::INTEGER_LITERAL,
	                                                                                      "4", Token::NO_POS},
											4))))};

	REQUIRE_FALSE(expr::is_list_of_integers(list));
}

TEST_CASE("Count works for range generator.", "[expression, traits]")
{
	Range_Generator range{1, 5, make_unique<test::Stack_Frame_Mock>()};
	REQUIRE(expr::count(range) == 5);
}
