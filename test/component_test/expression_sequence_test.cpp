// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/parser.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

TEST_CASE("Let expression, positive case", "[let, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(let [(a 10)"
	                "      (b 3) ]"
	                "     (* a b) )"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(actual->stringify() == "30");
}

TEST_CASE("Let expression, return value missing", "[let, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(let [(a 10)"
	                "      (b 3) ])"};
	Parser parser{scanner};
	// WHEN/THEN
	CHECK_THROWS_MATCHES(
			// WHEN
			parser.parse()->evaluate(*stack),
			//  THEN
			Semantic_Error,
			test::pretty_message(
					"Semantic error at line 1, column 6: Too few arguments. Function 'let' "
					"expects 2 arguments: '\\'definitions', '\\'return'. But 1 parameter given: "
					"'[(a 10) (b 3)]'."));
}

TEST_CASE("Let expression must begin with name/value pairs to define a scope (1/2)", "[let, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(let [10"
	                "      (b 3) ]"
	                "     (* a b) )"};
	Parser parser{scanner};
	// WHEN/THEN
	CHECK_THROWS_MATCHES(
			// WHEN
			parser.parse()->evaluate(*stack),
			//  THEN
			Semantic_Error,
			test::pretty_message("Semantic error at line 1, column 7: This is no name/value pair that can "
	                                     "be defined in let scope: '10'."));
}

TEST_CASE("Let expression must begin with name/value pairs to define a scope (2/2)", "[let, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(let [a"
	                "      (b 3) ]"
	                "     (* a b) )"};
	Parser parser{scanner};
	// WHEN/THEN
	CHECK_THROWS_MATCHES(
			// WHEN
			parser.parse()->evaluate(*stack),
			//  THEN
			Semantic_Error,
			test::pretty_message("Semantic error at line 1, column 7: This is no name/value pair that can "
	                                     "be defined in let scope: 'a'."));
}

TEST_CASE("Do expression positive case", "[do, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(do (def! a 10)"
	                "    (def! b 3)"
	                "    (+ a b) )"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(actual->stringify() == "13");
}

TEST_CASE("Do expression might be empty", "[do, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(do)"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(actual->stringify() == "nil");
}

TEST_CASE("Do expression might consist of single expression", "[do, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(do (def! a 10))"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(actual->stringify() == "10");
}

TEST_CASE("No duplicate names in do-expression", "[do, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(do (def! a 10) \n"
	                "    (def! a 20) \n"
	                "    (+ a b) )"};
	Parser parser{scanner};
	// WHEN/THEN
	CHECK_THROWS_MATCHES(
			// WHEN
			parser.parse()->evaluate(*stack),
			//  THEN
			Semantic_Error,
			test::pretty_message("Semantic error at line 2, column 11: Name 'a' already defined."));
}

/**
 * @brief The evaluation function originally did not correctly evaluate nested function applications.
 */
TEST_CASE("Nested function applications evaluated correctly (def! within do)", "[do, bug, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! f (fun x (do (def! a 10)"
	                "                   (* x a) )))"
	                "(f 14)"};
	Parser parser{scanner};
	parser.parse()->evaluate(*stack);
	REQUIRE(parser.has_more_to_parse());
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(actual->stringify() == "140");
}

/**
 * @brief The evaluation function originally did not correctly evaluate nested function applications.
 */
TEST_CASE("Nested function applications evaluated correctly (fun within let)", "[let, bug, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! f (fun x (let [(F (fun x (* x x)))]"
	                "                    (F x) )))"
	                "(f 12)"};
	Parser parser{scanner};
	parser.parse()->evaluate(*stack);
	REQUIRE(parser.has_more_to_parse());
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(actual->stringify() == "144");
}

TEST_CASE("Nested function applications evaluated correctly (fun within let), functions in let-scope calling each "
          "other",
          "[let, bug, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(def! f (fun x (let [(SQ (fun x (* x x)))"
	                "                     (F (fun x (SQ x)))]"
	                "                    (F (SQ x)) )))"
	                "(f 12)"};
	Parser parser{scanner};
	parser.parse()->evaluate(*stack);
	REQUIRE(parser.has_more_to_parse());
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(actual->stringify() == "20736");
}

TEST_CASE("No duplicate names in let-expression", "[let, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{"(let [(a 10) \n"
	                "      (a 20)] \n"
	                "     (+ a b) )"};
	Parser parser{scanner};
	// WHEN/THEN
	CHECK_THROWS_MATCHES(
			// WHEN
			parser.parse()->evaluate(*stack),
			//  THEN
			Semantic_Error,
			test::pretty_message(
					"Semantic error at line 2, column 8: Name 'a' already defined in let scope."));
}
