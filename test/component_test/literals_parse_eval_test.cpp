// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

SCENARIO("nil is a function.", "[parse, evaluate, nil]")
{
	auto stack{make_shared<Global_Stack_Frame>()};

	WHEN("nil is evaluated")
	{
		Scanner scanner{make_unique<istringstream>("nil")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};

		THEN("it evaluates to itself")
		{
			REQUIRE(actual->is_nil());
			REQUIRE(actual->stringify() == "nil");
		}
	}

	WHEN("nil is applied as a function on 2 arguments")
	{
		Scanner scanner{make_unique<istringstream>("(nil 1 2)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};

		THEN("it always evaluates to the second argument")
		{
			REQUIRE(actual->is_integer());
			REQUIRE(actual->int_value() == 2);
		}
	}
}

SCENARIO("true is a function.", "[parse, evaluate, true]")
{
	auto stack{make_shared<Global_Stack_Frame>()};

	WHEN("true is evaluated")
	{
		Scanner scanner{make_unique<istringstream>("true")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};

		THEN("it evaluates to itself") { REQUIRE(actual->stringify() == "true"); }
	}

	WHEN("true is applied as a function on 2 arguments")
	{
		Scanner scanner{make_unique<istringstream>("(true 1 2)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};

		THEN("it always evaluates to the first argument")
		{
			REQUIRE(actual->is_integer());
			REQUIRE(actual->int_value() == 1);
		}
	}
}

SCENARIO("Integer literals are constant functions.", "[parse, evaluate, literal]")
{
	auto stack{make_shared<Global_Stack_Frame>()};

	WHEN("we use an literal as a function without passing parameters")
	{
		Scanner scanner{make_unique<istringstream>("(4711)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};

		THEN("we expect the result to be an integer number") { REQUIRE(actual->is_integer()); }
		AND_THEN("we expect the result to be 4711") { REQUIRE(actual->int_value() == 4711); }
	}

	WHEN("we use a literal as a function with passing an empty parameter")
	{
		Scanner scanner{make_unique<istringstream>("(4711 ())")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};

		THEN("we expect the result to be an integer number") { REQUIRE(actual->is_integer()); }
		AND_THEN("we expect the result to be 4711") { REQUIRE(actual->int_value() == 4711); }
	}
}

SCENARIO("Real literals are constant functions.", "[parse, evaluate, literal]")
{
	auto stack{make_shared<Global_Stack_Frame>()};

	WHEN("we use an literal as a function without passing parameters")
	{
		Scanner scanner{make_unique<istringstream>("(3.14)")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};

		THEN("we expect the result to be a real number") { REQUIRE(actual->is_real()); }
		AND_THEN("we expect the result to be 3.14") { REQUIRE(actual->real_value() == 3.14); }
	}

	WHEN("we use a literal as a function with passing an empty parameter")
	{
		Scanner scanner{make_unique<istringstream>("(3.14 ())")};
		Parser parser{scanner};
		const auto actual{parser.parse()->evaluate(*stack)};

		THEN("we expect the result to be a real number") { REQUIRE(actual->is_real()); }
		AND_THEN("we expect the result to be 3.14") { REQUIRE(actual->real_value() == 3.14); }
	}
}
