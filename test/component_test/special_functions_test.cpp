// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression_traits.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

TEST_CASE("Cons operator builds a list of two integer literals.", "[cons, operator, list]")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{"(1 , 2)"};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE(expr::is_list_of_integers(*actual));
	REQUIRE(actual->head().int_value() == 1);
	REQUIRE(actual->tail().head().int_value() == 2);
	REQUIRE(actual->tail().tail().is_nil());
	REQUIRE(actual->stringify() == "[1 2]");
}

TEST_CASE("Cons operator builds a list of two real literals.", "[cons, operator, list]")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{"(1.5 , 2.5)"};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE(actual->head().real_value() == 1.5);
	REQUIRE(actual->tail().head().real_value() == 2.5);
	REQUIRE(actual->tail().tail().is_nil());
	REQUIRE(actual->stringify() == "[1.5 2.5]");
}

TEST_CASE("Cons operator can be used to implement cons function.", "[cons, operator, list]")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{"(def! CONS (fun (l r) l , r))"};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE(actual->is_function());
	REQUIRE(actual->stringify() == "𝛌(l r).[l r]");
}

TEST_CASE("Cons function can be used to construct lists.", "[cons, operator, list]")
{
	auto stack{make_runtime()};
	Scanner scanner{"(cons 1 (cons 2 (cons 3 4)))"};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE(expr::is_list_of_integers(*actual));
	REQUIRE(actual->head().int_value() == 1);
	REQUIRE(actual->tail().head().int_value() == 2);
	REQUIRE(actual->tail().tail().head().int_value() == 3);
	REQUIRE(actual->tail().tail().tail().head().int_value() == 4);
	REQUIRE(actual->tail().tail().tail().tail().is_nil());
	REQUIRE(actual->stringify() == "[1 2 3 4]");
}
