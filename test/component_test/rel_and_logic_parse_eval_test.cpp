// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/parser.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

//-----------------------------------------------------------------------------
// not
TEST_CASE("not true --> false")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(not true)")};
	Parser parser{scanner};

	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
}

TEST_CASE("not not true --> true")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(not (not true))")};
	Parser parser{scanner};

	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
}

TEST_CASE("not false --> true")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(not nil)")};
	Parser parser{scanner};

	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
}

TEST_CASE("not not false --> false")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(not (not nil))")};
	Parser parser{scanner};

	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
}

//-----------------------------------------------------------------------------
// and
TEST_CASE("Test of logical and.")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(and)\n"
	                                           "(and nil)\n"
	                                           "(and true)\n"
	                                           "(and nil nil)\n"
	                                           "(and true nil)\n"
	                                           "(and nil true)\n"
	                                           "(and true true)\n"
	                                           "(and nil nil nil)\n"
	                                           "(and true nil nil)\n"
	                                           "(and nil true nil)\n"
	                                           "(and true true nil)\n"
	                                           "(and nil nil true)\n"
	                                           "(and true nil true)\n"
	                                           "(and nil true true)\n"
	                                           "(and true true true)")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE_FALSE(parser.has_more_to_parse());
}

//-----------------------------------------------------------------------------
// or
TEST_CASE("Test of logical or.")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(or)\n"
	                                           "(or nil)\n"
	                                           "(or true)\n"
	                                           "(or nil nil)\n"
	                                           "(or true nil)\n"
	                                           "(or nil true)\n"
	                                           "(or true true)\n"
	                                           "(or nil nil nil)\n"
	                                           "(or true nil nil)\n"
	                                           "(or nil true nil)\n"
	                                           "(or true true nil)\n"
	                                           "(or nil nil true)\n"
	                                           "(or true nil true)\n"
	                                           "(or nil true true)\n"
	                                           "(or true true true)")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE_FALSE(parser.has_more_to_parse());
}

//-----------------------------------------------------------------------------
// xor
TEST_CASE("Test of logical xor.")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(xor)\n"
	                                           "(xor nil)\n"
	                                           "(xor true)\n"
	                                           "(xor nil nil)\n"
	                                           "(xor true nil)\n"
	                                           "(xor nil true)\n"
	                                           "(xor true true)\n"
	                                           "(xor nil nil nil)\n"
	                                           "(xor true nil nil)\n"
	                                           "(xor nil true nil)\n"
	                                           "(xor true true nil)\n"
	                                           "(xor nil nil true)\n"
	                                           "(xor true nil true)\n"
	                                           "(xor nil true true)\n"
	                                           "(xor true true true)")};
	Parser parser{scanner};

	REQUIRE(parser.has_more_to_parse());
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
	REQUIRE_FALSE(parser.has_more_to_parse());
}

//-----------------------------------------------------------------------------
// =
TEST_CASE("1 = 1 --> true")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(= 1 1)")};
	Parser parser{scanner};

	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "true");
}

TEST_CASE("1 = 2 --> false")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(= 1 2)")};
	Parser parser{scanner};

	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
}

TEST_CASE("2 = 1 --> false")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(= 2 1)")};
	Parser parser{scanner};

	REQUIRE(parser.parse()->evaluate(*stack)->stringify() == "nil");
}

TEST_CASE("1 = 1.0 --> false (mixing types does not work)")
{
	auto stack{make_shared<Global_Stack_Frame>()};
	Scanner scanner{make_unique<istringstream>("(= 1 5.0)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "nil");
}

//-----------------------------------------------------------------------------
// /=
TEST_CASE("1 /= 1 --> false")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(/= 1 1)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "nil");
}

TEST_CASE("1 /= 2 --> true")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(/= 1 2)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "true");
}

TEST_CASE("2 /= 1 --> true")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(/= 2 1)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "true");
}

TEST_CASE("1 /= 1.0 --> true (mixing types does not work)")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(/= 1 5.0)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "true");
}

//-----------------------------------------------------------------------------
// <
TEST_CASE("1 < 1 --> false")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(< 1 1)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "nil");
}

TEST_CASE("1 < 2 --> true")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(< 1 2)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "true");
}

TEST_CASE("2 < 1 --> false")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(< 2 1)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "nil");
}

TEST_CASE("1 < 2.0 --> false (mixing types does not work)")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(< 1 2.0)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "nil");
}

//-----------------------------------------------------------------------------
// =<
TEST_CASE("1 =< 1 --> true")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(=< 1 1)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "true");
}

TEST_CASE("1 =< 2 --> true")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(=< 1 2)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "true");
}

TEST_CASE("2 =< 1 --> false")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(=< 2 1)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "nil");
}

TEST_CASE("1 =< 0.5 --> true (mixing types does not work)")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(=< 1 2.0)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "true");
}

//-----------------------------------------------------------------------------
// >
TEST_CASE("1 > 1 --> false")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(> 1 1)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "nil");
}

TEST_CASE("1 > 2 --> false")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(> 1 2)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "nil");
}

TEST_CASE("2 > 1 --> true")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(> 2 1)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "true");
}

TEST_CASE("1 > 0.5 --> false (mixing types does not work)")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(> 1 0.5)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "nil");
}

//-----------------------------------------------------------------------------
// >
TEST_CASE("1 >= 1 --> true")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(>= 1 1)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "true");
}

TEST_CASE("1 >= 2 --> false")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(>= 1 2)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "nil");
}

TEST_CASE("2 >= 1 --> true")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(>= 2 1)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};

	REQUIRE(actual->stringify() == "true");
}

// Caution: The test case was originally titled "-1 >= 1.0 ...".  This caused a
// CTest error "unrecognised token: -1".  Strange enough,  it did work when run
// from IDE but fails when run on command line.
//
// The comparison operators of the expression classes need a redesign.
TEST_CASE("Test -1 >= 1.0 --> true (mixing types does not work)")
{
	auto stack{make_runtime()};
	Scanner scanner{make_unique<istringstream>("(>= -1 1.0)")};
	Parser parser{scanner};
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE(actual->stringify() == "true");
}
