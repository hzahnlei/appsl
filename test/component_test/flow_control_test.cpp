// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "test_tools/pretty_message_matcher.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

TEST_CASE("true is actually a binary function", "[control flow, true, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"((true then:"a" else:"b"))"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(actual->stringify() == R"("a")");
}

TEST_CASE("nil is actually a binary function", "[control flow, true, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"((nil then:"a" else:"b"))"};
	Parser parser{scanner};
	// WHEN
	const auto actual{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	// THEN
	CHECK(actual->stringify() == R"("b")");
}

TEST_CASE("true and nil can be used like a if-then-else construct", "[control flow, if, then, else, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"( ((odd? 1) then:"odd" else:"even") )"
	                R"( ((odd? 2) then:"odd" else:"even") )"};
	Parser parser{scanner};
	// WHEN
	const auto actual1{parser.parse()->evaluate(*stack)};
	REQUIRE(parser.has_more_to_parse());
	const auto actual2{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	//  THEN
	CHECK(actual1->stringify() == R"("odd")");
	CHECK(actual2->stringify() == R"("even")");
}

TEST_CASE("true and nil can be used to build complex if-then-else constructs", "[control flow, if, then, else, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"( (def! check10 (fun x )"
	                R"(                    ((< x 10) )"
	                R"(                           then: "lesser" )"
	                R"(                           else: ((= x 10) )"
	                R"(                                        then: "equal" )"
	                R"(                                        else: "greater" ) ) )) )"
	                R"( (check10 9) )"
	                R"( (check10 10) )"
	                R"( (check10 11) )"};
	Parser parser{scanner};
	// WHEN
	parser.parse()->evaluate(*stack);
	REQUIRE(parser.has_more_to_parse());
	const auto actual1{parser.parse()->evaluate(*stack)};
	REQUIRE(parser.has_more_to_parse());
	const auto actual2{parser.parse()->evaluate(*stack)};
	REQUIRE(parser.has_more_to_parse());
	const auto actual3{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	//  THEN
	CHECK(actual1->stringify() == R"("lesser")");
	CHECK(actual2->stringify() == R"("equal")");
	CHECK(actual3->stringify() == R"("greater")");
}

TEST_CASE("case expressions are more readable than nested if-then-else constructs", "[control flow, case, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"( (def! check10 (fun x )"
	                R"(                    (case (< x 10) -> "lesser" )"
	                R"(                          (= 10 x) -> "equal" )"
	                R"(                          default  -> "greater" ) )) )"
	                R"( (check10 9) )"
	                R"( (check10 10) )"
	                R"( (check10 11) )"};
	Parser parser{scanner};
	// WHEN
	parser.parse()->evaluate(*stack);
	REQUIRE(parser.has_more_to_parse());
	const auto actual1{parser.parse()->evaluate(*stack)};
	REQUIRE(parser.has_more_to_parse());
	const auto actual2{parser.parse()->evaluate(*stack)};
	REQUIRE(parser.has_more_to_parse());
	const auto actual3{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	//  THEN
	CHECK(actual1->stringify() == R"("lesser")");
	CHECK(actual2->stringify() == R"("equal")");
	CHECK(actual3->stringify() == R"("greater")");
}

TEST_CASE("'default' may be skipped in case expressions", "[control flow, case, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"( (def! check10 (fun x )"
	                R"(                    (case (< x 10) -> "lesser" )"
	                R"(                          (= 10 x) -> "equal" )"
	                R"(                                      "greater" ) )) )"
	                R"( (check10 9) )"
	                R"( (check10 10) )"
	                R"( (check10 11) )"};
	Parser parser{scanner};
	// WHEN
	parser.parse()->evaluate(*stack);
	REQUIRE(parser.has_more_to_parse());
	const auto actual1{parser.parse()->evaluate(*stack)};
	REQUIRE(parser.has_more_to_parse());
	const auto actual2{parser.parse()->evaluate(*stack)};
	REQUIRE(parser.has_more_to_parse());
	const auto actual3{parser.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser.has_more_to_parse());
	//  THEN
	CHECK(actual1->stringify() == R"("lesser")");
	CHECK(actual2->stringify() == R"("equal")");
	CHECK(actual3->stringify() == R"("greater")");
}

/*
 * Please note how the error in the case-expression only gets recognized once the function gets actually applied. This
 * is one of the shortcomings of dynamically typed, interpreted languages. Maybe I can fix this in the future by adding
 * additional checks upfront.
 */
TEST_CASE("case expressions require a default", "[control flow, case, fast]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner{R"( (def! check10 (fun x )"
	                R"(                    (case (< x 10) -> "lesser" )"
	                R"(                          (= 10 x) -> "equal" ) )) )"
	                R"( (check10 1) )"};
	Parser parser{scanner};
	parser.parse()->evaluate(*stack);
	REQUIRE(parser.has_more_to_parse());
	// WHEN/THEN
	CHECK_THROWS_MATCHES(
			// WHEN
			parser.parse()->evaluate(*stack),
			//  THEN
			Semantic_Error,
			test::pretty_message("Semantic error at line 1, column 105: The last case in a case expression "
	                                     "needs to be either a single expression or a pair of the form 'default -> "
	                                     "<expression>'. This '(= 10 x) -> \"equal\"' is neither of those."));
}

// A bug within the evaluation function for case-expressions prevented case-expressions from being evaluated correctly.
// This test verifies that the observed error is actually gone.
TEST_CASE("case-expressions returned 'and' function instead of result", "[control flow, case, fast, bug]")
{
	// GIVEN
	auto stack{make_runtime()};
	Scanner scanner1{R"( (def! IN-RANGE? (fun [n lower upper] )"
	                 R"(                      (case (Integer? n) -> (and (>= n lower) )"
	                 R"(                                                 (=< n upper) ) )"
	                 R"(                            default      -> (Error "Expected integer." nil) ) )) )"};
	Parser parser1{scanner1};
	REQUIRE(parser1.has_more_to_parse());
	const auto result1{parser1.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser1.has_more_to_parse());
	REQUIRE(result1->stringify() == "𝛌[n lower upper].(case (Integer? n) -> (and (>= n lower) (=< n upper)) "
	                                "default -> (Error \"Expected integer.\" nil))");
	// WHEN
	Scanner scanner2{"(IN-RANGE? 3 1 100)"};
	Parser parser2{scanner2};
	REQUIRE(parser2.has_more_to_parse());
	const auto result2{parser2.parse()->evaluate(*stack)};
	REQUIRE_FALSE(parser2.has_more_to_parse());
	// THEN
	CHECK(result2->stringify() == "true");
}
