// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "test_tools/list_expression_builder.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("for_each on Nil_Expression does not iterate.", "[Algorithm, for_each]")
{
	auto iter_count{0U};
	expr::for_each(*Nil_Expression::NIL, [&](const auto &) { iter_count++; });
	REQUIRE(iter_count == 0);
}

TEST_CASE("for_each on Integer_Literal iterates exactly once.", "[Algorithm, for_each]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS};
	const Integer_Literal_Expression literal_expr{token, 123};

	auto iter_count{0U};
	expr::for_each(literal_expr,
	               [&](const auto &expr)
	               {
			       iter_count++;
			       REQUIRE(expr == literal_expr);
		       });
	REQUIRE(iter_count == 1);
}

TEST_CASE("for_each on empty List_Expression does not iterate.", "[Algorithm, for_each]")
{
	const List_Expression list{Token{Token::Type::L_BRACKET, "[", Token::NO_POS}};

	auto iter_count{0U};
	expr::for_each(list, [&](const auto &) { iter_count++; });
	REQUIRE(iter_count == 0);
}

TEST_CASE("for_each on List_Expression iterates each element in order and exactly once.", "[Algorithm, for_each]")
{
	const List_Expression list{
			Token{Token::Type::L_BRACKET, "[", Token::NO_POS},
			make_unique<Integer_Literal_Expression>(Token{Token::Type::INTEGER_LITERAL, "1", Token::NO_POS},
	                                                        1),
			make_unique<List_Expression>(
					Token::NONE,
					make_unique<Integer_Literal_Expression>(
							Token{Token::Type::INTEGER_LITERAL, "2", Token::NO_POS}, 2),
					make_unique<List_Expression>(
							Token::NONE,
							make_unique<Integer_Literal_Expression>(
									Token{Token::Type::INTEGER_LITERAL, "3",
	                                                                      Token::NO_POS},
									3),
							make_unique<List_Expression>(
									Token::NONE,
									make_unique<Integer_Literal_Expression>(
											Token{Token::Type::INTEGER_LITERAL,
	                                                                                      "4", Token::NO_POS},
											4))))};

	auto iter_count{0U};
	expr::for_each(list,
	               [&](const auto &expr)
	               {
			       iter_count++;
			       REQUIRE(expr.is_integer());
			       REQUIRE(expr.int_value() == iter_count);
		       });
	REQUIRE(iter_count == 4);
}

TEST_CASE("all_of on Nil_Expression does not iterate and is always true.", "[Algorithm, all_of]")
{
	auto iter_count{0U};
	REQUIRE(expr::all_of(*Nil_Expression::NIL,
	                     [&](const auto &)
	                     {
				     iter_count++;
				     return true;
			     }));
	REQUIRE(iter_count == 0);

	iter_count = 0U;
	REQUIRE(expr::all_of(*Nil_Expression::NIL,
	                     [&](const auto &)
	                     {
				     iter_count++;
				     return false;
			     }));
	REQUIRE(iter_count == 0);
}

TEST_CASE("all_of is only true if predicate holds for all elements of collection.", "[Algorithm, all_of]")
{
	const List_Expression list{
			Token{Token::Type::L_BRACKET, "[", Token::NO_POS},
			make_unique<Integer_Literal_Expression>(Token{Token::Type::INTEGER_LITERAL, "1", Token::NO_POS},
	                                                        1),
			make_unique<List_Expression>(
					Token::NONE,
					make_unique<Integer_Literal_Expression>(
							Token{Token::Type::INTEGER_LITERAL, "2", Token::NO_POS}, 2),
					make_unique<List_Expression>(
							Token::NONE,
							make_unique<Integer_Literal_Expression>(
									Token{Token::Type::INTEGER_LITERAL, "3",
	                                                                      Token::NO_POS},
									3),
							make_unique<List_Expression>(
									Token::NONE,
									make_unique<Integer_Literal_Expression>(
											Token{Token::Type::INTEGER_LITERAL,
	                                                                                      "4", Token::NO_POS},
											4))))};

	auto iter_count{0U};
	REQUIRE(expr::all_of(list,
	                     [&](const auto &expr)
	                     {
				     iter_count++;
				     return expr.is_integer();
			     }));
	REQUIRE(iter_count == 4);
}

TEST_CASE("all_of is false if predicate does not hold for at least one elements of collection.", "[Algorithm, all_of]")
{
	const List_Expression list{
			Token{Token::Type::L_BRACKET, "[", Token::NO_POS},
			make_unique<Integer_Literal_Expression>(Token{Token::Type::INTEGER_LITERAL, "1", Token::NO_POS},
	                                                        1),
			make_unique<List_Expression>(
					Token::NONE,
					make_unique<Integer_Literal_Expression>(
							Token{Token::Type::INTEGER_LITERAL, "2", Token::NO_POS}, 2),
					make_unique<List_Expression>(
							Token::NONE,
							make_unique<Name_Expression>(Token{Token::Type::NAME, "name",
	                                                                                   Token::NO_POS}),
							make_unique<List_Expression>(
									Token::NONE,
									make_unique<Integer_Literal_Expression>(
											Token{Token::Type::INTEGER_LITERAL,
	                                                                                      "4", Token::NO_POS},
											4))))};

	auto iter_count{0U};
	REQUIRE_FALSE(expr::all_of(list,
	                           [&](const auto &expr)
	                           {
					   iter_count++;
					   return expr.is_integer();
				   }));
	REQUIRE(iter_count == 3);
}

TEST_CASE("All elements from both lists are zipped if both lists are equally long.", "[Algorithm, zip]")
{
	const auto list1{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 1},
	                                          Integer_Literal_Expression{Token::NONE, 2},
	                                          Integer_Literal_Expression{Token::NONE, 3})};
	const auto list2{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 10},
	                                          Integer_Literal_Expression{Token::NONE, 20},
	                                          Integer_Literal_Expression{Token::NONE, 30})};

	auto iter_count{0U};
	stringstream actual;
	expr::zip(*list1, *list2,
	          [&](const auto &expr1, const auto &expr2)
	          {
			  actual << expr1.stringify() << ";" << expr2.stringify() << '\n';
			  iter_count++;
		  });
	REQUIRE(iter_count == 3);
	REQUIRE(actual.str() == "1;10\n2;20\n3;30\n");
}

TEST_CASE("Only n elements from both lists are zipped if 1st list contains n < m elements.", "[Algorithm, zip]")
{
	const auto list1{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 1},
	                                          Integer_Literal_Expression{Token::NONE, 2})};
	const auto list2{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 10},
	                                          Integer_Literal_Expression{Token::NONE, 20},
	                                          Integer_Literal_Expression{Token::NONE, 30})};

	auto iter_count{0U};
	stringstream actual;
	expr::zip(*list1, *list2,
	          [&](const auto &expr1, const auto &expr2)
	          {
			  actual << expr1.stringify() << ";" << expr2.stringify() << '\n';
			  iter_count++;
		  });
	REQUIRE(iter_count == 2);
	REQUIRE(actual.str() == "1;10\n2;20\n");
}

TEST_CASE("The rest of both lists can be examined if the 1st list is shorter", "[Algorithm, zip]")
{
	const auto list1{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 1},
	                                          Integer_Literal_Expression{Token::NONE, 2})};
	const auto list2{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 10},
	                                          Integer_Literal_Expression{Token::NONE, 20},
	                                          Integer_Literal_Expression{Token::NONE, 30})};

	const auto &[remainder1, remainder2]{expr::zip(*list1, *list2, [&](const auto &, const auto &) {})};
	REQUIRE(remainder1 == *Nil_Expression::NIL);
	REQUIRE(remainder2 == *test::as_list_expression(Integer_Literal_Expression{Token::NONE, 30}));
}

TEST_CASE("Only m elements from both lists are zipped if 2nd list contains m < n elements.", "[Algorithm, zip]")
{
	const auto list1{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 1},
	                                          Integer_Literal_Expression{Token::NONE, 2},
	                                          Integer_Literal_Expression{Token::NONE, 3})};
	const auto list2{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 10},
	                                          Integer_Literal_Expression{Token::NONE, 20})};

	auto iter_count{0U};
	stringstream actual;
	expr::zip(*list1, *list2,
	          [&](const auto &expr1, const auto &expr2)
	          {
			  actual << expr1.stringify() << ";" << expr2.stringify() << '\n';
			  iter_count++;
		  });
	REQUIRE(iter_count == 2);
	REQUIRE(actual.str() == "1;10\n2;20\n");
}

TEST_CASE("The rest of both lists can be examined if the 2nd list is shorter", "[Algorithm, zip]")
{
	const auto list1{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 1},
	                                          Integer_Literal_Expression{Token::NONE, 2},
	                                          Integer_Literal_Expression{Token::NONE, 3})};
	const auto list2{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 10},
	                                          Integer_Literal_Expression{Token::NONE, 20})};

	const auto &[remainder1, remainder2]{expr::zip(*list1, *list2, [&](const auto &, const auto &) {})};
	REQUIRE(remainder1 == *test::as_list_expression(Integer_Literal_Expression{Token::NONE, 3}));
	REQUIRE(remainder2 == *Nil_Expression::NIL);
}

TEST_CASE("Nothing zipped if 1st list empty.", "[Algorithm, zip]")
{
	const auto list1{make_unique<Nil_Expression>(Token::NONE)};
	const auto list2{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 10},
	                                          Integer_Literal_Expression{Token::NONE, 20},
	                                          Integer_Literal_Expression{Token::NONE, 30})};

	auto iter_count{0U};
	expr::zip(*list1, *list2, [&](const auto &, const auto &) { iter_count++; });
	REQUIRE(iter_count == 0);
}

TEST_CASE("Nothing zipped if 2nd list empty.", "[Algorithm, zip]")
{
	const auto list1{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 1},
	                                          Integer_Literal_Expression{Token::NONE, 2},
	                                          Integer_Literal_Expression{Token::NONE, 3})};
	const auto list2{make_unique<Nil_Expression>(Token::NONE)};

	auto iter_count{0U};
	expr::zip(*list1, *list2, [&](const auto &, const auto &) { iter_count++; });
	REQUIRE(iter_count == 0);
}

TEST_CASE("Nothing zipped if both lists empty.", "[Algorithm, zip]")
{
	const auto list1{make_unique<Nil_Expression>(Token::NONE)};
	const auto list2{make_unique<Nil_Expression>(Token::NONE)};

	auto iter_count{0U};
	expr::zip(*list1, *list2, [&](const auto &, const auto &) { iter_count++; });
	REQUIRE(iter_count == 0);
}

TEST_CASE("enumerate on Nil_Expression does not iterate.", "[Algorithm, enumerate]")
{
	auto iter_count{0U};
	expr::enumerate(*Nil_Expression::NIL, [&](const auto &, const auto) { iter_count++; });
	REQUIRE(iter_count == 0);
}

TEST_CASE("enumerate on Integer_Literal iterates exactly once.", "[Algorithm, enumerate]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS};
	const Integer_Literal_Expression literal_expr{token, 123};

	auto iter_count{0U};
	expr::enumerate(literal_expr,
	                [&](const auto &expr, const auto i)
	                {
				REQUIRE(expr == literal_expr);
				REQUIRE(i == iter_count);
				iter_count++;
			});
	REQUIRE(iter_count == 1);
}

TEST_CASE("enumerate on empty List_Expression does not iterate.", "[Algorithm, enumerate]")
{
	const List_Expression list{Token{Token::Type::L_BRACKET, "[", Token::NO_POS}};

	auto iter_count{0U};
	expr::enumerate(list, [&](const auto &, const auto) { iter_count++; });
	REQUIRE(iter_count == 0);
}

TEST_CASE("enumerate on List_Expression iterates each element in order and exactly once.", "[Algorithm, enumerate]")
{
	const auto list{test::as_list_expression(Integer_Literal_Expression{Token::NONE, 0},
	                                         Integer_Literal_Expression{Token::NONE, 1},
	                                         Integer_Literal_Expression{Token::NONE, 2})};

	auto iter_count{0U};
	expr::enumerate(*list,
	                [&](const auto &expr, const auto i)
	                {
				REQUIRE(expr.is_integer());
				REQUIRE(expr.int_value() == iter_count);
				REQUIRE(i == iter_count);
				iter_count++;
			});
	REQUIRE(iter_count == 3);
}
