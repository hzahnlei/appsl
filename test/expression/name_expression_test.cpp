// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "test_tools/stack_frame_mock.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("Name_Expression's token can be accessed.", "[Name_Expression]")
{
	const Token token{Token::Type::NAME, "some-name", Token::NO_POS};
	const Name_Expression expr{token};
	REQUIRE(expr.token() == token);
}

TEST_CASE("Comparison Name_Expression for equality works.", "[Name_Expression]")
{
	const Name_Expression expr{Token{Token::Type::NAME, "some-name", Token::NO_POS}};
	REQUIRE(expr.operator==(expr)); // Getting an error with C++20 for expr==expr
}

TEST_CASE("Comparison Name_Expression for equality works (not equal).", "[Name_Expression]")
{
	const Name_Expression expr1{Token{Token::Type::NAME, "some-name", Token::NO_POS}};
	const Name_Expression expr2{Token{Token::Type::NAME, "some-other-name", Token::NO_POS}};
	REQUIRE_FALSE(expr1.operator==(expr2));
}

TEST_CASE("Name_Expression's head is itself.", "[Name_Expression]")
{
	// A name can be seen as list with one element.
	const Name_Expression expr{Token{Token::Type::NAME, "some-name", Token::NO_POS}};
	REQUIRE(expr.head() == expr);
}

TEST_CASE("Name_Expression's tail is nil.", "[Name_Expression]")
{
	const Name_Expression expr{Token{Token::Type::NAME, "some-name", Token::NO_POS}};
	REQUIRE(expr.tail() == *Nil_Expression::NIL);
}

// Evaluation of Name_Expression requires Stack_Frame. Therefore not implemented as unit but as component test.

TEST_CASE("Name_Expression is a name.", "[Name_Expression]")
{
	const Name_Expression expr{Token{Token::Type::NAME, "some-name", Token::NO_POS}};
	REQUIRE(expr.is_name());
	REQUIRE(expr.name() == "some-name");
}

TEST_CASE("Name_Expression's printing operator works.", "[Name_Expression]")
{
	const Name_Expression expr{Token{Token::Type::NAME, "some-name", Token::NO_POS}};
	stringstream result;
	expr << result;
	REQUIRE(result.str() == "(EXPR type:name (TOKEN type:NAME line:0 col:0 val:'some-name'))");
}

TEST_CASE("Expression's global printing operator works for Name_Expression.", "[Name_Expression]")
{
	const Name_Expression expr{Token{Token::Type::NAME, "some-name", Token::NO_POS}};
	stringstream result;
	result << expr;
	REQUIRE(result.str() == "(EXPR type:name (TOKEN type:NAME line:0 col:0 val:'some-name'))");
}
