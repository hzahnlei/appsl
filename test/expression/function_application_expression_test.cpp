// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function_application_expression.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

TEST_CASE("Check parsing result of nil applied as argument to constant function", "[fast]")
{
	// (1 nil) parsed to:
	// funapp
	//  +----int 1
	//  +----funapp
	//        +---- nil
	//        +---- nil
	const Function_Application_Expression fun_app{
			Token{Token::Type::COLON, "(", Token::NO_POS},
			make_shared<Integer_Literal_Expression>(
					Token{Token::Type::INTEGER_LITERAL, "1", Token::Position{0, 1}}, 1),
			make_shared<Function_Application_Expression>(Token{Token::Type::COLON, "(", Token::NO_POS},
	                                                             Nil_Expression::NIL)};

	REQUIRE(is_effectively_empty_arguments_list(fun_app));
}
