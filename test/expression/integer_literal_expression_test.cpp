// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "test_tools/stack_frame_mock.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("Integer_Literal_Expression's token can be accessed.", "[Integer_Literal_Expression]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS};
	const Integer_Literal_Expression expr{token, 123};
	REQUIRE(expr.token() == token);
}

TEST_CASE("Comparison Integer_Literal_Expression for equality works.", "[Integer_Literal_Expression]")
{
	const Integer_Literal_Expression expr{Token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS}, 123};
	REQUIRE(expr.operator==(expr)); // Getting an error with C++20 for expr==expr
}

TEST_CASE("Comparison Integer_Literal_Expression for equality works (not equal).", "[Integer_Literal_Expression]")
{
	const Integer_Literal_Expression expr1{Token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS}, 123};
	const Integer_Literal_Expression expr2{Token{Token::Type::INTEGER_LITERAL, "456", Token::NO_POS}, 456};
	REQUIRE_FALSE(expr1.operator==(expr2));
}

TEST_CASE("Integer_Literal_Expression's head is itself.", "[Integer_Literal_Expression]")
{
	// A literal can be seen as list with one element.
	const Integer_Literal_Expression expr{Token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS}, 123};
	REQUIRE(expr.head() == expr);
}

TEST_CASE("Integer_Literal_Expression's tail is nil.", "[Integer_Literal_Expression]")
{
	const Integer_Literal_Expression expr{Token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS}, 123};
	REQUIRE(expr.tail() == *Nil_Expression::NIL);
}

TEST_CASE("Integer_Literal_Expression evaluates to itself.", "[Integer_Literal_Expression]")
{
	const auto expr = make_shared<Integer_Literal_Expression>(
			Token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS}, 123);
	REQUIRE(*expr->evaluate(test::STACK_MOCK) == *expr);
}

TEST_CASE("Integer_Literal_Expression is not a name.", "[Integer_Literal_Expression]")
{
	const Integer_Literal_Expression expr{Token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS}, 123};
	REQUIRE_FALSE(expr.is_name());
}

TEST_CASE("Integer_Literal_Expression is an integer literal.", "[Integer_Literal_Expression]")
{
	const Integer_Literal_Expression expr{Token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS}, 123};
	REQUIRE(expr.is_integer());
	REQUIRE(expr.int_value() == 123);
}

TEST_CASE("Integer_Literal_Expression's printing operator works.", "[Integer_Literal_Expression]")
{
	const Integer_Literal_Expression expr{Token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS}, 123};
	stringstream result;
	expr << result;
	REQUIRE(result.str() ==
	        "(EXPR type:integer_literal value:123 (TOKEN type:INTEGER_LITERAL line:0 col:0 val:'123'))");
}

TEST_CASE("Expression's global printing operator works for Integer_Literal_Expression.", "[Integer_Literal_Expression]")
{
	const Integer_Literal_Expression expr{Token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS}, 123};
	stringstream result;
	result << expr;
	REQUIRE(result.str() ==
	        "(EXPR type:integer_literal value:123 (TOKEN type:INTEGER_LITERAL line:0 col:0 val:'123'))");
}
