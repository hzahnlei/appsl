// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/nil_expression.hpp"
#include "test_tools/stack_frame_mock.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("Nil_Expression's token can be accessed.", "[Nil_Expression]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::NO_POS};
	const Nil_Expression expr{token};
	REQUIRE(expr.token() == token);
}

TEST_CASE("Comparison Nil_Expression for equality works.", "[Nil_Expression]")
{
	const Nil_Expression expr{Token{Token::Type::NAME, "nil", Token::NO_POS}};
	REQUIRE(expr.operator==(expr)); // Getting an error with C++20 for expr==expr
}

TEST_CASE("Nil_Expression's head is nil.", "[Nil_Expression]")
{
	const Nil_Expression expr{Token{Token::Type::NAME, "nil", Token::NO_POS}};
	REQUIRE(expr.head() == *Nil_Expression::NIL);
}

TEST_CASE("Nil_Expression's tail is nil.", "[Nil_Expression]")
{
	const Nil_Expression expr{Token{Token::Type::NAME, "nil", Token::NO_POS}};
	REQUIRE(expr.tail() == *Nil_Expression::NIL);
}

TEST_CASE("Nil_Expression evaluates to itself.", "[Nil_Expression]")
{
	const auto expr{make_shared<Nil_Expression>(Token{Token::Type::NAME, "nil", Token::NO_POS})};
	REQUIRE(*expr->evaluate(test::STACK_MOCK) == *expr);
}

TEST_CASE("Nil_Expression is not a name.", "[Nil_Expression]")
{
	const Nil_Expression expr{Token{Token::Type::NAME, "nil", Token::NO_POS}};
	REQUIRE_FALSE(expr.is_name());
}

TEST_CASE("Nil_Expression's printing operator works.", "[Nil_Expression]")
{
	const Nil_Expression expr{Token{Token::Type::NAME, "nil", Token::NO_POS}};
	stringstream result;
	expr << result;
	REQUIRE(result.str() == "(EXPR type:nil (TOKEN type:NAME line:0 col:0 val:'nil'))");
}

TEST_CASE("Expression's global printing operator works for Nil_Expression.", "[Nil_Expression]")
{
	const Nil_Expression expr{Token{Token::Type::NAME, "nil", Token::NO_POS}};
	stringstream result;
	result << expr;
	REQUIRE(result.str() == "(EXPR type:nil (TOKEN type:NAME line:0 col:0 val:'nil'))");
}
