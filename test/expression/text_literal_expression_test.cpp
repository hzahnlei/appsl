// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/text_literal_expression.hpp"
#include "test_tools/stack_frame_mock.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("Text_Literal_Expression's token can be accessed.", "[Text_Literal_Expression]")
{
	const Token token{Token::Type::TEXT_LITERAL, "some text", Token::NO_POS};
	const Text_Literal_Expression expr{token};
	REQUIRE(expr.token() == token);
}

TEST_CASE("Comparison Text_Literal_Expression for equality works.", "[Text_Literal_Expression]")
{
	const Text_Literal_Expression expr{Token{Token::Type::TEXT_LITERAL, "some text", Token::NO_POS}};
	REQUIRE(expr.operator==(expr)); // Getting an error with C++20 for expr==expr
}

TEST_CASE("Comparison Text_Literal_Expression for equality works (not equal).", "[Text_Literal_Expression]")
{
	const Text_Literal_Expression expr1{Token{Token::Type::TEXT_LITERAL, "some text", Token::NO_POS}};
	const Text_Literal_Expression expr2{Token{Token::Type::TEXT_LITERAL, "some other text", Token::NO_POS}};
	REQUIRE_FALSE(expr1.operator==(expr2));
}

TEST_CASE("Text_Literal_Expression's head is itself.", "[Text_Literal_Expression]")
{
	// A name can be seen as list with one element.
	const Text_Literal_Expression expr{Token{Token::Type::TEXT_LITERAL, "some text", Token::NO_POS}};
	REQUIRE(expr.head() == expr);
}

TEST_CASE("Text_Literal_Expression's tail is nil.", "[Text_Literal_Expression]")
{
	const Text_Literal_Expression expr{Token{Token::Type::TEXT_LITERAL, "some text", Token::NO_POS}};
	REQUIRE(expr.tail() == *Nil_Expression::NIL);
}

TEST_CASE("Text_Literal_Expression evaluates to itself.", "[Text_Literal_Expression]")
{
	const auto expr{make_shared<Text_Literal_Expression>(Token{Token::Type::TEXT_LITERAL, "abc", Token::NO_POS})};
	REQUIRE(*expr->evaluate(test::STACK_MOCK) == *expr);
}

TEST_CASE("Text_Literal_Expression is not a name.", "[Text_Literal_Expression]")
{
	const Text_Literal_Expression expr{Token{Token::Type::TEXT_LITERAL, "somename", Token::NO_POS}};
	REQUIRE_FALSE(expr.is_name());
}

TEST_CASE("Text_Literal_Expression is a text literal.", "[Text_Literal_Expression]")
{
	const Text_Literal_Expression expr{Token{Token::Type::TEXT_LITERAL, "some text", Token::NO_POS}};
	REQUIRE(expr.is_text());
	REQUIRE(expr.text_value() == "some text");
}

TEST_CASE("Text_Literal_Expression's printing operator works.", "[Text_Literal_Expression]")
{
	const Text_Literal_Expression expr{Token{Token::Type::TEXT_LITERAL, "some text", Token::NO_POS}};
	stringstream result;
	expr << result;
	REQUIRE(result.str() == "(EXPR type:text_literal (TOKEN type:TEXT_LITERAL line:0 col:0 val:'some text'))");
}

TEST_CASE("Expression's global printing operator works for Text_Literal_Expression.", "[Text_Literal_Expression]")
{
	const Text_Literal_Expression expr{Token{Token::Type::TEXT_LITERAL, "some text", Token::NO_POS}};
	stringstream result;
	result << expr;
	REQUIRE(result.str() == "(EXPR type:text_literal (TOKEN type:TEXT_LITERAL line:0 col:0 val:'some text'))");
}
