// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/map_literal_expression.hpp"
#include "appsl/expression/text_literal_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include "appsl/use_stl.hpp"
#include <catch2/catch_test_macros.hpp>

namespace appsl
{

	const auto _1{make_shared<Integer_Literal_Expression>(Token{Token::Type::INTEGER_LITERAL, "1", Token::NO_POS},
	                                                      1)};
	const auto ONE{make_shared<Text_Literal_Expression>(Token{Token::Type::TEXT_LITERAL, "one", Token::NO_POS})};
	const auto _2{make_shared<Integer_Literal_Expression>(Token{Token::Type::INTEGER_LITERAL, "2", Token::NO_POS},
	                                                      2)};
	const auto TWO{make_shared<Text_Literal_Expression>(Token{Token::Type::TEXT_LITERAL, "two", Token::NO_POS})};

	TEST_CASE("Iterate keys of Map_Literal_Expression", "[fast]")
	{
		// GIVEN
		Map_Literal_Expression::Key_Value_Map underlying_map;
		underlying_map.try_emplace(_1, ONE);
		underlying_map.try_emplace(_2, TWO);
		Map_Literal_Expression const the_map{Token{Token::Type::L_CURLY_BRACE, "#{", Token::NO_POS},
		                                     move(underlying_map)};
		// WHEN
		// n/a
		// THEN
		REQUIRE(!the_map.head().is_nil());
		REQUIRE(the_map.head().is_integer());
		REQUIRE(the_map.head().int_value() == 1);

		REQUIRE(!the_map.tail().head().is_nil());
		REQUIRE(the_map.tail().head().is_integer());
		REQUIRE(the_map.tail().head().int_value() == 2);

		// This may  seem odd:  (Integer) literals'  heads are pointing to them
		// selfes!  One may have expected nil here.  But,  this design decision
		// allows to use literals as lists. Everythin is a function, everything
		// is a list.
		REQUIRE(!the_map.tail().head().head().is_nil());
		REQUIRE(the_map.tail().head().head().is_integer());
		REQUIRE(the_map.tail().head().head().int_value() == 2);

		REQUIRE(the_map.tail().head().tail().is_nil());
		REQUIRE(the_map.tail().tail().is_nil());
	}

	TEST_CASE("Map_Literal_Expression can be seen as a function", "[fast]")
	{
		// GIVEN
		Map_Literal_Expression::Key_Value_Map underlying_map;
		Map_Literal_Expression const the_map{Token{Token::Type::L_CURLY_BRACE, "#{", Token::NO_POS},
		                                     move(underlying_map)};
		// WHEN
		const auto is_function{the_map.is_function()};
		// THEN
		REQUIRE(is_function);
	}

	TEST_CASE("Map_Literal_Expression returns value for key", "[fast]")
	{
		// GIVEN
		Map_Literal_Expression::Key_Value_Map underlying_map;
		underlying_map.try_emplace(_1, ONE);
		underlying_map.try_emplace(_2, TWO);
		Map_Literal_Expression const the_map{Token{Token::Type::L_CURLY_BRACE, "#{", Token::NO_POS},
		                                     move(underlying_map)};
		auto stack{make_runtime()};
		stack->define_argument("key", _2);
		// WHEN
		const auto result{the_map.evaluate(*stack)};
		// THEN
		REQUIRE(result->is_text());
		REQUIRE(result->text_value() == "two");
	}

	TEST_CASE("Map_Literal_Expression returns itself if called without arguments", "[fast]")
	{
		// GIVEN
		Map_Literal_Expression::Key_Value_Map underlying_map;
		underlying_map.try_emplace(_1, ONE);
		underlying_map.try_emplace(_2, TWO);
		auto the_map{make_shared<Map_Literal_Expression>(Token{Token::Type::L_CURLY_BRACE, "#{", Token::NO_POS},
		                                                 move(underlying_map))};
		auto stack{make_runtime()};
		// WHEN
		const auto result{the_map->evaluate(*stack)};
		// THEN
		REQUIRE(*result == *the_map);
	}

	TEST_CASE("Build list of keys", "[fast]")
	{
		// GIVEN
		Map_Literal_Expression::Key_Value_Map underlying_map;
		underlying_map.try_emplace(_1, ONE);
		underlying_map.try_emplace(_2, TWO);
		const Token some_token{Token::Type::L_CURLY_BRACE, "#{", Token::NO_POS};
		// WHEN
		const auto list_of_keys{Map_Literal_Expression::list_of_keys(some_token, underlying_map)};
		// THEN
		REQUIRE(list_of_keys->stringify() == "[1 2]");
	}

} // namespace appsl
