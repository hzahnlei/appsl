// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/list_expression.hpp"
#include "test_tools/stack_frame_mock.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("List_Expression's token can be accessed.", "[List_Expression]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 1}};
	const List_Expression expr{token};
	REQUIRE(expr.token() == token);
}

TEST_CASE("Comparison List_Expression for equality works.", "[List_Expression]")
{
	const List_Expression expr{Token::NONE};
	REQUIRE(expr.operator==(expr)); // Getting an error with C++20 for expr==expr
}

TEST_CASE("List_Expression evaluates to itself.", "[List_Expression]")
{
	const auto expr{make_shared<List_Expression>(Token::NONE)};
	REQUIRE(*expr->evaluate(test::STACK_MOCK) == *expr);
}

TEST_CASE("List_Expression's head is nil.", "[List_Expression]")
{
	const Nil_Expression expr{Token{Token::Type::NAME, "nil", Token::NO_POS}};
	REQUIRE(expr.head() == *Nil_Expression::NIL);
}

TEST_CASE("List_Expression's tail is nil.", "[List_Expression]")
{
	const Nil_Expression expr{Token{Token::Type::NAME, "nil", Token::NO_POS}};
	REQUIRE(expr.tail() == *Nil_Expression::NIL);
}

TEST_CASE("List_Expression is not a name.", "[List_Expression]")
{
	const List_Expression expr{Token::NONE};
	REQUIRE_FALSE(expr.is_name());
}

TEST_CASE("List_Expression's printing operator works.", "[List_Expression]")
{
	const List_Expression expr{Token::NONE};
	stringstream result;
	expr << result;
	REQUIRE(result.str() ==
	        "(EXPR type:list (TOKEN type:EOF line:0 col:0 val:'') head:(EXPR type:nil (TOKEN type:EOF line:0 col:0 "
	        "val:'')) tail:(EXPR type:nil (TOKEN type:EOF line:0 col:0 val:'')))");
}

TEST_CASE("Expression's global printing operator works for List_Expression.", "[List_Expression]")
{
	const List_Expression expr{Token::NONE};
	stringstream result;
	result << expr;
	REQUIRE(result.str() ==
	        "(EXPR type:list (TOKEN type:EOF line:0 col:0 val:'') head:(EXPR type:nil (TOKEN type:EOF line:0 col:0 "
	        "val:'')) tail:(EXPR type:nil (TOKEN type:EOF line:0 col:0 val:'')))");
}
