// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

// Global_Stack_Frame is a container class.
// It heavily depends on other classes.
// Therefore I cound not write strict unit tests.
// Instead I implemented component tests.