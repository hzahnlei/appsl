// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

TEST_CASE("A directly instanciated runtime is lacking additional definitions.", "[Stack_Frame, factory]")
{
	const Global_Stack_Frame stack;
	REQUIRE(stack.lookup("odd?").is_nil());
}

TEST_CASE("Runtimes from the factory feature additional definitions.", "[Stack_Frame, factory]")
{
	const auto stack{make_runtime()};
	REQUIRE_FALSE(stack->lookup("odd?").is_nil());
}
