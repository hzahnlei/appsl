#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <junkbox/junkbox.hpp>

namespace appsl::test
{

	using junkbox::Base_Exception;

	class Pretty_Message_Matcher final : public Catch::Matchers::MatcherBase<Base_Exception>
	{

	    private:
		const string m_expected_pretty_message;

	    public:
		explicit Pretty_Message_Matcher(const string &expected_pretty_message)
				: m_expected_pretty_message(expected_pretty_message)
		{
		}

	    public:
		auto match(const Base_Exception &actual_exception) const -> bool override
		{
			return m_expected_pretty_message == actual_exception.pretty_message();
		}

		auto describe() const -> string override { return ""; }
	};

	inline auto pretty_message(const string &expected_pretty_message) -> Pretty_Message_Matcher
	{
		return Pretty_Message_Matcher{expected_pretty_message};
	}

} // namespace appsl::test
