#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/list_expression.hpp"
#include <memory>

namespace appsl::test
{

	template <typename E> auto as_list_expression(const E &head) -> unique_ptr<Expression>
	{
		return make_unique<List_Expression>(Token::NONE, head.clone());
	}

	template <typename E, typename... Es>
	auto as_list_expression(const E &head, const Es &...tail) -> unique_ptr<Expression>
	{
		return make_unique<List_Expression>(Token::NONE, head.clone(), as_list_expression(tail...));
	}

} // namespace appsl::test
