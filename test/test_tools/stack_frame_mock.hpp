#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/stack_frame.hpp"
#include "appsl/use_stl.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl::test
{

	class Stack_Frame_Mock final : public Stack_Frame
	{

	    public:
		//---- Public interface ---------------------------------------

		auto lookup(const string &) const -> const Expression & override { return *Nil_Expression::NIL; }

		auto define_argument(const string &, shared_ptr<const Expression>) -> void override { NOT_SUPPORTED(); }

		auto define_local_var(const string &, shared_ptr<const Expression>) -> void override
		{
			NOT_SUPPORTED();
		}

		auto new_child() -> shared_ptr<Stack_Frame> override { NOT_SUPPORTED(); }

		auto is_defined_locally(const string &) const -> bool override { NOT_SUPPORTED(); }

		auto is_defined(const string &) const -> bool override { NOT_SUPPORTED(); }

		auto is_defined_globally(const string &) const -> bool override { NOT_SUPPORTED(); }

		auto parent() const -> Stack_Frame & override { NOT_SUPPORTED(); }

		auto global() const -> Stack_Frame & override { NOT_SUPPORTED(); }

		auto boolean_literal_from(const bool) const -> shared_ptr<const Expression> override
		{
			NOT_SUPPORTED();
		}

		auto redefine(const string &, shared_ptr<const Expression>) -> void { NOT_SUPPORTED(); }

		auto argument_count() const -> size_t override { return 0U; }

		auto is_global() const noexcept -> bool override { NOT_SUPPORTED(); }

		auto for_each_local(const function<void(const string &, shared_ptr<const Expression>)> &) const
				-> void override
		{
			NOT_SUPPORTED();
		}

		auto dump(const string &, ostream &) const -> ostream & override;

		//---- Control and inspection ---------------------------------

		// If required
	};

	static Stack_Frame_Mock STACK_MOCK{};

	inline auto Stack_Frame_Mock::dump(const string &indentation, ostream &out) const -> ostream &
	{
		return out << indentation << "STACK FRAME MOCK\n";
	}

} // namespace appsl::test
