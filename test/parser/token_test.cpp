// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/token.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("Textual representation can be created for all token types.", "[Token]")
{
	REQUIRE(Token::debug_representation(Token::Type::COLON) == "COLON");
	REQUIRE(Token::debug_representation(Token::Type::END_OF_FILE) == "EOF");
	REQUIRE(Token::debug_representation(Token::Type::INTEGER_LITERAL) == "INTEGER_LITERAL");
	REQUIRE(Token::debug_representation(Token::Type::L_BRACKET) == "L_BRACKET");
	REQUIRE(Token::debug_representation(Token::Type::L_CURLY_BRACE) == "L_CURLY_BRACE");
	REQUIRE(Token::debug_representation(Token::Type::L_PARENTHESIS) == "L_PARENTHESIS");
	REQUIRE(Token::debug_representation(Token::Type::NAME) == "NAME");
	REQUIRE(Token::debug_representation(Token::Type::QUOTE) == "QUOTE");
	REQUIRE(Token::debug_representation(Token::Type::R_BRACKET) == "R_BRACKET");
	REQUIRE(Token::debug_representation(Token::Type::R_CURLY_BRACE) == "R_CURLY_BRACE");
	REQUIRE(Token::debug_representation(Token::Type::R_PARENTHESIS) == "R_PARENTHESIS");
	REQUIRE(Token::debug_representation(Token::Type::REAL_LITERAL) == "REAL_LITERAL");
	REQUIRE(Token::debug_representation(Token::Type::TEXT_LITERAL) == "TEXT_LITERAL");
}

TEST_CASE("Textual representation for error reporting can be created for all token types.", "[Token]")
{
	REQUIRE(Token::representation_for_error_reporting(Token::Type::COLON) == ":");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::END_OF_FILE) == "EOF");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::INTEGER_LITERAL) == "integer literal");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::L_BRACKET) == "[");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::L_CURLY_BRACE) == "{");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::L_PARENTHESIS) == "(");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::NAME) == "name");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::QUOTE) == "'");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::R_BRACKET) == "]");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::R_CURLY_BRACE) == "}");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::R_PARENTHESIS) == ")");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::REAL_LITERAL) == "real literal");
	REQUIRE(Token::representation_for_error_reporting(Token::Type::TEXT_LITERAL) == "text literal");
}

TEST_CASE("Debug representation can be created for tokens.", "[Token]")
{
	Token token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}};
	stringstream result;
	result << token;
	REQUIRE(result.str() == "(TOKEN type:INTEGER_LITERAL line:1 col:2 val:'123')");
}

TEST_CASE("Position can be passed as a pair to ctor.", "[Token]")
{
	Token token{Token::Type::INTEGER_LITERAL, "123", make_pair(1, 2)};

	REQUIRE(token.type() == Token::Type::INTEGER_LITERAL);
	REQUIRE(token.lexeme() == "123");
	REQUIRE(token.line() == 1U);
	REQUIRE(token.column() == 2U);
}

TEST_CASE("Position can be passed as a position object to ctor.", "[Token]")
{
	Token token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}};

	REQUIRE(token.type() == Token::Type::INTEGER_LITERAL);
	REQUIRE(token.lexeme() == "123");
	REQUIRE(token.line() == 1U);
	REQUIRE(token.column() == 2U);
}

TEST_CASE("Tokens are equal if type, lexeme and position are equal.", "[Token]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}};
	const Token similarToken{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}};

	REQUIRE(token == token);
	REQUIRE(token == &token);
	REQUIRE(token == similarToken);
}

TEST_CASE("Tokens are not equal if rhs is nullptr.", "[Token]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}};

	REQUIRE_FALSE(token == static_cast<Token *>(nullptr));
}

TEST_CASE("Tokens are not equal if lexeme differs.", "[Token]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}};
	const Token similarTokenWithDifferentLexeme{Token::Type::INTEGER_LITERAL, "abc", Token::Position{1, 2}};

	REQUIRE_FALSE(token == similarTokenWithDifferentLexeme);
}

TEST_CASE("Tokens are not equal if type differs.", "[Token]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}};
	const Token tokenWithDifferentType{Token::Type::TEXT_LITERAL, "123", Token::Position{1, 2}};

	REQUIRE_FALSE(token == tokenWithDifferentType);
}

TEST_CASE("Tokens are not equal if line differs.", "[Token]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}};
	const Token similarTokenWithDifferentLine{Token::Type::INTEGER_LITERAL, "123", Token::Position{10, 2}};

	REQUIRE_FALSE(token == similarTokenWithDifferentLine);
}

TEST_CASE("Tokens are not equal if column differs.", "[Token]")
{
	const Token token{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 2}};
	const Token similarTokenWithDifferentColumn{Token::Type::INTEGER_LITERAL, "123", Token::Position{1, 20}};

	REQUIRE_FALSE(token == similarTokenWithDifferentColumn);
}
