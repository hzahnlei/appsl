// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/lexical_error.hpp"
#include "appsl/parser/scanner.hpp"
#include <catch2/catch_test_macros.hpp>

using namespace appsl;

TEST_CASE("Single line comment prefixes recognized.", "[Scanner]") { REQUIRE(Scanner::is_single_line_comment(';')); }

static constexpr auto NEXT_CHAR_DOES_NOT_MATTER{'X'};

TEST_CASE("Whitespace or comment prefixes recognized.", "[Scanner]")
{
	REQUIRE(Scanner::is_whitespace_comment_or_hashbang(';', NEXT_CHAR_DOES_NOT_MATTER));
	REQUIRE(Scanner::is_whitespace_comment_or_hashbang(';', ';'));
	REQUIRE(Scanner::is_whitespace_comment_or_hashbang(' ', NEXT_CHAR_DOES_NOT_MATTER));
	REQUIRE(Scanner::is_whitespace_comment_or_hashbang(' ', ' '));
	REQUIRE(Scanner::is_whitespace_comment_or_hashbang('\t', NEXT_CHAR_DOES_NOT_MATTER));
	REQUIRE(Scanner::is_whitespace_comment_or_hashbang('\t', ' '));
	REQUIRE(Scanner::is_whitespace_comment_or_hashbang('\n', NEXT_CHAR_DOES_NOT_MATTER));
	REQUIRE(Scanner::is_whitespace_comment_or_hashbang('\n', ' '));
	REQUIRE(Scanner::is_whitespace_comment_or_hashbang('#', '!'));
}

TEST_CASE("None whitespace and none comment prefixes recognized.", "[Scanner]")
{
	REQUIRE_FALSE(Scanner::is_whitespace_comment_or_hashbang('*', '/'));
	REQUIRE_FALSE(Scanner::is_whitespace_comment_or_hashbang('/', ' '));
}

TEST_CASE("Recognize characters that are not allowed in names.", "[Scanner]")
{
	REQUIRE(Scanner::is_forbidden_in_name('.'));
	REQUIRE(Scanner::is_forbidden_in_name(':'));
	REQUIRE(Scanner::is_forbidden_in_name(','));
	REQUIRE(Scanner::is_forbidden_in_name(';'));
	REQUIRE(Scanner::is_forbidden_in_name('('));
	REQUIRE(Scanner::is_forbidden_in_name(')'));
	REQUIRE(Scanner::is_forbidden_in_name('{'));
	REQUIRE(Scanner::is_forbidden_in_name('}'));
	REQUIRE(Scanner::is_forbidden_in_name('['));
	REQUIRE(Scanner::is_forbidden_in_name(']'));
	REQUIRE(Scanner::is_forbidden_in_name('\''));
	REQUIRE(Scanner::is_forbidden_in_name('"'));
}

TEST_CASE("Recognize characters that are allowed in names.", "[Scanner]")
{
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('<'));
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('>'));
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('='));
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('+'));
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('-'));
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('?'));
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('!'));
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('_'));
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('&'));
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('|'));
	REQUIRE_FALSE(Scanner::is_forbidden_in_name('@'));
}

TEST_CASE("ASCII characters can be converted into corresponding hex numbers.", "[Scanner]")
{
	REQUIRE(Scanner::hex_representation(' ') == "0x20");
	REQUIRE(Scanner::hex_representation('a') == "0x61");
	REQUIRE(Scanner::hex_representation('A') == "0x41");
	REQUIRE(Scanner::hex_representation('z') == "0x7A");
}
