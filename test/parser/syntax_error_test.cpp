// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/syntax_error.hpp"
#include <catch2/catch_test_macros.hpp>
#include <sstream>

using namespace appsl;

TEST_CASE("Syntax_Error can be constructed.", "[Syntax_Error]")
{
	const Syntax_Error ex{"Some message.", Token::Position{10, 20}};
	REQUIRE(ex.line() == 10U);
	REQUIRE(ex.column() == 20U);
	REQUIRE(ex.message() == "Some message.");
	REQUIRE(string{ex.what()} == "Some message.");
	REQUIRE(ex.pretty_message() == "Syntax error at line 10, column 20: Some message.");
}

TEST_CASE("Syntax_Error can be printed to stream by reference.", "[Syntax_Error]")
{
	const Syntax_Error ex{"Some message.", Token::Position{10, 20}};
	stringstream actual;
	actual << ex;
	REQUIRE(actual.str() == "Syntax error at line 10, column 20: Some message.");
}

TEST_CASE("Syntax_Error can be printed to stream by pointer.", "[Syntax_Error]")
{
	const Syntax_Error exception{"Some message.", Token::Position{10, 20}};
	stringstream actual;
	actual << &exception;
	REQUIRE(actual.str() == "Syntax error at line 10, column 20: Some message.");
}

TEST_CASE("Syntax_Error out operator does not fail on nullptr.", "[Syntax_Error]")
{
	stringstream actual;
	actual << static_cast<Syntax_Error *>(nullptr);
	REQUIRE(actual.str() == "nullptr");
}
