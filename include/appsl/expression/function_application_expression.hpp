#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/abstract_collection_expression.hpp"
#include "appsl/expression/nil_expression.hpp"

namespace appsl
{

	class Function_Application_Expression final : public Abstract_Collection_Expression
	{
	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

	    public:
		Function_Application_Expression(const Token &token, shared_ptr<const Expression> function_name,
		                                shared_ptr<const Expression> arguments = Nil_Expression::NIL);

		auto operator==(const Expression &other) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_function_application() const -> bool override;
		auto is_list() const -> bool override;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;

		inline auto function() const -> const Expression &;
		inline auto arguments() const -> const Expression &;

	    private:
		auto evaluate_with_function_name_checked(Stack_Frame &) const -> shared_ptr<const Expression>;
		auto evaluate_with_function_evaluation(Stack_Frame &) const -> shared_ptr<const Expression>;
		auto evaluate_with_function_determined(const Expression &, Stack_Frame &) const
				-> shared_ptr<const Expression>;

		auto type_of_collection() const -> string override;
		auto opening_delimiter() const -> string override;
		auto closing_delimiter() const -> string override;
		auto local_env_from_arguments(Stack_Frame &, const Expression &evaluated_fun,
		                              const Expression &parameters) const -> shared_ptr<Stack_Frame>;
		auto make_sure_parameter_and_argument_count_match(const Expression &evaluated_fun,
		                                                  const Expression &parameters,
		                                                  const Expression &arguments) const -> void;
		auto populate_env_from_arguments(Stack_Frame &, const Expression &evaluated_fun,
		                                 const Expression &parameters, const Expression &arguments) const
				-> void;
		auto extract_value_form_name_value_pair_if_applicable(const Expression &evaluated_fun,
		                                                      const Expression &parameter,
		                                                      const Expression &arguments) const
				-> const Expression &;
		auto actual_argument_value_form_named_argument(const Expression &evaluated_fun,
		                                               const Expression &parameter,
		                                               const Expression &arguments) const -> const Expression &;
		auto evaluated_argument_list(Stack_Frame &, const Expression &) const -> shared_ptr<const Expression>;
	};

	inline auto Function_Application_Expression::tag() const -> const Expression & { return *TYPE_TAG; }

	inline auto Function_Application_Expression::tagged_expression() const -> const Expression & { return *this; }

	auto Function_Application_Expression::function() const -> const Expression & { return head(); }

	auto Function_Application_Expression::arguments() const -> const Expression & { return tail(); }

	[[nodiscard]] auto is_effectively_empty_arguments_list(const Expression &) -> bool;

} // namespace appsl
