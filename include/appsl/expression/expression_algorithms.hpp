#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"

namespace appsl::expr
{

	using Action = function<void(const Expression &)>;

	auto for_each(const Expression &, const Action) -> void;

	using Predicate = function<bool(const Expression &)>;

	auto all_of(const Expression &, const Predicate) -> bool;

	auto any_of(const Expression &, const Predicate) -> bool;

	using BiAction = function<void(const Expression &, const Expression &)>;

	auto zip(const Expression &, const Expression &, const BiAction)
			-> pair<const Expression &, const Expression &>;

	using EnumAction = function<void(const Expression &, const size_t)>;

	auto enumerate(const Expression &, const EnumAction) -> void;

	auto nth(const Expression &, size_t) -> const Expression &;

} // namespace appsl::expr
