#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/parser/token.hpp"

namespace appsl
{

	class Nil_Expression final : public Expression
	{

	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

		static const shared_ptr<const Nil_Expression> NIL;

	    private:
		const Token m_token;

	    public:
		explicit Nil_Expression(const Token &);
		explicit Nil_Expression(Token &&);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto token() const -> const Token & override;

		[[nodiscard]] auto is_iterable() const -> bool override;
		[[nodiscard]] auto head() const -> const Expression & override;
		[[nodiscard]] auto tail() const -> const Expression & override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_nil() const -> bool override;
		auto is_name() const -> bool override;
		auto name() const -> const string & override;
		auto is_built_in_function() const -> bool override;
		auto is_function() const -> bool override;
		auto is_function_application() const -> bool override;
		auto is_fun_macro() const -> bool override;
		auto is_integer() const -> bool override;
		auto int_value() const -> Integer_Representation override;
		auto is_real() const -> bool override;
		auto real_value() const -> Real_Representation override;
		auto parameter_list() const -> const Expression & override;
		auto is_variadic_parameter() const -> bool override;
		auto is_quoted() const -> bool override;
		auto is_def_macro() const -> bool override;
		auto is_name_value_pair() const -> bool override;
		auto is_text() const -> bool override;
		auto text_value() const -> const string & override;
		auto is_list() const -> bool override;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;

		auto operator<<(ostream &) const -> ostream & override;

		auto stringify(ostream &, const string &indentation) const -> void override;
	};

	inline auto Nil_Expression::is_iterable() const -> bool { return false; }
	inline auto Nil_Expression::head() const -> const Expression & { return *this; }
	inline auto Nil_Expression::tail() const -> const Expression & { return *this; }

	inline auto Nil_Expression::tag() const -> const Expression & { return *TYPE_TAG; }

	inline auto Nil_Expression::tagged_expression() const -> const Expression & { return *this; }

} // namespace appsl
