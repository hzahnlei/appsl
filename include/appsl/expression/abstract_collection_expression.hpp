#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/expression/nil_expression.hpp"

namespace appsl
{

	class Abstract_Collection_Expression : public Expression
	{

	    protected:
		const Token m_token;
		const shared_ptr<const Expression> m_head;
		const shared_ptr<const Expression> m_tail;

	    public:
		Abstract_Collection_Expression(const Token &, shared_ptr<const Expression> head,
		                               shared_ptr<const Expression> tail);
		~Abstract_Collection_Expression() override = default;

		auto token() const -> const Token & final;

		[[nodiscard]] auto is_iterable() const -> bool final;
		[[nodiscard]] auto head() const -> const Expression & final;
		[[nodiscard]] auto tail() const -> const Expression & final;

		auto is_nil() const -> bool final;
		auto is_name() const -> bool final;
		auto name() const -> const string & final;
		auto is_built_in_function() const -> bool final;
		auto is_function() const -> bool final;
		auto is_function_application() const -> bool override;
		auto is_fun_macro() const -> bool final;
		auto is_integer() const -> bool final;
		auto int_value() const -> Integer_Representation final;
		auto is_real() const -> bool final;
		auto real_value() const -> Real_Representation final;
		auto parameter_list() const -> const Expression & final;
		auto is_variadic_parameter() const -> bool final;
		auto is_quoted() const -> bool final;
		auto is_def_macro() const -> bool final;
		auto is_name_value_pair() const -> bool final;
		auto is_text() const -> bool final;
		auto text_value() const -> const string & final;

		auto operator<<(ostream &) const -> ostream & final;

		auto stringify(ostream &, const string &indentation) const -> void final;

	    private:
		virtual auto type_of_collection() const -> string = 0;
		virtual auto opening_delimiter() const -> string = 0;
		virtual auto closing_delimiter() const -> string = 0;
		auto stringify_all_elements(ostream &, const string &indentation) const -> void;
	};

	inline auto Abstract_Collection_Expression::is_iterable() const -> bool { return !m_head->is_nil(); }
	inline auto Abstract_Collection_Expression::head() const -> const Expression & { return *m_head; }
	inline auto Abstract_Collection_Expression::tail() const -> const Expression & { return *m_tail; }

} // namespace appsl
