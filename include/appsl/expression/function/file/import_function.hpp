#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/abstract_builtin_function.hpp"

namespace appsl
{

	class Import_Function final : public Abstract_Builtin_Function
	{

	    public:
		explicit Import_Function(const Fun_Display_Name &);

		auto operator==(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto parameter_list() const -> const Expression & override;

	    private:
		auto ensure_valid_file_name(const Expression &) const -> void;
		auto content_of_file(const string &) const -> string;
		auto evaluate(Stack_Frame &, const string &) const -> void;
	};

} // namespace appsl
