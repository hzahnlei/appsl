#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{
	using junkbox::Assertion_Violation;
	using junkbox::Base_Exception;

	class Program_Error final : public Base_Exception
	{

	    private:
		const shared_ptr<const Expression> m_cause;

	    public:
		explicit Program_Error(const shared_ptr<const Expression> &) noexcept;

		auto pretty_message() const noexcept -> Message override;

		auto cause() const -> const shared_ptr<const Expression> &;
	};

} // namespace appsl
