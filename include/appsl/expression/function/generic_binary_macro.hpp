#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/abstract_builtin_function.hpp"
#include <functional>
#include <memory>

namespace appsl
{

	/*!
	 * A base class for functions that take two expressions as arguments and then return another expression as the
	 * result of the application of a given function.
	 *
	 * The generic aspect is, that the function passed to the constructor can be any C++ function Expression ×
	 * Expression -> Expression. This class is then wrapping the C++ function so that it is accessible from the Lisp
	 * language.
	 *
	 * This is not a macro, meaning that the argument gets evaluated before applying this function.
	 */
	class Generic_Binary_Macro final : public Abstract_Builtin_Function
	{

	    public:
		using Binary_Cpp_Fun = function<shared_ptr<const Expression>(Stack_Frame &, const Expression &,
		                                                             const Expression &)>;

	    private:
		const Binary_Cpp_Fun m_binary_fun;

	    public:
		Generic_Binary_Macro(const Fun_Display_Name &, Binary_Cpp_Fun);

		auto operator==(const Expression &other) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression> override;

		auto parameter_list() const -> const Expression & override;
	};

} // namespace appsl
