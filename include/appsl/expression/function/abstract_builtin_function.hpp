#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"

namespace appsl
{

	class Abstract_Builtin_Function : public Expression
	{

	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

		using Fun_Display_Name = string;

	    private:
		const Fun_Display_Name m_display_name;

	    public:
		explicit Abstract_Builtin_Function(const Fun_Display_Name &);

		auto operator<=(const Expression &) const -> bool final;
		auto operator<(const Expression &) const -> bool final;
		auto token() const -> const Token & final;

		auto head() const -> const Expression & final;
		auto tail() const -> const Expression & final;

		auto is_nil() const -> bool final;
		auto is_name() const -> bool final;
		auto name() const -> const string & override;
		auto is_built_in_function() const -> bool final;
		auto is_function() const -> bool final;
		auto is_function_application() const -> bool final;
		auto is_fun_macro() const -> bool override;
		auto is_integer() const -> bool final;
		auto int_value() const -> Integer_Representation override;
		auto is_real() const -> bool override;
		auto real_value() const -> Real_Representation override;
		auto is_variadic_parameter() const -> bool final;
		auto is_quoted() const -> bool final;
		auto is_def_macro() const -> bool override;
		auto is_name_value_pair() const -> bool final;
		auto is_text() const -> bool final;
		auto text_value() const -> const string & final;
		auto is_list() const -> bool final;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;

		auto operator<<(ostream &) const -> ostream & final;
		auto stringify(ostream &out, const string &indentation) const -> void final;

		auto function_display_name() const -> const Fun_Display_Name &;

	    private:
		auto make_sure_arguments_given(const Stack_Frame &) const -> void;
	};

	inline auto Abstract_Builtin_Function::tag() const -> const Expression & { return *TYPE_TAG; }

	inline auto Abstract_Builtin_Function::tagged_expression() const -> const Expression & { return *this; }

} // namespace appsl
