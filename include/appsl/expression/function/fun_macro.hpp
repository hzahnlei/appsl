#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/abstract_builtin_function.hpp"

namespace appsl
{

	class Fun_Macro final : public Abstract_Builtin_Function
	{

	    public:
		explicit Fun_Macro(const Fun_Display_Name &);

		auto operator==(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_fun_macro() const -> bool override;

		auto parameter_list() const -> const Expression & override;

	    private:
		auto ensure_correct_param_list(const Expression &) const -> set<string>;
		using Is_In_Scope = function<bool(const string &)>;
		auto ensure_names_are_defined_in_scope(const Expression &, const Is_In_Scope &) const -> void;
		auto ensure_name_is_defined_in_scope(const Expression &, const Is_In_Scope &) const -> void;
		auto ensure_body_uses_only_names_from_scope(const Expression &, const Is_In_Scope &) const -> void;
		auto names_from_let_definitions(const Expression &) const -> set<string>;
	};

} // namespace appsl
