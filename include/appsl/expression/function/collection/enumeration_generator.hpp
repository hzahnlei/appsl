#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/parser/token.hpp"

namespace appsl
{

	class Enumeration_Generator final : public Expression
	{
	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

	    private:
		const shared_ptr<const Expression> m_input_collection;
		mutable Expression::Integer_Representation m_counter;
		mutable shared_ptr<const Expression> m_current_input;
		mutable shared_ptr<const Expression> m_current_mapped;

	    public:
		explicit Enumeration_Generator(const shared_ptr<const Expression> &collection);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto token() const -> const Token & override;

		[[nodiscard]] auto is_iterable() const -> bool override;
		[[nodiscard]] auto head() const -> const Expression & override;
		[[nodiscard]] auto tail() const -> const Expression & override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_nil() const -> bool override;
		auto is_name() const -> bool override;
		auto name() const -> const string & override;
		auto is_built_in_function() const -> bool override;
		auto is_function() const -> bool override;
		auto is_function_application() const -> bool override;
		auto is_fun_macro() const -> bool override;
		auto is_integer() const -> bool override;
		auto int_value() const -> Integer_Representation override;
		auto is_real() const -> bool override;
		auto real_value() const -> Real_Representation override;
		auto parameter_list() const -> const Expression & override;
		auto is_variadic_parameter() const -> bool override;
		auto is_quoted() const -> bool override;
		auto is_def_macro() const -> bool override;
		auto is_name_value_pair() const -> bool override;
		auto is_text() const -> bool override;
		auto text_value() const -> const string & override;
		auto is_list() const -> bool override;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;

		auto operator<<(ostream &) const -> ostream & override;

		auto stringify(ostream &, const string &indentation) const -> void override;

	    private:
		auto compute_next_if_input_available() const -> void;
		auto compute_next() const -> shared_ptr<const Expression>;
	};

	inline auto Enumeration_Generator::tag() const -> const Expression & { return *TYPE_TAG; }

	inline auto Enumeration_Generator::tagged_expression() const -> const Expression & { return *this; }

} // namespace appsl
