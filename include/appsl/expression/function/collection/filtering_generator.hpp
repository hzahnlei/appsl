#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/parser/token.hpp"

namespace appsl
{

	class Filtering_Generator final : public Expression
	{
	    public:
		static const shared_ptr<const Expression> TYPE_TAG;
		enum class Mode
		{
			keep,
			discard
		};

	    private:
		const shared_ptr<const Expression> m_input_collection;
		const shared_ptr<const Expression> m_pred_fun;
		const Mode m_mode;
		shared_ptr<Stack_Frame> m_stack;
		mutable shared_ptr<const Expression> m_current_input;
		mutable shared_ptr<const Expression> m_current_filtered;
		const string &m_map_functions_param_name;

	    public:
		Filtering_Generator(const shared_ptr<const Expression> &collection,
		                    const shared_ptr<const Expression> &pred_fun, const Mode, shared_ptr<Stack_Frame>);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto token() const -> const Token & override;

		[[nodiscard]] auto is_iterable() const -> bool override;
		[[nodiscard]] auto head() const -> const Expression & override;
		[[nodiscard]] auto tail() const -> const Expression & override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_nil() const -> bool override;
		auto is_name() const -> bool override;
		auto name() const -> const string & override;
		auto is_built_in_function() const -> bool override;
		auto is_function() const -> bool override;
		auto is_function_application() const -> bool override;
		auto is_fun_macro() const -> bool override;
		auto is_integer() const -> bool override;
		auto int_value() const -> Integer_Representation override;
		auto is_real() const -> bool override;
		auto real_value() const -> Real_Representation override;
		auto parameter_list() const -> const Expression & override;
		auto is_variadic_parameter() const -> bool override;
		auto is_quoted() const -> bool override;
		auto is_def_macro() const -> bool override;
		auto is_name_value_pair() const -> bool override;
		auto is_text() const -> bool override;
		auto text_value() const -> const string & override;
		auto is_list() const -> bool override;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;

		auto operator<<(ostream &) const -> ostream & override;

		auto stringify(ostream &, const string &indentation) const -> void override;

	    private:
		auto apply_predicate_if_input_available() const -> void;
		auto apply_keep_logic() const -> void;
		auto apply_discard_logic() const -> void;
		auto apply_predicate() const -> shared_ptr<const Expression>;
	};

	inline auto Filtering_Generator::tag() const -> const Expression & { return *TYPE_TAG; }

	inline auto Filtering_Generator::tagged_expression() const -> const Expression & { return *this; }

} // namespace appsl
