#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/function/collection/filtering_generator.hpp"

namespace appsl
{

	class Filter_Function final : public Abstract_Builtin_Function
	{
	    private:
		Filtering_Generator::Mode m_mode;

	    public:
		explicit Filter_Function(const Fun_Display_Name &);
		Filter_Function(const Fun_Display_Name &, const Filtering_Generator::Mode);

		auto operator==(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto parameter_list() const -> const Expression & override;
	};

} // namespace appsl
