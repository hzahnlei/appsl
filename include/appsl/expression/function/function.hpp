#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/abstract_builtin_function.hpp"

namespace appsl
{

	class Function final : public Expression
	{

	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

		using Captured_Scope = map<string, shared_ptr<const Expression>>;

	    private:
		const shared_ptr<const Expression> m_parameters;
		const shared_ptr<const Expression> m_body;
		const Captured_Scope m_captured_scope;

	    public:
		Function(shared_ptr<const Expression> parameters, shared_ptr<const Expression> body);
		Function(shared_ptr<const Expression> parameters, shared_ptr<const Expression> body,
		         Captured_Scope &&capture);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool final;
		auto operator<(const Expression &) const -> bool final;

		auto clone() const -> unique_ptr<Expression> override;

		auto token() const -> const Token & final;

		auto head() const -> const Expression & final;
		auto tail() const -> const Expression & final;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_nil() const -> bool override;
		auto is_name() const -> bool override;
		auto name() const -> const string & override;
		auto is_built_in_function() const -> bool override;
		auto is_function() const -> bool override;
		auto is_function_application() const -> bool override;
		auto is_fun_macro() const -> bool override;
		auto is_integer() const -> bool override;
		auto int_value() const -> Integer_Representation override;
		auto is_real() const -> bool override;
		auto real_value() const -> Real_Representation override;
		auto parameter_list() const -> const Expression & override;
		auto is_variadic_parameter() const -> bool override;
		auto is_quoted() const -> bool override;
		auto is_def_macro() const -> bool override;
		auto is_name_value_pair() const -> bool override;
		auto is_text() const -> bool override;
		auto text_value() const -> const string & override;
		auto is_list() const -> bool override;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;

		auto operator<<(ostream &out) const -> ostream & override;
		auto stringify(ostream &, const string &indentation) const -> void override;

	    private:
		auto evaluate_with_runtime_only(Stack_Frame &stack) const -> shared_ptr<const Expression>;
		auto evaluate_with_runtime_and_captured_scope(Stack_Frame &stack) const -> shared_ptr<const Expression>;
	};

	inline auto Function::tag() const -> const Expression & { return *TYPE_TAG; }

	inline auto Function::tagged_expression() const -> const Expression & { return *this; }

} // namespace appsl
