#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/abstract_builtin_function.hpp"

namespace appsl
{

	class Def_Macro final : public Abstract_Builtin_Function
	{

	    public:
		explicit Def_Macro(const Fun_Display_Name &);

		auto operator==(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto parameter_list() const -> const Expression & override;

		auto is_def_macro() const -> bool override;

	    private:
		auto make_sure_name_is_undefined(const Expression &, const Stack_Frame &) const -> void;
		auto make_sure_value_is_not_free(const Expression &name, const Expression &value,
		                                 const Stack_Frame &) const -> void;
	};

} // namespace appsl
