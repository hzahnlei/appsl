#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/abstract_builtin_function.hpp"
#include <functional>
#include <memory>
#include <set>
#include <string>

namespace appsl
{

	class Case_Macro final : public Abstract_Builtin_Function
	{

	    public:
		explicit Case_Macro(const Fun_Display_Name &);

		auto operator==(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto parameter_list() const -> const Expression & override;

	    private:
		static auto assert_correctness(const Expression &) -> size_t;
	};

} // namespace appsl
