#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/token.hpp"
#include "appsl/use_stl.hpp"

namespace appsl
{

	// Forward declarations
	class Stack_Frame;
	class Expression;

	struct Clonable_Expression
	{
		virtual ~Clonable_Expression() = default;
		[[nodiscard]] virtual auto clone() const -> unique_ptr<Expression> = 0;
	};

	struct Iterable_Expression
	{
		virtual ~Iterable_Expression() = default;
		[[nodiscard]] virtual auto is_iterable() const -> bool = 0;
		[[nodiscard]] virtual auto head() const -> const Expression & = 0;
		[[nodiscard]] virtual auto tail() const -> const Expression & = 0;
	};

	struct Composite_Expression
	{
		virtual ~Composite_Expression() = default;
		[[nodiscard]] virtual auto holds_properties() const -> bool = 0;
		[[nodiscard]] virtual auto has_property(const string &) const -> bool = 0;
		[[nodiscard]] virtual auto by_name(const string &) const -> const Expression & = 0;
	};

	class Expression : public enable_shared_from_this<Expression>,
			   public Iterable_Expression,
			   public Clonable_Expression,
			   public Composite_Expression
	{

	    public:
		using Integer_Representation = int64_t;
		using Real_Representation = double;

		Expression() = default;
		explicit Expression(Expression &) = delete;
		explicit Expression(const Expression &) = delete;
		explicit Expression(Expression &&) = default;
		virtual ~Expression() = default;

		auto operator=(const Expression &) -> Expression & = delete;
		auto operator=(Expression &&) -> Expression & = default;

		virtual auto operator==(const Expression &) const -> bool = 0;
		auto operator==(Expression const *) const -> bool;
		virtual auto operator<=(const Expression &) const -> bool = 0;
		auto operator<=(Expression const *) const -> bool;
		virtual auto operator<(const Expression &) const -> bool = 0;
		auto operator<(Expression const *) const -> bool;

		[[nodiscard]] virtual auto token() const -> const Token & = 0;

		virtual auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> = 0;

		//---- Iterable expression ------------------------------------
		[[nodiscard]] auto is_iterable() const -> bool override;
		[[nodiscard]] auto head() const -> const Expression & override;
		[[nodiscard]] auto tail() const -> const Expression & override;

		[[nodiscard]] virtual auto is_nil() const -> bool = 0;
		[[nodiscard]] virtual auto is_name() const -> bool = 0;
		[[nodiscard]] virtual auto name() const -> const string & = 0;
		[[nodiscard]] virtual auto is_built_in_function() const -> bool = 0; // Candidate for removal
		[[nodiscard]] virtual auto is_function() const -> bool = 0;
		[[nodiscard]] virtual auto is_function_application() const -> bool = 0;
		[[nodiscard]] virtual auto is_fun_macro() const -> bool = 0;
		[[nodiscard]] virtual auto is_integer() const -> bool = 0;
		[[nodiscard]] virtual auto int_value() const -> Integer_Representation = 0;
		[[nodiscard]] virtual auto is_real() const -> bool = 0;
		[[nodiscard]] virtual auto real_value() const -> Real_Representation = 0;
		[[nodiscard]] virtual auto parameter_list() const -> const Expression & = 0;
		[[nodiscard]] virtual auto is_variadic_parameter() const -> bool = 0;
		[[nodiscard]] virtual auto is_quoted() const -> bool = 0;
		[[nodiscard]] virtual auto is_def_macro() const -> bool = 0; // Candidate for removal
		[[nodiscard]] virtual auto is_name_value_pair() const -> bool = 0;
		[[nodiscard]] virtual auto is_text() const -> bool = 0;
		[[nodiscard]] virtual auto text_value() const -> const string & = 0;
		[[nodiscard]] virtual auto is_list() const -> bool = 0; // Candidate for removal
		[[nodiscard]] virtual auto is_map() const -> bool;
		[[nodiscard]] virtual auto is_tagged() const -> bool;
		[[nodiscard]] virtual auto tag() const -> const Expression & = 0;
		[[nodiscard]] virtual auto tagged_expression() const -> const Expression & = 0;
		[[nodiscard]] virtual auto is_key_value_pair() const -> bool;
		[[nodiscard]] virtual auto is_set() const -> bool;
		[[nodiscard]] virtual auto is_field_accessor() const -> bool;

		//---- Composite expression (record) --------------------------
		[[nodiscard]] virtual auto is_record() const -> bool; // Replace by holds_properties()
		[[nodiscard]] virtual auto holds_properties() const -> bool override;
		[[nodiscard]] virtual auto has_property(const string &) const -> bool override;
		[[nodiscard]] virtual auto by_name(const string &) const -> const Expression & override;

		[[nodiscard]] virtual auto by_key(const Expression &) const -> const Expression &;

		// Application specific datatypes, need to derive from Custom_Object_Expression
		[[nodiscard]] virtual auto is_custom_object() const -> bool;
		[[nodiscard]] virtual auto custom_object_type() const -> const Expression &;

		// TODO sould be noexcept
		virtual auto operator<<(ostream &) const -> ostream & = 0;

		virtual auto stringify(ostream &, const string &indentation) const -> void = 0;
		[[nodiscard]] auto stringify() const -> string;
	};

	auto operator<<(ostream &, const Expression &) -> ostream &;
	auto operator<<(ostream &, const Expression *) -> ostream &;

	inline auto Expression::operator==(Expression const *const other) const -> bool
	{
		return nullptr != other && *this == *other;
	}

	inline auto Expression::operator<=(Expression const *const other) const -> bool
	{
		return nullptr != other && *this <= *other;
	}

	inline auto Expression::operator<(Expression const *const other) const -> bool
	{
		return nullptr != other && *this < *other;
	}

	//---- Iterable expression ------------------------------------

	/**
	 * @brief Most expressions are iterable. Hence, iterable=true is a reasonable assumption in most cases.
	 */
	inline auto Expression::is_iterable() const -> bool { return true; }

	/**
	 * @brief Iterable expressions by default iterate themselfes.
	 */
	inline auto Expression::head() const -> const Expression & { return *this; }

	inline auto Expression::is_map() const -> bool { return false; }

	inline auto Expression::is_tagged() const -> bool { return false; }

	inline auto Expression::is_custom_object() const -> bool { return false; }

	inline auto Expression::is_key_value_pair() const -> bool { return false; }

	//---- Composite expression (record) ----------------------------------
	inline auto Expression::is_record() const -> bool { return false; }

	inline auto Expression::is_set() const -> bool { return false; }

	inline auto Expression::is_field_accessor() const -> bool { return false; }

} // namespace appsl
