#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"

namespace appsl
{

	class Tagged_Expression final : public Expression
	{

	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

	    private:
		const Token m_token;
		const shared_ptr<const Expression> m_expression;
		const shared_ptr<const Expression> m_tag;

	    public:
		Tagged_Expression(const Token &, shared_ptr<const Expression> expr, shared_ptr<const Expression> tag);
		Tagged_Expression(Token &&, shared_ptr<const Expression> expr, shared_ptr<const Expression> tag);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto token() const -> const Token & override;

		auto head() const -> const Expression & override;
		auto tail() const -> const Expression & override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_nil() const -> bool override;
		auto is_name() const -> bool override;
		auto name() const -> const string & override;
		auto is_built_in_function() const -> bool override;
		auto is_function() const -> bool override;
		auto is_function_application() const -> bool override;
		auto is_fun_macro() const -> bool override;
		auto is_integer() const -> bool override;
		auto int_value() const -> Integer_Representation override;
		auto is_real() const -> bool override;
		auto real_value() const -> Real_Representation override;
		auto parameter_list() const -> const Expression & override;
		auto is_variadic_parameter() const -> bool override;
		auto is_quoted() const -> bool override;
		auto is_def_macro() const -> bool override;
		auto is_name_value_pair() const -> bool override;
		auto is_text() const -> bool override;
		auto text_value() const -> const string & override;
		auto is_list() const -> bool override;
		auto is_tagged() const -> bool override;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;

		auto operator<<(ostream &) const -> ostream & override;

		auto stringify(ostream &, const string &indentation) const -> void override;
	};

	auto operator<<(ostream &, const Expression &) -> ostream &;
	auto operator<<(ostream &, const Expression *) -> ostream &;

	inline auto Tagged_Expression::is_tagged() const -> bool { return true; }

	inline auto Tagged_Expression::tag() const -> const Expression & { return *m_tag; }

	inline auto Tagged_Expression::tagged_expression() const -> const Expression & { return *m_expression; }

} // namespace appsl
