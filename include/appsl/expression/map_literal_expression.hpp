#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/parser/token.hpp"

namespace appsl
{

	class Map_Literal_Expression final : public Expression
	{
	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

		struct Less_Than
		{
			constexpr bool operator()(const std::shared_ptr<const appsl::Expression> &lhs,
			                          const std::shared_ptr<const appsl::Expression> &rhs) const
			{
				return *lhs < *rhs;
			}
		};

		using Key_Value_Map = map<shared_ptr<const Expression>, shared_ptr<const Expression>, Less_Than>;

		static auto list_of_keys(const Token &, const Key_Value_Map &) -> shared_ptr<const Expression>;

	    private:
		const Token m_token;
		const Key_Value_Map m_ordered_kv_pairs;
		mutable shared_ptr<const Expression> m_list_of_keys;

	    public:
		Map_Literal_Expression(const Token &, Key_Value_Map &&);
		Map_Literal_Expression(Token &&, Key_Value_Map &&);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto token() const -> const Token & override;

		auto head() const -> const Expression & override;
		auto tail() const -> const Expression & override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_nil() const -> bool override;
		auto is_name() const -> bool override;
		auto name() const -> const string & override;
		auto is_built_in_function() const -> bool override;
		auto is_function() const -> bool override;
		auto is_function_application() const -> bool override;
		auto is_fun_macro() const -> bool override;
		auto is_integer() const -> bool override;
		auto int_value() const -> Integer_Representation override;
		auto is_real() const -> bool override;
		auto real_value() const -> Real_Representation override;
		auto parameter_list() const -> const Expression & override;
		auto is_variadic_parameter() const -> bool override;
		auto is_quoted() const -> bool override;
		auto is_def_macro() const -> bool override;
		auto is_name_value_pair() const -> bool override;
		auto is_text() const -> bool override;
		auto text_value() const -> const string & override;
		auto is_list() const -> bool override;
		auto is_map() const -> bool override;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;
		auto by_key(const Expression &) const -> const Expression & override;

		auto operator<<(ostream &) const -> ostream & override;

		auto stringify(ostream &, const string &indentation) const -> void override;

	    private:
		auto apply_mapping_if_input_available() const -> void;
		auto apply_mapping() const -> shared_ptr<const Expression>;
	};

	inline auto Map_Literal_Expression::is_map() const -> bool { return true; }

	inline auto Map_Literal_Expression::tag() const -> const Expression & { return *TYPE_TAG; }

	inline auto Map_Literal_Expression::tagged_expression() const -> const Expression & { return *this; }

} // namespace appsl
