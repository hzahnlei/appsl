#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/parser/token.hpp"

namespace appsl
{

	class Record_Expression final : public Expression
	{
	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

		using Name_Value_List = vector<shared_ptr<const Expression>>;
		using Const_NV_Iter = Name_Value_List::const_iterator;

		static auto find_nv_pair(const Name_Value_List &collection, const string &name) -> Const_NV_Iter;
		static auto list_of_names(const Token &, const Name_Value_List &) -> shared_ptr<const Expression>;

	    private:
		const Token m_token;
		const Name_Value_List m_collection;
		mutable shared_ptr<const Expression> m_list_of_names;

	    public:
		Record_Expression(const Token &, Name_Value_List &&);
		Record_Expression(Token &&, Name_Value_List &&);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto token() const -> const Token & override;

		auto head() const -> const Expression & override;
		auto tail() const -> const Expression & override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_nil() const -> bool override;
		auto is_name() const -> bool override;
		auto name() const -> const string & override;
		auto is_built_in_function() const -> bool override;
		auto is_function() const -> bool override;
		auto is_function_application() const -> bool override;
		auto is_fun_macro() const -> bool override;
		auto is_integer() const -> bool override;
		auto int_value() const -> Integer_Representation override;
		auto is_real() const -> bool override;
		auto real_value() const -> Real_Representation override;
		auto parameter_list() const -> const Expression & override;
		auto is_variadic_parameter() const -> bool override;
		auto is_quoted() const -> bool override;
		auto is_def_macro() const -> bool override;
		auto is_name_value_pair() const -> bool override;
		auto is_text() const -> bool override;
		auto text_value() const -> const string & override;
		auto is_list() const -> bool override;
		auto is_map() const -> bool override;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;

		//---- Composite expression (record) --------------------------
		[[nodiscard]] auto is_record() const -> bool override;
		[[nodiscard]] auto holds_properties() const -> bool override;
		[[nodiscard]] auto has_property(const string &) const -> bool override;
		[[nodiscard]] auto by_name(const string &) const -> const Expression & override;

		auto operator<<(ostream &) const -> ostream & override;

		auto stringify(ostream &, const string &indentation) const -> void override;

	    private:
		auto apply_mapping_if_input_available() const -> void;
		auto apply_mapping() const -> shared_ptr<const Expression>;
	};

	inline auto Record_Expression::is_map() const -> bool { return false; }

	inline auto Record_Expression::tag() const -> const Expression & { return *TYPE_TAG; }

	inline auto Record_Expression::tagged_expression() const -> const Expression & { return *this; }

	//---- Composite expression (record) ----------------------------------

	inline auto Record_Expression::is_record() const -> bool { return true; }

	inline auto Record_Expression::holds_properties() const -> bool { return true; }

} // namespace appsl
