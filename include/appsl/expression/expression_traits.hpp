#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/use_stl.hpp"

namespace appsl::expr
{

	auto is_list_of_integers(const Expression &) -> bool;

	auto function_from_function_application(const Expression &function_application) -> const Expression &;

	auto is_free(const Expression &, const Stack_Frame &) -> bool;

	auto count(const Expression &) -> size_t;

	auto unquoted(const Expression &) -> const Expression &;

	auto gracefully_unquoted(const Expression &) -> const Expression &;

	auto unvariadic(const Expression &) -> const Expression &;

	auto gracefully_unvariadic(const Expression &) -> const Expression &;

	auto is_literal(const Expression &) -> bool;

	auto is_literal_or_collection_thereof(const Expression &) -> bool;

	using Set_of_Names = set<string, less<>>;
	auto names_from(const Expression &) -> Set_of_Names;

	auto is_number(const Expression &) -> bool;

	auto pure_param_name_without_quotes_etc(const Expression &) -> const Expression &;

	auto pure_argument_value_without_name(const Expression &) -> const Expression &;

	auto is_valid_type_name(const Expression &) -> bool;

	/**
	 * @brief An alias for count to improve readability when working with records.
	 */
	[[nodiscard]] inline auto record_field_count(const Expression &record) -> size_t { return count(record); }

	auto gracefully_untagged(const Expression &) -> const Expression &;

} // namespace appsl::expr
