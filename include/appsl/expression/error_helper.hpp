#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/use_stl.hpp"

namespace appsl
{

	[[nodiscard]] auto is_error_object(const Expression &) -> bool;

	/**
	 * @brief Same as (Error <message> <cause>) or (tagged (message: <message> cause: <cause>)
	 * Error) with <message> an explanatory text literal and cause an optional Error object.
	 */
	[[nodiscard]] auto error_from(const string &message, shared_ptr<const Expression> cause = Nil_Expression::NIL)
			-> shared_ptr<Expression>;

} // namespace appsl
