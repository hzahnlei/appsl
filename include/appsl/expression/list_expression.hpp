#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/abstract_collection_expression.hpp"
#include "appsl/expression/nil_expression.hpp"

namespace appsl
{

	class List_Expression final : public Abstract_Collection_Expression
	{
	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

		List_Expression(const Token &token, shared_ptr<const Expression> head = Nil_Expression::NIL,
		                shared_ptr<const Expression> tail = Nil_Expression::NIL);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_list() const -> bool override;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;

	    private:
		auto type_of_collection() const -> string override;
		auto opening_delimiter() const -> string override;
		auto closing_delimiter() const -> string override;
	};

	inline auto List_Expression::tag() const -> const Expression & { return *TYPE_TAG; }

	inline auto List_Expression::tagged_expression() const -> const Expression & { return *this; }

} // namespace appsl
