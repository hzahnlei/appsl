#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/expression/map_literal_expression.hpp"
#include "appsl/expression/nil_expression.hpp"

namespace appsl
{

	//---- Abstract collection expression ---------------------------------

	/**
	 * @brief Collection elements evaluated only once.
	 *
	 * Every expression returned by the parser gets evaluated. In the case of collections that means that each
	 * element gets evaluated. A new, fully evaluated collection is the result. Elements of a collecion do not
	 * change after this initial evaluation. Hence, list, set, map and record do not reevaluate their elements but
	 * evaluate to them selfes. This is much cheaper computationally than (unnecessarily) reevaluating all
	 * collection members.
	 *
	 * So, if collections do not really evaluate but just return them selfes, how are their elements evaluated
	 * eventually? This is where collection prototypes come into play. The parser returns collection prototypes
	 * rather than actual collections. These prototypes are short lived objects with a single purpose. Evaluating
	 * prototypes means to evaluate every single element and build a new collection. A list prototype (which
	 * evaluates its elements) becomes a list (which just evaluates to itself). A map prototype becomes an actual
	 * map etc.
	 *
	 * Objects of this type are short lived like virtual particles. Therefore, most methods do not exhibit
	 * meaningful behaviour as this will never be invoked. The only meaningful function is the evaluation function
	 * to produce the actual collection.
	 */
	class Abstract_Collection_Prototype : public Expression
	{

	    protected:
		const Token m_token;
		const shared_ptr<const Expression> m_head;
		const shared_ptr<const Expression> m_tail;

	    public:
		Abstract_Collection_Prototype(const Token &, shared_ptr<const Expression> head,
		                              shared_ptr<const Expression> tail);
		~Abstract_Collection_Prototype() override = default;

		auto token() const -> const Token & final;

		auto head() const -> const Expression & final;
		auto tail() const -> const Expression & final;

		auto is_nil() const -> bool final;
		auto is_name() const -> bool final;
		auto name() const -> const string & final;
		auto is_built_in_function() const -> bool final;
		auto is_function() const -> bool final;
		auto is_function_application() const -> bool final;
		auto is_fun_macro() const -> bool final;
		auto is_integer() const -> bool final;
		auto int_value() const -> Integer_Representation final;
		auto is_real() const -> bool final;
		auto real_value() const -> Real_Representation final;
		auto parameter_list() const -> const Expression & final;
		auto is_variadic_parameter() const -> bool final;
		auto is_quoted() const -> bool final;
		auto is_def_macro() const -> bool final;
		auto is_name_value_pair() const -> bool final;
		auto is_text() const -> bool final;
		auto text_value() const -> const string & final;
		auto tagged_expression() const -> const Expression & final;

		auto operator<<(ostream &) const -> ostream & final;

		auto stringify(ostream &, const string &indentation) const -> void override;

	    private:
		virtual auto type_of_collection() const -> string = 0;
		virtual auto opening_delimiter() const -> string = 0;
		virtual auto closing_delimiter() const -> string = 0;
		auto stringify_all_elements(ostream &, const string &indentation) const -> void;
	};

	//---- List expression prototype --------------------------------------

	class List_Prototype final : public Abstract_Collection_Prototype
	{

	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

		List_Prototype(const Token &, shared_ptr<const Expression> head = Nil_Expression::NIL,
		               shared_ptr<const Expression> tail = Nil_Expression::NIL);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_list() const -> bool override;
		auto tag() const -> const Expression & override;

	    private:
		auto type_of_collection() const -> string override;
		auto opening_delimiter() const -> string override;
		auto closing_delimiter() const -> string override;
	};

	//---- Map expression prototype ---------------------------------------

	class Map_Prototype final : public Abstract_Collection_Prototype
	{

	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

		Map_Prototype(const Token &, shared_ptr<const Expression> head, shared_ptr<const Expression> tail);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_list() const -> bool override;
		auto tag() const -> const Expression & override;

		auto stringify(ostream &, const string &) const -> void override;

	    private:
		auto type_of_collection() const -> string override;
		auto opening_delimiter() const -> string override;
		auto closing_delimiter() const -> string override;
	};

	//---- Record expression prototype ------------------------------------

	class Record_Prototype final : public Abstract_Collection_Prototype
	{
	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

		Record_Prototype(const Token &, shared_ptr<const Expression> head, shared_ptr<const Expression> tail);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_list() const -> bool override;
		auto tag() const -> const Expression & override;

		auto stringify(ostream &, const string &) const -> void override;

	    private:
		auto type_of_collection() const -> string override;
		auto opening_delimiter() const -> string override;
		auto closing_delimiter() const -> string override;
	};

	//---- Set expression prototype ---------------------------------------

	class Set_Prototype final : public Abstract_Collection_Prototype
	{

	    public:
		static const shared_ptr<const Expression> TYPE_TAG;

		Set_Prototype(const Token &, shared_ptr<const Expression> head = Nil_Expression::NIL,
		              shared_ptr<const Expression> tail = Nil_Expression::NIL);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		auto clone() const -> unique_ptr<Expression> override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_list() const -> bool override;
		auto is_set() const -> bool override;
		auto tag() const -> const Expression & override;

	    private:
		auto type_of_collection() const -> string override;
		auto opening_delimiter() const -> string override;
		auto closing_delimiter() const -> string override;
	};

} // namespace appsl
