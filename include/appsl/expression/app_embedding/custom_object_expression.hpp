#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/name_value_pair_expression.hpp"
#include "appsl/parser/token.hpp"

namespace appsl
{

	/**
	 * @brief Encapsulate application specific objects as an "expression".
	 */
	class Custom_Object_Expression : public Expression
	{

	    private:
		const Token m_token;
		const shared_ptr<const Expression> m_type;
		const shared_ptr<const Expression> m_properties;

		static const shared_ptr<const Expression> TYPE_TAG;

	    public:
		explicit Custom_Object_Expression(const Token &, shared_ptr<const Expression> kind,
		                                  shared_ptr<const Expression> properties = Nil_Expression::NIL);
		explicit Custom_Object_Expression(Token &&, shared_ptr<const Expression> kind,
		                                  shared_ptr<const Expression> properties = Nil_Expression::NIL);

		auto operator==(const Expression &) const -> bool override;
		auto operator<=(const Expression &) const -> bool override;
		auto operator<(const Expression &) const -> bool override;

		// auto clone() const -> unique_ptr<Expression> override;

		auto token() const -> const Token & override;

		auto head() const -> const Expression & override;
		auto tail() const -> const Expression & override;

		auto evaluate(Stack_Frame &) const -> shared_ptr<const Expression> override;

		auto is_nil() const -> bool override;
		auto is_name() const -> bool override;
		auto name() const -> const string & override;
		auto is_built_in_function() const -> bool override;
		auto is_function() const -> bool override;
		auto is_function_application() const -> bool override;
		auto is_fun_macro() const -> bool override;
		auto is_integer() const -> bool override;
		auto int_value() const -> Integer_Representation override;
		auto is_real() const -> bool override;
		auto real_value() const -> Real_Representation override;
		auto parameter_list() const -> const Expression & override;
		auto is_variadic_parameter() const -> bool override;
		auto is_quoted() const -> bool override;
		auto is_def_macro() const -> bool override;
		auto is_name_value_pair() const -> bool override;
		auto is_text() const -> bool override;
		auto text_value() const -> const string & override;
		auto is_list() const -> bool override;
		auto tag() const -> const Expression & override;
		auto tagged_expression() const -> const Expression & override;
		[[nodiscard]] auto is_custom_object() const -> bool override;
		[[nodiscard]] auto custom_object_type() const -> const Expression & override;

		[[nodiscard]] auto is_record() const -> bool override;
		[[nodiscard]] auto holds_properties() const -> bool override;
		[[nodiscard]] auto has_property(const string &) const -> bool override;
		[[nodiscard]] auto by_name(const string &) const -> const Expression & override;

		auto operator<<(ostream &) const -> ostream & override;

		auto stringify(ostream &, const string &indentation) const -> void override;
	};

	inline auto Custom_Object_Expression::tag() const -> const Expression & { return *TYPE_TAG; }

	inline auto Custom_Object_Expression::tagged_expression() const -> const Expression & { return *this; }

	inline auto Custom_Object_Expression::is_custom_object() const -> bool { return true; }

	inline auto Custom_Object_Expression::custom_object_type() const -> const Expression & { return *m_type; }

	//---- Composite expression (record) ----------------------------------

	inline auto Custom_Object_Expression::is_record() const -> bool { return holds_properties(); }

	inline auto Custom_Object_Expression::holds_properties() const -> bool { return m_properties->is_record(); }

	inline auto Custom_Object_Expression::has_property(const string &name) const -> bool
	{
		return m_properties->has_property(name);
	}

	inline auto Custom_Object_Expression::by_name(const string &name) const -> const Expression &
	{
		return m_properties->by_name(name);
	}

} // namespace appsl
