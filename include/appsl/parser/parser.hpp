#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/parser/scanner.hpp"

namespace appsl
{

	class Parser final
	{

	    public:
		static auto integer_literal_from(const Token &) -> shared_ptr<const Expression>;
		static auto empty_list_at(const Token &) -> shared_ptr<const Expression>;
		static auto empty_set_at(const Token &) -> shared_ptr<const Expression>;
		static auto name_from(const Token &) -> shared_ptr<const Expression>;
		static auto real_literal_from(const Token &) -> shared_ptr<const Expression>;
		static auto text_literal_from(const Token &) -> shared_ptr<const Expression>;
		static auto cons_operator_from(const Token &) -> shared_ptr<const Expression>;
		static auto accessor_operator_from(const Token &token) -> shared_ptr<const Expression>;
		using Preliminary_Expr_Collection = vector<shared_ptr<const Expression>>;
		static auto accessor_expression_from(const Token &, const Preliminary_Expr_Collection &)
				-> shared_ptr<const Expression>;
		static auto proto_list_from(const Token &, const Preliminary_Expr_Collection &)
				-> shared_ptr<const Expression>;
		static auto proto_set_from(const Token &, const Preliminary_Expr_Collection &)
				-> shared_ptr<const Expression>;
		static auto proto_map_from(const Token &, const Preliminary_Expr_Collection &)
				-> shared_ptr<const Expression>;
		static auto proto_record_from(const Token &, const Preliminary_Expr_Collection &)
				-> shared_ptr<const Expression>;
		static auto function_application_from(const Token &, const Preliminary_Expr_Collection &)
				-> shared_ptr<const Expression>;
		static auto composed_function_application_from(const Preliminary_Expr_Collection &)
				-> shared_ptr<const Expression>;

	    private:
		Scanner &m_source;

	    public:
		explicit Parser(Scanner &) noexcept;
		explicit Parser(const Parser &) = delete;
		explicit Parser(Parser &&) = default;
		~Parser() = default;

		auto operator=(const Parser &) -> Parser & = delete;
		auto operator=(Parser &&) -> Parser & = delete;

		auto parse() -> shared_ptr<const Expression>;
		[[nodiscard]] auto has_more_to_parse() const -> bool;

	    private:
		auto try_parse_accessor_expression() -> shared_ptr<const Expression>;
		auto parse_accessor_expression(const shared_ptr<const Expression> &) -> shared_ptr<const Expression>;

		auto try_parse_kv_expression() -> shared_ptr<const Expression>;
		auto parse_kv_expression(const shared_ptr<const Expression> &) -> shared_ptr<const Expression>;

		auto try_parse_composed_expression() -> shared_ptr<const Expression>;
		auto parse_composed_expression(const shared_ptr<const Expression> &) -> shared_ptr<const Expression>;

		auto try_parse_cons_expression() -> shared_ptr<const Expression>;
		auto parse_cons_expression(const shared_ptr<const Expression> &) -> shared_ptr<const Expression>;
		auto parse_expression() -> shared_ptr<const Expression>;

		auto parse_list(const Token &) -> shared_ptr<const Expression>;
		auto parse_set(const Token &) -> shared_ptr<const Expression>;
		auto parse_map(const Token &) -> shared_ptr<const Expression>;
		auto parse_function_application_or_record(const Token &) -> shared_ptr<const Expression>;
		auto parse_collection(const Token &expected_end, const string &display_name)
				-> Preliminary_Expr_Collection;

		auto parse_name(const Token &) -> shared_ptr<const Expression>;
		auto parse_variadic_parameter(const Token &) -> shared_ptr<const Expression>;
		auto parse_named_argument(const Token &) -> shared_ptr<const Expression>;
		auto parse_quoted_expression(const Token &) -> shared_ptr<const Expression>;
	};

} // namespace appsl
