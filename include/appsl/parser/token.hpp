#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/use_stl.hpp"

namespace appsl
{

	class Token final
	{

	    public:
		enum class Type
		{
			COLON,
			COMMA,
			ELLIPSIS,
			END_OF_FILE,
			FULL_STOP,
			HASH,
			HASH_MAP,
			INTEGER_LITERAL,
			L_BRACKET,
			L_CURLY_BRACE,
			L_PARENTHESIS,
			NAME,
			QUOTE,
			R_ARROW,
			R_BRACKET,
			R_CURLY_BRACE,
			R_PARENTHESIS,
			REAL_LITERAL,
			TEXT_LITERAL
		};

		static auto debug_representation(Type) -> string;
		static auto representation_for_error_reporting(Type) -> string;

		static const Token NONE;

		using Position = pair<size_t, size_t>;
		static constexpr Position NO_POS{0, 0};

		using Lexeme = string;

	    private:
		Type m_type;
		Lexeme m_lexeme;
		Position m_position;

	    public:
		Token(Type, const Lexeme &, const Position &) noexcept;
		explicit Token(const Token &) = default;
		explicit Token(Token &&) = default;
		~Token() noexcept = default;

		auto operator=(const Token &) -> Token & = default;
		auto operator=(Token &&) -> Token & = default;

		auto operator==(const Token &other) const -> bool;
		inline auto operator==(const Token *other) const -> bool;

		[[nodiscard]] auto is_a(Type) const noexcept -> bool;
		[[nodiscard]] auto is_colon() const noexcept -> bool;
		[[nodiscard]] auto is_comma() const noexcept -> bool;
		[[nodiscard]] auto is_ellipsis() const noexcept -> bool;
		[[nodiscard]] auto is_eof() const noexcept -> bool;
		[[nodiscard]] auto is_full_stop() const noexcept -> bool;
		[[nodiscard]] auto is_integer_literal() const noexcept -> bool;
		[[nodiscard]] auto is_left_bracket() const noexcept -> bool;
		[[nodiscard]] auto is_left_curly_brace() const noexcept -> bool;
		[[nodiscard]] auto is_left_parenthesis() const noexcept -> bool;
		[[nodiscard]] auto is_any_literal() const noexcept -> bool;
		[[nodiscard]] auto is_name() const noexcept -> bool;
		[[nodiscard]] auto is_real_literal() const noexcept -> bool;
		[[nodiscard]] auto is_right_arrow() const noexcept -> bool;
		[[nodiscard]] auto is_right_bracket() const noexcept -> bool;
		[[nodiscard]] auto is_right_curly_brace() const noexcept -> bool;
		[[nodiscard]] auto is_right_parenthesis() const noexcept -> bool;
		[[nodiscard]] auto is_text_literal() const noexcept -> bool;

		[[nodiscard]] auto type() const noexcept -> Type;
		[[nodiscard]] auto lexeme() const noexcept -> const Lexeme &;
		[[nodiscard]] auto line() const noexcept -> size_t;
		[[nodiscard]] auto column() const noexcept -> size_t;
		[[nodiscard]] auto position() const noexcept -> const Position &;
	};

	auto operator<<(ostream &, const Token &) -> ostream &;

	auto Token::operator==(const Token *const other) const -> bool { return nullptr != other && *this == *other; }

} // namespace appsl
