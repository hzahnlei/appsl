#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/token.hpp"
#include <appsl/use_stl.hpp>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	class Abstract_Compiler_Error : public junkbox::Base_Exception
	{

	    private:
		const Token::Position m_position;

	    public:
		explicit Abstract_Compiler_Error(const Message &) noexcept = delete;
		Abstract_Compiler_Error(const string &, const Token::Position &) noexcept;

		[[nodiscard]] auto line() const noexcept -> size_t;
		[[nodiscard]] auto column() const noexcept -> size_t;
		[[nodiscard]] auto pretty_message() const noexcept -> Message final;

	    protected:
		[[nodiscard]] virtual auto error_category_name() const noexcept -> string = 0;
	};

} // namespace appsl
