#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/token.hpp"

namespace appsl
{

	class Scanner final
	{

	    public:
		using Character = int;

		static auto is_single_line_comment(Character curr_char) -> bool;
		static auto is_whitespace_comment_or_hashbang(Character curr_char, Character next_char) -> bool;

		static auto is_forbidden_in_name(Character some_char) -> bool;

		static auto hex_representation(Character some_char) -> string;

		static auto is_hash_bang(Character curr_char, Character next_char) -> bool;

	    private:
		const unique_ptr<istringstream> m_source;
		size_t m_line_count = 1;
		size_t m_columnCount = 1;
		size_t m_previous_line_count = 1;
		size_t m_previous_column_count = 1;
		Token m_current_token;

	    public:
		Scanner() = delete;
		explicit Scanner(unique_ptr<istringstream> source);
		explicit Scanner(string source);
		explicit Scanner(const Scanner &) = delete;
		explicit Scanner(Scanner &&) = delete;

		auto operator=(Scanner) -> Scanner & = delete;
		auto operator=(const Scanner &) -> Scanner & = delete;
		auto operator=(Scanner &&) -> Scanner & = delete;

		auto position() const -> Token::Position;

		auto tokens_available() const noexcept -> bool;
		auto current_token() const noexcept -> const Token &;
		auto advance_to_next_token() -> bool;

	    private:
		auto scan_skipping_possible_ws_and_comments() -> Token;
		auto try_eliminate_whitespace_and_comments() -> Character;
		auto scan_with_all_ws_and_comments_removed(Character curr_char) -> Token;
		auto scan_number_literal(Character previous_char) -> Token;
		auto scan_text_literal() -> Token;
		auto scan_number_literal_or_name(Character curr_char) -> Token;
		auto scan_name(Character previous_char, const Token::Position &position) -> Token;
		auto scan_full_stop_or_ellipsis() -> Token;
		auto scan_hash_or_map() -> Token;

		auto previous_position() const -> Token::Position;

		auto peek_next_char() const -> Character;
		auto consume_current_char() -> Character;
		auto unconsume_previous_char(Character previous_char) -> void;
	};

	namespace scan
	{
		auto this_script(const string &source) -> unique_ptr<Scanner>;
	} // namespace scan

} // namespace appsl
