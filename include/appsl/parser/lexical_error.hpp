#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/abstract_compiler_error.hpp"

namespace appsl
{

	class Lexical_Error final : public Abstract_Compiler_Error
	{

	    public:
		Lexical_Error(const Message &, const Token::Position &) noexcept;

	    protected:
		auto error_category_name() const noexcept -> string override;
	};

} // namespace appsl
