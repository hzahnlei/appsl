#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/abstract_stack_frame.hpp"

namespace appsl
{

	class Local_Stack_Frame final : public Abstract_Stack_Frame
	{

	    private:
		shared_ptr<Stack_Frame> m_parent_scope;
		shared_ptr<Stack_Frame> m_global_scope;

	    public:
		Local_Stack_Frame(shared_ptr<Stack_Frame> parent, shared_ptr<Stack_Frame> global);

		[[nodiscard]] auto parent() const -> Stack_Frame & override;
		[[nodiscard]] auto global() const -> Stack_Frame & override;

		[[nodiscard]] auto is_global() const noexcept -> bool override;

		auto dump(const string &, ostream &) const -> ostream & override;
	};

	inline auto Local_Stack_Frame::is_global() const noexcept -> bool { return false; }

	inline auto Local_Stack_Frame::dump(const string &indentation, ostream &out) const -> ostream &
	{
		m_parent_scope->dump(indentation + "    ", out);
		for (const auto &kv : m_local_scope)
		{
			out << indentation << kv.first << " = " << kv.second->stringify() << '\n';
		}
		return out;
	}

} // namespace appsl
