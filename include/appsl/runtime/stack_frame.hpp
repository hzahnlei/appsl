#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/use_stl.hpp"

namespace appsl
{

	class Stack_Frame : public enable_shared_from_this<Stack_Frame>
	{

	    public:
		Stack_Frame() = default;
		Stack_Frame(const Stack_Frame &) = delete;
		auto operator=(const Stack_Frame &) -> Stack_Frame & = delete;
		Stack_Frame(Stack_Frame &&) = delete;
		auto operator=(Stack_Frame &&) -> Stack_Frame & = delete;
		virtual ~Stack_Frame() = default;

		[[nodiscard]] virtual auto lookup(const string &) const -> const Expression & = 0;
		virtual auto define_argument(const string &, shared_ptr<const Expression>) -> void = 0;
		virtual auto define_local_var(const string &, shared_ptr<const Expression>) -> void = 0;
		[[nodiscard]] virtual auto new_child() -> shared_ptr<Stack_Frame> = 0;
		[[nodiscard]] virtual auto is_defined_locally(const string &) const -> bool = 0;
		[[nodiscard]] virtual auto is_defined(const string &) const -> bool = 0;
		[[nodiscard]] virtual auto is_defined_globally(const string &) const -> bool = 0;
		[[nodiscard]] virtual auto parent() const -> Stack_Frame & = 0;
		[[nodiscard]] virtual auto global() const -> Stack_Frame & = 0;
		[[nodiscard]] virtual auto boolean_literal_from(bool) const -> shared_ptr<const Expression> = 0;
		[[nodiscard]] virtual auto argument_count() const -> size_t = 0;

		[[nodiscard]] virtual auto is_global() const noexcept -> bool = 0;
		virtual auto for_each_local(const function<void(const string &, shared_ptr<const Expression>)> &) const
				-> void = 0;

		auto operator<<(ostream &) const -> ostream &;
		virtual auto dump(const string &, ostream &) const -> ostream & = 0;
	};

	inline auto Stack_Frame::operator<<(ostream &out) const -> ostream & { return dump("", out); }

	inline auto operator<<(ostream &out, const Stack_Frame &stack) -> ostream & { return stack << out; }

	inline auto operator<<(ostream &out, const Stack_Frame *const stack) -> ostream &
	{
		return nullptr == stack ? out << string{"nullptr"} : stack->operator<<(out);
	}

} // namespace appsl
