#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include <junkbox/junkbox.hpp>

namespace appsl
{

	class Runtime_Error final : public junkbox::Base_Exception
	{

	    public:
		explicit Runtime_Error(const Message &) noexcept;

		[[nodiscard]] auto pretty_message() const noexcept -> Message override;
	};

} // namespace appsl
