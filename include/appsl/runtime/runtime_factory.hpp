
#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include "appsl/use_stl.hpp"

namespace appsl
{

	[[nodiscard]] auto make_runtime() -> shared_ptr<Stack_Frame>;

} // namespace appsl
