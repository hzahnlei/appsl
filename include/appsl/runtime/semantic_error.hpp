#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/abstract_compiler_error.hpp"

namespace appsl
{

	class Semantic_Error final : public Abstract_Compiler_Error
	{

	    public:
		Semantic_Error(const Message &, const Token::Position &) noexcept;

	    protected:
		[[nodiscard]] auto error_category_name() const noexcept -> string override;
	};

} // namespace appsl
