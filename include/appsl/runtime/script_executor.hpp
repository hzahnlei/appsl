#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/stack_frame.hpp"
#include "appsl/use_stl.hpp"

namespace appsl
{

	/**
	 * @brief Meant for running small scripts that add additional definitions to the runtime environment.
	 */
	auto execute_script(const string &script, Stack_Frame &) -> void;

	auto execute_script(const string &script, Stack_Frame &, ostream &) -> void;

	/**
	 * @brief REPL == Read, execute and print loop.
	 */
	auto rep_loop(const string &hello, istream &in, ostream &out, ostream &err, Stack_Frame &) -> void;

} // namespace appsl
