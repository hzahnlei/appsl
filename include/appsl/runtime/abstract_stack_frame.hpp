#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	class Abstract_Stack_Frame : public Stack_Frame
	{

	    protected:
		map<string, shared_ptr<const Expression>> m_local_scope;
		size_t m_arg_count = 0;

	    public:
		[[nodiscard]] auto lookup(const string &) const -> const Expression & override;
		auto define_argument(const string &, shared_ptr<const Expression>) -> void final;
		auto define_local_var(const string &, shared_ptr<const Expression>) -> void final;
		[[nodiscard]] auto new_child() -> shared_ptr<Stack_Frame> override;

		[[nodiscard]] auto is_defined_locally(const string &) const -> bool final;

		[[nodiscard]] auto is_defined(const string &) const -> bool override;
		[[nodiscard]] auto is_defined_globally(const string &) const -> bool override;
		[[nodiscard]] auto boolean_literal_from(bool) const -> shared_ptr<const Expression> override;
		[[nodiscard]] auto argument_count() const -> size_t final;

		auto for_each_local(const function<void(const string &, shared_ptr<const Expression>)> &) const
				-> void final;

	    private:
		auto define(const string &, shared_ptr<const Expression>) -> void;
	};

	inline auto Abstract_Stack_Frame::define_local_var(const string &name, shared_ptr<const Expression> expression)
			-> void
	{
		define(name, expression);
	}

	inline auto Abstract_Stack_Frame::argument_count() const -> size_t { return m_arg_count; }

	inline auto Abstract_Stack_Frame::for_each_local(
			const function<void(const string &, shared_ptr<const Expression>)> &action) const -> void
	{
		for (const auto &kv : m_local_scope)
		{
			action(kv.first, kv.second);
		}
	}

} // namespace appsl
