#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/abstract_stack_frame.hpp"

namespace appsl
{

	class Global_Stack_Frame final : public Abstract_Stack_Frame
	{

	    private:
		const shared_ptr<const Expression> m_true;

	    public:
		Global_Stack_Frame();

		[[nodiscard]] auto lookup(const string &) const -> const Expression & override;
		[[nodiscard]] auto new_child() -> shared_ptr<Stack_Frame> override;

		[[nodiscard]] auto is_defined(const string &) const -> bool override;

		[[nodiscard]] auto is_defined_globally(const string &) const -> bool override;
		[[nodiscard]] auto parent() const -> Stack_Frame & override;
		[[nodiscard]] auto global() const -> Stack_Frame & override;
		[[nodiscard]] auto boolean_literal_from(bool) const -> shared_ptr<const Expression> override;

		[[nodiscard]] auto is_global() const noexcept -> bool override;

		auto dump(const string &, ostream &) const -> ostream & override;

	    private:
		auto register_builtin_functions() -> void;
	};

	inline auto Global_Stack_Frame::is_global() const noexcept -> bool { return true; }

	inline auto Global_Stack_Frame::dump(const string &indentation, ostream &out) const -> ostream &
	{
		return out << indentation << "GLOBAL RUNTIME\n";
	}

} // namespace appsl
