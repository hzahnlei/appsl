#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include <cctype>
#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <ostream>
#include <regex>
#include <set>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

namespace appsl
{
	using std::enable_shared_from_this;
	using std::function;
	using std::isalnum;
	using std::isalpha;
	using std::istream;
	using std::istringstream;
	using std::isupper;
	using std::less;
	using std::make_pair;
	using std::make_shared;
	using std::make_unique;
	using std::map;
	using std::move;
	using std::ostream;
	using std::pair;
	using std::rand;
	using std::regex;
	using std::set;
	using std::shared_ptr;
	using std::size_t;
	using std::sqrt; // using std::sqrtl; FIXME not available on GCC 13 -> upgrade build images to 14
	using std::srand;
	using std::string;
	using std::stringstream;
	using std::to_string;
	using std::unique_ptr;
	using std::vector;

	using namespace std::literals::string_literals;
} // namespace appsl
