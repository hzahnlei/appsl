// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsli_app.hpp"
#include <exception>
#include <iostream>
#include <junkbox/junkbox.hpp>

using junkbox::Base_Exception;
using std::cerr;
using std::cin;
using std::cout;
using std::exception;
using yacl::list_of_arguments;
using yacl::program_name;

enum class Exit_Code : int
{
	OK = 0,
	ERROR
};

/**
 * @brief Only handle command line options and than hand over to application.
 *
 * @details Purpose of main is only to handle aspects on its own level of abstraction, that is command line options,
 * environment variables, application wiring and exit codes. Everything else is handled by the application.
 *
 * @return int OK=0 in case of success, ERROR=1 else.
 */
auto main(const int arg_count, const char **args) -> int
{
	try
	{
		AppSLI_App app{program_name(arg_count, args)};
		app.run(list_of_arguments(arg_count, args), cin, cout, cerr);
		return static_cast<int>(Exit_Code::OK);
	}
	catch (const Base_Exception &cause)
	{
		cerr << cause.pretty_message() << '\n';
		return static_cast<int>(Exit_Code::ERROR);
	}
	catch (const exception &cause)
	{
		cerr << cause.what() << '\n';
		return static_cast<int>(Exit_Code::ERROR);
	}
	catch (...)
	{
		cerr << "An unknown exception has occurred.\n";
		return static_cast<int>(Exit_Code::ERROR);
	}
}
