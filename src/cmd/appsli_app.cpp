// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsli_app.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/parser/scanner.hpp"
#include "appsl/runtime/runtime_factory.hpp"
#include "appsl/runtime/script_executor.hpp"
#include "appsl/version.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>
#include <yacl/yacl.hpp>

using namespace junkbox;
using namespace fmt;
using namespace yacl;
using namespace appsl;

const string AppSLI_App::CAPTION{"\n"
                                 "                    #   #### #\n"
                                 "                    #  #     #\n"
                                 " ###  ####  ####   #   #     #\n"
                                 "    # #   # #   #  #    ###  #\n"
                                 " #### #   # #   #  #       # #\n"
                                 "#   # #   # #   # #        # #\n"
                                 " #### ####  ####  #    ####  ####\n"
                                 "      #     #\n"
                                 "      #     #\n"};

const string AppSLI_App::DESCRIPTION{"app/SL - Application Scripting Language."};

const string AppSLI_App::VERSION{format("{}.{}.{}", appsl::VERSION_MAJOR, appsl::VERSION_MINOR, appsl::VERSION_PATCH)};

const string AppSLI_App::COPYRIGHT{"(c) 2020, 2024 by Holger Zahnleiter, all rights reserved"};

AppSLI_App::AppSLI_App(const string &program_name) : Base_Application{command_line_parser(program_name)} {}

auto AppSLI_App::command_line_parser(const string &program_name) const -> Command_Line_Parser
{
	Command_Line_Parser_Builder cli{program_name, CAPTION};
	cli.version(VERSION)
			.copyright(COPYRIGHT)
			.author("Holger Zahnleiter")
			.license(License::MIT)
			.open_source_software_used("fmt")
			.open_source_software_used("Conan")
			.open_source_software_used("GCC")
			.open_source_software_used("Clang")
			.open_source_software_used("GNU Make")
			.open_source_software_used("CMake")
			.open_source_software_used("Catch 2")
			.open_source_software_used("C++ Junk Box")
			.open_source_software_used("C++ YACL");
	register_interactive_interpreter_command(cli);
	return cli.description(DESCRIPTION);
}

auto AppSLI_App::register_interactive_interpreter_command(Command_Line_Parser_Builder &cli) const -> void
{
	// clang-format off
	cli.anonymous_command()
		.description("Starts an interactive scripting shell or interprets a skript file (if given as an argument).")
		.optional().positional().text_param("filename")
			.description("Name of the skript file to be interpreted.")
			.allow_empty_text()
			.allow_white_space()
			.defaults_to("")
		.optional().flag_param("show-output")
			.description("Shows output of script. Output, except for errors, suppressed otherweise.")
			.defaults_to(false)
		.action(
			[&](const auto &context, auto &in, auto &out, auto &err)
			{
				const auto file_name{context.text_value_at(0)};
				if (file_name == "")
				{
					run_interactive_scripting_shell(in, out, err);
				}
				else
				{
					const auto show_output{context.flag_value_of("show-output")};
					if (show_output)
					{
						interpret_script_from_file(file_name, out);
					}
					else
					{
						interpret_script_from_file(file_name);
					}
				}
			});
	// clang-format on
}

auto AppSLI_App::run_interactive_scripting_shell(istream &in, ostream &out, ostream &err) const -> void
{
	auto runtime{make_runtime()};
	appsl::rep_loop("", in, out, err, *runtime);
}

auto AppSLI_App::interpret_script_from_file(const string &source_file_name) const -> void
{
	const auto source{file::load_text_file(source_file_name)};
	auto runtime{make_runtime()};
	appsl::execute_script(source, *runtime);
}

auto AppSLI_App::interpret_script_from_file(const string &source_file_name, ostream &out) const -> void
{
	const auto source{file::load_text_file(source_file_name)};
	auto runtime{make_runtime()};
	appsl::execute_script(source, *runtime, out);
}
