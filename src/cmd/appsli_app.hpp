#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include <iostream>
#include <string>
#include <yacl/yacl.hpp>

using std::istream;
using std::ostream;
using std::string;
using yacl::Base_Application;
using yacl::Command_Line_Parser;
using yacl::Command_Line_Parser_Builder;

class AppSLI_App final : public Base_Application
{

    public:
	static const string CAPTION;
	static const string DESCRIPTION;
	static const string VERSION;
	static const string COPYRIGHT;

	explicit AppSLI_App(const string &);
	~AppSLI_App() override = default;

    private:
	auto command_line_parser(const string &program_name) const -> Command_Line_Parser;

	auto register_interactive_interpreter_command(Command_Line_Parser_Builder &) const -> void;
	auto run_interactive_scripting_shell(istream &in, ostream &out, ostream &err) const -> void;
	auto interpret_script_from_file(const string &) const -> void;
	auto interpret_script_from_file(const string &, ostream &out) const -> void;
};
