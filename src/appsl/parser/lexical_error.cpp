// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/lexical_error.hpp"

namespace appsl
{

	Lexical_Error::Lexical_Error(const string &message, const Token::Position &position) noexcept
			: Abstract_Compiler_Error{message, position}
	{
	}

	auto Lexical_Error::error_category_name() const noexcept -> string { return "Lexical"; }

} // namespace appsl
