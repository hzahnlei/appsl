// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/parser.hpp"
#include "appsl/expression/accessor_expression.hpp"
#include "appsl/expression/collection_prototypes.hpp"
#include "appsl/expression/function_application_expression.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/key_value_pair_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/map_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/name_value_pair_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/expression/record_expression.hpp"
#include "appsl/expression/set_expression.hpp"
#include "appsl/expression/text_literal_expression.hpp"
#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/parser/syntax_error.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;
	using junkbox::algo::for_each;

	auto Parser::integer_literal_from(const Token &token) -> shared_ptr<const Expression>
	{
		DEV_ASSERT(token.is_integer_literal(), "Token has to be an integer literal.");
		return make_shared<Integer_Literal_Expression>(token, stoll(token.lexeme()));
	}

	auto Parser::empty_list_at(const Token &token) -> shared_ptr<const Expression>
	{
		return make_shared<List_Expression>(token);
	}

	auto Parser::empty_set_at(const Token &token) -> shared_ptr<const Expression>
	{
		return make_shared<Set_Expression>(token);
	}

	auto Parser::name_from(const Token &token) -> shared_ptr<const Expression>
	{
		DEV_ASSERT(token.is_name(), "Token has to be a name.");
		return make_shared<Name_Expression>(token);
	}

	auto Parser::real_literal_from(const Token &token) -> shared_ptr<const Expression>
	{
		DEV_ASSERT(token.is_real_literal(), "Token has to be a real literal.");
		return make_shared<Real_Literal_Expression>(token, stold(token.lexeme()));
	}

	auto Parser::text_literal_from(const Token &token) -> shared_ptr<const Expression>
	{
		DEV_ASSERT(token.is_text_literal(), "Token has to be a text literal.");
		return make_shared<Text_Literal_Expression>(token);
	}

	auto Parser::cons_operator_from(const Token &token) -> shared_ptr<const Expression>
	{
		DEV_ASSERT(token.is_comma(), "Token has to be a comma.");
		return make_shared<Name_Expression>(token);
	}

	auto Parser::accessor_operator_from(const Token &token) -> shared_ptr<const Expression>
	{
		DEV_ASSERT(token.is_full_stop(), "Token has to be a full stop.");
		return make_shared<Name_Expression>(token);
	}

	using Const_Expression_Iter = Parser::Preliminary_Expr_Collection::const_iterator;

	auto remaining_accessors_from(const Token &establishing_token,
	                              const Parser::Preliminary_Expr_Collection &collection,
	                              Const_Expression_Iter &curr_expr) -> shared_ptr<const Expression>
	{
		if (curr_expr == collection.cend())
		{
			return Nil_Expression::NIL;
		}
		const auto lhs{*curr_expr};
		curr_expr++;
		return make_shared<Field_Accessor>(establishing_token, lhs,
		                                   remaining_accessors_from(establishing_token, collection, curr_expr));
	}

	auto Parser::accessor_expression_from(const Token &establishing_token,
	                                      const Preliminary_Expr_Collection &collection)
			-> shared_ptr<const Expression>
	{
		auto curr_expr{collection.cbegin()};
		return remaining_accessors_from(establishing_token, collection, curr_expr);
	}

	auto remaining_proto_list_from(const Token &establishing_token,
	                               const Parser::Preliminary_Expr_Collection &collection,
	                               Const_Expression_Iter &curr_expr) -> shared_ptr<const Expression>
	{
		if (curr_expr == collection.cend())
		{
			return Nil_Expression::NIL;
		}
		const auto lhs{*curr_expr};
		curr_expr++;
		return make_shared<List_Prototype>(
				establishing_token, lhs,
				remaining_proto_list_from(establishing_token, collection, curr_expr));
	}

	auto Parser::proto_list_from(const Token &establishing_token, const Preliminary_Expr_Collection &collection)
			-> shared_ptr<const Expression>
	{
		if (collection.empty())
		{
			// () = [] = '() ≠ nil
			return empty_list_at(establishing_token);
		}
		auto curr_expr{collection.cbegin()};
		return remaining_proto_list_from(establishing_token, collection, curr_expr);
	}

	auto remaining_proto_set_from(const Token &establishing_token,
	                              const Parser::Preliminary_Expr_Collection &collection,
	                              Const_Expression_Iter &curr_expr) -> shared_ptr<const Expression>
	{
		if (curr_expr == collection.cend())
		{
			return Nil_Expression::NIL;
		}
		const auto lhs{*curr_expr};
		curr_expr++;
		return make_shared<Set_Prototype>(establishing_token, lhs,
		                                  remaining_proto_set_from(establishing_token, collection, curr_expr));
	}

	auto Parser::proto_set_from(const Token &establishing_token, const Preliminary_Expr_Collection &collection)
			-> shared_ptr<const Expression>
	{
		if (collection.empty())
		{
			// {}  ≠ nil
			return empty_set_at(establishing_token);
		}
		auto curr_expr{collection.cbegin()};
		return remaining_proto_set_from(establishing_token, collection, curr_expr);
	}

	auto remaining_proto_map_from(const Token &establishing_token,
	                              const Parser::Preliminary_Expr_Collection &collection,
	                              Const_Expression_Iter &curr_expr) -> shared_ptr<const Expression>
	{
		if (curr_expr == collection.cend())
		{
			return Nil_Expression::NIL;
		}
		const auto lhs{*curr_expr};
		curr_expr++;
		return make_shared<Map_Prototype>(establishing_token, lhs,
		                                  remaining_proto_map_from(establishing_token, collection, curr_expr));
	}

	auto Parser::proto_map_from(const Token &establishing_token, const Preliminary_Expr_Collection &collection)
			-> shared_ptr<const Expression>
	{
		auto curr_expr{collection.cbegin()};
		return remaining_proto_map_from(establishing_token, collection, curr_expr);
	}

	auto remaining_proto_record_from(const Token &establishing_token,
	                                 const Parser::Preliminary_Expr_Collection &collection,
	                                 Const_Expression_Iter &curr_expr) -> shared_ptr<const Expression>
	{
		if (curr_expr == collection.cend())
		{
			return Nil_Expression::NIL;
		}
		const auto lhs{*curr_expr};
		curr_expr++;
		return make_shared<Record_Prototype>(
				establishing_token, lhs,
				remaining_proto_record_from(establishing_token, collection, curr_expr));
	}

	auto Parser::proto_record_from(const Token &establishing_token, const Preliminary_Expr_Collection &collection)
			-> shared_ptr<const Expression>
	{
		// Check empty collection --> error?
		auto curr_expr{collection.cbegin()};
		return remaining_proto_record_from(establishing_token, collection, curr_expr);
	}

	auto remaining_function_application_from(const Token &establishing_token,
	                                         const Parser::Preliminary_Expr_Collection &collection,
	                                         Const_Expression_Iter &curr_expr) -> shared_ptr<const Expression>
	{
		if (curr_expr == collection.cend())
		{
			return Nil_Expression::NIL;
		}
		const auto lhs{*curr_expr};
		curr_expr++;
		return make_shared<Function_Application_Expression>(
				establishing_token, lhs,
				remaining_function_application_from(establishing_token, collection, curr_expr));
	}

	auto Parser::function_application_from(const Token &establishing_token,
	                                       const Preliminary_Expr_Collection &collection)
			-> shared_ptr<const Expression>
	{
		if (collection.empty())
		{
			// We interpret () as [], empty function application makes no sense.
			// [] = '(), but [] ≠ nil
			return empty_list_at(establishing_token);
		}
		auto curr_expr{collection.cbegin()};
		return remaining_function_application_from(establishing_token, collection, curr_expr);
		// TODO try iterative (non-recursive) approach from composed_function_application_from???
	}

	auto Parser::composed_function_application_from(const Preliminary_Expr_Collection &expressions)
			-> shared_ptr<const Expression>
	{
		auto result{expressions[0]};
		for (auto i{1U}; i < expressions.size(); i++)
		{
			result = make_shared<Function_Application_Expression>(
					expressions[i]->token(), expressions[i]->head().shared_from_this(),
					make_shared<Function_Application_Expression>(
							expressions[i]->token(), result,
							expressions[i]->tail().shared_from_this()));
		}
		return result;
	}

	Parser::Parser(Scanner &source) noexcept : m_source{source} {}

	auto Parser::parse() -> shared_ptr<const Expression>
	{
		return has_more_to_parse() ? try_parse_accessor_expression()
		                           : make_shared<Nil_Expression>(m_source.current_token());
	}

	auto Parser::has_more_to_parse() const -> bool { return m_source.tokens_available(); }

	inline auto is_accessor_operator(const Token &token) { return token.is_full_stop(); }

	auto Parser::try_parse_accessor_expression() -> shared_ptr<const Expression>
	{
		const auto expression{try_parse_kv_expression()};
		return is_accessor_operator(m_source.current_token()) ? parse_accessor_expression(expression)
		                                                      : expression;
	}

	auto Parser::parse_accessor_expression(const shared_ptr<const Expression> &expression)
			-> shared_ptr<const Expression>
	{
		Preliminary_Expr_Collection expressions{expression};
		while (is_accessor_operator(m_source.current_token()))
		{
			const auto accessor_op{m_source.current_token()};
			m_source.advance_to_next_token(); // Consume accesso operator '.'
			if (has_more_to_parse())
			{
				expressions.emplace_back(try_parse_kv_expression());
			}
			else
			{
				// TODO use official constant for '.'
				throw Syntax_Error{"Premature end of accessor expression. Expected another expression "
				                   "to follow accessor operator '.'.",
				                   accessor_op.position()};
			}
		}
		return accessor_expression_from(expression->token(), expressions);
	}

	inline auto is_mapping_operator(const Token &token) { return token.is_right_arrow(); }

	auto Parser::try_parse_kv_expression() -> shared_ptr<const Expression>
	{
		const auto expression{try_parse_composed_expression()};
		return is_mapping_operator(m_source.current_token()) ? parse_kv_expression(expression) : expression;
	}

	auto Parser::parse_kv_expression(const shared_ptr<const Expression> &lhs) -> shared_ptr<const Expression>
	{
		const auto mapping_op{m_source.current_token()};
		m_source.advance_to_next_token(); // Consume mapping operator '->'
		if (!has_more_to_parse())
		{
			throw Syntax_Error{"Premature end of key/value pair. Expected another expression after " +
			                                   text::single_quoted(mapping_op.lexeme()) + ".",
			                   mapping_op.position()};
		}
		const auto rhs{try_parse_accessor_expression()}; // Was try_parse_composed_expression before introducing
		                                                 // accessor
		return make_shared<Key_Value_Pair_Expression>(mapping_op, lhs, rhs);
	}

	inline auto is_composition_operator(const Token &token) { return token.is_name() && "|" == token.lexeme(); }

	auto Parser::try_parse_composed_expression() -> shared_ptr<const Expression>
	{
		const auto expression{try_parse_cons_expression()};
		return is_composition_operator(m_source.current_token()) ? parse_composed_expression(expression)
		                                                         : expression;
	}

	auto Parser::parse_composed_expression(const shared_ptr<const Expression> &expression)
			-> shared_ptr<const Expression>
	{
		Preliminary_Expr_Collection expressions{expression};
		while (is_composition_operator(m_source.current_token()))
		{
			const auto composition_op{m_source.current_token()};
			m_source.advance_to_next_token(); // Consume composition operator '|'
			if (has_more_to_parse())
			{
				expressions.emplace_back(try_parse_cons_expression());
			}
			else
			{
				// TODO use official constant for '|'
				throw Syntax_Error{"Premature end of composed expression. Expected another expression "
				                   "to follow composition operator '|'.",
				                   composition_op.position()};
			}
		}
		return composed_function_application_from(expressions);
	}

	inline auto is_cons_operator(const Token &token) { return token.is_comma(); }

	auto Parser::try_parse_cons_expression() -> shared_ptr<const Expression>
	{
		const auto potential_lhs_expression{parse_expression()};
		return is_cons_operator(m_source.current_token()) ? parse_cons_expression(potential_lhs_expression)
		                                                  : potential_lhs_expression;
	}

	auto Parser::parse_cons_expression(const shared_ptr<const Expression> &lhs_expression)
			-> shared_ptr<const Expression>
	{
		const auto cons_op{m_source.current_token()};
		m_source.advance_to_next_token(); // Consume cons operator
		if (has_more_to_parse())
		{
			return make_shared<List_Prototype>(cons_op, lhs_expression, try_parse_cons_expression());
		}
		else
		{
			throw Syntax_Error{"Premature end of cons cell. Expected right hand expression.",
			                   cons_op.position()};
		}
	}

	auto Parser::parse_expression() -> shared_ptr<const Expression>
	{ // TODO would like to make this &. but then I loose it after advance_to_next_token(). maybe
	  // advance_to_next_token should return the current token instead of bool?
		const auto curr_token{m_source.current_token()};
		m_source.advance_to_next_token(); // Consume current token
		switch (curr_token.type())
		{
		case Token::Type::INTEGER_LITERAL:
			return integer_literal_from(curr_token);
		case Token::Type::COMMA:
			return cons_operator_from(curr_token);
		case Token::Type::FULL_STOP:
			return accessor_operator_from(curr_token);
		case Token::Type::L_BRACKET:
			return parse_list(curr_token);
		case Token::Type::HASH_MAP:
			return parse_map(curr_token);
		case Token::Type::L_PARENTHESIS:
			return parse_function_application_or_record(curr_token);
		case Token::Type::L_CURLY_BRACE:
			return parse_set(curr_token);
		case Token::Type::NAME:
			return parse_name(curr_token);
		case Token::Type::QUOTE:
			return parse_quoted_expression(curr_token);
		case Token::Type::REAL_LITERAL:
			return real_literal_from(curr_token);
		case Token::Type::TEXT_LITERAL:
			return text_literal_from(curr_token);
		default:
			throw Syntax_Error{"Expected atom, quote, '(', '[', '#{' or ',' but found " +
			                                   text::single_quoted(curr_token.lexeme()) + ".",
			                   curr_token.position()};
		}
	}

	static const Token RIGHT_LIST_DELIMITER{Token::Type::R_BRACKET, "]", Token::NO_POS};
	auto Parser::parse_list(const Token &left_delimiter) -> shared_ptr<const Expression>
	{
		return proto_list_from(left_delimiter, parse_collection(RIGHT_LIST_DELIMITER, "list"));
	}

	static const Token RIGHT_SET_DELIMITER{Token::Type::R_CURLY_BRACE, "}", Token::NO_POS};
	auto Parser::parse_set(const Token &left_delimiter) -> shared_ptr<const Expression>
	{
		return proto_set_from(left_delimiter, parse_collection(RIGHT_SET_DELIMITER, "set"));
	}

	static const Token RIGHT_MAP_DELIMITER{Token::Type::R_CURLY_BRACE, "}", Token::NO_POS};
	auto Parser::parse_map(const Token &left_delimiter) -> shared_ptr<const Expression>
	{
		return proto_map_from(left_delimiter, parse_collection(RIGHT_MAP_DELIMITER, "map"));
	}

	static const Token RIGHT_FUN_APP_DELIMITER{Token::Type::R_PARENTHESIS, ")", Token::NO_POS};

	auto Parser::parse_function_application_or_record(const Token &left_delimiter) -> shared_ptr<const Expression>
	{
		const auto collection{parse_collection(RIGHT_FUN_APP_DELIMITER, "function application or record")};
		return !collection.empty() && collection[0]->is_name_value_pair()
		                       ? proto_record_from(left_delimiter, collection)
		                       : function_application_from(left_delimiter, collection);
	}

	auto Parser::parse_collection(const Token &expected_end, const string &display_name)
			-> Preliminary_Expr_Collection
	{
		Preliminary_Expr_Collection collection;
		while (has_more_to_parse() && !m_source.current_token().is_a(expected_end.type()))
		{
			const auto current_element{try_parse_accessor_expression()}; // Was try_parse_kv_expression
			                                                             // before introducing accessor
			collection.emplace_back(current_element);
		}
		if (!has_more_to_parse())
		{
			throw Syntax_Error{"Premature end of " + display_name +
			                                   " expression. Expected expression or closing delimiter " +
			                                   junkbox::text::single_quoted(expected_end.lexeme()) +
			                                   " but reached end of file.",
			                   m_source.current_token().position()};
		}
		if (!m_source.current_token().is_a(expected_end.type()))
		{
			throw Syntax_Error{"Unexpected continuation of " + display_name + ". Expected " +
			                                   junkbox::text::single_quoted(expected_end.lexeme()) +
			                                   " but found " +
			                                   junkbox::text::single_quoted(
									   m_source.current_token().lexeme()) +
			                                   ".",
			                   m_source.current_token().position()};
		}
		m_source.advance_to_next_token(); // Consume ')', '}' or ']'
		return collection;
	}

	auto Parser::parse_name(const Token &name_token) -> shared_ptr<const Expression>
	{
		if (this->m_source.current_token().is_ellipsis())
		{
			return parse_variadic_parameter(name_token);
		}
		else if (m_source.current_token().is_colon())
		{
			return parse_named_argument(name_token);
		}
		else
		{
			return name_from(name_token);
		}
	}

	auto Parser::parse_variadic_parameter(const Token &name_token) -> shared_ptr<const Expression>
	{
		const Token ellipsis{m_source.current_token()};
		m_source.advance_to_next_token(); // Consume '...'
		return make_shared<Variadic_Parameter>(ellipsis, name_from(name_token));
	}

	auto Parser::parse_named_argument(const Token &name_token) -> shared_ptr<const Expression>
	{
		const auto column_token{m_source.current_token()};
		m_source.advance_to_next_token(); // Consume ':'
		if (has_more_to_parse())
		{
			return make_shared<Name_Value_Pair_Expression>(column_token, name_from(name_token),
			                                               try_parse_kv_expression());
		}
		else
		{
			throw Syntax_Error{"Premature end of name/value pair. Expected right hand side expression.",
			                   m_source.position()};
		}
	}

	auto Parser::parse_quoted_expression(const Token &quote_token) -> shared_ptr<const Expression>
	{
		if (has_more_to_parse())
		{
			return make_shared<Quoted_Expression>(quote_token, try_parse_kv_expression());
		}
		else
		{
			throw Syntax_Error("Premature end of quoted expression. Expected expression but reached end of "
			                   "file.",
			                   m_source.position());
		}
	}

} // namespace appsl
