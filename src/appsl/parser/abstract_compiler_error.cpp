// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/abstract_compiler_error.hpp"
#include <fmt/core.h>

namespace appsl
{

	using fmt::format;

	Abstract_Compiler_Error::Abstract_Compiler_Error(const string &message,
	                                                 const Token::Position &position) noexcept
			: Base_Exception{message}, m_position{position}
	{
	}

	auto Abstract_Compiler_Error::line() const noexcept -> size_t { return m_position.first; }

	auto Abstract_Compiler_Error::column() const noexcept -> size_t { return m_position.second; }

	auto Abstract_Compiler_Error::pretty_message() const noexcept -> string
	{
		return format("{} error at line {}, column {}: {}", error_category_name(), line(), column(),
		              Base_Exception::message());
	}

} // namespace appsl
