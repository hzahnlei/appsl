// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/token.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	auto operator<<(ostream &out, const Token &token) -> ostream &
	{
		try
		{
			return out << "(TOKEN type:" << Token::debug_representation(token.type())
			           << " line:" << token.line() << " col:" << token.column()
			           << " val:" << text::single_quoted(token.lexeme()) << ")";
		}
		catch (...)
		{
			return out;
		}
	}

	auto Token::debug_representation(const Type token_type) -> string
	{
		switch (token_type)
		{
		case Type::COLON:
			return "COLON";
		case Type::COMMA:
			return "COMMA";
		case Type::ELLIPSIS:
			return "ELLIPSIS";
		case Type::END_OF_FILE:
			return "EOF";
		case Type::FULL_STOP:
			return "FULL_STOP";
		case Type::HASH:
			return "HASH";
		case Type::HASH_MAP:
			return "HASH_MAP";
		case Type::INTEGER_LITERAL:
			return "INTEGER_LITERAL";
		case Type::L_BRACKET:
			return "L_BRACKET";
		case Type::L_CURLY_BRACE:
			return "L_CURLY_BRACE";
		case Type::L_PARENTHESIS:
			return "L_PARENTHESIS";
		case Type::NAME:
			return "NAME";
		case Type::QUOTE:
			return "QUOTE";
		case Type::R_ARROW:
			return "R_ARROW";
		case Type::R_BRACKET:
			return "R_BRACKET";
		case Type::R_CURLY_BRACE:
			return "R_CURLY_BRACE";
		case Type::R_PARENTHESIS:
			return "R_PARENTHESIS";
		case Type::REAL_LITERAL:
			return "REAL_LITERAL";
		case Type::TEXT_LITERAL:
			return "TEXT_LITERAL";
		}
		return "Unknown token type: " + to_string(static_cast<int>(token_type));
	}

	auto Token::representation_for_error_reporting(const Type token_type) -> string
	{
		switch (token_type)
		{
		case Type::COLON:
			return ":";
		case Type::COMMA:
			return ",";
		case Type::ELLIPSIS:
			return "...";
		case Type::END_OF_FILE:
			return "EOF";
		case Type::FULL_STOP:
			return ".";
		case Type::HASH:
			return "#";
		case Type::HASH_MAP:
			return "#{";
		case Type::INTEGER_LITERAL:
			return "integer literal";
		case Type::L_BRACKET:
			return "[";
		case Type::L_CURLY_BRACE:
			return "{";
		case Type::L_PARENTHESIS:
			return "(";
		case Type::NAME:
			return "name";
		case Type::QUOTE:
			return "'";
		case Type::R_ARROW:
			return "->";
		case Type::R_BRACKET:
			return "]";
		case Type::R_CURLY_BRACE:
			return "}";
		case Type::R_PARENTHESIS:
			return ")";
		case Type::REAL_LITERAL:
			return "real literal";
		case Type::TEXT_LITERAL:
			return "text literal";
		}
		return "Unknown token type: " + to_string(static_cast<int>(token_type));
	}

	const Token Token::NONE{Type::END_OF_FILE, "", Position{0, 0}};

	Token::Token(const Type token_type, const Lexeme &lexeme, const Position &position) noexcept
			: m_type{token_type}, m_lexeme{lexeme}, m_position{position}
	{
	}

	auto Token::operator==(const Token &other) const -> bool
	{
		if (m_type != other.m_type)
		{
			return false;
		}
		else if (line() != other.line())
		{
			return false;
		}
		else if (column() != other.column())
		{
			return false;
		}
		else
		{
			return m_lexeme == other.m_lexeme;
		}
	}

	auto Token::is_a(const Type token_type) const noexcept -> bool { return m_type == token_type; }

	auto Token::is_colon() const noexcept -> bool { return Type::COLON == m_type; }

	auto Token::is_comma() const noexcept -> bool { return Type::COMMA == m_type; }

	auto Token::is_ellipsis() const noexcept -> bool { return Type::ELLIPSIS == m_type; }

	auto Token::is_eof() const noexcept -> bool { return Type::END_OF_FILE == m_type; }

	auto Token::is_full_stop() const noexcept -> bool { return Type::FULL_STOP == m_type; }

	auto Token::is_integer_literal() const noexcept -> bool { return Type::INTEGER_LITERAL == m_type; }

	auto Token::is_left_bracket() const noexcept -> bool { return Type::L_BRACKET == m_type; }

	auto Token::is_left_curly_brace() const noexcept -> bool { return Type::L_CURLY_BRACE == m_type; }

	auto Token::is_left_parenthesis() const noexcept -> bool { return Type::L_PARENTHESIS == m_type; }

	auto Token::is_any_literal() const noexcept -> bool
	{
		return Type::INTEGER_LITERAL == m_type || Type::REAL_LITERAL == m_type || Type::TEXT_LITERAL == m_type;
	}

	auto Token::is_name() const noexcept -> bool { return Type::NAME == m_type; }

	auto Token::is_real_literal() const noexcept -> bool { return Type::REAL_LITERAL == m_type; }

	auto Token::is_right_arrow() const noexcept -> bool { return Type::R_ARROW == m_type; }

	auto Token::is_right_bracket() const noexcept -> bool { return Type::R_BRACKET == m_type; }

	auto Token::is_right_curly_brace() const noexcept -> bool { return Type::R_CURLY_BRACE == m_type; }

	auto Token::is_right_parenthesis() const noexcept -> bool { return Type::R_PARENTHESIS == m_type; }

	auto Token::is_text_literal() const noexcept -> bool { return Type::TEXT_LITERAL == m_type; }

	auto Token::type() const noexcept -> Type { return m_type; }

	auto Token::lexeme() const noexcept -> const Lexeme & { return m_lexeme; }

	auto Token::line() const noexcept -> size_t { return m_position.first; }

	auto Token::column() const noexcept -> size_t { return m_position.second; }

	auto Token::position() const noexcept -> const Position & { return m_position; }

} // namespace appsl
