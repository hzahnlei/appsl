// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/syntax_error.hpp"

namespace appsl
{

	Syntax_Error::Syntax_Error(const string &message, const Token::Position &position) noexcept
			: Abstract_Compiler_Error{message, position}
	{
	}

	auto Syntax_Error::error_category_name() const noexcept -> string { return "Syntax"; }

} // namespace appsl
