// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/parser/scanner.hpp"
#include "appsl/parser/lexical_error.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	auto Scanner::is_single_line_comment(const Character curr_char) -> bool { return ';' == curr_char; }

	auto Scanner::is_whitespace_comment_or_hashbang(const Character curr_char, const Character next_char) -> bool
	{
		return isspace(curr_char) || is_single_line_comment(curr_char) || is_hash_bang(curr_char, next_char);
	}

	const string CHARACTERS_FORBIDDEN_IN_NAMES{".:,;(){}[]'\""};

	auto Scanner::is_forbidden_in_name(const Character some_char) -> bool
	{
		return string::npos != CHARACTERS_FORBIDDEN_IN_NAMES.find(static_cast<char>(some_char));
	}

	static const char *const LUT{"0123456789ABCDEF"};

	auto Scanner::hex_representation(const Character some_char) -> string
	{
		string result{"0x"};
		result.push_back(LUT[some_char >> 4]);
		result.push_back(LUT[some_char & 15]);
		return result;
	}

	auto Scanner::is_hash_bang(const Character curr_char, const Character next_char) -> bool
	{
		return '#' == curr_char && '!' == next_char;
	}

	Scanner::Scanner(unique_ptr<istringstream> source)
			: m_source(move(source)), m_current_token(scan_skipping_possible_ws_and_comments())
	{
	}

	Scanner::Scanner(string source) : Scanner(make_unique<istringstream>(source)) {}

	auto Scanner::position() const -> Token::Position { return Token::Position{m_line_count, m_columnCount}; }

	auto Scanner::tokens_available() const noexcept -> bool { return !m_current_token.is_eof(); }

	auto Scanner::current_token() const noexcept -> const Token & { return m_current_token; }

	auto Scanner::advance_to_next_token() -> bool
	{
		m_current_token = scan_skipping_possible_ws_and_comments();
		return !m_current_token.is_eof();
	}

	auto Scanner::scan_skipping_possible_ws_and_comments() -> Token
	{
		const auto curr_char{try_eliminate_whitespace_and_comments()};
		return EOF == curr_char ? Token{Token::Type::END_OF_FILE, "",
		                                Token::Position{m_line_count, m_columnCount}}
		                        : scan_with_all_ws_and_comments_removed(curr_char);
	}

	auto Scanner::try_eliminate_whitespace_and_comments() -> Character
	{
		auto curr_char{consume_current_char()};
		while (is_whitespace_comment_or_hashbang(curr_char, peek_next_char()))
		{
			while (isspace(curr_char))
			{
				curr_char = consume_current_char();
			}
			if (is_single_line_comment(curr_char))
			{
				while (EOF != curr_char && '\n' != curr_char)
				{
					curr_char = consume_current_char();
				}
			}
			if (is_hash_bang(curr_char, peek_next_char()))
			{
				while (EOF != curr_char && '\n' != curr_char)
				{
					curr_char = consume_current_char();
				}
				break; // Allow only one hash bang at the beginning.
			}
		}
		return curr_char;
	}

	auto Scanner::scan_with_all_ws_and_comments_removed(const Character curr_char) -> Token
	{
		const auto position{previous_position()};
		switch (curr_char)
		{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			return scan_number_literal(curr_char);
		case '"':
			return scan_text_literal();
		case ':':
			return Token{Token::Type::COLON, ":", position};
		case '\'':
			return Token{Token::Type::QUOTE, "'", position};
		case '.':
			return scan_full_stop_or_ellipsis();
		case ',':
			return Token{Token::Type::COMMA, ",", position};
		case '[':
			return Token{Token::Type::L_BRACKET, "[", position};
		case '{':
			return Token{Token::Type::L_CURLY_BRACE, "{", position};
		case '(':
			return Token{Token::Type::L_PARENTHESIS, "(", position};
		case '-':
			return scan_number_literal_or_name(curr_char);
		case '+':
			return scan_number_literal_or_name(curr_char);
		case ']':
			return Token{Token::Type::R_BRACKET, "]", position};
		case '}':
			return Token{Token::Type::R_CURLY_BRACE, "}", position};
		case ')':
			return Token{Token::Type::R_PARENTHESIS, ")", position};
		case '#':
			return scan_hash_or_map();
		default:
			return scan_name(curr_char, position);
		}
	}

	auto Scanner::scan_number_literal(const Character previous_char) -> Token
	{
		const auto position{previous_position()};
		string result{static_cast<char>(previous_char)};
		auto curr_char{consume_current_char()};
		while (EOF != curr_char && isdigit(curr_char))
		{
			result += static_cast<char>(curr_char);
			curr_char = consume_current_char();
		}
		if ('.' != curr_char)
		{
			unconsume_previous_char(curr_char);
			return Token{Token::Type::INTEGER_LITERAL, result, position};
		}
		else if (isdigit(peek_next_char()))
		{
			result += '.';
			curr_char = consume_current_char();
			while (EOF != curr_char && isdigit(curr_char))
			{
				result += static_cast<char>(curr_char);
				curr_char = consume_current_char();
			}
			unconsume_previous_char(curr_char);
			return Token{Token::Type::REAL_LITERAL, result, position};
		}
		else
		{
			throw Lexical_Error{"Premature end of real literal. Expected digits but found '.'.",
			                    previous_position()};
		}
	}

	auto Scanner::scan_text_literal() -> Token
	{
		const auto position{previous_position()};
		string result;
		auto curr_char{peek_next_char()};
		while (EOF != curr_char && '\n' != peek_next_char() && '"' != peek_next_char())
		{
			curr_char = consume_current_char();
			if ('\\' == curr_char && ('\\' == peek_next_char() || '"' == peek_next_char()))
			{
				curr_char = consume_current_char();
			}
			result += static_cast<char>(curr_char);
		}
		if ('\n' == peek_next_char())
		{
			throw Lexical_Error{"Premature end of text literal. Expected '\"' but reached end of line.",
			                    previous_position()};
		}
		else if ('"' != peek_next_char())
		{
			throw Lexical_Error{"Premature end of text literal. Expected '\"' but reached end of file.",
			                    previous_position()};
		}
		else
		{
			consume_current_char();
			return Token{Token::Type::TEXT_LITERAL, result, position};
		}
	}

	auto Scanner::scan_number_literal_or_name(const Character curr_char) -> Token
	{
		const auto position{previous_position()};
		if (curr_char == '-' && peek_next_char() == '>')
		{
			consume_current_char(); // '>'
			return Token{Token::Type::R_ARROW, "->", position};
		}
		return isdigit(peek_next_char())                        //
		                       ? scan_number_literal(curr_char) //
		                       : scan_name(curr_char, position);
	}

	auto Scanner::scan_name(const Character previous_char, const Token::Position &position) -> Token
	{
		string result{static_cast<char>(previous_char)};
		auto curr_char{consume_current_char()};
		while (EOF != curr_char)
		{
			if (is_whitespace_comment_or_hashbang(curr_char, peek_next_char()) ||
			    is_forbidden_in_name(curr_char) || (curr_char == '-' && peek_next_char() == '>'))
			{
				break;
			}
			else
			{
				result += static_cast<char>(curr_char);
				curr_char = consume_current_char();
			}
		}
		while (EOF != curr_char && curr_char == '\'')
		{
			result += static_cast<char>(curr_char);
			curr_char = consume_current_char();
		}
		unconsume_previous_char(curr_char);
		return Token{Token::Type::NAME, result, position};
	}

	auto Scanner::scan_full_stop_or_ellipsis() -> Token
	{
		const auto position{previous_position()};
		// First '.' already consumed.
		auto curr_char{peek_next_char()};
		if ('.' == curr_char)
		{
			consume_current_char(); // Consume '.'
			curr_char = peek_next_char();
			if ('.' == curr_char)
			{
				consume_current_char(); // Consume '.'
				return Token{Token::Type::ELLIPSIS, "...", position};
			}
			else if (EOF == curr_char)
			{
				throw Lexical_Error{"Premature end of ellipsis. Expected '.' but reached end of file.",
				                    this->position()};
			}
			else
			{
				throw Lexical_Error{"Premature end of ellipsis. Expected '.' but found " +
				                                    text::single_quoted(string{
										    static_cast<char>(curr_char)}) +
				                                    ".",
				                    this->position()};
			}
		}
		// else if (EOF == curr_char)
		// {
		//         throw Lexical_Error{"Premature end of ellipsis. Expected '.' but reached end of file.",
		//                             this->position()};
		// }
		// else
		// {
		//         throw Lexical_Error{"Premature end of ellipsis. Expected '.' but found " +
		//                                 text::single_quoted(string{static_cast<char>(curr_char)}) + ".",
		//                             this->position()};
		// }
		else
		{
			return Token{Token::Type::FULL_STOP, ".", position};
		}
	}

	auto Scanner::scan_hash_or_map() -> Token
	{
		const auto position{previous_position()};
		if (peek_next_char() == '{')
		{
			consume_current_char(); // '{'
			return Token{Token::Type::HASH_MAP, "#{", position};
		}
		return Token{Token::Type::HASH, "#", position};
	}

	auto Scanner::previous_position() const -> Token::Position
	{
		return Token::Position{m_previous_line_count, m_previous_column_count};
	}

	auto Scanner::peek_next_char() const -> Character { return m_source->peek(); }

	auto Scanner::consume_current_char() -> Character
	{
		const auto curr_char{m_source->get()};
		if (EOF != curr_char)
		{
			m_previous_line_count = m_line_count;
			m_previous_column_count = m_columnCount;
			if ('\n' == curr_char)
			{
				m_columnCount = 1;
				m_line_count++;
			}
			else
			{
				m_columnCount++;
			}
		}
		return curr_char;
	}

	auto Scanner::unconsume_previous_char(const Character previous_char) -> void
	{
		m_source->putback(static_cast<char>(previous_char));
		m_columnCount = m_previous_column_count;
		m_line_count = m_previous_line_count;
	}

	namespace scan
	{

		auto this_script(const string &source) -> unique_ptr<Scanner> { return make_unique<Scanner>(source); }

	} // namespace scan

} // namespace appsl
