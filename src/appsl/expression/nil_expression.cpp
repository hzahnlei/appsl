// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using fmt::format;

	const shared_ptr<const Expression> Nil_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Nil", Token::NO_POS})};

	const shared_ptr<const Nil_Expression> Nil_Expression::NIL{
			std::make_shared<Nil_Expression>(Token{Token::Type::END_OF_FILE, "", Token::NO_POS})};

	Nil_Expression::Nil_Expression(const Token &token) : m_token{token} {}

	Nil_Expression::Nil_Expression(Token &&token) : m_token{move(token)} {}

	auto Nil_Expression::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Nil_Expression const *const>(&other);
	}

	auto Nil_Expression::operator<=(const Expression &other) const -> bool { return *this == other; }

	auto Nil_Expression::operator<(const Expression &) const -> bool { return false; }

	auto Nil_Expression::clone() const -> unique_ptr<Expression> { return make_unique<Nil_Expression>(m_token); }

	auto Nil_Expression::token() const -> const Token & { return m_token; }

	static const string THEN_PARAM_NAME{"then"};
	static const string ELSE_PARAM_NAME{"else"};

	/**
	 * @brief Evaluate nil as a literl, constant function or binary function (to act as "false").
	 *
	 * This function originally was coded to simple. It did not distinguish the various cases as it does now.
	 * Therefore it was interpreted as an application of the binary function version of nil. This was the case when,
	 * by accident, somewhere on the call stack "then" and "else" had been installed. This ultimately lead to gready
	 * collection functions (map') to get stuck.
	 */
	auto Nil_Expression::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		if (stack.argument_count() == 0)
		{
			// No arguments given, nil not applied as a function but a literal.
			return shared_from_this();
		}
		else if (stack.argument_count() == 2)
		{
			if (stack.is_defined_locally(THEN_PARAM_NAME) && stack.is_defined_locally(ELSE_PARAM_NAME))
			{
				// then and else arguments given, nil applied as a function.
				const auto &else_branch{stack.lookup(ELSE_PARAM_NAME)};
				return else_branch.is_nil() ? shared_from_this() : else_branch.evaluate(stack);
			}
			// Wrong named arguments given.
			auto i{0U};
			string first;
			string second;
			stack.for_each_local(
					[&](const auto &name, const auto &)
					{
						if (i == 0)
							first = name;
						else if (i == 1)
							second = name;
						i++;
					});
			throw Runtime_Error{format(
					"Argument mismatch. nil can be either used as a literal, constant "
					"function (with no arguments) or as a binary function with arguments {} "
					"and {}. Arguments given are {} and {}.",
					THEN_PARAM_NAME, ELSE_PARAM_NAME, stack.argument_count(), first, second)};
		}
		else
		{
			// Too few or too many arguments given.
			throw Runtime_Error{
					format("Argument mismatch. nil can be either used as a literal, constant "
			                       "function (with no arguments) or as a binary function with arguments {} "
			                       "and {}. nil expects no or 2 arguments but {} arguments given.",
			                       THEN_PARAM_NAME, ELSE_PARAM_NAME, stack.argument_count())};
		}
	}

	auto Nil_Expression::is_nil() const -> bool { return true; }

	auto Nil_Expression::is_name() const -> bool { return false; }

	auto Nil_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Nil_Expression::is_built_in_function() const -> bool { return true; }

	auto Nil_Expression::is_function() const -> bool { return true; }

	auto Nil_Expression::is_function_application() const -> bool { return false; }

	auto Nil_Expression::is_fun_macro() const -> bool { return false; }

	auto Nil_Expression::is_integer() const -> bool { return false; }

	auto Nil_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Nil_Expression::is_real() const -> bool { return false; }

	auto Nil_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_shared<Quoted_Expression>(Token::NONE, make_shared<Name_Expression>(Token{
										    Token::Type::NAME, THEN_PARAM_NAME,
										    Token::NO_POS})),
			make_shared<List_Expression>(
					Token::NONE,
					make_shared<Quoted_Expression>(
							Token::NONE, make_shared<Name_Expression>(Token{
										     Token::Type::NAME, ELSE_PARAM_NAME,
										     Token::NO_POS})))};

	auto Nil_Expression::parameter_list() const -> const Expression & { return PARAMETERS; }

	auto Nil_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Nil_Expression::is_quoted() const -> bool { return false; }

	auto Nil_Expression::is_def_macro() const -> bool { return false; }

	auto Nil_Expression::is_name_value_pair() const -> bool { return false; }

	auto Nil_Expression::is_text() const -> bool { return false; }

	auto Nil_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Nil_Expression::is_list() const -> bool
	{
		return true; // Namely the empty one.
	}

	auto Nil_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:nil " << m_token << ")";
	}

	auto Nil_Expression::stringify(ostream &out, const string &) const -> void { out << "nil"; }

} // namespace appsl
