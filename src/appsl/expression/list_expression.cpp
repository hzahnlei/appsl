// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/abstract_collection_expression.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/parser/token.hpp"

namespace appsl
{

	const shared_ptr<const Expression> List_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "List", Token::NO_POS})};

	List_Expression::List_Expression(const Token &token, shared_ptr<const Expression> head,
	                                 shared_ptr<const Expression> tail)
			: Abstract_Collection_Expression{token, head, tail}
	{
	}

	auto List_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<List_Expression const *const>(&other)};
		return nullptr != other_list && head() == other_list->head() && tail() == other_list->tail();
	}

	auto List_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<List_Expression const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() <= other_list->tail();
	}

	auto List_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<List_Expression const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() < other_list->tail();
	}

	auto List_Expression::clone() const -> unique_ptr<Expression>
	{
		return make_unique<List_Expression>(token(), head().clone(), tail().clone());
	}

	auto List_Expression::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto List_Expression::is_list() const -> bool { return true; }

	auto List_Expression::type_of_collection() const -> string { return "list"; }

	auto List_Expression::opening_delimiter() const -> string { return "["; }

	auto List_Expression::closing_delimiter() const -> string { return "]"; }

} // namespace appsl
