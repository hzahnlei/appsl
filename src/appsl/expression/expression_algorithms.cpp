// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/runtime_error.hpp"

namespace appsl::expr
{

	auto for_each(const Expression &collection, const Action action) -> void
	{
		auto iter{const_cast<Expression *>(&collection)};
		while (iter->is_iterable())
		{
			action(iter->head());
			iter = const_cast<Expression *>(&(iter->tail()));
		}
	}

	auto all_of(const Expression &collection, const Predicate predicate) -> bool
	{
		auto iter{const_cast<Expression *>(&collection)};
		while (iter->is_iterable())
		{
			if (predicate(iter->head()))
			{
				iter = const_cast<Expression *>(&(iter->tail()));
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	auto any_of(const Expression &collection, const Predicate predicate) -> bool
	{
		auto iter{const_cast<Expression *>(&collection)};
		while (iter->is_iterable())
		{
			if (predicate(iter->head()))
			{
				return true;
			}
			else
			{
				iter = const_cast<Expression *>(&(iter->tail()));
			}
		}
		return false;
	}

	// TODO test
	auto none_of(const Expression &collection, const Predicate predicate) -> bool
	{
		auto iter{const_cast<Expression *>(&collection)};
		while (iter->is_iterable())
		{
			if (predicate(iter->head()))
			{
				return false;
			}
			else
			{
				iter = const_cast<Expression *>(&(iter->tail()));
			}
		}
		return true;
	}

	auto zip(const Expression &collection1, const Expression &collection2, const BiAction action)
			-> pair<const Expression &, const Expression &>
	{
		auto iter1{const_cast<Expression *>(&collection1)};
		auto iter2{const_cast<Expression *>(&collection2)};
		while (iter1->is_iterable() && iter2->is_iterable())
		{
			action(iter1->head(), iter2->head());
			iter1 = const_cast<Expression *>(&(iter1->tail()));
			iter2 = const_cast<Expression *>(&(iter2->tail()));
		}
		return pair<const Expression &, const Expression &>{*iter1, *iter2};
	}

	auto enumerate(const Expression &collection, const EnumAction action) -> void
	{
		auto i{0U};
		auto iter{const_cast<Expression *>(&collection)};
		while (iter->is_iterable())
		{
			action(iter->head(), i);
			iter = const_cast<Expression *>(&(iter->tail()));
			i++;
		}
	}

	auto nth(const Expression &collection, const size_t index) -> const Expression &
	{
		auto i{index};
		auto iter{const_cast<Expression *>(&collection)};
		while (i > 0 && iter->is_iterable())
		{
			iter = const_cast<Expression *>(&(iter->tail()));
			i--;
		}
		if (i == 0)
		{
			return iter->head();
		}
		else
		{
			throw Runtime_Error{"Index " + to_string(index) + "out of bounds."};
		}
	}

	// TODO test
	auto find(const Expression &collection, const Predicate predicate) -> const Expression &
	{
		auto iter{const_cast<Expression *>(&collection)};
		while (iter->is_iterable())
		{
			if (predicate(iter->head()))
			{
				return iter->head();
			}
			else
			{
				iter = const_cast<Expression *>(&(iter->tail()));
			}
		}
		return *Nil_Expression::NIL;
	}

} // namespace appsl::expr
