// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/record_expression.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using fmt::format;
	using junkbox::algo::for_each;
	using namespace junkbox;

	const shared_ptr<const Expression> Record_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Record", Token::NO_POS})};

	auto remaining_names(const Token &token, const Record_Expression::Name_Value_List &entries,
	                     Record_Expression::Const_NV_Iter &curr_entry) -> shared_ptr<const Expression>
	{
		if (curr_entry == entries.cend())
		{
			return Nil_Expression::NIL;
		}
		const auto &curr_nv{*curr_entry};
		curr_entry++;
		return make_shared<List_Expression>(token, curr_nv->head().shared_from_this(),
		                                    remaining_names(token, entries, curr_entry));
	}

	auto Record_Expression::list_of_names(const Token &token, const Name_Value_List &entries)
			-> shared_ptr<const Expression>
	{
		auto curr_entry{entries.cbegin()};
		return remaining_names(token, entries, curr_entry);
	}

	Record_Expression::Record_Expression(const Token &token, Name_Value_List &&collection)
			: m_token{token}, m_collection{move(collection)}
	{
		// FIXME assertions?
		m_list_of_names = list_of_names(m_token, m_collection);
	}

	Record_Expression::Record_Expression(Token &&token, Name_Value_List &&collection)
			: m_token{move(token)}, m_collection{move(collection)}
	{
		// FIXME assertions?
		m_list_of_names = list_of_names(m_token, m_collection);
	}

	auto Record_Expression::operator==(const Expression &other) const -> bool
	{
		// TODO
		return nullptr != dynamic_cast<Record_Expression const *const>(&other);
	}

	auto Record_Expression::operator<=(const Expression &other) const -> bool
	{
		// TODO
		return *this == other;
	}

	auto Record_Expression::operator<(const Expression &) const -> bool
	{
		// TODO
		return false;
	}

	auto Record_Expression::clone() const -> unique_ptr<Expression>
	{
		Name_Value_List copy;
		for_each(m_collection, [&](const auto &nv) { copy.emplace_back(nv->clone()); });
		return make_unique<Record_Expression>(m_token, move(copy));
	}

	auto Record_Expression::token() const -> const Token & { return m_token; }

	auto Record_Expression::head() const -> const Expression & { return m_list_of_names->head(); }

	auto Record_Expression::tail() const -> const Expression & { return m_list_of_names->tail(); }

	auto Record_Expression::find_nv_pair(const Name_Value_List &collection, const string &name) -> Const_NV_Iter
	{
		auto current_element{collection.cbegin()};
		while (current_element != collection.cend())
		{
			if ((*current_element)->head().name() == name)
			{
				return current_element;
			}
			current_element++;
		}
		return current_element;
	}

	auto all_field_names(const Expression &names)
	{
		string stringified;
		auto i{0U};
		expr::for_each(names,
		               [&](const auto &name)
		               {
				       if (i > 0)
				       {
					       stringified += ", ";
				       }
				       stringified += text::single_quoted(name.name());
				       i++;
			       });
		return stringified;
	}

	static const string NAME_PARAM_NAME{"name"};

	auto Record_Expression::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		if (stack.argument_count() == 0)
		{
			return shared_from_this();
		}
		// The argument to this function is the name of one of the fields. That name is not guaranteed to be
		// defined. This would lead to error messages when defining functions. Therefore we allow the name in
		// that function body to be quoted and silently unquote it here. Nothing will happen, if it unquoted
		// already. But, be advised: The preferred way to access field names is the field accessor syntax. It
		// uses a dot notation like many other programmin languages. Therefore it looks more familiar. And, as a
		// special case, the name (quoted or not) does not get checkt when defining a function.
		const auto &name{expr::gracefully_unquoted(stack.lookup(NAME_PARAM_NAME))};
		if (!name.is_name())
		{
			throw Semantic_Error{format("The fields of a record are accessed by their name. This {} ist "
			                            "not a name. Valid field names of this record are: {}.",
			                            text::single_quoted(name.stringify()),
			                            all_field_names(*m_list_of_names)),
			                     name.token().position()};
		}
		const auto hit{find_nv_pair(m_collection, name.name())};
		if (hit == m_collection.cend())
		{
			throw Semantic_Error{format("The fields of a record are accessed by their name. This record {} "
			                            "has no field {}. Valid field names of this record are: {}.",
			                            text::single_quoted(Expression::stringify()),
			                            text::single_quoted(name.stringify()),
			                            all_field_names(*m_list_of_names)),
			                     name.token().position()};
		}
		// TODO helper functions in expression traits to extract name or value etc.
		return (*hit)->tail().head().clone();
	}

	auto Record_Expression::is_nil() const -> bool { return false; }

	auto Record_Expression::is_name() const -> bool { return false; }

	auto Record_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Record_Expression::is_built_in_function() const -> bool { return false; }

	auto Record_Expression::is_function() const -> bool { return true; }

	auto Record_Expression::is_function_application() const -> bool { return false; }

	auto Record_Expression::is_fun_macro() const -> bool { return false; }

	auto Record_Expression::is_integer() const -> bool { return false; }

	auto Record_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Record_Expression::is_real() const -> bool { return false; }

	auto Record_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	// TODO write expression factory to more easily produce expressions like quoted(name()) etc.
	static const auto PARAMETERS{make_shared<Quoted_Expression>(
			Token{Token::Type::QUOTE, NAME_PARAM_NAME, Token::NO_POS},
			make_unique<Name_Expression>(Token{Token::Type::NAME, NAME_PARAM_NAME, Token::NO_POS}))};

	auto Record_Expression::parameter_list() const -> const Expression & { return *PARAMETERS; }

	auto Record_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Record_Expression::is_quoted() const -> bool { return false; }

	auto Record_Expression::is_def_macro() const -> bool { return false; }

	auto Record_Expression::is_name_value_pair() const -> bool { return false; }

	auto Record_Expression::is_text() const -> bool { return false; }

	auto Record_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Record_Expression::is_list() const -> bool { return false; }

	//---- Composite expression (record) ----------------------------------

	auto Record_Expression::has_property(const string &name) const -> bool
	{
		const auto hit{find_nv_pair(m_collection, name)};
		return hit != m_collection.cend();
	}

	auto Record_Expression::by_name(const string &name) const -> const Expression &
	{
		// if (!name.is_name())
		// {
		// 	throw Semantic_Error{format("The fields of a record are accessed by their name. This {} ist "
		// 	                            "not a name. Valid field names of this record are: {}.",
		// 	                            text::single_quoted(name.stringify()),
		// 	                            all_field_names(*m_list_of_names)),
		// 	                     name.token().position()};
		// }
		const auto hit{find_nv_pair(m_collection, name)};
		if (hit == m_collection.cend())
		{
			throw Runtime_Error{format("The fields of a record are accessed by their name. This record "
			                           "has no field {}. Valid field names of this record are: {}.",
			                           text::single_quoted(name), all_field_names(*m_list_of_names))};
		}
		return (*hit)->tail().head(); // k/v pair: tail = value, head = name
	}

	auto Record_Expression::operator<<(ostream &out) const -> ostream &
	{
		out << "(EXPR type:record content:(";
		auto i{0U};
		for_each(m_collection,
		         [&](const auto &expr)
		         {
				 if (i > 0)
				 {
					 out << ' ';
				 }
				 out << expr;
				 i++;
			 });
		return out << ") " << m_token << ')';
	}

	auto Record_Expression::stringify(ostream &out, const string &indentation) const -> void
	{
		out << "(";
		auto i{0U};
		for_each(m_collection,
		         [&](const auto &expr)
		         {
				 if (i > 0)
				 {
					 out << ' ';
				 }
				 expr->stringify(out, indentation);
				 i++;
			 });
		out << ')';
	}

} // namespace appsl
