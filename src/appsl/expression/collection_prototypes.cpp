// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/collection_prototypes.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/name_value_pair_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/record_expression.hpp"
#include "appsl/expression/set_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{
	using junkbox::algo::any_of;
	using junkbox::algo::for_each;
	using namespace junkbox;

	//---- Abstract collection prototype ----------------------------------

	Abstract_Collection_Prototype::Abstract_Collection_Prototype(const Token &token,
	                                                             shared_ptr<const Expression> head,
	                                                             shared_ptr<const Expression> tail)
			: m_token{token}, m_head{head}, m_tail{tail}
	{
	}

	auto Abstract_Collection_Prototype::token() const -> const Token & { return m_token; }

	auto Abstract_Collection_Prototype::head() const -> const Expression & { return *m_head; }

	auto Abstract_Collection_Prototype::tail() const -> const Expression & { return *m_tail; }

	auto Abstract_Collection_Prototype::is_nil() const -> bool { return false; }

	auto Abstract_Collection_Prototype::is_name() const -> bool { return false; }

	auto Abstract_Collection_Prototype::name() const -> const string & { NOT_SUPPORTED(); }

	auto Abstract_Collection_Prototype::is_built_in_function() const -> bool { return false; }

	auto Abstract_Collection_Prototype::is_function() const -> bool { return false; }

	auto Abstract_Collection_Prototype::is_function_application() const -> bool { return false; }

	auto Abstract_Collection_Prototype::is_fun_macro() const -> bool { return false; }

	auto Abstract_Collection_Prototype::is_integer() const -> bool { return false; }

	auto Abstract_Collection_Prototype::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Abstract_Collection_Prototype::is_real() const -> bool { return false; }

	auto Abstract_Collection_Prototype::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Abstract_Collection_Prototype::parameter_list() const -> const Expression & { NOT_SUPPORTED(); }

	auto Abstract_Collection_Prototype::is_variadic_parameter() const -> bool { return false; }

	auto Abstract_Collection_Prototype::is_quoted() const -> bool { return false; }

	auto Abstract_Collection_Prototype::is_def_macro() const -> bool { return false; }

	auto Abstract_Collection_Prototype::is_name_value_pair() const -> bool { return false; }

	auto Abstract_Collection_Prototype::is_text() const -> bool { return false; }

	auto Abstract_Collection_Prototype::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Abstract_Collection_Prototype::tagged_expression() const -> const Expression & { return *this; }

	auto Abstract_Collection_Prototype::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:" << type_of_collection() << " " << m_token << " head:" << *m_head
		           << " tail:" << *m_tail << ")";
	}

	auto Abstract_Collection_Prototype::stringify(ostream &out, const string &indentation) const -> void
	{
		out << opening_delimiter();
		stringify_all_elements(out, indentation);
		out << closing_delimiter();
	}

	auto Abstract_Collection_Prototype::stringify_all_elements(ostream &out, const string &indentation) const
			-> void
	{
		auto i{0U};
		expr::for_each(*this,
		               [&](const auto &element)
		               {
				       if (i > 0)
				       {
					       out << " ";
				       };
				       element.stringify(out, indentation);
				       i++;
			       });
	}

	//---- List expression prototype --------------------------------------

	const shared_ptr<const Expression> List_Prototype::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "List-Prototype", Token::NO_POS})};

	List_Prototype::List_Prototype(const Token &token, shared_ptr<const Expression> head,
	                               shared_ptr<const Expression> tail)
			: Abstract_Collection_Prototype{token, head, tail}
	{
	}

	auto List_Prototype::operator==(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<List_Prototype const *const>(&other)};
		return nullptr != other_list && head() == other_list->head() && tail() == other_list->tail();
	}

	auto List_Prototype::operator<=(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<List_Prototype const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() <= other_list->tail();
	}

	auto List_Prototype::operator<(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<List_Prototype const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() < other_list->tail();
	}

	auto List_Prototype::clone() const -> unique_ptr<Expression>
	{
		return make_unique<List_Prototype>(token(), head().clone(), tail().clone());
	}

	auto List_Prototype::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		return make_shared<List_Expression>(token(), head().evaluate(stack), tail().evaluate(stack));
	}

	auto List_Prototype::is_list() const -> bool { return false; }

	auto List_Prototype::tag() const -> const Expression & { return *TYPE_TAG; }

	auto List_Prototype::type_of_collection() const -> string { return "list-proto"; }

	auto List_Prototype::opening_delimiter() const -> string { return "["; }

	auto List_Prototype::closing_delimiter() const -> string { return "]"; }

	//---- Map expression prototype ---------------------------------------

	const shared_ptr<const Expression> Map_Prototype::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Map-Prototype", Token::NO_POS})};

	Map_Prototype::Map_Prototype(const Token &token, shared_ptr<const Expression> head,
	                             shared_ptr<const Expression> tail)
			: Abstract_Collection_Prototype{token, head, tail}
	{
	}

	auto Map_Prototype::operator==(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Map_Prototype const *const>(&other)};
		return nullptr != other_list && head() == other_list->head() && tail() == other_list->tail();
	}

	auto Map_Prototype::operator<=(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Map_Prototype const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() <= other_list->tail();
	}

	auto Map_Prototype::operator<(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Map_Prototype const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() < other_list->tail();
	}

	auto Map_Prototype::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Map_Prototype>(token(), m_head->clone(), m_tail->clone());
	}

	auto Map_Prototype::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		Map_Literal_Expression::Key_Value_Map evaluated_collection;
		expr::for_each(*this,
		               [&](const auto &kv)
		               {
				       if (!kv.is_key_value_pair())
				       {
					       throw Semantic_Error{
							       "Maps must be made up of key/value pairs only. " +
									       text::single_quoted(kv.stringify()) +
									       " is no key/value pair.",
							       kv.token().position()};
				       }

				       const auto &raw_key{kv.head()}; // Key
				       const auto evaluated_key{raw_key.evaluate(stack)};
				       if (evaluated_collection.find(evaluated_key) != evaluated_collection.cend())
				       {
					       throw Semantic_Error{
							       "Duplicate key " +
									       text::single_quoted(
											       raw_key.stringify()) +
									       " in map.",
							       raw_key.token().position()};
				       }
				       if (!expr::is_literal_or_collection_thereof(*evaluated_key))
				       {
					       throw Semantic_Error{"Key " + text::single_quoted(raw_key.stringify()) +
				                                                    " expressions for maps have to be "
				                                                    "literal expressions.",
				                                    raw_key.token().position()};
				       }
				       const auto &raw_value{kv.tail()}; // Value
				       const auto evaluated_value{raw_value.evaluate(stack)};
				       if (!expr::is_literal_or_collection_thereof(*evaluated_value))
				       {
					       throw Semantic_Error{
							       "Value " + text::single_quoted(raw_value.stringify()) +
									       " expressions for maps have to be "
									       "literal expressions.",
							       raw_value.token().position()};
				       }
				       evaluated_collection.try_emplace(evaluated_key, evaluated_value);
			       });
		return make_shared<Map_Literal_Expression>(token(), move(evaluated_collection));
	}

	auto Map_Prototype::is_list() const -> bool { return false; }

	auto Map_Prototype::tag() const -> const Expression & { return *TYPE_TAG; }

	auto Map_Prototype::type_of_collection() const -> string { return "map-proto"; }

	auto Map_Prototype::opening_delimiter() const -> string { return "#{"; }

	auto Map_Prototype::closing_delimiter() const -> string { return "}"; }

	auto Map_Prototype::stringify(ostream &out, const string &) const -> void
	{
		out << opening_delimiter();
		auto i{0U};
		expr::for_each(*this,
		               [&](const auto &expr)
		               {
				       if (i > 0)
				       {
					       out << ' ';
				       }
				       out << expr.head().stringify();
				       out << " -> ";
				       out << expr.tail().head().stringify();
				       i++;
			       });
		out << closing_delimiter();
	}

	//---- Record expression prototype ------------------------------------

	const shared_ptr<const Expression> Record_Prototype::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Record-Prototype", Token::NO_POS})};

	Record_Prototype::Record_Prototype(const Token &token, shared_ptr<const Expression> head,
	                                   shared_ptr<const Expression> tail)
			: Abstract_Collection_Prototype{token, head, tail}
	{
	}

	auto Record_Prototype::operator==(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Record_Prototype const *const>(&other)};
		return nullptr != other_list && head() == other_list->head() && tail() == other_list->tail();
	}

	auto Record_Prototype::operator<=(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Record_Prototype const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() <= other_list->tail();
	}

	auto Record_Prototype::operator<(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Record_Prototype const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() < other_list->tail();
	}

	auto Record_Prototype::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Record_Prototype>(token(), m_head->clone(), m_tail->clone());
	}

	auto contains_name(const Record_Expression::Name_Value_List &collection, const string &name) -> bool
	{
		return any_of(collection, [&](const auto &nv_pair) { return nv_pair->head().name() == name; });
	}

	auto Record_Prototype::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		Record_Expression::Name_Value_List evaluated_collection;
		expr::for_each(*this,
		               [&](const auto &nv)
		               {
				       if (!nv.is_name_value_pair())
				       {
					       throw Semantic_Error{
							       "Records must be made up of name/value pairs only. " +
									       text::single_quoted(nv.stringify()) +
									       " is no name/value pair.",
							       nv.token().position()};
				       }

				       const auto &name{nv.head()};
				       if (!name.is_name())
				       {
					       throw Semantic_Error{
							       "Field name " + text::single_quoted(name.stringify()) +
									       " expressions for records have to be "
									       "names.",
							       name.token().position()};
				       }
				       if (contains_name(evaluated_collection, name.name()))
				       {
					       throw Semantic_Error{
							       "Duplicate field name " +
									       text::single_quoted(name.stringify()) +
									       " in record.",
							       name.token().position()};
				       }
				       // It is all wrapped in function application cells. For records we need to unbox
			               // the actual name value pairs.
				       const auto &raw_value{nv.tail().head()};
				       const auto evaluated_value{raw_value.evaluate(stack)};
				       if (!expr::is_literal_or_collection_thereof(*evaluated_value))
				       {
					       throw Semantic_Error{
							       "Value " + text::single_quoted(raw_value.stringify()) +
									       " expressions for records have to be "
									       "literal expressions.",
							       raw_value.token().position()};
				       }
				       evaluated_collection.emplace_back(make_shared<Name_Value_Pair_Expression>(
						       nv.token(), name.clone(), evaluated_value));
			       });
		return make_shared<Record_Expression>(token(), move(evaluated_collection));
	}

	auto Record_Prototype::is_list() const -> bool { return false; }

	auto Record_Prototype::tag() const -> const Expression & { return *TYPE_TAG; }

	auto Record_Prototype::type_of_collection() const -> string { return "record-proto"; }

	auto Record_Prototype::opening_delimiter() const -> string { return "("; }

	auto Record_Prototype::closing_delimiter() const -> string { return ")"; }

	auto Record_Prototype::stringify(ostream &out, const string &) const -> void
	{
		out << opening_delimiter();
		auto i{0U};
		expr::for_each(*this,
		               [&](const auto &expr)
		               {
				       if (i > 0)
				       {
					       out << ' ';
				       }
				       out << expr.head().stringify(); // Name
				       out << ": ";
				       out << expr.tail().head().stringify(); // Value
				       i++;
			       });
		out << closing_delimiter();
	}

	//---- Set expression prototype ---------------------------------------

	const shared_ptr<const Expression> Set_Prototype::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Set-Prototype", Token::NO_POS})};

	Set_Prototype::Set_Prototype(const Token &token, shared_ptr<const Expression> head,
	                             shared_ptr<const Expression> tail)
			: Abstract_Collection_Prototype{token, head, tail}
	{
	}

	auto Set_Prototype::operator==(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Set_Prototype const *const>(&other)};
		return nullptr != other_list && head() == other_list->head() && tail() == other_list->tail();
	}

	auto Set_Prototype::operator<=(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Set_Prototype const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() <= other_list->tail();
	}

	auto Set_Prototype::operator<(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Set_Prototype const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() < other_list->tail();
	}

	auto Set_Prototype::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Set_Prototype>(token(), head().clone(), tail().clone());
	}

	auto Set_Prototype::is_list() const -> bool { return false; }

	auto Set_Prototype::is_set() const -> bool { return true; }

	using Expression_List = vector<shared_ptr<const Expression>>;

	auto contains(const Expression_List &collection, const Expression &expression) -> bool
	{
		return any_of(collection, [&](const auto &other) { return expression == *other; });
	}

	auto stringified(const Expression &raw, const Expression &evaluated)
	{
		const auto stringified_raw{text::single_quoted(raw.stringify())};
		const auto stringified_evaluated{text::single_quoted(evaluated.stringify())};
		return stringified_raw == stringified_evaluated ? stringified_raw
		                                                : stringified_raw + " = " + stringified_evaluated;
	}

	using Const_Expression_Iter = Expression_List::const_iterator;

	auto remaining_set_elements(const Token &token, const Expression_List &collection,
	                            Const_Expression_Iter &curr_elem) -> shared_ptr<const Expression>
	{
		if (curr_elem == collection.cend())
		{
			return Nil_Expression::NIL;
		}
		const auto &lhs{*curr_elem};
		curr_elem++;
		return make_shared<Set_Expression>(token, lhs, remaining_set_elements(token, collection, curr_elem));
	}

	auto Set_Prototype::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		Record_Expression::Name_Value_List evaluated_collection;
		expr::for_each(*this,
		               [&](const auto &element)
		               {
				       const auto evaluated_element{element.evaluate(stack)};
				       if (contains(evaluated_collection, *evaluated_element))
				       {
					       throw Semantic_Error{
							       "Duplicate element " +
									       stringified(element,
				                                                           *evaluated_element) +
									       " in set. A set must be made up from "
									       "unique elements. Consider using a list "
									       "instead of a set in case you need "
									       "duplicate elements.",
							       element.token().position()};
				       }
				       evaluated_collection.emplace_back(evaluated_element);
			       });
		auto curr_elem{evaluated_collection.cbegin()};
		return remaining_set_elements(token(), evaluated_collection, curr_elem);
	}

	auto Set_Prototype::tag() const -> const Expression & { return *TYPE_TAG; }

	auto Set_Prototype::type_of_collection() const -> string { return "set-proto"; }

	auto Set_Prototype::opening_delimiter() const -> string { return "{"; }

	auto Set_Prototype::closing_delimiter() const -> string { return "}"; }

} // namespace appsl
