// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/set_expression.hpp"
#include "appsl/expression/abstract_collection_expression.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/parser/token.hpp"

namespace appsl
{

	const shared_ptr<const Expression> Set_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Set", Token::NO_POS})};

	Set_Expression::Set_Expression(const Token &token, shared_ptr<const Expression> head,
	                               shared_ptr<const Expression> tail)
			: Abstract_Collection_Expression{token, head, tail}
	{
	}

	auto Set_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Set_Expression const *const>(&other)};
		return nullptr != other_list && head() == other_list->head() && tail() == other_list->tail();
	}

	auto Set_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Set_Expression const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() <= other_list->tail();
	}

	auto Set_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Set_Expression const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() < other_list->tail();
	}

	auto Set_Expression::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Set_Expression>(token(), head().clone(), tail().clone());
	}

	auto Set_Expression::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Set_Expression::type_of_collection() const -> string { return "set"; }

	auto Set_Expression::opening_delimiter() const -> string { return "{"; }

	auto Set_Expression::closing_delimiter() const -> string { return "}"; }

} // namespace appsl
