// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using fmt::format;
	using namespace junkbox;

	//---- Iterable expression --------------------------------------------

	/**
	 * @brief Iterable expressions are often atoms.
	 */
	inline auto Expression::tail() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Expression::custom_object_type() const -> const Expression & { return *Nil_Expression::NIL; }

	//---- Composite expression (record) ----------------------------------

	auto Expression::holds_properties() const -> bool { return false; }

	auto Expression::has_property(const string &) const -> bool { return false; }

	auto Expression::by_name(const string &name) const -> const Expression &
	{
		throw Runtime_Error{format("Trying to access field {} of what is supposed to be a record. However, "
		                           "this {} is no record, it is a {}.",
		                           text::single_quoted(name), text::single_quoted(this->stringify()),
		                           text::single_quoted(tag().stringify()))};
	}

	auto Expression::by_key(const Expression &key) const -> const Expression &
	{
		throw Runtime_Error{
				format("Trying to access value with key {} of what is supposed to be a map. However, "
		                       "this {} is no map, it is a {}.",
		                       text::single_quoted(key.stringify()), text::single_quoted(this->stringify()),
		                       text::single_quoted(tag().stringify()))};
	}

	auto operator<<(ostream &out, const Expression &expression) -> ostream & { return expression << out; }

	auto operator<<(ostream &out, const Expression *const expression) -> ostream &
	{
		return nullptr == expression ? out << string{"nullptr"} : expression->operator<<(out);
	}

	auto Expression::stringify() const -> string
	{
		stringstream result;
		stringify(result, "");
		return result.str();
	}

} // namespace appsl
