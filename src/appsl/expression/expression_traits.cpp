// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include "appsl/use_stl.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl::expr
{

	using namespace junkbox;

	inline auto is_integer_literal(const Expression &expression) { return expression.is_integer(); }

	auto is_list_of_integers(const Expression &collection) -> bool
	{
		return all_of(collection, is_integer_literal);
	}

	inline auto function_from_function_application(const Expression &function_application) -> const Expression &
	{
		return function_application.head();
	}

	auto is_free(const Expression &expressions, const Stack_Frame &stack) -> bool
	{ // TODO function?
		if (expressions.is_function_application())
		{
			// WIP check body for undefined names?
			const auto &fun{function_from_function_application(expressions)};
			return fun.is_name() ? stack.lookup(fun.name()).is_fun_macro() : fun.is_fun_macro();
		}
		else if (expressions.is_name())
		{
			return !stack.is_defined(expressions.name());
		}
		else if (is_literal(expressions))
		{
			return false;
		}
		else
		{
			return expr::any_of(expressions, [&](const auto &expr) { return is_free(expr, stack); });
		}
	}

	auto count(const Expression &collection) -> size_t
	{
		auto count{0U};
		for_each(collection, [&](const auto &) { count++; });
		return count;
	}

	auto unquoted(const Expression &quoted_expression) -> const Expression &
	{
		DEV_ASSERT(quoted_expression.is_quoted(), "Not a quoted expression.");
		return quoted_expression.head();
	}

	auto gracefully_unquoted(const Expression &quoted_expression) -> const Expression &
	{
		return quoted_expression.is_quoted() ? quoted_expression.head() : quoted_expression;
	}

	auto unvariadic(const Expression &variadic_param) -> const Expression &
	{
		DEV_ASSERT(variadic_param.is_variadic_parameter(), "Not a variadic parameter.");
		return variadic_param.head();
	}

	auto gracefully_unvariadic(const Expression &variadic_param) -> const Expression &
	{
		return variadic_param.is_variadic_parameter() ? variadic_param.head() : variadic_param;
	}

	auto is_literal(const Expression &expression) -> bool
	{
		return expression.is_nil() || expression.is_integer() || expression.is_real() || expression.is_text() ||
		       expression.is_custom_object() || expression.is_map() || expression.is_list() ||
		       expression.is_set() || expression.is_record() || expression.is_name_value_pair() ||
		       expression.is_key_value_pair() || (expression.is_tagged() && is_literal(expression.head()));
	}

	auto is_collection_of_literals(const Expression &expression)
	{
		// return !expression.head().is_nil() && !expression.tail().is_nil() &&
		//        all_of(expression, is_literal_or_collection_thereof);
		return expression.is_iterable() && all_of(expression, is_literal_or_collection_thereof);
	}

	auto is_literal_or_collection_thereof(const Expression &expression) -> bool
	{
		return is_literal(expression) || is_collection_of_literals(expression);
	}

	auto names_from(const Expression &expression, Set_of_Names &names) -> void
	{
		if (expression.is_name())
		{
			names.emplace(expression.name());
		}
		else if (expression.is_quoted())
		{
			names_from(unquoted(expression), names);
		}
		else if (expression.is_variadic_parameter())
		{
			names_from(unvariadic(expression), names);
		}
		else if (!expression.head().is_nil() && !is_literal(expression))
		{
			names_from(expression.head(), names);
			if (!expression.tail().is_nil())
			{
				names_from(expression.tail(), names);
			}
		}
	}

	auto names_from(const Expression &expression) -> Set_of_Names
	{
		Set_of_Names names;
		names_from(expression, names);
		return names;
	}

	auto is_number(const Expression &expression) -> bool { return expression.is_integer() || expression.is_real(); }

	auto pure_param_name_without_quotes_etc(const Expression &expression) -> const Expression &
	{
		if (expression.is_name())
		{
			return expression;
		}
		else if (expression.is_quoted())
		{
			return pure_param_name_without_quotes_etc(expr::unquoted(expression));
		}
		else if (expression.is_variadic_parameter())
		{
			return pure_param_name_without_quotes_etc(expr::unvariadic(expression));
		}
		else
		{
			throw Semantic_Error{"Parameter " + text::single_quoted(expression.stringify()) +
			                                     " has to be a name, quoted parameter or (quoted) variadic "
			                                     "parameter.",
			                     expression.token().position()};
		}
	}

	auto pure_argument_value_without_name(const Expression &expression) -> const Expression &
	{
		DEV_ASSERT(expression.is_name_value_pair(), "Not a name/value pair.");
		return expression.tail();
	}

	auto is_valid_type_name(const Expression &expression) -> bool
	{
		return expression.is_name() && !expression.name().empty() && isupper(expression.name()[0]) &&
		       isalpha(expression.name()[0]) &&
		       junkbox::algo::all_of(expression.name(), [](const auto c) { return isalnum(c) || c == '_'; });
	}

	auto gracefully_untagged(const Expression &tagged_expression) -> const Expression &
	{
		return tagged_expression.is_tagged() ? tagged_expression.head() : tagged_expression;
	}

} // namespace appsl::expr
