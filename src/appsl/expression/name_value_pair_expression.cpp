// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/name_value_pair_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{
	const shared_ptr<const Expression> Name_Value_Pair_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Name-Value-Pair", Token::NO_POS})};

	Name_Value_Pair_Expression::Name_Value_Pair_Expression(const Token &token,
	                                                       const shared_ptr<const Expression> &name,
	                                                       const shared_ptr<const Expression> &value)
			: m_token{token}, m_name{name},
			  m_value{make_shared<List_Expression>(token, value)} // FIXME why list?
	{
		DEV_ASSERT(nullptr != m_name, "Name must not be nullptr.");
		DEV_ASSERT(m_name->is_name(), "Name must be a name expression.");
		DEV_ASSERT(nullptr != m_value, "Value must not be nullptr.");
	}

	Name_Value_Pair_Expression::Name_Value_Pair_Expression(Token &&token, const shared_ptr<const Expression> &name,
	                                                       const shared_ptr<const Expression> &value)
			: m_token{move(token)}, m_name{name},
			  m_value{make_shared<List_Expression>(token, value)} // FIXME why list?
	{
		DEV_ASSERT(nullptr != m_name, "Name must not be nullptr.");
		DEV_ASSERT(m_name->is_name(), "Name must be a name expression.");
		DEV_ASSERT(nullptr != m_value, "Value must not be nullptr.");
	}

	auto Name_Value_Pair_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_pair{dynamic_cast<Name_Value_Pair_Expression const *const>(&other)};
		return nullptr != other_pair && *m_name == *other_pair->m_name && *m_value == *other_pair->m_value;
	}

	auto Name_Value_Pair_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_pair{dynamic_cast<Name_Value_Pair_Expression const *const>(&other)};
		return nullptr != other_pair && *m_name == *other_pair->m_name && *m_value <= *other_pair->m_value;
	}

	auto Name_Value_Pair_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_pair{dynamic_cast<Name_Value_Pair_Expression const *const>(&other)};
		return nullptr != other_pair && *m_name == *other_pair->m_name && *m_value < *other_pair->m_value;
	}

	auto Name_Value_Pair_Expression::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Name_Value_Pair_Expression>(m_token, m_name->clone(), m_value->clone());
	}

	auto Name_Value_Pair_Expression::token() const -> const Token & { return m_token; }

	auto Name_Value_Pair_Expression::head() const -> const Expression & { return *m_name; }

	auto Name_Value_Pair_Expression::tail() const -> const Expression & { return *m_value; }

	auto Name_Value_Pair_Expression::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Name_Value_Pair_Expression::is_nil() const -> bool { return false; }

	auto Name_Value_Pair_Expression::is_name() const -> bool { return false; }

	auto Name_Value_Pair_Expression::name() const -> const string &
	{
		return m_name->name(); // TODO rethink, aktually this function was meant for name expression
	}                              // should this throw NOT_SUPPORTED?

	auto Name_Value_Pair_Expression::is_built_in_function() const -> bool { return false; }

	auto Name_Value_Pair_Expression::is_function() const -> bool { return false; }

	auto Name_Value_Pair_Expression::is_function_application() const -> bool { return false; }

	auto Name_Value_Pair_Expression::is_fun_macro() const -> bool { return false; }

	auto Name_Value_Pair_Expression::is_integer() const -> bool { return false; }

	auto Name_Value_Pair_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Name_Value_Pair_Expression::is_real() const -> bool { return false; }

	auto Name_Value_Pair_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Name_Value_Pair_Expression::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Name_Value_Pair_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Name_Value_Pair_Expression::is_quoted() const -> bool { return false; }

	auto Name_Value_Pair_Expression::is_def_macro() const -> bool { return false; }

	auto Name_Value_Pair_Expression::is_name_value_pair() const -> bool { return true; }

	auto Name_Value_Pair_Expression::is_text() const -> bool { return false; }

	auto Name_Value_Pair_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Name_Value_Pair_Expression::is_list() const -> bool { return false; }

	auto Name_Value_Pair_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:name_value_pair " << m_token << " name:" << *m_name << " value:" << *m_value
		           << ")";
	}

	auto Name_Value_Pair_Expression::stringify(ostream &out, const string &) const -> void
	{
		out << m_name->stringify() << ": " << m_value->head().stringify();
	}

} // namespace appsl
