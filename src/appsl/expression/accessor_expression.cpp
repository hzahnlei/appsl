// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/accessor_expression.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{
	using fmt::format;

	using namespace junkbox;

	const shared_ptr<const Expression> Field_Accessor::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Field-Accessor", Token::NO_POS})};

	Field_Accessor::Field_Accessor(const Token &token, shared_ptr<const Expression> record,
	                               shared_ptr<const Expression> fields)
			: Abstract_Collection_Expression{token, record, fields}
	{
	}

	auto Field_Accessor::operator==(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Field_Accessor const *const>(&other)};
		return nullptr != other_list && head() == other_list->head() && tail() == other_list->tail();
	}

	auto Field_Accessor::operator<=(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Field_Accessor const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() <= other_list->tail();
	}

	auto Field_Accessor::operator<(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Field_Accessor const *const>(&other)};
		return nullptr != other_list && head() <= other_list->head() && tail() < other_list->tail();
	}

	auto Field_Accessor::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Field_Accessor>(token(), head().clone(), tail().clone());
	}

	auto stringify_expressions(const Expression &unevaluated, const Expression &evaluated)
	{
		const auto uneval_str{text::single_quoted(unevaluated.stringify())};
		const auto eval_str{text::single_quoted(evaluated.stringify())};
		return uneval_str == eval_str ? uneval_str : format("{} = {}", uneval_str, eval_str);
	}

	auto Field_Accessor::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		auto evaluated_record_or_map{m_head->evaluate(stack)};
		auto result{evaluated_record_or_map};

		expr::for_each(*m_tail,
		               [&](const auto &field_or_key)
		               {
				       if (result->is_record())
				       {
					       const auto evaluated_name{
							       field_or_key.is_name() ? field_or_key.shared_from_this()
										      : field_or_key.evaluate(stack)};
					       result = result->by_name(evaluated_name->name()).shared_from_this();
				       }
				       else
				       {
					       // Supposedly a map -> if not, the an appropriate error message
				               // will be given
					       const auto evaluated_key{field_or_key.evaluate(stack)};
					       result = result->by_key(*evaluated_key).shared_from_this();
				       }
			       });

		return result->clone();
	}

	auto Field_Accessor::is_list() const -> bool { return false; }

	auto Field_Accessor::type_of_collection() const -> string { return "accessor"; }

	auto Field_Accessor::opening_delimiter() const -> string { return "["; }

	auto Field_Accessor::closing_delimiter() const -> string { return "]"; }

} // namespace appsl
