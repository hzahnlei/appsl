// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/map_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using junkbox::algo::for_each;

	const shared_ptr<const Expression> Map_Literal_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Map", Token::NO_POS})};

	using Const_KV_Iter = Map_Literal_Expression::Key_Value_Map::const_iterator;

	auto remaining_keys(const Token &token, const Map_Literal_Expression::Key_Value_Map &entries,
	                    Const_KV_Iter &curr_entry) -> shared_ptr<const Expression>
	{
		if (curr_entry == entries.cend())
		{
			return Nil_Expression::NIL;
		}
		const auto &[curr_key, curr_value]{*curr_entry};
		curr_entry++;
		return make_shared<List_Expression>(token, curr_key, remaining_keys(token, entries, curr_entry));
	}

	auto Map_Literal_Expression::list_of_keys(const Token &token, const Key_Value_Map &entries)
			-> shared_ptr<const Expression>
	{
		auto curr_entry{entries.cbegin()};
		return remaining_keys(token, entries, curr_entry);
	}

	Map_Literal_Expression::Map_Literal_Expression(const Token &token, Key_Value_Map &&collection)
			: m_token{token}, m_ordered_kv_pairs{move(collection)}
	{
		// TODO assertions?
		m_list_of_keys = list_of_keys(m_token, m_ordered_kv_pairs);
	}

	Map_Literal_Expression::Map_Literal_Expression(Token &&token, Key_Value_Map &&collection)
			: m_token{move(token)}, m_ordered_kv_pairs{move(collection)}
	{
		// TODO assertions?
		m_list_of_keys = list_of_keys(m_token, m_ordered_kv_pairs);
	}

	auto Map_Literal_Expression::operator==(const Expression &other) const -> bool
	{
		// TODO
		return nullptr != dynamic_cast<Map_Literal_Expression const *const>(&other);
	}

	auto Map_Literal_Expression::operator<=(const Expression &other) const -> bool
	{
		// TODO
		return *this == other;
	}

	auto Map_Literal_Expression::operator<(const Expression &) const -> bool
	{
		// TODO
		return false;
	}

	auto Map_Literal_Expression::clone() const -> unique_ptr<Expression>
	{
		Key_Value_Map copy;
		for_each(m_ordered_kv_pairs,
		         [&](const auto &kv) { copy.try_emplace(kv.first->clone(), kv.second->clone()); });
		return make_unique<Map_Literal_Expression>(m_token, move(copy));
	}

	auto Map_Literal_Expression::token() const -> const Token & { return m_token; }

	auto Map_Literal_Expression::head() const -> const Expression & { return m_list_of_keys->head(); }

	auto Map_Literal_Expression::tail() const -> const Expression & { return m_list_of_keys->tail(); }

	static const string KEY_PARAM_NAME{"key"};

	auto Map_Literal_Expression::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		if (stack.argument_count() == 0)
		{
			return shared_from_this();
		}
		const auto &key{stack.lookup(KEY_PARAM_NAME)};
		const auto hit{m_ordered_kv_pairs.find(key.shared_from_this())};
		if (hit == m_ordered_kv_pairs.cend())
		{
			return Nil_Expression::NIL;
		}
		return hit->second->clone();
	}

	auto Map_Literal_Expression::is_nil() const -> bool { return false; }

	auto Map_Literal_Expression::is_name() const -> bool { return false; }

	auto Map_Literal_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Map_Literal_Expression::is_built_in_function() const -> bool { return false; }

	auto Map_Literal_Expression::is_function() const -> bool { return true; }

	auto Map_Literal_Expression::is_function_application() const -> bool { return false; }

	auto Map_Literal_Expression::is_fun_macro() const -> bool { return false; }

	auto Map_Literal_Expression::is_integer() const -> bool { return false; }

	auto Map_Literal_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Map_Literal_Expression::is_real() const -> bool { return false; }

	auto Map_Literal_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	static const auto PARAMETERS{
			make_unique<Name_Expression>(Token{Token::Type::NAME, KEY_PARAM_NAME, Token::NO_POS})};

	auto Map_Literal_Expression::parameter_list() const -> const Expression & { return *PARAMETERS; }

	auto Map_Literal_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Map_Literal_Expression::is_quoted() const -> bool { return false; }

	auto Map_Literal_Expression::is_def_macro() const -> bool { return false; }

	auto Map_Literal_Expression::is_name_value_pair() const -> bool { return false; }

	auto Map_Literal_Expression::is_text() const -> bool { return false; }

	auto Map_Literal_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Map_Literal_Expression::is_list() const -> bool { return true; }

	auto Map_Literal_Expression::by_key(const Expression &key) const -> const Expression &
	{
		const auto hit{m_ordered_kv_pairs.find(key.shared_from_this())};
		if (hit == m_ordered_kv_pairs.cend())
		{
			return *Nil_Expression::NIL;
		}
		return *(hit->second);
	}

	auto Map_Literal_Expression::operator<<(ostream &out) const -> ostream &
	{
		out << "(EXPR type:map content:#{";
		auto i{0U};
		for_each(m_ordered_kv_pairs,
		         [&](const auto &expr)
		         {
				 if (i > 0)
				 {
					 out << ' ';
				 }
				 out << *(expr.first) << " => " << *(expr.second);
				 i++;
			 });
		return out << "} " << m_token << ')';
	}

	auto Map_Literal_Expression::stringify(ostream &out, const string &indentation) const -> void
	{
		out << "#{";
		auto i{0U};
		for_each(m_ordered_kv_pairs,
		         [&](const auto &expr)
		         {
				 if (i > 0)
				 {
					 out << ' ';
				 }
				 expr.first->stringify(out, indentation);
				 out << " -> ";
				 expr.second->stringify(out, indentation);
				 i++;
			 });
		out << '}';
	}

} // namespace appsl
