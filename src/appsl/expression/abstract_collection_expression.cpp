// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/abstract_collection_expression.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/expression_traits.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{
	using namespace junkbox;

	Abstract_Collection_Expression::Abstract_Collection_Expression(const Token &token,
	                                                               shared_ptr<const Expression> head,
	                                                               shared_ptr<const Expression> tail)
			: m_token{token}, m_head{head}, m_tail{tail}
	{
	}

	auto Abstract_Collection_Expression::token() const -> const Token & { return m_token; }

	auto Abstract_Collection_Expression::is_nil() const -> bool { return false; }

	auto Abstract_Collection_Expression::is_name() const -> bool { return false; }

	auto Abstract_Collection_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Abstract_Collection_Expression::is_built_in_function() const -> bool { return false; }

	auto Abstract_Collection_Expression::is_function() const -> bool
	{
		return expr::is_literal_or_collection_thereof(*this);
	}

	auto Abstract_Collection_Expression::is_function_application() const -> bool { return false; }

	auto Abstract_Collection_Expression::is_fun_macro() const -> bool { return false; }

	auto Abstract_Collection_Expression::is_integer() const -> bool { return false; }

	auto Abstract_Collection_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Abstract_Collection_Expression::is_real() const -> bool { return false; }

	auto Abstract_Collection_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Abstract_Collection_Expression::parameter_list() const -> const Expression &
	{
		return *Nil_Expression::NIL;
	}

	auto Abstract_Collection_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Abstract_Collection_Expression::is_quoted() const -> bool { return false; }

	auto Abstract_Collection_Expression::is_def_macro() const -> bool { return false; }

	auto Abstract_Collection_Expression::is_name_value_pair() const -> bool { return false; }

	auto Abstract_Collection_Expression::is_text() const -> bool { return false; }

	auto Abstract_Collection_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Abstract_Collection_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:" << type_of_collection() << " " << m_token << " head:" << *m_head
		           << " tail:" << *m_tail << ")";
	}

	auto Abstract_Collection_Expression::stringify(ostream &out, const string &indentation) const -> void
	{
		out << opening_delimiter();
		stringify_all_elements(out, indentation);
		out << closing_delimiter();
	}

	auto Abstract_Collection_Expression::stringify_all_elements(ostream &out, const string &indentation) const
			-> void
	{
		auto i{0U};
		expr::for_each(*this,
		               [&](const auto &element)
		               {
				       if (i > 0)
				       {
					       out << " ";
				       };
				       element.stringify(out, indentation);
				       i++;
			       });
	}

} // namespace appsl
