// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/app_embedding/custom_object_expression.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{
	using fmt::format;
	using namespace junkbox;

	const shared_ptr<const Expression> Custom_Object_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Custom-Object", Token::NO_POS})};

	Custom_Object_Expression::Custom_Object_Expression(const Token &token, const shared_ptr<const Expression> kind,
	                                                   const shared_ptr<const Expression> properties)
			: m_token{token}, m_type{kind}, m_properties{properties}
	{
		DEV_ASSERT(nullptr != m_type, "Type tag must not be nullptr.");
		DEV_ASSERT(m_type->is_name(), "Type tag must be a name.");
		DEV_ASSERT(expr::is_valid_type_name(*m_type), "Invalid type tag name.");
		DEV_ASSERT(m_properties->is_nil() || m_properties->is_record(), "Properties must be nil or record");
	}

	Custom_Object_Expression::Custom_Object_Expression(Token &&token, const shared_ptr<const Expression> kind,
	                                                   const shared_ptr<const Expression> properties)
			: m_token{move(token)}, m_type{kind}, m_properties{properties}
	{
		DEV_ASSERT(nullptr != m_type, "Type tag must not be nullptr.");
		DEV_ASSERT(m_type->is_name(), "Type tag must be a name.");
		DEV_ASSERT(expr::is_valid_type_name(*m_type), "Invalid type tag name.");
		DEV_ASSERT(m_properties->is_nil() || m_properties->is_record(), "Properties must be nil or record");
	}

	auto Custom_Object_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_lit{dynamic_cast<Custom_Object_Expression const *const>(&other)};
		return nullptr != other_lit && m_type == other_lit->m_type;
	}

	auto Custom_Object_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_lit{dynamic_cast<Custom_Object_Expression const *const>(&other)};
		return nullptr != other_lit && m_type == other_lit->m_type;
	}

	auto Custom_Object_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_lit{dynamic_cast<Custom_Object_Expression const *const>(&other)};
		return nullptr != other_lit && m_type == other_lit->m_type;
	}

	// auto Custom_Object_Expression::clone() const -> unique_ptr<Expression>
	// {
	// 	return make_unique<Custom_Object_Expression>(m_token, m_type);
	// }

	auto Custom_Object_Expression::token() const -> const Token & { return m_token; }

	auto Custom_Object_Expression::head() const -> const Expression & { return *this; }

	auto Custom_Object_Expression::tail() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Custom_Object_Expression::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		if (stack.argument_count() > 0 && !holds_properties())
		{
			// TODO add property name to error message, therefore we need to pass the argument list to the
			// evaluation function, maybe this way we can get rid of Runtime::argument_count
			throw Semantic_Error{format("You are trying to access a property of a custom object. Cusom "
			                            "objects of type {} have no properties.",
			                            text::single_quoted(m_type->stringify())),
			                     token().position()};
		}
		// Evaluates to nil if no properties or no arguments. Otherwise returns the property value, else
		// crashes.
		return m_properties->evaluate(stack);
	}

	auto Custom_Object_Expression::is_nil() const -> bool { return false; }

	auto Custom_Object_Expression::is_name() const -> bool { return false; }

	auto Custom_Object_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Custom_Object_Expression::is_built_in_function() const -> bool { return true; }

	auto Custom_Object_Expression::is_function() const -> bool { return true; }

	auto Custom_Object_Expression::is_function_application() const -> bool { return false; }

	auto Custom_Object_Expression::is_fun_macro() const -> bool { return false; }

	auto Custom_Object_Expression::is_integer() const -> bool { return true; }

	auto Custom_Object_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Custom_Object_Expression::is_real() const -> bool { return false; }

	auto Custom_Object_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Custom_Object_Expression::parameter_list() const -> const Expression &
	{
		return m_properties->parameter_list();
	}

	auto Custom_Object_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Custom_Object_Expression::is_quoted() const -> bool { return false; }

	auto Custom_Object_Expression::is_def_macro() const -> bool { return false; }

	auto Custom_Object_Expression::is_name_value_pair() const -> bool { return false; }

	auto Custom_Object_Expression::is_text() const -> bool { return false; }

	auto Custom_Object_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Custom_Object_Expression::is_list() const -> bool { return false; }

	auto Custom_Object_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:custom-object object-type:" << m_type << " properties:" << m_properties << " "
		           << m_token << ")";
	}

	auto Custom_Object_Expression::stringify(ostream &out, const string &) const -> void
	{
		// TODO do not use "tagged" but the official constant to name the tagged macro
		out << (holds_properties()
		                        ? format("(tagged {} with: {})", m_properties->stringify(), m_type->stringify())
		                        : format("(tagged (custom_object) with: {})", m_type->stringify()));
	}

} // namespace appsl
