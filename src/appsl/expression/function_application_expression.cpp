// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function_application_expression.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	auto is_effectively_empty_arguments_list(const Expression &arguments) -> bool
	{
		return arguments.head().is_nil() || arguments.tail().is_nil() || arguments.tail().head().is_nil() ||
		       (arguments.tail().is_nil() && arguments.head().head().is_nil() &&
		        arguments.head().tail().is_nil());
	}

	using namespace junkbox;

	const shared_ptr<const Expression> Function_Application_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Function-Application", Token::NO_POS})};

	Function_Application_Expression::Function_Application_Expression(const Token &token,
	                                                                 shared_ptr<const Expression> function_name,
	                                                                 shared_ptr<const Expression> arguments)
			: Abstract_Collection_Expression{token, function_name, arguments}
	{
	}

	auto Function_Application_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_list{dynamic_cast<Function_Application_Expression const *const>(&other)};
		return nullptr != other_list && head() == other_list->head() && tail() == other_list->tail();
	}

	auto Function_Application_Expression::operator<=(const Expression &other) const -> bool
	{
		return *this == other;
	}

	auto Function_Application_Expression::operator<(const Expression &) const -> bool { return false; }

	auto Function_Application_Expression::clone() const -> unique_ptr<Expression>
	{
		// TODO clones of head and tail?
		return make_unique<Function_Application_Expression>(token(), m_head, m_tail);
	}

	auto Function_Application_Expression::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		return function().is_name() ? evaluate_with_function_name_checked(stack)
		                            : evaluate_with_function_evaluation(stack);
	}

	auto Function_Application_Expression::is_function_application() const -> bool { return true; }

	auto Function_Application_Expression::is_list() const -> bool { return false; }

	auto Function_Application_Expression::evaluate_with_function_name_checked(Stack_Frame &stack) const
			-> shared_ptr<const Expression>
	{
		if (stack.is_defined(function().name()))
		{
			// The name might actually stand for something complex and therefore needs evaluation.
			return evaluate_with_function_evaluation(stack);
		}
		else
		{
			throw Semantic_Error{fmt::format("No function defined for symbol {}.",
			                                 text::single_quoted(function().name())),
			                     function().token().position()};
		}
	}

	auto function_details_for_error_message(const Expression &fun_decl, const Expression &fun_def)
	{
		const auto stringified_fun_decl{text::single_quoted(fun_decl.stringify())};
		const auto stringified_fun_def{text::single_quoted(fun_def.stringify())};
		return stringified_fun_decl == stringified_fun_def ? stringified_fun_decl
		                                                   : stringified_fun_decl + " = " + stringified_fun_def;
	}

	auto Function_Application_Expression::evaluate_with_function_evaluation(Stack_Frame &stack) const
			-> shared_ptr<const Expression>
	{
		const auto fun{function().evaluate(stack)};
		if (fun->is_function())
		{
			return evaluate_with_function_determined(*fun, stack);
		}
		else
		{
			throw Semantic_Error{fmt::format("Not a function: {}.",
			                                 function_details_for_error_message(function(), *fun)),
			                     function().token().position()};
		}
	}

	auto Function_Application_Expression::evaluate_with_function_determined(const Expression &fun,
	                                                                        Stack_Frame &stack) const
			-> shared_ptr<const Expression>
	{
		const auto &parameters{fun.parameter_list()};
		auto local_env{local_env_from_arguments(stack, fun, parameters)};
		return fun.evaluate(*local_env);
	}

	auto Function_Application_Expression::type_of_collection() const -> string { return "function application"; }

	auto Function_Application_Expression::opening_delimiter() const -> string { return "("; }

	auto Function_Application_Expression::closing_delimiter() const -> string { return ")"; }

	inline auto name_of_variadic(const Expression &parameter)
	{
		return expr::unvariadic(parameter).token().lexeme();
	}

	inline auto name_of_quoted(const Expression &parameter) { return expr::unquoted(parameter).token().lexeme(); }

	inline auto name_of_quoted_variadic(const Expression &parameter)
	{
		return expr::unvariadic(expr::unquoted(parameter)).token().lexeme();
	}

	auto last_param_is_variadic(const Expression &parameters)
	{
		if (parameters.is_nil())
		{
			return false;
		}
		else if (parameters == parameters.head())
		{
			return false;
		}
		else if (parameters.is_variadic_parameter())
		{
			return true;
		}
		else if (parameters.tail().is_nil())
		{
			return last_param_is_variadic(parameters.head());
		}
		else
		{
			return last_param_is_variadic(parameters.tail());
		}
	}

	auto too_few_arguments(const size_t parameter_count, const size_t argument_count, const Expression &parameters)
	{
		return (argument_count < parameter_count && !last_param_is_variadic(parameters)) ||
		       (argument_count < (parameter_count - 1) && last_param_is_variadic(parameters));
	}

	auto too_many_arguments(const size_t parameter_count, const size_t argument_count, const Expression &parameters)
	{
		return argument_count > parameter_count && !last_param_is_variadic(parameters);
	}

	auto position_of_last_argument(const Expression &arguments, const Token::Position &default_pos)
	{
		if (arguments.is_nil())
		{
			return default_pos;
		}
		else if (arguments.head().is_nil())
		{
			return arguments.token().position();
		}
		else if (arguments.tail().is_nil())
		{
			return arguments.head().token().position();
		}
		else
		{
			return position_of_last_argument(arguments.tail(), default_pos);
		}
	}

	auto Function_Application_Expression::local_env_from_arguments(Stack_Frame &stack,
	                                                               const Expression &evaluated_fun,
	                                                               const Expression &parameters) const
			-> shared_ptr<Stack_Frame>
	{
		make_sure_parameter_and_argument_count_match(evaluated_fun, parameters, arguments());
		auto local_runtime{stack.new_child()};
		populate_env_from_arguments(*local_runtime, evaluated_fun, parameters, arguments());
		return local_runtime;
	}

	auto stringified_list(const Expression &list) -> string
	{
		string result;
		auto i{0U};
		expr::for_each(list,
		               [&](const auto &element)
		               {
				       if (i > 0)
				       {
					       result += ", ";
				       }
				       result += text::single_quoted(element.stringify());
				       i++;
			       });
		return result;
	}

	auto parameters_for_error_message(const Expression &parameter_list)
	{
		return parameter_list.is_nil() ? "" : ": " + stringified_list(parameter_list);
	}

	auto expected_argument_count_for_error_message(const size_t &arg_count) -> string
	{
		switch (arg_count)
		{
		case 0:
			return "no arguments";
		case 1:
			return "1 argument";
		default:
			return fmt::format("{} arguments", arg_count);
		}
	}

	auto actual_param_count_for_error_message(const size_t &arg_count) -> string
	{
		switch (arg_count)
		{
		case 0:
			return "no parameters";
		case 1:
			return "1 parameter";
		default:
			return fmt::format("{} parameters", arg_count);
		}
	}

	auto arguments_for_error_message(const size_t &arg_count, const Expression &arg_list)
	{
		return arg_count == 0 ? "" : ": " + stringified_list(arg_list);
	}

	auto Function_Application_Expression::make_sure_parameter_and_argument_count_match(
			const Expression &evaluated_fun, const Expression &parameters,
			const Expression &arguments) const -> void
	{
		const auto parameter_count{expr::count(parameters)};
		const auto argument_count{is_effectively_empty_arguments_list(arguments) && parameters.is_nil()
		                                          ? 0
		                                          : expr::count(arguments)};

		if (too_few_arguments(parameter_count, argument_count, parameters))
		{

			throw Semantic_Error{fmt::format("Too few arguments. Function {} expects {}{}. But {} given{}.",
			                                 function_details_for_error_message(function(), evaluated_fun),
			                                 expected_argument_count_for_error_message(parameter_count),
			                                 parameters_for_error_message(parameters),
			                                 actual_param_count_for_error_message(argument_count),
			                                 arguments_for_error_message(argument_count, arguments)),
			                     position_of_last_argument(arguments, function().token().position())};
		}
		else if (too_many_arguments(parameter_count, argument_count, parameters))
		{
			throw Semantic_Error{
					fmt::format("Too many arguments. Function {} expects {}{}. But {} given{}.",
			                            function_details_for_error_message(function(), evaluated_fun),
			                            expected_argument_count_for_error_message(parameter_count),
			                            parameters_for_error_message(parameters),
			                            actual_param_count_for_error_message(argument_count),
			                            arguments_for_error_message(argument_count, arguments)),
					position_of_last_argument(arguments, function().token().position())};
		}
	}

	inline auto more_parameters_to_be_processed(const Expression &parameters)
	{
		return !parameters.tail().is_nil();
	}

	inline auto more_arguments_to_be_processed(const Expression &arguments) { return !arguments.tail().is_nil(); }

	inline auto more_parameters_and_arguments_to_be_processed(const Expression &parameters,
	                                                          const Expression &arguments)
	{
		return more_parameters_to_be_processed(parameters) && more_arguments_to_be_processed(arguments);
	}

	auto Function_Application_Expression::populate_env_from_arguments(Stack_Frame &stack,
	                                                                  const Expression &evaluated_fun,
	                                                                  const Expression &parameter,
	                                                                  const Expression &arguments) const -> void
	{
		if (parameter.is_variadic_parameter())
		{
			stack.define_argument(name_of_variadic(parameter), evaluated_argument_list(stack, arguments));
		}
		else if (parameter.is_quoted())
		{
			if (expr::unquoted(parameter).is_variadic_parameter())
			{
				stack.define_argument(name_of_quoted_variadic(parameter), arguments.shared_from_this());
			}
			else
			{
				const auto &actual_argument{extract_value_form_name_value_pair_if_applicable(
						evaluated_fun, expr::unquoted(parameter), arguments)};
				stack.define_argument(name_of_quoted(parameter), actual_argument.shared_from_this());
			}
		}
		else if (parameter.is_name())
		{
			const auto &actual_argument{extract_value_form_name_value_pair_if_applicable(
					evaluated_fun, parameter, arguments)};

			// One might activate this to see the current "stack frame"
			// std::cout << "#################################################\n";
			// std::cout << runtime;

			// Notice how we refer to the parent environment in order to populate our parameters from there,
			// not from the environment we are about to populate.
			stack.define_argument(parameter.name(), actual_argument.evaluate(stack.parent()));
		}
		else if (!parameter.is_nil())
		{
			populate_env_from_arguments(stack, evaluated_fun, parameter.head(), arguments);
			if (more_parameters_and_arguments_to_be_processed(parameter, arguments))
			{
				populate_env_from_arguments(stack, evaluated_fun, parameter.tail(), arguments.tail());
			}
		}
	}

	/**
	 * @brief Applied only to actual lists.
	 */
	auto evaluated_argument_actual_list(Stack_Frame &stack, const Expression &arguments)
			-> shared_ptr<const Expression>
	{
		if (arguments.is_nil())
		{
			return Nil_Expression::NIL;
		}
		else if (arguments.tail().is_nil())
		{
			return make_shared<List_Expression>(arguments.token(), arguments.head().evaluate(stack));
		}
		else
		{
			return make_shared<List_Expression>(arguments.token(), arguments.head().evaluate(stack),
			                                    evaluated_argument_actual_list(stack, arguments.tail()));
		}
	}

	/**
	 * @brief Might be applied to lists and non-lists alike.
	 */
	auto Function_Application_Expression::evaluated_argument_list(Stack_Frame &stack,
	                                                              const Expression &arguments) const
			-> shared_ptr<const Expression>
	{
		if (arguments.is_nil())
		{
			return Nil_Expression::NIL;
		}
		else if (arguments.tail().is_nil())
		{
			return arguments.head().evaluate(stack);
		}
		else
		{
			return make_shared<List_Expression>(arguments.token(), arguments.head().evaluate(stack),
			                                    evaluated_argument_actual_list(stack, arguments.tail()));
		}
	}

	inline auto is_named_argument(const Expression &arguments) { return arguments.head().is_name_value_pair(); }

	auto Function_Application_Expression::extract_value_form_name_value_pair_if_applicable(
			const Expression &evaluated_fun, const Expression &parameter, const Expression &arguments) const
			-> const Expression &
	{
		return is_named_argument(arguments)
		                       ? actual_argument_value_form_named_argument(evaluated_fun, parameter, arguments)
		                       : arguments.head();
	}

	inline auto argument_name_matches_parameter_name(const Expression &arguments, const Expression &parameter)
	{
		return arguments.head().name() == parameter.name();
	}

	auto Function_Application_Expression::actual_argument_value_form_named_argument(
			const Expression &evaluated_fun, const Expression &parameter, const Expression &arguments) const
			-> const Expression &
	{
		if (argument_name_matches_parameter_name(arguments, parameter))
		{
			return arguments.head().tail().head();
		}
		else
		{
			throw Semantic_Error{
					fmt::format("Function {} = {} has a parameter with name {}. However, the "
			                            "given named argument {} = {} does not match that name.",
			                            text::single_quoted(function().stringify()),
			                            text::single_quoted(evaluated_fun.stringify()),
			                            text::single_quoted(parameter.name()),
			                            text::single_quoted(arguments.head().head().stringify()),
			                            text::single_quoted(arguments.head().tail().head().stringify())),
					arguments.head().head().token().position()};
		}
	}

} // namespace appsl
