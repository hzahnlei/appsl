// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	const shared_ptr<const Expression> Real_Literal_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Real", Token::NO_POS})};

	Real_Literal_Expression::Real_Literal_Expression(const Token &token, const Real_Representation value)
			: m_token{token}, m_value{value}
	{
	}

	Real_Literal_Expression::Real_Literal_Expression(Token &&token, const Real_Representation value)
			: m_token{move(token)}, m_value{value}
	{
	}

	auto Real_Literal_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_real{dynamic_cast<Real_Literal_Expression const *const>(&other)};
		return nullptr != other_real && m_value == other_real->m_value;
	}

	auto Real_Literal_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_real{dynamic_cast<Real_Literal_Expression const *const>(&other)};
		return nullptr != other_real && m_value <= other_real->m_value;
	}

	auto Real_Literal_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_real{dynamic_cast<Real_Literal_Expression const *const>(&other)};
		return nullptr != other_real && m_value < other_real->m_value;
	}

	auto Real_Literal_Expression::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Real_Literal_Expression>(m_token, m_value);
	}

	auto Real_Literal_Expression::token() const -> const Token & { return m_token; }

	auto Real_Literal_Expression::head() const -> const Expression & { return *this; }

	auto Real_Literal_Expression::tail() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Real_Literal_Expression::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Real_Literal_Expression::is_nil() const -> bool { return false; }

	auto Real_Literal_Expression::is_name() const -> bool { return false; }

	auto Real_Literal_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Real_Literal_Expression::is_built_in_function() const -> bool { return true; }

	auto Real_Literal_Expression::is_function() const -> bool { return true; }

	auto Real_Literal_Expression::is_function_application() const -> bool { return false; }

	auto Real_Literal_Expression::is_fun_macro() const -> bool { return false; }

	auto Real_Literal_Expression::is_integer() const -> bool { return false; }

	auto Real_Literal_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Real_Literal_Expression::is_real() const -> bool { return true; }

	auto Real_Literal_Expression::real_value() const -> Real_Representation { return m_value; }

	auto Real_Literal_Expression::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Real_Literal_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Real_Literal_Expression::is_quoted() const -> bool { return false; }

	auto Real_Literal_Expression::is_def_macro() const -> bool { return false; }

	auto Real_Literal_Expression::is_name_value_pair() const -> bool { return false; }

	auto Real_Literal_Expression::is_text() const -> bool { return false; }

	auto Real_Literal_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Real_Literal_Expression::is_list() const -> bool { return false; }

	auto Real_Literal_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:real_literal value:" << m_value << " " << m_token << ")";
	}

	auto Real_Literal_Expression::stringify(ostream &out, const string &) const -> void { out << m_value; }

} // namespace appsl
