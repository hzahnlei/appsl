// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/error_helper.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/name_value_pair_expression.hpp"
#include "appsl/expression/record_expression.hpp"
#include "appsl/expression/tagged_expression.hpp"
#include "appsl/expression/text_literal_expression.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using fmt::format;
	using namespace junkbox;

	static const string MESSAGE{"message"};
	static const auto MESSAGE_FIELD_NAME{
			make_shared<const Name_Expression>(Token{Token::Type::NAME, MESSAGE, Token::NO_POS})};
	static const string CAUSE{"cause"};
	static const auto CAUSE_FIELD_NAME{
			make_shared<const Name_Expression>(Token{Token::Type::NAME, CAUSE, Token::NO_POS})};
	static const string ERROR{"Error"};
	static const auto ERROR_TYPE_NAME{
			make_shared<const Name_Expression>(Token{Token::Type::NAME, ERROR, Token::NO_POS})};

	auto is_error_object(const Expression &error) -> bool
	{
		return error.is_tagged() && error.head().is_record() && error.tail().is_name() &&
		       error.tail().name() == ERROR && expr::record_field_count(error.tail()) == 2 &&
		       error.tail().head().is_name() && error.tail().head().name() == MESSAGE &&
		       error.tail().tail().is_name() && error.tail().tail().name() == CAUSE;
	}

	auto error_from(const string &message, const shared_ptr<const Expression> cause) -> shared_ptr<Expression>
	{
		if (message.empty())
		{
			throw Runtime_Error{"Error message must not be empty."};
		}
		if (cause == nullptr)
		{
			throw Runtime_Error{format("The cause to the error message {} must not be a nullptr. It "
			                           "has to be nil instead.",
			                           text::single_quoted(message))};
		}
		if (!cause->is_nil() && !is_error_object(*cause))
		{
			throw Runtime_Error{format("The cause to the error message {} must be a record of the form "
			                           "({}:<message> {}:<cause>) tagged with type {}, where <message> is "
			                           "an explanatory text literal and <cause> an optional error record. "
			                           "However, {} is not a valid error record.",
			                           text::single_quoted(message), MESSAGE, CAUSE, ERROR,
			                           text::single_quoted(cause->stringify()))};
		}
		return make_shared<Tagged_Expression>(
				Token::NONE,
				make_shared<Record_Expression>(
						Token::NONE,
						Record_Expression::Name_Value_List{
								make_shared<Name_Value_Pair_Expression>(
										Token::NONE, MESSAGE_FIELD_NAME,
										make_shared<Text_Literal_Expression>(Token{
												Token::Type::TEXT_LITERAL,
												message,
												Token::NO_POS})),
								make_shared<Name_Value_Pair_Expression>(
										Token::NONE, CAUSE_FIELD_NAME,
										Nil_Expression::NIL)}),
				ERROR_TYPE_NAME);
	}

} // namespace appsl
