// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/tagged_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	const shared_ptr<const Expression> Tagged_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Tagged-Expression", Token::NO_POS})};

	Tagged_Expression::Tagged_Expression(const Token &token, shared_ptr<const Expression> expr,
	                                     shared_ptr<const Expression> tag)
			: m_token{token}, m_expression{expr}, m_tag{tag}
	{
		DEV_ASSERT(m_tag != nullptr, "Type tag must not be nullptr");
		DEV_ASSERT(m_expression != nullptr, "Tagged expression must not be nullptr");
		DEV_ASSERT(m_tag->is_name(), "Type tags must be names.");
	}

	Tagged_Expression::Tagged_Expression(Token &&token, shared_ptr<const Expression> expr,
	                                     shared_ptr<const Expression> tag)
			: m_token{move(token)}, m_expression{expr}, m_tag{tag}
	{
		DEV_ASSERT(m_tag->is_name(), "Type tags must be names.");
	}

	auto Tagged_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_tagged{dynamic_cast<Tagged_Expression const *const>(&other)};
		return nullptr != other_tagged && *m_tag == *other_tagged->m_tag &&
		       *m_expression == *other_tagged->m_expression;
	}

	auto Tagged_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_tagged{dynamic_cast<Tagged_Expression const *const>(&other)};
		return nullptr != other_tagged && *m_tag <= *other_tagged->m_tag &&
		       *m_expression <= *other_tagged->m_expression;
	}

	auto Tagged_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_tagged{dynamic_cast<Tagged_Expression const *const>(&other)};
		return nullptr != other_tagged && *m_tag < *other_tagged->m_tag &&
		       *m_expression < *other_tagged->m_expression;
	}

	auto Tagged_Expression::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Tagged_Expression>(m_token, m_tag->clone(), m_expression->clone());
	}

	auto Tagged_Expression::token() const -> const Token & { return m_token; }

	auto Tagged_Expression::head() const -> const Expression & { return *m_expression; }

	auto Tagged_Expression::tail() const -> const Expression & { return *m_tag; }

	auto Tagged_Expression::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Tagged_Expression::is_nil() const -> bool { return false; }

	auto Tagged_Expression::is_name() const -> bool { return false; }

	auto Tagged_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Tagged_Expression::is_built_in_function() const -> bool { return false; }

	auto Tagged_Expression::is_function() const -> bool { return false; }

	auto Tagged_Expression::is_function_application() const -> bool { return false; }

	auto Tagged_Expression::is_fun_macro() const -> bool { return false; }

	auto Tagged_Expression::is_integer() const -> bool { return false; }

	auto Tagged_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Tagged_Expression::is_real() const -> bool { return false; }

	auto Tagged_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Tagged_Expression::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Tagged_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Tagged_Expression::is_quoted() const -> bool { return false; }

	auto Tagged_Expression::is_def_macro() const -> bool { return false; }

	auto Tagged_Expression::is_name_value_pair() const -> bool { return false; }

	auto Tagged_Expression::is_text() const -> bool { return false; }

	auto Tagged_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Tagged_Expression::is_list() const -> bool { return false; }

	auto Tagged_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:tagged tag:" << m_tag << " expr:" << m_expression << " " << m_token << ")";
	}

	auto Tagged_Expression::stringify(ostream &out, const string &indentation) const -> void
	{
		out << "(tagged ";
		m_expression->stringify(out, indentation);
		out << " with: ";
		m_tag->stringify(out, indentation);
		out << ')';
	}

} // namespace appsl
