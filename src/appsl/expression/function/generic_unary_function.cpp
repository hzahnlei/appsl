// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/generic_unary_function.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Generic_Unary_Function::Generic_Unary_Function(const string &name, const Unary_Cpp_Fun fun)
			: Abstract_Builtin_Function{name}, m_unary_fun(fun)
	{
	}

	auto Generic_Unary_Function::operator==(const Expression &other) const -> bool
	{
		const auto other_fun = dynamic_cast<Generic_Unary_Function const *const>(&other);
		return nullptr != other_fun && function_display_name() == other_fun->function_display_name();
	}

	auto Generic_Unary_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Generic_Unary_Function>(function_display_name(), m_unary_fun);
	}

	static const string EXPR_PARAM_NAME{"expr"};

	auto Generic_Unary_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &expr{stack.lookup(EXPR_PARAM_NAME)};
		return m_unary_fun(expr).shared_from_this();
	}

	static const Name_Expression EXPR_PARAM{Token{Token::Type::NAME, EXPR_PARAM_NAME, Token::NO_POS}};

	auto Generic_Unary_Function::parameter_list() const -> const Expression & { return EXPR_PARAM; }

} // namespace appsl
