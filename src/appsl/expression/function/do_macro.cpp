// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/do_macro.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Do_Macro::Do_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Do_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Do_Macro const *const>(&other);
	}

	auto Do_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Do_Macro>(function_display_name());
	}

	static const string EXPRESSIONS_PARAM_NAME{"expressions"};

	auto Do_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		// LOCAL SCOPE: I do not create a new local environment. Everything is woring fine, all tests are
		// green (in both cases). However, I wonder if this is really correct.
		//
		// const auto &expressions{stack.lookup(EXPRESSIONS_PARAM_NAME)};
		// auto local_scope{stack.new_child()};
		// shared_ptr<const Expression> last_expression{Nil_Expression::NIL};
		// expr::for_each(expressions,
		//                [&](const auto &expression) { last_expression = expression.evaluate(*local_scope); });
		// return last_expression;
		//
		const auto &expressions{stack.lookup(EXPRESSIONS_PARAM_NAME)};
		shared_ptr<const Expression> last_expression{Nil_Expression::NIL};
		expr::for_each(expressions,
		               [&](const auto &expression) { last_expression = expression.evaluate(stack); });
		return last_expression;
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(
					Token::NONE,
					make_unique<Variadic_Parameter>(
							Token::NONE,
							make_unique<Name_Expression>(Token{Token::Type::NAME,
	                                                                                   EXPRESSIONS_PARAM_NAME,
	                                                                                   Token::NO_POS})))};

	auto Do_Macro::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
