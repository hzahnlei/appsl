// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/let_macro.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Let_Macro::Let_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Let_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Let_Macro const *const>(&other);
	}

	auto Let_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Let_Macro>(function_display_name());
	}

	static const string DEFINITIONS_PARAM_NAME{"definitions"};

	static const string RETURN_PARAM_NAME{"return"};

	auto Let_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		// LOCAL SCOPE: I do not create a new local environment. Everything is woring fine, all tests are green
		// (in both cases). However, I wonder if this is really correct.
		//
		// const auto &definitions{stack.lookup(DEFINITIONS_PARAM_NAME)};
		// auto local_scope{stack.new_child()};
		// apply_definitions(definitions, *local_scope);
		// return stack.lookup(RETURN_PARAM_NAME).evaluate(*local_scope);
		//
		const auto &definitions{stack.lookup(DEFINITIONS_PARAM_NAME)};
		apply_definitions(definitions, stack);
		return stack.lookup(RETURN_PARAM_NAME).evaluate(stack);
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(
					Token::NONE,
					make_unique<Name_Expression>(Token{Token::Type::NAME, DEFINITIONS_PARAM_NAME,
	                                                                   Token::NO_POS})),
			make_unique<List_Expression>(
					Token::NONE,
					make_unique<Quoted_Expression>(Token::NONE, make_unique<Name_Expression>(Token{
												    Token::Type::NAME,
												    RETURN_PARAM_NAME,
												    Token::NO_POS})))};

	auto Let_Macro::parameter_list() const -> const Expression & { return PARAMETERS; }

	auto is_name_expr_pair(const Expression &definition)
	{
		return definition.head().is_name() && !definition.tail().head().is_nil();
	}

	auto Let_Macro::apply_definitions(const Expression &definitions, Stack_Frame &stack) const -> void
	{
		expr::for_each(definitions,
		               [&](const auto &definition)
		               {
				       if (is_name_expr_pair(definition))
				       {
					       const auto name{definition.head().name()};
					       if (!stack.is_defined_locally(name))
					       {
						       const auto value{definition.tail().head().evaluate(stack)};
						       stack.define_local_var(name, value);
					       }
					       else
					       {
						       throw Semantic_Error{
								       "Name " + text::single_quoted(name) +
										       " already defined in let scope.",
								       definition.head().token().position()};
					       }
				       }
				       else
				       {
					       throw Semantic_Error{
							       "This is no name/value pair that can be defined in let "
							       "scope: " + text::single_quoted(definition.stringify()) +
									       ".",
							       definition.token().position()};
				       }
			       });
	}

} // namespace appsl
