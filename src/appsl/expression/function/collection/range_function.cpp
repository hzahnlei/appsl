// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/collection/range_function.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/function/collection/range_generator.hpp"
#include "appsl/expression/function_application_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Range_Function::Range_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Range_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Range_Function const *const>(&other);
	}

	auto Range_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Range_Function>(function_display_name());
	}

	static const string FROM_PARAM_NAME{"from"};
	static const string TO_PARAM_NAME{"to"};

	auto Range_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		// For the sake of lazyness arguments passed in are unevaluated.
		// They are evaluated here, because it is now required.
		const auto &from{stack.lookup(FROM_PARAM_NAME)};
		const auto &to{stack.lookup(TO_PARAM_NAME)};
		return make_shared<Range_Generator>(from.int_value(), to.int_value(), stack.shared_from_this());
		// This was a bug: stack.global().new_child());
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Name_Expression>(Token{Token::Type::NAME, FROM_PARAM_NAME, Token::NO_POS}),
			make_unique<List_Expression>(Token::NONE, make_unique<Name_Expression>(Token{Token::Type::NAME,
	                                                                                             TO_PARAM_NAME,
	                                                                                             Token::NO_POS}))};

	auto Range_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
