// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/collection/mapping_generator.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	const shared_ptr<const Expression> Mapping_Generator::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Mapping-Generator", Token::NO_POS})};

	Mapping_Generator::Mapping_Generator(const shared_ptr<const Expression> &collection,
	                                     const shared_ptr<const Expression> &map_fun, shared_ptr<Stack_Frame> stack)
			: m_input_collection{collection}, m_map_fun{map_fun}, m_stack{stack},
			  m_current_input{collection},
			  m_map_functions_param_name(
					  expr::pure_param_name_without_quotes_etc(m_map_fun->parameter_list().head())
							  .name())
	{
		DEV_ASSERT(!m_map_fun->parameter_list().head().is_nil(), "Map function has too few parameters.");
		DEV_ASSERT(m_map_fun->parameter_list().tail().is_nil(), "Map function has too many parameters.");

		apply_mapping_if_input_available();
	}

	auto Mapping_Generator::operator==(const Expression &other) const -> bool
	{
		// TODO
		return nullptr != dynamic_cast<Mapping_Generator const *const>(&other);
	}

	auto Mapping_Generator::operator<=(const Expression &other) const -> bool
	{
		// TODO
		return *this == other;
	}

	auto Mapping_Generator::operator<(const Expression &) const -> bool
	{
		// TODO
		return false;
	}

	auto Mapping_Generator::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Mapping_Generator>(m_input_collection, m_map_fun, m_stack);
	}

	auto Mapping_Generator::token() const -> const Token & { return Token::NONE; }

	auto Mapping_Generator::is_iterable() const -> bool { return m_current_input->is_iterable(); }

	auto Mapping_Generator::head() const -> const Expression & { return *m_current_mapped; }

	auto Mapping_Generator::tail() const -> const Expression &
	{
		m_current_input = m_current_input->tail().shared_from_this();
		apply_mapping_if_input_available();
		return *this;
	}

	auto Mapping_Generator::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Mapping_Generator::is_nil() const -> bool { return m_current_input->is_nil(); }

	auto Mapping_Generator::is_name() const -> bool { return false; }

	auto Mapping_Generator::name() const -> const string & { NOT_SUPPORTED(); }

	auto Mapping_Generator::is_built_in_function() const -> bool { return false; }

	auto Mapping_Generator::is_function() const -> bool { return false; }

	auto Mapping_Generator::is_function_application() const -> bool { return false; }

	auto Mapping_Generator::is_fun_macro() const -> bool { return false; }

	auto Mapping_Generator::is_integer() const -> bool { return false; }

	auto Mapping_Generator::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Mapping_Generator::is_real() const -> bool { return false; }

	auto Mapping_Generator::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Mapping_Generator::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Mapping_Generator::is_variadic_parameter() const -> bool { return false; }

	auto Mapping_Generator::is_quoted() const -> bool { return false; }

	auto Mapping_Generator::is_def_macro() const -> bool { return false; }

	auto Mapping_Generator::is_name_value_pair() const -> bool { return false; }

	auto Mapping_Generator::is_text() const -> bool { return false; }

	auto Mapping_Generator::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Mapping_Generator::is_list() const -> bool { return true; }

	auto Mapping_Generator::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:mapping-generator collection:" << m_input_collection
		           << " mapping:" << m_map_fun << ")";
	}

	auto Mapping_Generator::stringify(ostream &out, const string &indentation) const -> void
	{
		auto i{0U};
		do
		{
			if (i > 0)
			{
				out << '\n';
			}
			head().stringify(out, indentation);
			i++;
			// } while (!tail().is_nil());
		} while (tail().is_iterable());
	}

	inline auto input_available(const Expression &input) { return !input.head().is_nil(); }

	auto Mapping_Generator::apply_mapping_if_input_available() const -> void
	{
		m_current_mapped = input_available(*m_current_input) ? apply_mapping() : Nil_Expression::NIL;
	}

	auto Mapping_Generator::apply_mapping() const -> shared_ptr<const Expression>
	{
		auto local_env{m_stack->new_child()};
		// The map function expects exactly one argument!
		local_env->define_local_var(m_map_functions_param_name, m_current_input->head().shared_from_this());
		return m_map_fun->evaluate(*local_env);
	};

} // namespace appsl
