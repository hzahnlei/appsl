// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/collection/tail_function.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Tail_Function::Tail_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Tail_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Tail_Function const *const>(&other);
	}

	auto Tail_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Tail_Function>(function_display_name());
	}

	static const string COLLECTION_PARAM_NAME{"collection"};

	auto Tail_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &collection{stack.lookup(COLLECTION_PARAM_NAME)};
		return collection.tail().shared_from_this();
	}

	static const Name_Expression COLLECTION_PARAM{Token{Token::Type::NAME, COLLECTION_PARAM_NAME, Token::NO_POS}};

	auto Tail_Function::parameter_list() const -> const Expression & { return COLLECTION_PARAM; }

} // namespace appsl
