// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/collection/count_function.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Count_Function::Count_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Count_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Count_Function const *const>(&other);
	}

	auto Count_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Count_Function>(function_display_name());
	}

	static const string COLLECTION_PARAM_NAME{"collection"};

	auto Count_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &collection{stack.lookup(COLLECTION_PARAM_NAME)};
		return make_shared<Integer_Literal_Expression>(Token::NONE, expr::count(collection));
	}

	static const Name_Expression COLLECTION_PARAM{Token{Token::Type::NAME, COLLECTION_PARAM_NAME, Token::NO_POS}};

	auto Count_Function::parameter_list() const -> const Expression & { return COLLECTION_PARAM; }

} // namespace appsl
