// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/collection/filtering_generator.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	const shared_ptr<const Expression> Filtering_Generator::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Filtering-Generator", Token::NO_POS})};

	Filtering_Generator::Filtering_Generator(const shared_ptr<const Expression> &collection,
	                                         const shared_ptr<const Expression> &pred_fun, const Mode mode,
	                                         shared_ptr<Stack_Frame> stack)
			: m_input_collection{collection}, m_pred_fun{pred_fun}, m_mode{mode}, m_stack{stack},
			  m_current_input{collection},
			  m_map_functions_param_name(
					  expr::pure_param_name_without_quotes_etc(m_pred_fun->parameter_list().head())
							  .name())
	{
		DEV_ASSERT(!m_pred_fun->parameter_list().head().is_nil(), "Predicate function has too few parameters.");
		DEV_ASSERT(m_pred_fun->parameter_list().tail().is_nil(), "Predicate function has too many parameters.");

		apply_predicate_if_input_available();
	}

	auto Filtering_Generator::operator==(const Expression &other) const -> bool
	{
		// TODO
		return nullptr != dynamic_cast<Filtering_Generator const *const>(&other);
	}

	auto Filtering_Generator::operator<=(const Expression &other) const -> bool
	{
		// TODO
		return *this == other;
	}

	auto Filtering_Generator::operator<(const Expression &) const -> bool
	{
		// TODO
		return false;
	}

	auto Filtering_Generator::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Filtering_Generator>(m_input_collection, m_pred_fun, m_mode, m_stack);
	}

	auto Filtering_Generator::token() const -> const Token & { return Token::NONE; }

	auto Filtering_Generator::is_iterable() const -> bool { return m_current_input->is_iterable(); }

	auto Filtering_Generator::head() const -> const Expression & { return *m_current_filtered; }

	auto Filtering_Generator::tail() const -> const Expression &
	{
		m_current_input = m_current_input->tail().shared_from_this();
		apply_predicate_if_input_available();
		return *this;
	}

	auto Filtering_Generator::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Filtering_Generator::is_nil() const -> bool { return m_current_input->is_nil(); }

	auto Filtering_Generator::is_name() const -> bool { return false; }

	auto Filtering_Generator::name() const -> const string & { NOT_SUPPORTED(); }

	auto Filtering_Generator::is_built_in_function() const -> bool { return false; }

	auto Filtering_Generator::is_function() const -> bool { return false; }

	auto Filtering_Generator::is_function_application() const -> bool { return false; }

	auto Filtering_Generator::is_fun_macro() const -> bool { return false; }

	auto Filtering_Generator::is_integer() const -> bool { return false; }

	auto Filtering_Generator::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Filtering_Generator::is_real() const -> bool { return false; }

	auto Filtering_Generator::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Filtering_Generator::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Filtering_Generator::is_variadic_parameter() const -> bool { return false; }

	auto Filtering_Generator::is_quoted() const -> bool { return false; }

	auto Filtering_Generator::is_def_macro() const -> bool { return false; }

	auto Filtering_Generator::is_name_value_pair() const -> bool { return false; }

	auto Filtering_Generator::is_text() const -> bool { return false; }

	auto Filtering_Generator::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Filtering_Generator::is_list() const -> bool { return true; }

	auto Filtering_Generator::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:filtering-generator collection:" << m_input_collection
		           << " predicate:" << m_pred_fun << ")";
	}

	auto Filtering_Generator::stringify(ostream &out, const string &indentation) const -> void
	{
		auto i{0U};
		do
		{
			if (i > 0)
			{
				out << '\n';
			}
			head().stringify(out, indentation);
			i++;
			// } while (!tail().is_nil());
		} while (tail().is_iterable());
	}

	inline auto input_available(const Expression &input) { return !input.head().is_nil(); }

	auto Filtering_Generator::apply_predicate_if_input_available() const -> void
	{
		if (m_mode == Mode::keep)
		{
			apply_keep_logic();
		}
		else
		{
			apply_discard_logic();
		}
	}

	inline auto Filtering_Generator::apply_keep_logic() const -> void
	{
		while (input_available(*m_current_input))
		{
			if (apply_predicate()->is_iterable()) // aka == true
			{
				m_current_filtered = m_current_input->head().shared_from_this();
				return;
			}
			m_current_input = m_current_input->tail().shared_from_this();
		}
		m_current_filtered = Nil_Expression::NIL;
	}

	inline auto Filtering_Generator::apply_discard_logic() const -> void
	{
		while (input_available(*m_current_input))
		{
			if (!apply_predicate()->is_iterable()) // aka == false
			{
				m_current_filtered = m_current_input->head().shared_from_this();
				return;
			}
			m_current_input = m_current_input->tail().shared_from_this();
		}
		m_current_filtered = Nil_Expression::NIL;
	}

	auto Filtering_Generator::apply_predicate() const -> shared_ptr<const Expression>
	{
		auto local_env{m_stack->new_child()};
		// The map function expects exactly one argument!
		local_env->define_local_var(m_map_functions_param_name, m_current_input->head().shared_from_this());
		return m_pred_fun->evaluate(*local_env);
	};

} // namespace appsl
