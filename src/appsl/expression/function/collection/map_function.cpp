// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/collection/map_function.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/function/collection/mapping_generator.hpp"
#include "appsl/expression/function_application_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Map_Function::Map_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Map_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Map_Function const *const>(&other);
	}

	auto Map_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Map_Function>(function_display_name());
	}

	static const string COLLECTION_PARAM_NAME{"collection"};
	static const string TRANSFORM_PARAM_NAME{"transformation"};

	auto Map_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		// For the sake of lazyness arguments passed in are unevaluated. They are evaluated here, because it is
		// now required. Local scope prevends arguments leaking.
		auto local_env{stack.new_child()};
		const auto collection{stack.lookup(COLLECTION_PARAM_NAME).evaluate(*local_env)};
		const auto map_fun{stack.lookup(TRANSFORM_PARAM_NAME).evaluate(*local_env)};
		if (map_fun->is_function())
		{
			if (map_fun->parameter_list().head().is_nil())
			{
				throw Semantic_Error{"Too few parameters. Transformation function to be applied by " +
				                                     text::single_quoted(function_display_name()) +
				                                     " must have exactly one parameter. But " +
				                                     text::single_quoted(map_fun->stringify()) +
				                                     " has none.",
				                     map_fun->token().position()};
			}
			else if (!map_fun->parameter_list().tail().is_nil())
			{
				throw Semantic_Error{
						"Too many parameters. Transformation function to be applied by " +
								text::single_quoted(function_display_name()) +
								" must have exactly one parameter. But " +
								text::single_quoted(map_fun->stringify()) + " has " +
								to_string(expr::count(map_fun->parameter_list())) + ".",
						map_fun->token().position()};
			}
			else
			{
				return make_shared<Mapping_Generator>(collection, map_fun, stack.shared_from_this());
				// This was a bug: stack.global().new_child()
			}
		}
		else
		{
			throw Semantic_Error{"Not a function: " + TRANSFORM_PARAM_NAME + " = " +
			                                     text::single_quoted(map_fun->stringify()) +
			                                     ". Cannot be applied by " +
			                                     text::single_quoted(function_display_name()) +
			                                     " as a transformation function.",
			                     map_fun->token().position()};
		};
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(
					Token::NONE,
					make_unique<Name_Expression>(Token{Token::Type::NAME, COLLECTION_PARAM_NAME,
	                                                                   Token::NO_POS})),
			make_unique<Quoted_Expression>(
					Token::NONE,
					make_unique<Name_Expression>(Token{Token::Type::NAME, TRANSFORM_PARAM_NAME,
	                                                                   Token::NO_POS}))};

	auto Map_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
