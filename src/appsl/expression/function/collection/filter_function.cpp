// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/collection/filter_function.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/function/collection/filtering_generator.hpp"
#include "appsl/expression/function_application_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Filter_Function::Filter_Function(const Fun_Display_Name &name)
			: Filter_Function{name, Filtering_Generator::Mode::keep}
	{
	}

	Filter_Function::Filter_Function(const Fun_Display_Name &name, const Filtering_Generator::Mode mode)
			: Abstract_Builtin_Function{name}, m_mode{mode}
	{
	}

	auto Filter_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Filter_Function const *const>(&other);
	}

	auto Filter_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Filter_Function>(function_display_name());
	}

	static const string COLLECTION_PARAM_NAME{"collection"};
	static const string PREDICATE_PARAM_NAME{"predicate"};

	auto Filter_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		// For the sake of lazyness arguments passed in are unevaluated.
		// They are evaluated here, because it is now required.
		const auto collection{stack.lookup(COLLECTION_PARAM_NAME).evaluate(stack)};
		const auto pred_fun{stack.lookup(PREDICATE_PARAM_NAME).evaluate(stack)};
		if (pred_fun->is_function())
		{
			if (pred_fun->parameter_list().head().is_nil())
			{
				throw Semantic_Error{"Too few parameters. Predicate function to be applied by " +
				                                     text::single_quoted(function_display_name()) +
				                                     " must have exactly one parameter. But " +
				                                     text::single_quoted(pred_fun->stringify()) +
				                                     " has none.",
				                     pred_fun->token().position()};
			}
			else if (!pred_fun->parameter_list().tail().is_nil())
			{
				throw Semantic_Error{
						"Too many parameters. Predicate function to be applied by " +
								text::single_quoted(function_display_name()) +
								" must have exactly one parameter. But " +
								text::single_quoted(pred_fun->stringify()) + " has " +
								to_string(expr::count(pred_fun->parameter_list())) +
								".",
						pred_fun->token().position()};
			}
			else
			{
				return make_shared<Filtering_Generator>(collection, pred_fun, m_mode,
				                                        stack.shared_from_this());
				// This was a bug: stack.global().new_child());
			}
		}
		else
		{
			throw Semantic_Error{"Not a function: " + PREDICATE_PARAM_NAME + " = " +
			                                     text::single_quoted(pred_fun->stringify()) +
			                                     ". Cannot be applied by " +
			                                     text::single_quoted(function_display_name()) +
			                                     " as a predicate function.",
			                     pred_fun->token().position()};
		};
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(
					Token::NONE,
					make_unique<Name_Expression>(Token{Token::Type::NAME, COLLECTION_PARAM_NAME,
	                                                                   Token::NO_POS})),
			make_unique<Quoted_Expression>(
					Token::NONE,
					make_unique<Name_Expression>(Token{Token::Type::NAME, PREDICATE_PARAM_NAME,
	                                                                   Token::NO_POS}))};

	auto Filter_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
