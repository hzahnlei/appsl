// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/collection/range_generator.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	const shared_ptr<const Expression> Range_Generator::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Range-Generator", Token::NO_POS})};

	Range_Generator::Range_Generator(const long from, const long to, shared_ptr<Stack_Frame> stack)
			: m_from{from}, m_to{to}, m_stack{stack}, m_current_input{from},
			  m_current_mapped{make_shared<Integer_Literal_Expression>(Token::NONE, from)}
	{
	}

	auto Range_Generator::operator==(const Expression &other) const -> bool
	{
		// TODO
		return nullptr != dynamic_cast<Range_Generator const *const>(&other);
	}

	auto Range_Generator::operator<=(const Expression &other) const -> bool
	{
		// TODO
		return *this == other;
	}

	auto Range_Generator::operator<(const Expression &) const -> bool
	{
		// TODO
		return false;
	}

	auto Range_Generator::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Range_Generator>(m_from, m_to, m_stack);
	}

	auto Range_Generator::token() const -> const Token & { return Token::NONE; }

	auto Range_Generator::is_iterable() const -> bool { return m_current_input <= m_to; }

	auto Range_Generator::head() const -> const Expression & { return *m_current_mapped; }

	auto Range_Generator::tail() const -> const Expression &
	{
		if (m_current_input >= m_to)
		{
			return *Nil_Expression::NIL;
		}
		else
		{
			m_current_input++;
			m_current_mapped = make_shared<Integer_Literal_Expression>(Token::NONE, m_current_input);
		}
		return *this;
	}

	auto Range_Generator::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Range_Generator::is_nil() const -> bool { return m_current_input > m_to; }

	auto Range_Generator::is_name() const -> bool { return false; }

	auto Range_Generator::name() const -> const string & { NOT_SUPPORTED(); }

	auto Range_Generator::is_built_in_function() const -> bool { return false; }

	auto Range_Generator::is_function() const -> bool { return false; }

	auto Range_Generator::is_function_application() const -> bool { return false; }

	auto Range_Generator::is_fun_macro() const -> bool { return false; }

	auto Range_Generator::is_integer() const -> bool { return false; }

	auto Range_Generator::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Range_Generator::is_real() const -> bool { return false; }

	auto Range_Generator::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Range_Generator::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Range_Generator::is_variadic_parameter() const -> bool { return false; }

	auto Range_Generator::is_quoted() const -> bool { return false; }

	auto Range_Generator::is_def_macro() const -> bool { return false; }

	auto Range_Generator::is_name_value_pair() const -> bool { return false; }

	auto Range_Generator::is_text() const -> bool { return false; }

	auto Range_Generator::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Range_Generator::is_list() const -> bool { return true; }

	auto Range_Generator::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:range-generator from:" << to_string(m_from) << " to:" << to_string(m_to)
		           << ")";
	}

	auto Range_Generator::stringify(ostream &out, const string &indentation) const -> void
	{
		auto i{0U};
		do
		{
			if (i > 0)
			{
				out << '\n';
			}
			head().stringify(out, indentation);
			i++;
			// } while (!tail().is_nil());
		} while (tail().is_iterable());
	}

} // namespace appsl
