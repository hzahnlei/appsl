// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/collection/head_function.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Head_Function::Head_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Head_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Head_Function const *const>(&other);
	}

	auto Head_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Head_Function>(function_display_name());
	}

	static const string COLLECTION_PARAM_NAME{"collection"};

	auto Head_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &collection{stack.lookup(COLLECTION_PARAM_NAME)};
		return collection.head().shared_from_this();
	}

	static const Name_Expression COLLECTION_PARAM{Token{Token::Type::NAME, COLLECTION_PARAM_NAME, Token::NO_POS}};

	auto Head_Function::parameter_list() const -> const Expression & { return COLLECTION_PARAM; }

} // namespace appsl
