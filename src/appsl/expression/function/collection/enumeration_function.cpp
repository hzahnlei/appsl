// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/collection/enumeration_function.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/function/collection/enumeration_generator.hpp"
#include "appsl/expression/function_application_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Enumeration_Function::Enumeration_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Enumeration_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Enumeration_Function const *const>(&other);
	}

	auto Enumeration_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Enumeration_Function>(function_display_name());
	}

	static const string COLLECTION_PARAM_NAME{"collection"};

	auto Enumeration_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		// For the sake of lazyness arguments passed in are unevaluated.
		// They are evaluated here, because it is now required.
		const auto &collection{stack.lookup(COLLECTION_PARAM_NAME)};
		return make_shared<Enumeration_Generator>(collection.clone());
	}

	static const Name_Expression PARAMETERS{Token{Token::Type::NAME, COLLECTION_PARAM_NAME, Token::NO_POS}};

	auto Enumeration_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
