// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/text/upper_case_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/text_literal_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Upper_Case_Function::Upper_Case_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Upper_Case_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Upper_Case_Function const *const>(&other);
	}

	auto Upper_Case_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Upper_Case_Function>(function_display_name());
	}

	static const string STR_PARAM_NAME{"str"};

	auto Upper_Case_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &str{stack.lookup(STR_PARAM_NAME)};
		if (!str.is_text())
		{
			throw Semantic_Error{"Function " + text::single_quoted(function_display_name()) +
			                                     " is only applicable to text literals. However, " +
			                                     text::single_quoted(str.stringify()) +
			                                     " is no text literal.",
			                     str.token().position()};
		}
		string all_upper_case;
		algo::for_each(str.text_value(), [&](const auto c) { all_upper_case.push_back((char)toupper(c)); });
		return make_shared<Text_Literal_Expression>(
				Token{Token::Type::TEXT_LITERAL, all_upper_case, str.token().position()});
	}

	static const Name_Expression PARAMETERS{Token{Token::Type::NAME, STR_PARAM_NAME, Token::NO_POS}};

	auto Upper_Case_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
