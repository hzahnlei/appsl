// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/case_macro.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;
	using namespace fmt;

	Case_Macro::Case_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Case_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Case_Macro const *const>(&other);
	}

	auto Case_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Case_Macro>(function_display_name());
	}

	static const string CASES_PARAM_NAME{"cases"};

	auto Case_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &cases{stack.lookup(CASES_PARAM_NAME)};
		assert_correctness(cases);
		auto iter{const_cast<Expression *>(&cases)};
		while (iter->is_iterable())
		{
			const auto &curr_case{iter->head()};
			if (curr_case.is_key_value_pair())
			{
				auto local_scope{stack.new_child()};
				// Access to condition of condition -> expression pair
				const auto condition{curr_case.head().evaluate(*local_scope)};
				if (!condition->is_nil())
				{
					// Access to expression of condition -> expression pair
					return curr_case.tail().evaluate(*local_scope);
				}
			}
			else
			{
				// Default case
				auto local_scope{stack.new_child()};
				return curr_case.evaluate(*local_scope);
			}
			iter = const_cast<Expression *>(&(iter->tail()));
		}
		throw Runtime_Error{format("No case did match: {}", cases.stringify())};
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(
					Token::NONE,
					make_unique<Variadic_Parameter>(Token::NONE, make_unique<Name_Expression>(Token{
												     Token::Type::NAME,
												     CASES_PARAM_NAME,
												     Token::NO_POS})))};

	auto Case_Macro::parameter_list() const -> const Expression & { return PARAMETERS; }

	static const string DEFAULT_NAME{"default"};

	auto Case_Macro::assert_correctness(const Expression &cases) -> size_t
	{
		const auto case_count{expr::count(cases)}; // This iterates the list, so we iterate twice. Optimize?
		auto i{0U};
		const auto is_last_case{[&]() { return i >= case_count - 1; }};
		expr::for_each(cases,
		               [&](const auto &case_expr)
		               {
				       if (is_last_case())
				       {
					       if (case_expr.is_key_value_pair() &&
				                   (!case_expr.head().is_name() ||
				                    case_expr.head().name() != DEFAULT_NAME))
					       {
						       throw Semantic_Error{
								       format("The last case in a case expression "
					                                      "needs to be either a single expression "
					                                      "or a pair of the form '{} -> "
					                                      "<expression>'. This {} is neither of "
					                                      "those.",
					                                      DEFAULT_NAME,
					                                      text::single_quoted(
											      case_expr.stringify())),
								       case_expr.token().position()};
					       }
				       }
				       else if (!case_expr.is_key_value_pair())
				       {
					       throw Semantic_Error{format("The caseses in a case expression, except "
				                                           "for the last/default case, need to be "
				                                           "pairs of the form '<condition> -> "
				                                           "<expression>'. This {} is not such a pair.",
				                                           text::single_quoted(case_expr.stringify())),
				                                    case_expr.token().position()};
				       }
				       i++;
			       });
		return case_count;
	}

} // namespace appsl
