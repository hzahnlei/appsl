// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/relational/less_than_function.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Less_Than_Function::Less_Than_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Less_Than_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Less_Than_Function const *const>(&other);
	}

	auto Less_Than_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Less_Than_Function>(function_display_name());
	}

	static const string LHS_PARAM_NAME{"lhs"};
	static const string RHS_PARAM_NAME{"rhs"};

	auto Less_Than_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &lhs{stack.lookup(LHS_PARAM_NAME)};
		const auto &rhs{stack.lookup(RHS_PARAM_NAME)};
		return stack.boolean_literal_from(lhs < rhs);
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_shared<Name_Expression>(Token{Token::Type::NAME, LHS_PARAM_NAME, Token::NO_POS}),
			make_shared<List_Expression>(Token::NONE, make_shared<Name_Expression>(Token{Token::Type::NAME,
	                                                                                             RHS_PARAM_NAME,
	                                                                                             Token::NO_POS}))};

	auto Less_Than_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

	// auto Less_Than_Function::stringify(ostream &out, const string &indentation) const -> void
	// {
	//         out << "<";
	// }

	// auto Less_Than_Function::function_name() const -> string
	// {
	//         return "less than";
	// }

} // namespace appsl
