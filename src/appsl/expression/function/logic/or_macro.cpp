// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/logic/or_macro.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Or_Macro::Or_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Or_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Or_Macro const *const>(&other);
	}

	auto Or_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Or_Macro>(function_display_name());
	}

	static const string TERMS_PARAM_NAME{"terms"};

	auto Or_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &terms{stack.lookup(TERMS_PARAM_NAME)};
		const auto result{expr::any_of(terms,
		                               [&](const auto &term) { return !(term.evaluate(stack)->is_nil()); })};
		return stack.boolean_literal_from(result);
	}

	static const Quoted_Expression TERMS_PARAMETER{
			Token::NONE, make_shared<Variadic_Parameter>(
						     Token::NONE, make_shared<Name_Expression>(Token{Token::Type::NAME,
	                                                                                             TERMS_PARAM_NAME,
	                                                                                             Token::NO_POS}))};

	auto Or_Macro::parameter_list() const -> const Expression & { return TERMS_PARAMETER; }

} // namespace appsl
