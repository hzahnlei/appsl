// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/logic/and_macro.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	And_Macro::And_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto And_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<And_Macro const *const>(&other);
	}

	auto And_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<And_Macro>(function_display_name());
	}

	static const string FACTORS_PARAM_NAME{"factors"};

	auto And_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &terms{stack.lookup(FACTORS_PARAM_NAME)};
		const auto result{expr::all_of(terms,
		                               [&](const auto &term) { return !(term.evaluate(stack)->is_nil()); })};
		return stack.boolean_literal_from(result);
	}

	static const Quoted_Expression FACTORS_PARAMETER{
			Token::NONE, make_shared<Variadic_Parameter>(
						     Token::NONE, make_shared<Name_Expression>(Token{Token::Type::NAME,
	                                                                                             FACTORS_PARAM_NAME,
	                                                                                             Token::NO_POS}))};

	auto And_Macro::parameter_list() const -> const Expression & { return FACTORS_PARAMETER; }

} // namespace appsl
