// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/logic/true_function.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	True_Function::True_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto True_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<True_Function const *const>(&other);
	}

	auto True_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<True_Function>(function_display_name());
	}

	static const string THEN_PARAM_NAME{"then"};
	static const string ELSE_PARAM_NAME{"else"};

	auto True_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &then_branch{stack.lookup(THEN_PARAM_NAME)};
		return then_branch.evaluate(stack);
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_shared<Quoted_Expression>(Token::NONE, make_shared<Name_Expression>(Token{
										    Token::Type::NAME, THEN_PARAM_NAME,
										    Token::NO_POS})),
			make_shared<List_Expression>( // TODO why list?
					Token::NONE,
					make_shared<Quoted_Expression>(
							Token::NONE, make_shared<Name_Expression>(Token{
										     Token::Type::NAME, ELSE_PARAM_NAME,
										     Token::NO_POS})))};

	auto True_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
