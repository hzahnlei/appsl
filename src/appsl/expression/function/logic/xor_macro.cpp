// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/logic/xor_macro.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Xor_Macro::Xor_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Xor_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Xor_Macro const *const>(&other);
	}

	auto Xor_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Xor_Macro>(function_display_name());
	}

	static const string TERMS_PARAM_NAME{"terms"};

	auto Xor_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &terms{stack.lookup(TERMS_PARAM_NAME)};
		auto true_count{0U};
		expr::any_of(terms,
		             [&](const auto &term)
		             {
				     if (!term.evaluate(stack)->is_nil())
				     {
					     true_count++;
				     }
				     return true_count > 1;
			     });
		return stack.boolean_literal_from(1 == true_count);
	}

	static const Quoted_Expression TERMS_PARAMETER{
			Token::NONE, make_shared<Variadic_Parameter>(
						     Token::NONE, make_shared<Name_Expression>(Token{Token::Type::NAME,
	                                                                                             TERMS_PARAM_NAME,
	                                                                                             Token::NO_POS}))};

	auto Xor_Macro::parameter_list() const -> const Expression & { return TERMS_PARAMETER; }

} // namespace appsl
