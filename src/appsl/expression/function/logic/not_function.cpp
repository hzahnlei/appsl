// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/logic/not_function.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Not_Function::Not_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Not_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Not_Function const *const>(&other);
	}

	auto Not_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Not_Function>(function_display_name());
	}

	static const string VALUE_PARAM_NAME{"value"};

	auto Not_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &value{stack.lookup(VALUE_PARAM_NAME)};
		// return stack.boolean_literal_from(value.is_nil());
		return stack.boolean_literal_from(!value.is_iterable());
	}

	static const List_Expression PARAMETER{
			Token::NONE,
			make_shared<Name_Expression>(Token{Token::Type::NAME, VALUE_PARAM_NAME, Token::NO_POS})};

	auto Not_Function::parameter_list() const -> const Expression & { return PARAMETER; }

} // namespace appsl
