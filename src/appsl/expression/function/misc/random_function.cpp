// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/misc/random_function.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Random_Function::Random_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Random_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Random_Function const *const>(&other);
	}

	auto Random_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Random_Function>(function_display_name());
	}

	auto Random_Function::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		const auto rnd{rand()};
		const auto str_representation{to_string(rnd)};
		return make_shared<Integer_Literal_Expression>(
				Token{Token::Type::INTEGER_LITERAL, str_representation, Token::NO_POS}, rnd);
	}

	auto Random_Function::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

} // namespace appsl
