// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/misc/seed_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Seed_Function::Seed_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Seed_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Seed_Function const *const>(&other);
	}

	auto Seed_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Seed_Function>(function_display_name());
	}

	static const string SEED_PARAM_NAME{"s"};

	auto Seed_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &seed{stack.lookup(SEED_PARAM_NAME)};
		if (!seed.is_integer())
		{
			throw appsl::Runtime_Error{"Argument s = " + text::single_quoted(seed.stringify()) +
			                           " to seed function is not an integer."};
		}
		if (seed.int_value() < 0)
		{
			throw appsl::Runtime_Error{"Argument s = " + text::single_quoted(seed.stringify()) +
			                           " to seed function too small: 0 ≤ s ≤ " + to_string(RAND_MAX) + "!"};
		}
		if (seed.int_value() > RAND_MAX)
		{
			throw appsl::Runtime_Error{"Argument s = " + text::single_quoted(seed.stringify()) +
			                           " to seed function too big: 0 ≤ s ≤ " + to_string(RAND_MAX) + "!"};
		}
		srand(seed.int_value());
		return seed.clone();
	}

	static const Name_Expression SEED_PARAM{Token{Token::Type::NAME, SEED_PARAM_NAME, Token::NO_POS}};

	auto Seed_Function::parameter_list() const -> const Expression & { return SEED_PARAM; }

} // namespace appsl
