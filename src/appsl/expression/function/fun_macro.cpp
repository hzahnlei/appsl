// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/fun_macro.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/function/function.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;
	using fmt::format;

	Fun_Macro::Fun_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Fun_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Fun_Macro const *const>(&other);
	}

	auto Fun_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Fun_Macro>(function_display_name());
	}

	inline auto needs_to_be_captured(const string &name_from_body, const set<string> &parameter_names,
	                                 const Stack_Frame &stack)
	{
		// TODO this condition causes unnecessary captures, it needs to be more strict
		return parameter_names.find(name_from_body) == parameter_names.end() &&
		       stack.is_defined(name_from_body) && !stack.is_defined_globally(name_from_body);
	}

	auto captured_scope(const Expression &function_body, const set<string> &parameter_names,
	                    const Stack_Frame &stack)
	{
		auto names_from_body = expr::names_from(function_body);
		Function::Captured_Scope capture;
		algo::for_each(names_from_body,
		               [&](const auto &name_from_body)
		               {
				       if (needs_to_be_captured(name_from_body, parameter_names, stack))
				       {
					       capture[name_from_body] =
							       stack.lookup(name_from_body).shared_from_this();
				       }
			       });
		return capture;
	}

	static const string PARAMS_PARAM_NAME{"params"};

	static const string BODY_PARAM_NAME{"body"};

	auto Fun_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &function_parameters{stack.lookup(PARAMS_PARAM_NAME)};
		const auto param_names{ensure_correct_param_list(function_parameters)};

		const auto &function_body{stack.lookup(BODY_PARAM_NAME)};
		const auto scope{[&](const auto &name)
		                 { return param_names.find(name) != param_names.end() || stack.is_defined(name); }};
		ensure_names_are_defined_in_scope(function_body, scope);

		// Functions with a non-empty capture are closures.
		return make_shared<Function>(function_parameters.shared_from_this(), function_body.shared_from_this(),
		                             captured_scope(function_body, param_names, stack));
	}

	auto Fun_Macro::is_fun_macro() const -> bool { return true; }

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(Token::NONE, make_unique<Name_Expression>(Token{
										    Token::Type::NAME,
										    PARAMS_PARAM_NAME, Token::NO_POS})),
			make_unique<List_Expression>(
					Token::NONE,
					make_unique<Quoted_Expression>(
							Token::NONE, make_unique<Name_Expression>(Token{
										     Token::Type::NAME, BODY_PARAM_NAME,
										     Token::NO_POS})))};

	auto Fun_Macro::parameter_list() const -> const Expression & { return PARAMETERS; }

	inline auto not_defined_yet(const set<string> &names, const string &name)
	{
		return names.find(name) == names.end();
	}

	auto Fun_Macro::ensure_correct_param_list(const Expression &parameters) const -> set<string>
	{
		set<string> param_names;
		Expression const *variadic_param_found{nullptr};
		expr::for_each(parameters,
		               [&](const auto &param)
		               {
				       if (nullptr == variadic_param_found)
				       {
					       if (param.is_variadic_parameter())
					       {
						       variadic_param_found = &param;
					       }
					       const auto &param_name{expr::pure_param_name_without_quotes_etc(param)};
					       if (not_defined_yet(param_names, param_name.name()))
					       {
						       param_names.emplace(param_name.name());
					       }
					       else
					       {
						       throw Semantic_Error{
								       format("Duplicate definition of parameter {}.",
					                                      text::single_quoted(param.stringify())),
								       param.token().position()};
					       }
				       }
				       else
				       {
					       throw Semantic_Error{
							       format("A function can only have one variadic "
				                                      "parameter. And, it has to be the last "
				                                      "parameter. However, variadic parameter {} is "
				                                      "followed by further parameters, for example {}.",
				                                      text::single_quoted(
										      variadic_param_found
												      ->stringify()),
				                                      text::single_quoted(param.stringify())),
							       param.token().position()};
				       }
			       });
		return param_names;
	}

	auto Fun_Macro::ensure_names_are_defined_in_scope(const Expression &expression, const Is_In_Scope &scope) const
			-> void
	{
		if (expression.is_name())
		{
			ensure_name_is_defined_in_scope(expression, scope);
		}
		else if (expression.is_function_application())
		{
			ensure_body_uses_only_names_from_scope(expression, scope);
		}
		else if (expression.is_name_value_pair())
		{
			ensure_names_are_defined_in_scope(expr::pure_argument_value_without_name(expression), scope);
		}
	}

	auto Fun_Macro::ensure_name_is_defined_in_scope(const Expression &name, const Is_In_Scope &is_in_scope) const
			-> void
	{
		if (!is_in_scope(name.name()))
		{
			throw Semantic_Error{format("Name {} is undefined.", text::single_quoted(name.stringify())),
			                     name.token().position()};
		}
	}

	// TODO move these inline functions to "expression traits"?
	inline auto is_fun_macro_name(const Expression &expression) -> bool
	{
		// TODO Use constant instead of "fun", same constant as for registering with runtime
		return expression.is_name() && "fun" == expression.name();
	}

	inline auto is_def_macro_name(const Expression &expression)
	{
		return expression.is_name() && "def!" == expression.name();
	}

	inline auto is_let_macro_name(const Expression &expression)
	{
		return expression.is_name() && "let" == expression.name();
	}

	inline auto is_tagged_macro_name(const Expression &expression)
	{
		// TODO Use constant instead of "tagged", same constant as for registering with runtime
		return expression.is_name() && "tagged" == expression.name();
	}

	auto Fun_Macro::ensure_body_uses_only_names_from_scope(const Expression &fun_app,
	                                                       const Is_In_Scope &is_in_scope) const -> void
	{
		if (fun_app.head().is_function_application())
		{
			ensure_names_are_defined_in_scope(fun_app.head(), is_in_scope);
		}
		else if (is_fun_macro_name(fun_app.head()))
		{
			const auto param_names{ensure_correct_param_list(fun_app.tail().head())};
			const auto function_scope{[&](const auto &name) {
				return param_names.find(name) != param_names.end() || is_in_scope(name);
			}};
			const auto &function_body{fun_app.tail().tail()};
			ensure_names_are_defined_in_scope(function_body, function_scope);
		}
		else if (is_def_macro_name(fun_app.head()))
		{
			const auto &name_to_be_defined{fun_app.tail().head()};
			const auto macro_scope{[&](const auto &name)
			                       { return name_to_be_defined.name() == name || is_in_scope(name); }};
			const auto &value_to_be_assigned{fun_app.tail().tail()};
			ensure_names_are_defined_in_scope(value_to_be_assigned, macro_scope);
		}
		else if (is_let_macro_name(fun_app.head())) // head = let
		{
			// tail = definitions and return value
			const auto &definitions{fun_app.tail().head()}; // tail.head = definitions
			const auto let_names{names_from_let_definitions(definitions)};
			const auto let_scope{[&](const auto &name)
			                     { return let_names.find(name) != let_names.end() || is_in_scope(name); }};
			expr::for_each(definitions, [&](const auto &def)
			               { ensure_names_are_defined_in_scope(def.tail(), let_scope); });
			const auto &return_value{fun_app.tail().tail()}; // tail.tail = return value
			ensure_names_are_defined_in_scope(return_value, let_scope);
		}
		else if (is_tagged_macro_name(fun_app.head()))
		{
			// TODO instead of checking specific functions it would be better if parameters could be marked
			// as "must be free" or "might be free". This might also be used by the def! macro.
			ensure_names_are_defined_in_scope(fun_app.head(), is_in_scope);
			// Tail is the tag name which may not be defined. But this is OK in case of the 'tagged' macro.
			// ensure_names_are_defined_in_scope(fun_app.tail(), is_in_scope);
		}
		else
		{
			ensure_names_are_defined_in_scope(fun_app.head(), is_in_scope);
			ensure_names_are_defined_in_scope(fun_app.tail(), is_in_scope);
		}
	}

	/**
	 * @brief Make sure a let-expression only references names that are actually defined.
	 *
	 * A let-expression has the form of (let <definitions> <return value>) where <definitions> is a list of pairs of
	 * the form (<name> <expression>). This assigns <expressions> to the given names. <return value> is a simple
	 * expression. It may reference the names defined by the <definitions> list.
	 *
	 * This check makes sure the definitions in the <definitions> list only reference names from the scope. The
	 * scope is made up from the parent scope plus the names successively defined by the let-expression. Finally the
	 * function ensures the <return value> expression only refereces names from the scope.
	 *
	 * The expression tree (aka AST) has the following form:
	 * <pre>
	 * (FUNAPP)
	 * +--head-- (NAME "let")
	 * +--tail-- (FUNAPP)
	 *           +--head-- <definitions>
	 *           +--tail-- <return value>
	 * </pre>
	 * <defintions> is a list of pairs, where the pairs are of the form (<name> <expression>):
	 * <pre>
	 * (LIST)
	 * +--head-- (LIST)
	 * !         +--head-- (NAME <name1>)
	 * !         +--tail-- (EXPR <value1>)
	 * +--tail-- (LIST)
	 *           +--head-- (LIST)
	 *           !         +--head-- (NAME <name2>)
	 *           !         +--tail-- (EXPR <value2>)
	 *           +--tail-- (LIST)
	 *                     +--head-- (LIST)
	 *                     !         +--head-- (NAME <name3>)
	 *                     !         +--tail-- (EXPR <value2)
	 *                     +--tail-- ...
	 * </pre>
	 * The <return value> is an expression that gets evaluated before returning its result. (This is a rather
	 * procedural description. In Lisp language the evaluated return value is what the let-expression evaluates to.)
	 * The <return value> expression may reference the parent scope as well as the local scope defined by the
	 * let-expression.
	 */
	auto Fun_Macro::names_from_let_definitions(const Expression &let_definitions) const -> set<string>
	{
		set<string> names;
		expr::for_each(let_definitions, [&](const auto &def) { names.emplace(def.head().name()); });
		return names;
	}

} // namespace appsl
