// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/file/import_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/text_literal_expression.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/parser/scanner.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Import_Function::Import_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Import_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Import_Function const *const>(&other);
	}

	auto Import_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Import_Function>(function_display_name());
	}

	static const string FILENAME_PARAM_NAME{"file-name"};

	auto Import_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &file_name{stack.lookup(FILENAME_PARAM_NAME)};
		ensure_valid_file_name(file_name);
		const auto source_code{content_of_file(file_name.text_value())};
		evaluate(stack.parent(), source_code);
		return stack.boolean_literal_from(true);
	}

	static const Name_Expression FILENAME_PARAM{Token{Token::Type::NAME, FILENAME_PARAM_NAME, Token::NO_POS}};

	auto Import_Function::parameter_list() const -> const Expression & { return FILENAME_PARAM; }

	auto Import_Function::ensure_valid_file_name(const Expression &file_name) const -> void
	{
		if (!file_name.is_text())
		{
			throw Semantic_Error{"File name has to be a text literal. " +
			                                     text::single_quoted(file_name.stringify()) +
			                                     " is no text literal.",
			                     file_name.token().position()};
		}
		else if (file_name.text_value().empty())
		{
			throw Semantic_Error{"File name must not be empty.", file_name.token().position()};
		}
	}

	auto Import_Function::content_of_file(const string &file_name) const -> string
	{
		try
		{
			return junkbox::file::load_text_file(file_name);
		}
		catch (const Base_Exception &cause)
		{
			throw Runtime_Error{"Failed to load text file " + text::single_quoted(file_name) + ". " +
			                    cause.pretty_message()};
		}
		catch (const exception &cause)
		{
			throw Runtime_Error{"Failed to load text file " + text::single_quoted(file_name) + ". " +
			                    cause.what()};
		}
	}

	auto Import_Function::evaluate(Stack_Frame &stack, const string &source_code) const -> void
	{
		try
		{
			Scanner scanner{source_code};
			Parser parser{scanner};
			while (parser.has_more_to_parse())
			{
				parser.parse()->evaluate(stack);
			}
		}
		catch (const Base_Exception &cause)
		{
			throw Runtime_Error{"Failed to parse " + text::single_quoted(source_code) + ". " +
			                    cause.pretty_message()};
		}
		catch (const exception &cause)
		{
			throw Runtime_Error{"Failed to parse " + text::single_quoted(source_code) + ". " +
			                    cause.what()};
		}
	}

} // namespace appsl
