// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/generic_binary_macro.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Generic_Binary_Macro::Generic_Binary_Macro(const string &name, const Binary_Cpp_Fun fun)
			: Abstract_Builtin_Function{name}, m_binary_fun(fun)
	{
	}

	auto Generic_Binary_Macro::operator==(const Expression &other) const -> bool
	{
		const auto other_fun = dynamic_cast<Generic_Binary_Macro const *const>(&other);
		return nullptr != other_fun && function_display_name() == other_fun->function_display_name();
	}

	auto Generic_Binary_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Generic_Binary_Macro>(function_display_name(), m_binary_fun);
	}

	static const string LHS_PARAM_NAME{"lhs"};
	static const string RHS_PARAM_NAME{"rhs"};

	auto Generic_Binary_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &lhs{stack.lookup(LHS_PARAM_NAME)};
		const auto &rhs{stack.lookup(RHS_PARAM_NAME)};
		const auto local_env{stack.new_child()};
		return m_binary_fun(*local_env, lhs, rhs);
	}

	static const List_Expression EXPR_PARAMS{
			Token::NONE,
			make_shared<Quoted_Expression>(Token::NONE, make_shared<Name_Expression>(Token{
										    Token::Type::NAME, LHS_PARAM_NAME,
										    Token::NO_POS})),
			make_shared<Quoted_Expression>(Token::NONE, make_shared<Name_Expression>(Token{
										    Token::Type::NAME, RHS_PARAM_NAME,
										    Token::NO_POS}))};

	auto Generic_Binary_Macro::parameter_list() const -> const Expression & { return EXPR_PARAMS; }

} // namespace appsl
