// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/function/function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	const shared_ptr<const Expression> Abstract_Builtin_Function::TYPE_TAG{Function::TYPE_TAG};

	Abstract_Builtin_Function::Abstract_Builtin_Function(const Fun_Display_Name &name) : m_display_name{name} {}

	auto Abstract_Builtin_Function::operator<=(const Expression &other) const -> bool { return *this == other; }

	auto Abstract_Builtin_Function::operator<(const Expression &) const -> bool { return false; }

	auto Abstract_Builtin_Function::token() const -> const Token & { NOT_SUPPORTED(); }

	auto Abstract_Builtin_Function::head() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Abstract_Builtin_Function::tail() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Abstract_Builtin_Function::is_nil() const -> bool { return false; }

	auto Abstract_Builtin_Function::is_name() const -> bool { return false; }

	auto Abstract_Builtin_Function::name() const -> const string & { NOT_SUPPORTED(); }

	auto Abstract_Builtin_Function::is_built_in_function() const -> bool { return true; }

	auto Abstract_Builtin_Function::is_function() const -> bool { return true; }

	auto Abstract_Builtin_Function::is_function_application() const -> bool
	{
		return false; // Was true before. Doesn't seem to make a difference.
	}

	auto Abstract_Builtin_Function::is_fun_macro() const -> bool { return false; }

	auto Abstract_Builtin_Function::is_integer() const -> bool { return false; }

	auto Abstract_Builtin_Function::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Abstract_Builtin_Function::is_real() const -> bool { return false; }

	auto Abstract_Builtin_Function::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Abstract_Builtin_Function::is_variadic_parameter() const -> bool { return false; }

	auto Abstract_Builtin_Function::is_quoted() const -> bool { return false; }

	auto Abstract_Builtin_Function::is_def_macro() const -> bool { return false; }

	auto Abstract_Builtin_Function::is_name_value_pair() const -> bool { return false; }

	auto Abstract_Builtin_Function::is_text() const -> bool { return false; }

	auto Abstract_Builtin_Function::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Abstract_Builtin_Function::is_list() const -> bool { return false; }

	auto Abstract_Builtin_Function::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:built-in-function name:" << m_display_name << ")";
	}

	auto Abstract_Builtin_Function::stringify(ostream &out, const string &) const -> void { out << m_display_name; }

	auto Abstract_Builtin_Function::function_display_name() const -> const Fun_Display_Name &
	{
		return m_display_name;
	}

	auto Abstract_Builtin_Function::make_sure_arguments_given(const Stack_Frame &stack) const -> void
	{
		expr::for_each(parameter_list(),
		               [&](const auto &parameter)
		               {
				       if (!stack.is_defined_locally(parameter.name()))
				       {
					       throw Runtime_Error{"No argument value given for parameter " +
				                                   text::single_quoted(parameter.name()) +
				                                   " of function " +
				                                   text::single_quoted(Expression::stringify()) + "."};
				       }
			       });
	}

} // namespace appsl
