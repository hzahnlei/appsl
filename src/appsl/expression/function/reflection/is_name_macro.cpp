// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/reflection/is_name_macro.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Is_Name_Macro::Is_Name_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Is_Name_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Is_Name_Macro const *const>(&other);
	}

	auto Is_Name_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Is_Name_Macro>(function_display_name());
	}

	static const string EXPR_PARAM_NAME{"expr"};

	auto Is_Name_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &expr{stack.lookup(EXPR_PARAM_NAME)};
		return stack.boolean_literal_from(expr.is_name());
	}

	static const Quoted_Expression EXPR_PARAM{
			Token::NONE,
			make_unique<Name_Expression>(Token{Token::Type::NAME, EXPR_PARAM_NAME, Token::NO_POS})};

	auto Is_Name_Macro::parameter_list() const -> const Expression & { return EXPR_PARAM; }

} // namespace appsl
