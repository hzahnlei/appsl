// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/function.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	const shared_ptr<const Expression> Function::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Function", Token::NO_POS})};

	Function::Function(shared_ptr<const Expression> parameters, shared_ptr<const Expression> body)
			: m_parameters(parameters), m_body(body)
	{
	}

	Function::Function(shared_ptr<const Expression> parameters, shared_ptr<const Expression> body,
	                   Captured_Scope &&capture)
			: m_parameters(parameters), m_body(body), m_captured_scope(move(capture))
	{
	}

	auto Function::operator==(const Expression &other) const -> bool
	{
		const auto other_fun_fun = dynamic_cast<Function const *const>(&other);
		return nullptr != other_fun_fun && m_parameters == other_fun_fun->m_parameters &&
		       m_body == other_fun_fun->m_body;
	}

	auto Function::operator<=(const Expression &other) const -> bool { return *this == other; }

	auto Function::operator<(const Expression &) const -> bool { return false; }

	auto Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Function>(m_parameters->clone(), m_body->clone());
	}

	auto Function::token() const -> const Token &
	{
		// NOT_SUPPORTED();
		return m_body->token();
	}

	auto Function::head() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Function::tail() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		return m_captured_scope.empty() ? evaluate_with_runtime_only(stack)
		                                : evaluate_with_runtime_and_captured_scope(stack);
	}

	auto Function::evaluate_with_runtime_only(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		// Local scope prevends arguments leaking.
		auto local_scope{stack.new_child()};
		return m_body->evaluate(*local_scope);
	}

	auto Function::evaluate_with_runtime_and_captured_scope(Stack_Frame &stack) const
			-> shared_ptr<const Expression>
	{
		auto local_scope{stack.new_child()};
		algo::for_each(m_captured_scope, [&](const auto &name_value)
		               { local_scope->define_local_var(name_value.first, name_value.second); });
		return m_body->evaluate(*local_scope);
	}

	auto Function::is_nil() const -> bool { return false; }

	auto Function::is_name() const -> bool { return false; }

	auto Function::name() const -> const string & { NOT_SUPPORTED(); }

	auto Function::is_built_in_function() const -> bool { return false; }

	auto Function::is_function() const -> bool { return true; }

	auto Function::is_function_application() const -> bool { return false; }

	auto Function::is_fun_macro() const -> bool { return false; }

	auto Function::is_integer() const -> bool { return false; }

	auto Function::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Function::is_real() const -> bool { return false; }

	auto Function::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Function::parameter_list() const -> const Expression & { return *m_parameters; }

	auto Function::is_variadic_parameter() const -> bool { return false; }

	auto Function::is_quoted() const -> bool { return false; }

	auto Function::is_def_macro() const -> bool { return false; }

	auto Function::is_name_value_pair() const -> bool { return false; }

	auto Function::is_text() const -> bool { return false; }

	auto Function::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Function::is_list() const -> bool { return false; }

	auto Function::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:function params:" << m_parameters << " body:" << m_body << ")";
	}

	auto Function::stringify(ostream &out, const string &indentation) const -> void
	{
		out << "𝛌";
		m_parameters->stringify(out, indentation);
		out << ".";
		m_body->stringify(out, indentation);
		if (!m_captured_scope.empty())
		{
			auto i{0U};
			out << " (where ";
			algo::for_each(m_captured_scope,
			               [&](const auto &name_value)
			               {
					       if (i > 0)
					       {
						       out << ' ';
					       }
					       out << name_value.first << '=' << name_value.second->stringify();
					       i++;
				       });
			out << ')';
		}
	}

} // namespace appsl
