// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/arithmetic/sqrt_function.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include "appsl/use_stl.hpp"

namespace appsl
{

	SQRT_Function::SQRT_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto SQRT_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<SQRT_Function const *const>(&other);
	}

	auto SQRT_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<SQRT_Function>(function_display_name());
	}

	auto sqrt_of_integer(const Expression &x) -> unique_ptr<Expression>
	{
		const auto x_value{x.int_value()};
		return make_unique<Integer_Literal_Expression>(Token::NONE, sqrt(x_value));
	}

	auto sqrt_of_real(const Expression &x) -> unique_ptr<Expression>
	{
		const auto x_value{x.real_value()};
		return make_unique<Real_Literal_Expression>(Token::NONE, sqrtl(x_value));
	}

	const string X_PARAM_NAME{"x"};

	auto SQRT_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &x{stack.lookup(X_PARAM_NAME)};
		return x.is_real() ? sqrt_of_real(x) : sqrt_of_integer(x);
	}

	const Name_Expression PARAMETERS{Token{Token::Type::NAME, X_PARAM_NAME, Token::NO_POS}};

	auto SQRT_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
