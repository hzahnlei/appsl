// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/arithmetic/sum_function.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Sum_Function::Sum_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Sum_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Sum_Function const *const>(&other);
	}

	auto Sum_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Sum_Function>(function_display_name());
	}

	auto add_list_of_integers(const Stack_Frame &, const Expression &first_term, const Expression &terms)
			-> unique_ptr<Expression>
	{
		Integer_Literal_Expression::Expression::Integer_Representation sum{first_term.int_value()};
		expr::for_each(terms,
		               [&sum](const auto &term)
		               {
				       if (term.is_integer())
				       {
					       sum += term.int_value();
				       }
				       else
				       {
					       throw Runtime_Error{"Sum function is about to add integer numbers. "
				                                   "However, " +
				                                   text::single_quoted(term.head().stringify()) +
				                                   " is no integer number."};
				       }
			       });
		return make_unique<Integer_Literal_Expression>(Token::NONE, sum);
	}

	auto add_list_of_reals(const Stack_Frame &, const Expression &first_term, const Expression &terms)
			-> unique_ptr<Expression>
	{
		Expression::Real_Representation sum{first_term.real_value()};
		expr::for_each(terms,
		               [&sum](const auto &term)
		               {
				       if (term.is_real())
				       {
					       sum += term.real_value();
				       }
				       else
				       {
					       throw Runtime_Error{
							       "Sum function is about to add real numbers. However, " +
							       text::single_quoted(term.head().stringify()) +
							       " is no real number."};
				       }
			       });
		return make_unique<Real_Literal_Expression>(Token::NONE, sum);
	}

	static const string TERM_PARAM_NAME{"term"};
	static const string TERMS_PARAM_NAME{"terms"};

	auto Sum_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &term{stack.lookup(TERM_PARAM_NAME)};
		const auto &terms{stack.lookup(TERMS_PARAM_NAME)};
		return term.is_real() ? add_list_of_reals(stack, term, terms)
		                      : add_list_of_integers(stack, term, terms);
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Name_Expression>(Token{Token::Type::NAME, TERM_PARAM_NAME, Token::NO_POS}),
			make_unique<List_Expression>(
					// FIXME why this list? why not using variadic parameter directly?
	                                //  I have seen stringification in error messages withot "..." when removing
	                                //  list wrapper
					Token::NONE,
					make_unique<Variadic_Parameter>(Token::NONE, make_unique<Name_Expression>(Token{
												     Token::Type::NAME,
												     TERMS_PARAM_NAME,
												     Token::NO_POS})))};

	auto Sum_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
