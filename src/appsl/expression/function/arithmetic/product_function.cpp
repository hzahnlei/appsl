// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/arithmetic/product_function.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{
	using namespace junkbox;

	Product_Function::Product_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Product_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Product_Function const *const>(&other);
	}

	auto Product_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Product_Function>(function_display_name());
	}

	auto multiply_list_of_integers(const Stack_Frame &, const Expression &first_factor, const Expression &factors)
			-> unique_ptr<Expression>
	{
		Integer_Literal_Expression::Expression::Integer_Representation product{first_factor.int_value()};
		expr::for_each(factors,
		               [&product](const auto &factor)
		               {
				       if (factor.is_integer())
				       {
					       product *= factor.int_value();
				       }
				       else
				       {
					       throw Runtime_Error{"Product function is about to multiply integer "
				                                   "numbers. However, " +
				                                   text::single_quoted(factor.head().stringify()) +
				                                   " is no integer number."};
				       }
			       });
		return make_unique<Integer_Literal_Expression>(Token::NONE, product);
	}

	auto multiply_list_of_reals(const Stack_Frame &, const Expression &first_factor, const Expression &factors)
			-> unique_ptr<Expression>
	{
		Expression::Real_Representation product{first_factor.real_value()};
		expr::for_each(factors,
		               [&product](const auto &factor)
		               {
				       if (factor.is_real())
				       {
					       product *= factor.real_value();
				       }
				       else
				       {
					       throw Runtime_Error{"Product function is about to multiply real "
				                                   "numbers. However, " +
				                                   text::single_quoted(factor.head().stringify()) +
				                                   " is no real number."};
				       }
			       });
		return make_unique<Real_Literal_Expression>(Token::NONE, product);
	}

	static const string FACTOR_PARAM_NAME{"factor"};
	static const string FACTORS_PARAM_NAME{"factors"};

	auto Product_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &factor{stack.lookup(FACTOR_PARAM_NAME)};
		const auto &factors{stack.lookup(FACTORS_PARAM_NAME)};
		return factor.is_real() ? multiply_list_of_reals(stack, factor, factors)
		                        : multiply_list_of_integers(stack, factor, factors);
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_shared<Name_Expression>(Token{Token::Type::NAME, FACTOR_PARAM_NAME, Token::NO_POS}),
			make_shared<List_Expression>(
					Token::NONE,
					make_shared<Variadic_Parameter>(Token::NONE, make_unique<Name_Expression>(Token{
												     Token::Type::NAME,
												     FACTORS_PARAM_NAME,
												     Token::NO_POS})))};

	auto Product_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
