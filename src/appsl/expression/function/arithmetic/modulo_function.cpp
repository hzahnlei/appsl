// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/arithmetic/modulo_function.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Modulo_Function::Modulo_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Modulo_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Modulo_Function const *const>(&other);
	}

	auto Modulo_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Modulo_Function>(function_display_name());
	}

	static const string N_PARAM_NAME{"n"};
	static const string M_PARAM_NAME{"m"};

	auto Modulo_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &n{stack.lookup(N_PARAM_NAME)};
		if (n.is_integer())
		{
			const auto &m{stack.lookup(M_PARAM_NAME)};
			if (m.is_integer())
			{
				return make_shared<Integer_Literal_Expression>(Token::NONE,
				                                               n.int_value() % m.int_value());
			}
			else
			{
				throw Runtime_Error{"Argument value " + text::single_quoted(m.stringify()) +
				                    " for parameter " + text::single_quoted(M_PARAM_NAME) +
				                    " has to be of type integer."};
			}
		}
		else
		{
			throw Runtime_Error{"Argument value " + text::single_quoted(n.stringify()) + " for parameter " +
			                    text::single_quoted(N_PARAM_NAME) + " has to be of type integer."};
		}
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_shared<Name_Expression>(Token{Token::Type::NAME, N_PARAM_NAME, Token::NO_POS}),
			make_shared<List_Expression>(Token::NONE,
	                                             make_shared<Name_Expression>(Token{Token::Type::NAME, M_PARAM_NAME,
	                                                                                Token::NO_POS}))};

	auto Modulo_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
