// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/arithmetic/division_function.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Div_Function::Div_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Div_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Div_Function const *const>(&other);
	}

	auto Div_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Div_Function>(function_display_name());
	}

	auto divide_list_of_integers(const Stack_Frame &, const Expression &first_divisor, const Expression &divisors)
			-> unique_ptr<Expression>
	{
		Integer_Literal_Expression::Expression::Integer_Representation quotient{first_divisor.int_value()};
		expr::for_each(divisors,
		               [&quotient](const auto &divisor)
		               {
				       if (divisor.is_integer())
				       {
					       quotient /= divisor.int_value();
				       }
				       else
				       {
					       throw Runtime_Error{"Division function is about to divide integer "
				                                   "numbers. However, " +
				                                   text::single_quoted(divisor.head().stringify()) +
				                                   " is no integer number."};
				       }
			       });
		return make_unique<Integer_Literal_Expression>(Token::NONE, quotient);
	}

	auto divide_scalar_or_list_of_integers(const Stack_Frame &stack, const Expression &divisor,
	                                       const Expression &divisors) -> unique_ptr<Expression>
	{
		// return divisors.is_nil() ? make_unique<Integer_Literal_Expression>(Token::NONE, 1 /
		// divisor.int_value())
		//                          : divide_list_of_integers(stack, divisor, divisors);
		return divisors.is_iterable()
		                       ? divide_list_of_integers(stack, divisor, divisors)
		                       : make_unique<Integer_Literal_Expression>(Token::NONE, 1 / divisor.int_value());
	}

	auto divide_list_of_reals(const Stack_Frame &, const Expression &first_divisor, const Expression &divisors)
			-> unique_ptr<Expression>
	{
		Expression::Real_Representation quotient{first_divisor.real_value()};
		expr::for_each(divisors,
		               [&quotient](const auto &divisor)
		               {
				       if (divisor.is_real())
				       {
					       quotient /= divisor.real_value();
				       }
				       else
				       {
					       throw Runtime_Error{"Division function is about to divide real "
				                                   "numbers. However, " +
				                                   text::single_quoted(divisor.head().stringify()) +
				                                   " is no real number."};
				       }
			       });
		return make_unique<Real_Literal_Expression>(Token::NONE, quotient);
	}

	auto divide_scalar_or_list_of_reals(const Stack_Frame &stack, const Expression &divisor,
	                                    const Expression &divisors) -> unique_ptr<Expression>
	{
		return divisors.is_nil() ? make_unique<Real_Literal_Expression>(Token::NONE, 1.0 / divisor.real_value())
		                         : divide_list_of_reals(stack, divisor, divisors);
	}

	static const string DIVISOR_PARAM_NAME{"divisor"};
	static const string DIVISORS_PARAM_NAME{"divisors"};

	auto Div_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &divisor{stack.lookup(DIVISOR_PARAM_NAME)};
		const auto &divisors{stack.lookup(DIVISORS_PARAM_NAME)};
		return divisor.is_real() ? divide_scalar_or_list_of_reals(stack, divisor, divisors)
		                         : divide_scalar_or_list_of_integers(stack, divisor, divisors);
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Name_Expression>(Token{Token::Type::NAME, DIVISOR_PARAM_NAME, Token::NO_POS}),
			make_unique<List_Expression>(Token::NONE,
	                                             make_unique<Variadic_Parameter>(
								     Token::NONE, make_unique<Name_Expression>(Token{
												  Token::Type::NAME,
												  DIVISORS_PARAM_NAME,
												  Token::NO_POS})))};

	auto Div_Function::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
