// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/def_macro.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Def_Macro::Def_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Def_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Def_Macro const *const>(&other);
	}

	auto Def_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Def_Macro>(function_display_name());
	}

	static const string NAME_PARAM_NAME{"name"};

	static const string VALUE_PARAM_NAME{"value"};

	auto Def_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &name_to_be_defined{stack.lookup(NAME_PARAM_NAME)};
		make_sure_name_is_undefined(name_to_be_defined, stack);

		// This was a bug. We referenced the top of the stack. This was a problem if the variable to be defined
		// (still referencing nil) eclipsed a variable or parameter with the same name. Therefore do not
		// reference the top of the stack (with the undefined variable) but one frame deeper.
		//
		// auto local_env{stack.new_child()};
		// auto value_to_be_set{stack.lookup(VALUE_PARAM_NAME).evaluate(*local_env)};
		//
		const auto value_to_be_set{stack.lookup(VALUE_PARAM_NAME).evaluate(stack.parent())};
		make_sure_value_is_not_free(name_to_be_defined, *value_to_be_set, stack);

		stack.parent().define_local_var(name_to_be_defined.name(), value_to_be_set);
		return value_to_be_set;
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(Token::NONE, make_unique<Name_Expression>(Token{
										    Token::Type::NAME, NAME_PARAM_NAME,
										    Token::NO_POS})),
			make_unique<List_Expression>(
					Token::NONE,
					make_unique<Quoted_Expression>(Token::NONE, make_unique<Name_Expression>(Token{
												    Token::Type::NAME,
												    VALUE_PARAM_NAME,
												    Token::NO_POS})))};

	auto Def_Macro::parameter_list() const -> const Expression & { return PARAMETERS; }

	auto Def_Macro::is_def_macro() const -> bool { return true; }

	auto Def_Macro::make_sure_name_is_undefined(const Expression &name_to_be_defined,
	                                            const Stack_Frame &stack) const -> void
	{
		if (!name_to_be_defined.is_name())
		{
			throw Semantic_Error{"Argument value " + text::single_quoted(name_to_be_defined.stringify()) +
			                                     " for parameter " + text::single_quoted(NAME_PARAM_NAME) +
			                                     " has to be a name.",
			                     name_to_be_defined.token().position()};
		}
		else if (stack.parent().is_defined_locally(name_to_be_defined.name()))
		{
			throw Semantic_Error{"Name " + text::single_quoted(name_to_be_defined.stringify()) +
			                                     " already defined.",
			                     name_to_be_defined.token().position()};
		}
	}

	auto Def_Macro::make_sure_value_is_not_free(const Expression &name, const Expression &value,
	                                            const Stack_Frame &stack) const -> void
	{
		if (expr::is_free(value, stack))
		{
			throw Semantic_Error{"Name " + text::single_quoted(name.stringify()) +
			                                     " is to be defined to be " +
			                                     text::single_quoted(value.stringify()) +
			                                     ". However, the value is a free expression and cannot be "
			                                     "assigned.",
			                     value.token().position()};
		}
	}

} // namespace appsl
