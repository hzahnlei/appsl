// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/error/program_error.hpp"

namespace appsl
{

	Program_Error::Program_Error(const shared_ptr<const Expression> &cause) noexcept
			: Base_Exception{"Program error."}, m_cause{cause}
	{
	}

	auto Program_Error::pretty_message() const noexcept -> Message
	{
		try
		{
			return what() + " "s + m_cause->stringify();
		}
		catch (...)
		{
			return "";
		}
	}

	auto Program_Error::cause() const -> const shared_ptr<const Expression> & { return m_cause; }

} // namespace appsl
