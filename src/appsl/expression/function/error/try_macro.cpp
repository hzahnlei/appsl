// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/error/try_macro.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/function/error/program_error.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Try_Macro::Try_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Try_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Try_Macro const *const>(&other);
	}

	auto Try_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Try_Macro>(function_display_name());
	}

	static const string TO_PARAM_NAME{"to"};

	static const string ON_ERROR_PARAM_NAME{"on-error"};

	auto Try_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &try_to{stack.lookup(TO_PARAM_NAME)};
		try
		{
			return try_to.evaluate(stack);
		}
		catch (const Program_Error &error)
		{
			// const auto on_error = stack.lookup(ON_ERROR_PARAM_NAME).evaluate(stack);
			// const auto &error_handler_parameter =
			//     expr::pure_param_name_without_quotes_etc(on_error->parameter_list().head()).name();
			// auto local_scope{stack.new_child()};
			// local_scope->define(error_handler_parameter, error.cause());
			// return on_error->evaluate(*local_scope);
			return stack.lookup(ON_ERROR_PARAM_NAME).evaluate(stack);
		}
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(
					Token::NONE, make_unique<Name_Expression>(Token{Token::Type::NAME,
	                                                                                TO_PARAM_NAME, Token::NO_POS})),
			make_unique<List_Expression>(
					Token::NONE,
					make_unique<Quoted_Expression>(Token::NONE, make_unique<Name_Expression>(Token{
												    Token::Type::NAME,
												    ON_ERROR_PARAM_NAME,
												    Token::NO_POS})))};

	auto Try_Macro::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
