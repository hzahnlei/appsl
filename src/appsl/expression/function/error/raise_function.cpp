// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/error/raise_function.hpp"
#include "appsl/expression/function/error/program_error.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Raise_Function::Raise_Function(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Raise_Function::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Raise_Function const *const>(&other);
	}

	auto Raise_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Raise_Function>(function_display_name());
	}

	static const string CAUSE_PARAM_NAME{"cause"};

	auto Raise_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &cause{stack.lookup(CAUSE_PARAM_NAME)};
		throw Program_Error{cause.shared_from_this()};
	}

	static const Name_Expression CAUSE_PARAM{Token{Token::Type::NAME, CAUSE_PARAM_NAME, Token::NO_POS}};

	auto Raise_Function::parameter_list() const -> const Expression & { return CAUSE_PARAM; }

} // namespace appsl
