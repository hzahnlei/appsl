// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/generic_predicate_function.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Generic_Predicate_Function::Generic_Predicate_Function(const Fun_Display_Name &name,
	                                                       const Predicate_Cpp_Fun predicate)
			: Abstract_Builtin_Function{name}, m_predicate(predicate)
	{
	}

	auto Generic_Predicate_Function::operator==(const Expression &other) const -> bool
	{
		const auto other_fun = dynamic_cast<Generic_Predicate_Function const *const>(&other);
		return nullptr != other_fun && function_display_name() == other_fun->function_display_name();
	}

	auto Generic_Predicate_Function::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Generic_Predicate_Function>(function_display_name(), m_predicate);
	}

	static const string EXPR_PARAM_NAME{"expr"};

	auto Generic_Predicate_Function::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &expr{stack.lookup(EXPR_PARAM_NAME)};
		return stack.boolean_literal_from(m_predicate(expr));
	}

	static const Name_Expression EXPR_PARAM{Token{Token::Type::NAME, EXPR_PARAM_NAME, Token::NO_POS}};

	auto Generic_Predicate_Function::parameter_list() const -> const Expression & { return EXPR_PARAM; }

} // namespace appsl
