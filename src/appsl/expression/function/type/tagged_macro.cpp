// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/type/tagged_macro.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/expression/tagged_expression.hpp"
#include "appsl/expression/text_literal_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Tagged_Macro::Tagged_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Tagged_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Tagged_Macro const *const>(&other);
	}

	auto Tagged_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Tagged_Macro>(function_display_name());
	}

	static const string EXPR_PARAM_NAME{"expr"};
	static const string WITH_PARAM_NAME{"with"};

	auto Tagged_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &expr{stack.lookup(EXPR_PARAM_NAME)};
		const auto &tag{stack.lookup(WITH_PARAM_NAME)};
		ensure_valid_tag(expr, tag);
		const auto evaluated_expr{expr.evaluate(stack)};
		ensure_taggable(*evaluated_expr, tag);
		return make_shared<Tagged_Expression>(Token::NONE, evaluated_expr, tag.clone());
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(Token::NONE, make_unique<Name_Expression>(Token{
										    Token::Type::NAME, EXPR_PARAM_NAME,
										    Token::NO_POS})),
			make_unique<Quoted_Expression>(Token::NONE, make_unique<Name_Expression>(Token{
										    Token::Type::NAME, WITH_PARAM_NAME,
										    Token::NO_POS}))};

	auto Tagged_Macro::parameter_list() const -> const Expression & { return PARAMETERS; }

	auto Tagged_Macro::ensure_valid_tag(const Expression &expr, const Expression &tag) const -> void
	{
		if (!tag.is_name())
		{
			throw Semantic_Error{"Trying to tag expression " + text::single_quoted(expr.stringify()) +
			                                     " with tag " + text::single_quoted(tag.stringify()) +
			                                     ". But the tag is not a name (aka identifier). Only names "
			                                     "are allowed as tag names. Furthermore such tag names "
			                                     "must start with "
			                                     "an upper case letter to set them appart visually from "
			                                     "other names that denote literals and functions etc. "
			                                     "Also, tag names must not contain '!' (functions with "
			                                     "side-effects) nor '?' (predicate functions).",
			                     tag.token().position()};
		}
		if (!expr::is_valid_type_name(tag))
		{
			throw Semantic_Error{"Trying to tag expression " + text::single_quoted(expr.stringify()) +
			                                     " with tag " + text::single_quoted(tag.stringify()) +
			                                     ". But the tag name is invalid. Tag names must start with "
			                                     "an upper case letter to set them appart visually from "
			                                     "other names that denote literals and functions etc. "
			                                     "Also, tag names must not contain '!' (functions with "
			                                     "side-effects) nor '?' (predicate functions).",
			                     tag.token().position()};
		}
	}

	auto Tagged_Macro::ensure_taggable(const Expression &expr, const Expression &tag) const -> void
	{
		// FIXME is this restriction necessary?
		if (!expr.is_map() && !expr.is_record())
		{
			throw Semantic_Error{
					"Trying to tag expression " + text::single_quoted(expr.stringify()) +
							" with type tag " + text::single_quoted(tag.stringify()) +
							". Only maps and records are allowed to be tagged. All other "
							"expressions are tagged already and that default tag must "
							"not be overridden.",
					tag.token().position()};
		}
	}

} // namespace appsl
