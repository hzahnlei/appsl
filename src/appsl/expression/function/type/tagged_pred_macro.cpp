// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/type/tagged_pred_macro.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/expression/tagged_expression.hpp"
#include "appsl/expression/text_literal_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Tagged_Predicate_Macro::Tagged_Predicate_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name}
	{
	}

	auto Tagged_Predicate_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Tagged_Predicate_Macro const *const>(&other);
	}

	auto Tagged_Predicate_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Tagged_Predicate_Macro>(function_display_name());
	}

	static const string EXPR_PARAM_NAME{"expr"};
	static const string WITH_PARAM_NAME{"with"};

	auto Tagged_Predicate_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &expr{stack.lookup(EXPR_PARAM_NAME)};
		const auto &tag{stack.lookup(WITH_PARAM_NAME)};

		if (!tag.is_name())
		{
			throw Semantic_Error{"Trying to determine tag of expression. However, type tags must be names. "
			                     "Type tag argument " +
			                                     text::single_quoted(tag.stringify()) + " is no name.",
			                     tag.token().position()};
		}

		const auto evaluated_expr{expr.evaluate(stack)};
		return stack.boolean_literal_from(evaluated_expr->tag().name() == tag.name());
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(Token::NONE, make_unique<Name_Expression>(Token{
										    Token::Type::NAME, EXPR_PARAM_NAME,
										    Token::NO_POS})),
			make_unique<Quoted_Expression>(Token::NONE, make_unique<Name_Expression>(Token{
										    Token::Type::NAME, WITH_PARAM_NAME,
										    Token::NO_POS}))};

	auto Tagged_Predicate_Macro::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
