// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/type/tag_macro.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/real_literal_expression.hpp"
#include "appsl/expression/tagged_expression.hpp"
#include "appsl/expression/text_literal_expression.hpp"
#include "appsl/runtime/semantic_error.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	Tag_Macro::Tag_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Tag_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Tag_Macro const *const>(&other);
	}

	auto Tag_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Tag_Macro>(function_display_name());
	}

	static const string OF_PARAM_NAME{"of"};

	auto Tag_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &expr{stack.lookup(OF_PARAM_NAME)};
		const auto evaluated_expr{expr.evaluate(stack)};
		return evaluated_expr->tag().clone();
	}

	static const auto PARAMETERS{make_unique<Quoted_Expression>(
			Token::NONE,
			make_unique<Name_Expression>(Token{Token::Type::NAME, OF_PARAM_NAME, Token::NO_POS}))};

	auto Tag_Macro::parameter_list() const -> const Expression & { return *PARAMETERS; }

} // namespace appsl
