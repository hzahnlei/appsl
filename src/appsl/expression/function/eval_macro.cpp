// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/function/eval_macro.hpp"
#include "appsl/expression/expression.hpp"
#include "appsl/expression/function/abstract_builtin_function.hpp"
#include "appsl/expression/list_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/quoted_expression.hpp"
#include "appsl/parser/token.hpp"
#include "appsl/runtime/stack_frame.hpp"

namespace appsl
{

	Eval_Macro::Eval_Macro(const Fun_Display_Name &name) : Abstract_Builtin_Function{name} {}

	auto Eval_Macro::operator==(const Expression &other) const -> bool
	{
		return nullptr != dynamic_cast<Eval_Macro const *const>(&other);
	}

	auto Eval_Macro::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Eval_Macro>(function_display_name());
	}

	static const string EXPR_PARAM_NAME{"expr"};

	auto Eval_Macro::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &to_be_evaluated{stack.lookup(EXPR_PARAM_NAME)};
		auto local_scope{stack.new_child()};
		return to_be_evaluated.is_name() ? to_be_evaluated.evaluate(*local_scope)->evaluate(*local_scope)
		                                 : to_be_evaluated.evaluate(*local_scope);
	}

	static const List_Expression PARAMETERS{
			Token::NONE,
			make_unique<Quoted_Expression>(Token::NONE, make_unique<Name_Expression>(Token{
										    Token::Type::NAME, EXPR_PARAM_NAME,
										    Token::NO_POS}))};

	auto Eval_Macro::parameter_list() const -> const Expression & { return PARAMETERS; }

} // namespace appsl
