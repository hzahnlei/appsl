// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/text_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;

	const shared_ptr<const Expression> Text_Literal_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Text", Token::NO_POS})};

	Text_Literal_Expression::Text_Literal_Expression(const Token &token) : m_token{token} {}

	Text_Literal_Expression::Text_Literal_Expression(Token &&token) : m_token{move(token)} {}

	auto Text_Literal_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_name{dynamic_cast<Text_Literal_Expression const *const>(&other)};
		return nullptr != other_name && m_token.lexeme() == other_name->m_token.lexeme();
	}

	auto Text_Literal_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_name{dynamic_cast<Text_Literal_Expression const *const>(&other)};
		return nullptr != other_name && m_token.lexeme() <= other_name->m_token.lexeme();
	}

	auto Text_Literal_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_name{dynamic_cast<Text_Literal_Expression const *const>(&other)};
		return nullptr != other_name && m_token.lexeme() < other_name->m_token.lexeme();
	}

	auto Text_Literal_Expression::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Text_Literal_Expression>(m_token);
	}

	auto Text_Literal_Expression::token() const -> const Token & { return m_token; }

	auto Text_Literal_Expression::head() const -> const Expression & { return *this; }

	auto Text_Literal_Expression::tail() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Text_Literal_Expression::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Text_Literal_Expression::is_nil() const -> bool { return false; }

	auto Text_Literal_Expression::is_name() const -> bool { return false; }

	auto Text_Literal_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Text_Literal_Expression::is_built_in_function() const -> bool { return true; }

	auto Text_Literal_Expression::is_function() const -> bool { return true; }

	auto Text_Literal_Expression::is_function_application() const -> bool { return false; }

	auto Text_Literal_Expression::is_fun_macro() const -> bool { return false; }

	auto Text_Literal_Expression::is_integer() const -> bool { return false; }

	auto Text_Literal_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Text_Literal_Expression::is_real() const -> bool { return false; }

	auto Text_Literal_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Text_Literal_Expression::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Text_Literal_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Text_Literal_Expression::is_quoted() const -> bool { return false; }

	auto Text_Literal_Expression::is_def_macro() const -> bool { return false; }

	auto Text_Literal_Expression::is_name_value_pair() const -> bool { return false; }

	auto Text_Literal_Expression::is_text() const -> bool { return true; }

	auto Text_Literal_Expression::text_value() const -> const string & { return m_token.lexeme(); }

	auto Text_Literal_Expression::is_list() const -> bool { return false; }

	auto Text_Literal_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:text_literal " << m_token << ")";
	}

	auto Text_Literal_Expression::stringify(ostream &out, const string &) const -> void
	{
		out << text::double_quoted(m_token.lexeme());
	}

} // namespace appsl
