// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/variadic_parameter.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	const shared_ptr<const Expression> Variadic_Parameter::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Variadic-Parameter", Token::NO_POS})};

	Variadic_Parameter::Variadic_Parameter(const Token &token, shared_ptr<const Expression> parameter)
			: m_token{token}, m_parameter{parameter}
	{
	}

	Variadic_Parameter::Variadic_Parameter(const Token &&token, shared_ptr<const Expression> parameter)
			: m_token{token}, m_parameter{parameter}
	{
	}

	auto Variadic_Parameter::operator==(const Expression &other) const -> bool
	{
		const auto other_variadic{dynamic_cast<Variadic_Parameter const *const>(&other)};
		return nullptr != other_variadic && *m_parameter == *(other_variadic->m_parameter);
	}

	auto Variadic_Parameter::operator<=(const Expression &other) const -> bool
	{
		const auto other_variadic{dynamic_cast<Variadic_Parameter const *const>(&other)};
		return nullptr != other_variadic && *m_parameter <= *(other_variadic->m_parameter);
	}

	auto Variadic_Parameter::operator<(const Expression &other) const -> bool
	{
		const auto other_variadic{dynamic_cast<Variadic_Parameter const *const>(&other)};
		return nullptr != other_variadic && *m_parameter < *(other_variadic->m_parameter);
	}

	auto Variadic_Parameter::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Variadic_Parameter>(m_token, m_parameter->clone());
	}

	auto Variadic_Parameter::token() const -> const Token & { return m_token; }

	auto Variadic_Parameter::head() const -> const Expression & { return *m_parameter; }

	auto Variadic_Parameter::tail() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Variadic_Parameter::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Variadic_Parameter::is_nil() const -> bool { return false; }

	auto Variadic_Parameter::is_name() const -> bool { return false; }

	auto Variadic_Parameter::name() const -> const string & { NOT_SUPPORTED(); }

	auto Variadic_Parameter::is_built_in_function() const -> bool { return false; }

	auto Variadic_Parameter::is_function() const -> bool { return false; }

	auto Variadic_Parameter::is_function_application() const -> bool { return false; }

	auto Variadic_Parameter::is_fun_macro() const -> bool { return false; }

	auto Variadic_Parameter::is_integer() const -> bool { return false; }

	auto Variadic_Parameter::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Variadic_Parameter::is_real() const -> bool { return false; }

	auto Variadic_Parameter::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Variadic_Parameter::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Variadic_Parameter::is_variadic_parameter() const -> bool { return true; }

	auto Variadic_Parameter::is_quoted() const -> bool { return false; }

	auto Variadic_Parameter::is_def_macro() const -> bool { return false; }

	auto Variadic_Parameter::is_name_value_pair() const -> bool { return false; }

	auto Variadic_Parameter::is_text() const -> bool { return false; }

	auto Variadic_Parameter::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Variadic_Parameter::is_list() const -> bool { return false; }

	auto Variadic_Parameter::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:variadic_parameter " << *m_parameter << ")";
	}

	auto Variadic_Parameter::stringify(ostream &out, const string &indentation) const -> void
	{
		m_parameter->stringify(out, indentation);
		out << "...";
	}

} // namespace appsl
