// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/quoted_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	const shared_ptr<const Expression> Quoted_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Quoted-Expression", Token::NO_POS})};

	Quoted_Expression::Quoted_Expression(const Token &token, shared_ptr<const Expression> expression)
			: m_token{token}, m_expression{expression}
	{
	}

	Quoted_Expression::Quoted_Expression(const Token &&token, shared_ptr<const Expression> expression)
			: m_token{token}, m_expression{expression}
	{
	}

	auto Quoted_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_quoted{dynamic_cast<Quoted_Expression const *const>(&other)};
		return nullptr != other_quoted && *m_expression == *(other_quoted->m_expression);
	}

	auto Quoted_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_quoted{dynamic_cast<Quoted_Expression const *const>(&other)};
		return nullptr != other_quoted && *m_expression <= *(other_quoted->m_expression);
	}

	auto Quoted_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_quoted{dynamic_cast<Quoted_Expression const *const>(&other)};
		return nullptr != other_quoted && *m_expression < *(other_quoted->m_expression);
	}

	auto Quoted_Expression::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Quoted_Expression>(m_token, m_expression->clone());
	}

	auto Quoted_Expression::token() const -> const Token & { return m_token; }

	auto Quoted_Expression::head() const -> const Expression & { return *m_expression; }

	auto Quoted_Expression::tail() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Quoted_Expression::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Quoted_Expression::is_nil() const -> bool { return false; }

	auto Quoted_Expression::is_name() const -> bool { return false; }

	auto Quoted_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Quoted_Expression::is_built_in_function() const -> bool { return false; }

	auto Quoted_Expression::is_function() const -> bool { return false; }

	auto Quoted_Expression::is_function_application() const -> bool { return false; }

	auto Quoted_Expression::is_fun_macro() const -> bool { return false; }

	auto Quoted_Expression::is_integer() const -> bool { return false; }

	auto Quoted_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Quoted_Expression::is_real() const -> bool { return false; }

	auto Quoted_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Quoted_Expression::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Quoted_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Quoted_Expression::is_quoted() const -> bool { return true; }

	auto Quoted_Expression::is_def_macro() const -> bool { return false; }

	auto Quoted_Expression::is_name_value_pair() const -> bool { return false; }

	auto Quoted_Expression::is_text() const -> bool { return false; }

	auto Quoted_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Quoted_Expression::is_list() const -> bool { return false; }

	auto Quoted_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:quoted " << *m_expression << ")";
	}

	auto Quoted_Expression::stringify(ostream &out, const string &indentation) const -> void
	{
		out << "'";
		m_expression->stringify(out, indentation);
	}

} // namespace appsl
