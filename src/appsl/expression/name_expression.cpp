// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{

	const shared_ptr<const Expression> Name_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Name", Token::NO_POS})};

	Name_Expression::Name_Expression(const Token &token) : m_token{token} {}

	Name_Expression::Name_Expression(Token &&token) : m_token{move(token)} {}

	auto Name_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_name{dynamic_cast<Name_Expression const *const>(&other)};
		return nullptr != other_name && m_token.lexeme() == other_name->m_token.lexeme();
	}

	auto Name_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_name{dynamic_cast<Name_Expression const *const>(&other)};
		return nullptr != other_name && m_token.lexeme() <= other_name->m_token.lexeme();
	}

	auto Name_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_name = dynamic_cast<Name_Expression const *const>(&other);
		return nullptr != other_name && m_token.lexeme() < other_name->m_token.lexeme();
	}

	auto Name_Expression::clone() const -> unique_ptr<Expression> { return make_unique<Name_Expression>(m_token); }

	auto Name_Expression::token() const -> const Token & { return m_token; }

	auto Name_Expression::head() const -> const Expression & { return *this; }

	auto Name_Expression::tail() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Name_Expression::evaluate(Stack_Frame &stack) const -> shared_ptr<const Expression>
	{
		const auto &looked_up{stack.lookup(name())};
		return looked_up.is_name() ? looked_up.evaluate(stack) : looked_up.shared_from_this();
	}

	auto Name_Expression::is_nil() const -> bool { return false; }

	auto Name_Expression::is_name() const -> bool { return true; }

	auto Name_Expression::name() const -> const string & { return m_token.lexeme(); }

	auto Name_Expression::is_built_in_function() const -> bool { return false; }

	auto Name_Expression::is_function() const -> bool { return false; }

	auto Name_Expression::is_function_application() const -> bool { return false; }

	auto Name_Expression::is_fun_macro() const -> bool { return false; }

	auto Name_Expression::is_integer() const -> bool { return false; }

	auto Name_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Name_Expression::is_real() const -> bool { return false; }

	auto Name_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Name_Expression::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Name_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Name_Expression::is_quoted() const -> bool { return false; }

	auto Name_Expression::is_def_macro() const -> bool { return false; }

	auto Name_Expression::is_name_value_pair() const -> bool { return false; }

	auto Name_Expression::is_text() const -> bool { return false; }

	auto Name_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Name_Expression::is_list() const -> bool { return false; }

	auto Name_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:name " << m_token << ")";
	}

	auto Name_Expression::stringify(ostream &out, const string &) const -> void { out << m_token.lexeme(); }

} // namespace appsl
