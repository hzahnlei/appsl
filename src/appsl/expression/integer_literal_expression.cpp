// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/integer_literal_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{
	const shared_ptr<const Expression> Integer_Literal_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Int", Token::NO_POS})};

	Integer_Literal_Expression::Integer_Literal_Expression(const Token &token,
	                                                       const Expression::Integer_Representation value)
			: m_token{token}, m_value{value}
	{
	}

	Integer_Literal_Expression::Integer_Literal_Expression(Token &&token,
	                                                       const Expression::Integer_Representation value)
			: m_token{move(token)}, m_value{value}
	{
	}

	auto Integer_Literal_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_int{dynamic_cast<Integer_Literal_Expression const *const>(&other)};
		return nullptr != other_int && m_value == other_int->m_value;
	}

	auto Integer_Literal_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_int{dynamic_cast<Integer_Literal_Expression const *const>(&other)};
		return nullptr != other_int && m_value <= other_int->m_value;
	}

	auto Integer_Literal_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_int{dynamic_cast<Integer_Literal_Expression const *const>(&other)};
		return nullptr != other_int && m_value < other_int->m_value;
	}

	auto Integer_Literal_Expression::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Integer_Literal_Expression>(m_token, m_value);
	}

	auto Integer_Literal_Expression::token() const -> const Token & { return m_token; }

	auto Integer_Literal_Expression::head() const -> const Expression & { return *this; }

	auto Integer_Literal_Expression::tail() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Integer_Literal_Expression::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Integer_Literal_Expression::is_nil() const -> bool { return false; }

	auto Integer_Literal_Expression::is_name() const -> bool { return false; }

	auto Integer_Literal_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Integer_Literal_Expression::is_built_in_function() const -> bool { return true; }

	auto Integer_Literal_Expression::is_function() const -> bool { return true; }

	auto Integer_Literal_Expression::is_function_application() const -> bool { return false; }

	auto Integer_Literal_Expression::is_fun_macro() const -> bool { return false; }

	auto Integer_Literal_Expression::is_integer() const -> bool { return true; }

	auto Integer_Literal_Expression::int_value() const -> Integer_Representation { return m_value; }

	auto Integer_Literal_Expression::is_real() const -> bool { return false; }

	auto Integer_Literal_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Integer_Literal_Expression::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Integer_Literal_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Integer_Literal_Expression::is_quoted() const -> bool { return false; }

	auto Integer_Literal_Expression::is_def_macro() const -> bool { return false; }

	auto Integer_Literal_Expression::is_name_value_pair() const -> bool { return false; }

	auto Integer_Literal_Expression::is_text() const -> bool { return false; }

	auto Integer_Literal_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Integer_Literal_Expression::is_list() const -> bool { return false; }

	auto Integer_Literal_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:integer_literal value:" << m_value << " " << m_token << ")";
	}

	auto Integer_Literal_Expression::stringify(ostream &out, const string &) const -> void
	{
		if (m_token.is_eof())
		{
			out << m_value;
		}
		else
		{
			out << m_token.lexeme();
		}
	}

} // namespace appsl
