// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/expression/key_value_pair_expression.hpp"
#include "appsl/expression/name_expression.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/stack_frame.hpp"
#include <junkbox/junkbox.hpp>

namespace appsl
{
	const shared_ptr<const Expression> Key_Value_Pair_Expression::TYPE_TAG{
			make_shared<Name_Expression>(Token{Token::Type::NAME, "Key-Value-Pair", Token::NO_POS})};

	Key_Value_Pair_Expression::Key_Value_Pair_Expression(const Token &token,
	                                                     const shared_ptr<const Expression> &key,
	                                                     const shared_ptr<const Expression> &value)
			: m_token{token}, m_key{key}, m_value{value}
	{
		DEV_ASSERT(nullptr != m_key, "Key must not be nullptr.");
		DEV_ASSERT(!m_key->is_nil(), "Key must not be nil.");
		DEV_ASSERT(nullptr != m_key, "Value must not be nullptr.");
	}

	Key_Value_Pair_Expression::Key_Value_Pair_Expression(Token &&token, const shared_ptr<const Expression> &key,
	                                                     const shared_ptr<const Expression> &value)
			: m_token{move(token)}, m_key{key}, m_value{value}
	{
		DEV_ASSERT(nullptr != m_key, "Key must not be nullptr.");
		DEV_ASSERT(!m_key->is_nil(), "Key must not be nil.");
		DEV_ASSERT(nullptr != m_key, "Value must not be nullptr.");
	}

	auto Key_Value_Pair_Expression::operator==(const Expression &other) const -> bool
	{
		const auto other_pair{dynamic_cast<Key_Value_Pair_Expression const *const>(&other)};
		return nullptr != other_pair && *m_key == *other_pair->m_key && *m_value == *other_pair->m_value;
	}

	auto Key_Value_Pair_Expression::operator<=(const Expression &other) const -> bool
	{
		const auto other_pair{dynamic_cast<Key_Value_Pair_Expression const *const>(&other)};
		return nullptr != other_pair && *m_key == *other_pair->m_key && *m_value <= *other_pair->m_value;
	}

	auto Key_Value_Pair_Expression::operator<(const Expression &other) const -> bool
	{
		const auto other_pair{dynamic_cast<Key_Value_Pair_Expression const *const>(&other)};
		return nullptr != other_pair && *m_key == *other_pair->m_key && *m_value < *other_pair->m_value;
	}

	auto Key_Value_Pair_Expression::clone() const -> unique_ptr<Expression>
	{
		return make_unique<Key_Value_Pair_Expression>(m_token, m_key->clone(), m_value->clone());
	}

	auto Key_Value_Pair_Expression::token() const -> const Token & { return m_token; }

	auto Key_Value_Pair_Expression::head() const -> const Expression & { return *m_key; }

	auto Key_Value_Pair_Expression::tail() const -> const Expression & { return *m_value; }

	auto Key_Value_Pair_Expression::evaluate(Stack_Frame &) const -> shared_ptr<const Expression>
	{
		return shared_from_this();
	}

	auto Key_Value_Pair_Expression::is_nil() const -> bool { return false; }

	auto Key_Value_Pair_Expression::is_name() const -> bool { return false; }

	auto Key_Value_Pair_Expression::name() const -> const string & { NOT_SUPPORTED(); }

	auto Key_Value_Pair_Expression::is_built_in_function() const -> bool { return false; }

	auto Key_Value_Pair_Expression::is_function() const -> bool { return false; }

	auto Key_Value_Pair_Expression::is_function_application() const -> bool { return false; }

	auto Key_Value_Pair_Expression::is_fun_macro() const -> bool { return false; }

	auto Key_Value_Pair_Expression::is_integer() const -> bool { return false; }

	auto Key_Value_Pair_Expression::int_value() const -> Integer_Representation { NOT_SUPPORTED(); }

	auto Key_Value_Pair_Expression::is_real() const -> bool { return false; }

	auto Key_Value_Pair_Expression::real_value() const -> Real_Representation { NOT_SUPPORTED(); }

	auto Key_Value_Pair_Expression::parameter_list() const -> const Expression & { return *Nil_Expression::NIL; }

	auto Key_Value_Pair_Expression::is_variadic_parameter() const -> bool { return false; }

	auto Key_Value_Pair_Expression::is_quoted() const -> bool { return false; }

	auto Key_Value_Pair_Expression::is_def_macro() const -> bool { return false; }

	auto Key_Value_Pair_Expression::is_name_value_pair() const -> bool { return true; }

	auto Key_Value_Pair_Expression::is_text() const -> bool { return false; }

	auto Key_Value_Pair_Expression::text_value() const -> const string & { NOT_SUPPORTED(); }

	auto Key_Value_Pair_Expression::is_list() const -> bool { return false; }

	auto Key_Value_Pair_Expression::operator<<(ostream &out) const -> ostream &
	{
		return out << "(EXPR type:key_value_pair " << m_token << " key:" << *m_key << " value:" << *m_value
		           << ")";
	}

	auto Key_Value_Pair_Expression::stringify(ostream &out, const string &) const -> void
	{
		out << m_key->stringify() << " -> " << m_value->stringify();
	}

} // namespace appsl
