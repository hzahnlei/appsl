// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/local_stack_frame.hpp"
#include "appsl/runtime/runtime_error.hpp"

namespace appsl
{

	Local_Stack_Frame::Local_Stack_Frame(shared_ptr<Stack_Frame> parent, shared_ptr<Stack_Frame> global)
			: m_parent_scope{parent}, m_global_scope{global}
	{
	}

	auto Local_Stack_Frame::parent() const -> Stack_Frame & { return *m_parent_scope; }

	auto Local_Stack_Frame::global() const -> Stack_Frame & { return *m_global_scope; }

} // namespace appsl
