// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/abstract_stack_frame.hpp"
#include "appsl/runtime/local_stack_frame.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using namespace junkbox;
	using fmt::format;

	auto Abstract_Stack_Frame::lookup(const string &name) const -> const Expression &
	{
		auto name_expr_pair{m_local_scope.find(name)};
		return (name_expr_pair == m_local_scope.end()) ? parent().lookup(name) : *(name_expr_pair->second);
	}

	auto Abstract_Stack_Frame::define_argument(const string &name, shared_ptr<const Expression> expression) -> void
	{
		define(name, expression);
		m_arg_count++;
	}

	auto Abstract_Stack_Frame::new_child() -> shared_ptr<Stack_Frame>
	{
		return make_shared<Local_Stack_Frame>(shared_from_this(), global().shared_from_this());
	}

	auto Abstract_Stack_Frame::is_defined_locally(const string &name) const -> bool
	{
		return m_local_scope.find(name) != m_local_scope.end();
	}

	auto Abstract_Stack_Frame::is_defined(const string &name) const -> bool
	{
		return is_defined_locally(name) || parent().is_defined(name);
	}

	auto Abstract_Stack_Frame::is_defined_globally(const string &name) const -> bool
	{
		return global().is_defined(name);
	}

	auto Abstract_Stack_Frame::boolean_literal_from(const bool value) const -> shared_ptr<const Expression>
	{
		return global().boolean_literal_from(value);
	}

	auto Abstract_Stack_Frame::define(const string &name, shared_ptr<const Expression> expression) -> void
	{
		if (name == "_")
		{
			return;
		}
		auto name_expr_pair{m_local_scope.find(name)};
		if (name_expr_pair == m_local_scope.end())
		{
			m_local_scope[name] = expression;
		}
		else
		{
			throw Runtime_Error{format("Name {} already defined in scope. Cannot override {} with {}.",
			                           text::single_quoted(name),
			                           text::single_quoted(name_expr_pair->second->stringify()),
			                           text::single_quoted(expression->stringify()))};
		}
	}

} // namespace appsl
