// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/semantic_error.hpp"

namespace appsl
{

	Semantic_Error::Semantic_Error(const string &message, const Token::Position &position) noexcept
			: Abstract_Compiler_Error{message, position}
	{
	}

	auto Semantic_Error::error_category_name() const noexcept -> string { return "Semantic"; }

} // namespace appsl
