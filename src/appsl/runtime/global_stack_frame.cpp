// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/global_stack_frame.hpp"
#include "appsl/expression/expression_traits.hpp"
#include "appsl/expression/function/arithmetic/division_function.hpp"
#include "appsl/expression/function/arithmetic/modulo_function.hpp"
#include "appsl/expression/function/arithmetic/product_function.hpp"
#include "appsl/expression/function/arithmetic/sqrt_function.hpp"
#include "appsl/expression/function/arithmetic/subtraction_function.hpp"
#include "appsl/expression/function/arithmetic/sum_function.hpp"
#include "appsl/expression/function/case_macro.hpp"
#include "appsl/expression/function/collection/count_function.hpp"
#include "appsl/expression/function/collection/enumeration_function.hpp"
#include "appsl/expression/function/collection/filter_function.hpp"
#include "appsl/expression/function/collection/filtering_generator.hpp"
#include "appsl/expression/function/collection/head_function.hpp"
#include "appsl/expression/function/collection/map_function.hpp"
#include "appsl/expression/function/collection/range_function.hpp"
#include "appsl/expression/function/collection/tail_function.hpp"
#include "appsl/expression/function/def_macro.hpp"
#include "appsl/expression/function/do_macro.hpp"
#include "appsl/expression/function/error/raise_function.hpp"
#include "appsl/expression/function/error/try_macro.hpp"
#include "appsl/expression/function/eval_macro.hpp"
#include "appsl/expression/function/file/import_function.hpp"
#include "appsl/expression/function/fun_macro.hpp"
#include "appsl/expression/function/generic_binary_macro.hpp"
#include "appsl/expression/function/generic_predicate_function.hpp"
#include "appsl/expression/function/generic_unary_function.hpp"
#include "appsl/expression/function/let_macro.hpp"
#include "appsl/expression/function/logic/and_macro.hpp"
#include "appsl/expression/function/logic/not_function.hpp"
#include "appsl/expression/function/logic/or_macro.hpp"
#include "appsl/expression/function/logic/true_function.hpp"
#include "appsl/expression/function/logic/xor_macro.hpp"
#include "appsl/expression/function/misc/random_function.hpp"
#include "appsl/expression/function/misc/seed_function.hpp"
#include "appsl/expression/function/reflection/is_name_macro.hpp"
#include "appsl/expression/function/relational/equal_to_function.hpp"
#include "appsl/expression/function/relational/less_than_function.hpp"
#include "appsl/expression/function/text/upper_case_function.hpp"
#include "appsl/expression/function/type/tag_macro.hpp"
#include "appsl/expression/function/type/tagged_macro.hpp"
#include "appsl/expression/function/type/tagged_pred_macro.hpp"
#include "appsl/expression/nil_expression.hpp"
#include "appsl/runtime/local_stack_frame.hpp"
#include "appsl/runtime/runtime_error.hpp"
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using fmt::format;
	using namespace junkbox;

	Global_Stack_Frame::Global_Stack_Frame() : m_true{make_unique<True_Function>("true")}
	{
		register_builtin_functions();
	}

	auto Global_Stack_Frame::lookup(const string &name) const -> const Expression &
	{
		auto name_expr_pair{m_local_scope.find(name)};
		return (name_expr_pair == m_local_scope.end()) ? *Nil_Expression::NIL : *(name_expr_pair->second);
	}

	auto Global_Stack_Frame::new_child() -> shared_ptr<Stack_Frame>
	{
		return make_shared<Local_Stack_Frame>(shared_from_this(), shared_from_this());
	}

	auto Global_Stack_Frame::is_defined(const string &name) const -> bool { return is_defined_locally(name); }

	auto Global_Stack_Frame::is_defined_globally(const string &name) const -> bool
	{
		return is_defined_locally(name);
	}

	auto Global_Stack_Frame::parent() const -> Stack_Frame & { NOT_SUPPORTED(); }

	auto Global_Stack_Frame::global() const -> Stack_Frame & { NOT_SUPPORTED(); }

	auto Global_Stack_Frame::boolean_literal_from(const bool value) const -> shared_ptr<const Expression>
	{
		return value ? m_true : Nil_Expression::NIL;
	}

	auto Global_Stack_Frame::register_builtin_functions() -> void
	{
		define_local_var("+", make_unique<Sum_Function>("+"));
		define_local_var("def!", make_unique<Def_Macro>("def!"));
		define_local_var("fun", make_unique<Fun_Macro>("fun"));
		define_local_var("*", make_unique<Product_Function>("*"));
		define_local_var("eval", make_unique<Eval_Macro>("eval"));
		define_local_var("/", make_unique<Div_Function>("/"));
		define_local_var("-", make_unique<Subtract_Function>("-"));
		define_local_var("mod", make_unique<Modulo_Function>("mod"));
		define_local_var("=", make_unique<Equal_To_Function>("="));
		define_local_var("not", make_unique<Not_Function>("not"));
		define_local_var("true", m_true);
		define_local_var("<", make_unique<Less_Than_Function>("<"));
		define_local_var("or", make_unique<Or_Macro>("or"));
		define_local_var("and", make_unique<And_Macro>("and"));
		define_local_var("xor", make_unique<Xor_Macro>("xor"));
		define_local_var("nil", make_unique<Nil_Expression>(Token::NONE));
		define_local_var("count", make_unique<Count_Function>("count"));
		define_local_var("head", make_unique<Head_Function>("head"));
		define_local_var("tail", make_unique<Tail_Function>("tail"));
		define_local_var("built-in?", make_unique<Generic_Predicate_Function>(
							      "built-in?", &Expression::is_built_in_function));
		define_local_var("function?",
		                 make_unique<Generic_Predicate_Function>("function?", &Expression::is_function));
		define_local_var("Integer?",
		                 make_unique<Generic_Predicate_Function>("Integer?", &Expression::is_integer));
		define_local_var("name?", make_unique<Is_Name_Macro>("name?"));
		define_local_var("number?", make_unique<Generic_Predicate_Function>("number?", expr::is_number));
		define_local_var("quoted?", make_unique<Generic_Predicate_Function>("quoted?", &Expression::is_quoted));
		define_local_var("Real?", make_unique<Generic_Predicate_Function>("Real?", &Expression::is_real));
		define_local_var("Text?", make_unique<Generic_Predicate_Function>("Text?", &Expression::is_text));
		define_local_var("variadic?", make_unique<Generic_Predicate_Function>(
							      "variadic?", &Expression::is_variadic_parameter));
		define_local_var("literal?", make_unique<Generic_Predicate_Function>(
							     "literal?", expr::is_literal_or_collection_thereof));
		define_local_var("list?", make_unique<Generic_Predicate_Function>("list?", &Expression::is_list));

		define_local_var("unquoted",
		                 make_unique<Generic_Unary_Function>("unquoted", expr::gracefully_unquoted));
		define_local_var("unvariadic",
		                 make_unique<Generic_Unary_Function>("unvadiadic", expr::gracefully_unvariadic));

		define_local_var("map", make_unique<Map_Function>("map"));

		define_local_var("import!", make_unique<Import_Function>("import!"));

		define_local_var("raise!", make_unique<Raise_Function>("raise!"));
		define_local_var("try", make_unique<Try_Macro>("try"));

		define_local_var("let", make_unique<Let_Macro>("let"));

		define_local_var("range", make_unique<Range_Function>("range"));

		define_local_var("upper_case", make_unique<Upper_Case_Function>("upper_case"));
		define_local_var("seed!", make_unique<Seed_Function>("seed!"));
		define_local_var("random", make_unique<Random_Function>("random"));
		define_local_var("tagged", make_unique<Tagged_Macro>("tagged"));
		define_local_var("tagged?", make_unique<Tagged_Predicate_Macro>("tagged?"));
		define_local_var("is_tagged?",
		                 make_unique<Generic_Predicate_Function>("is_tagged?", &Expression::is_tagged));
		define_local_var("tag", make_unique<Tag_Macro>("tag"));
		define_local_var("sqrt", make_unique<SQRT_Function>("sqrt"));
		const auto filter_fun{make_shared<const Filter_Function>("filter")};
		define_local_var("filter", filter_fun);
		define_local_var("keep", filter_fun);
		define_local_var("discard", make_unique<Filter_Function>("sqrt", Filtering_Generator::Mode::discard));
		define_local_var("enumerated", make_unique<Enumeration_Function>("enumerated"));
		define_local_var("case", make_unique<Case_Macro>("case"));
		define_local_var("custom_object?", make_unique<Generic_Predicate_Function>(
								   "custom_object?", &Expression::is_custom_object));
		define_local_var("record?", make_unique<Generic_Predicate_Function>("record?", &Expression::is_record));
		define_local_var(
				"has_field?",
				make_unique<Generic_Binary_Macro>(
						"has_field?",
						[](Stack_Frame &stack, const Expression &record,
		                                   const Expression &field_name)
						{
							if (!field_name.is_name())
							{
								throw Runtime_Error(format(
										"The 'field_name' argument to the {} "
										"function has to be a name. This {} "
										"is not a name.",
										text::single_quoted("has_field?"),
										text::single_quoted(
												field_name.stringify())));
							}
							return stack.boolean_literal_from(
									record.evaluate(stack)->has_property(
											field_name.name()));
						}));
		define_local_var("untagged",
		                 make_unique<Generic_Unary_Function>("untagged?", expr::gracefully_untagged));
		define_local_var("do", make_unique<Do_Macro>("do"));
	}

} // namespace appsl
