// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/script_executor.hpp"
#include "appsl/expression/expression_algorithms.hpp"
#include "appsl/parser/parser.hpp"
#include "appsl/parser/scanner.hpp"
#include "appsl/use_stl.hpp"
#include "appsl/version.hpp"
#include <chrono>
#include <fmt/core.h>
#include <junkbox/junkbox.hpp>

namespace appsl
{

	using fmt::format;
	using namespace std::chrono;
	using namespace junkbox;

	auto execute_script(const string &script, Stack_Frame &stack) -> void
	{
		Scanner scanner{script};
		Parser parser{scanner};
		while (parser.has_more_to_parse())
		{
			const auto result{parser.parse()->evaluate(stack)};
			// result may actually be a generator that lazily produces expression. Such expressions might
			// have side effects and therefore must be evaluated eagerly. The script executor that produces
			// console output evaluates all expressions implicitely by stringifying the result.
			expr::for_each(*result,
			               [&](const auto &)
			               {
					       // Intentionally left blank. Just consume all results.
				       });
		}
	}

	auto execute_script(const string &script, Stack_Frame &stack, ostream &out) -> void
	{
		Scanner scanner{script};
		Parser parser{scanner};
		try
		{
			while (parser.has_more_to_parse())
			{
				const auto result{parser.parse()->evaluate(stack)};
				result->stringify(out, "");
				out << '\n';
			}
		}
		catch (const Base_Exception &cause)
		{
			out << cause.pretty_message() << '\n';
		}
	}

	auto rep_loop(const string &hello, istream &in, ostream &out, ostream &err, Stack_Frame &stack) -> void
	{
		out << "app/SL Interpreter " << appsl::VERSION_MAJOR << '.' << appsl::VERSION_MINOR << '.'
		    << appsl::VERSION_PATCH << '\n';
		if (hello != "")
		{
			out << hello << '\n';
		}
		out << "By your command...\n";

		while (in)
		{
			const auto t1{high_resolution_clock::now()};

			try
			{
				out << "> ";
				string user_input;
				getline(in, user_input);
				Scanner scanner{user_input};
				Parser parser{scanner};
				while (parser.has_more_to_parse())
				{
					const auto expr{parser.parse()};
					expr->evaluate(stack)->stringify(out, "");
					out << '\n';
				}
			}
			catch (const Base_Exception &cause)
			{
				err << cause.pretty_message() << '\n';
			}

			const auto t2{high_resolution_clock::now()};
			const auto duration_us{duration_cast<microseconds>(t2 - t1).count()};
			out << "took " << duration_us << "µs\n";
		}

		out << "Bye!\n";
	}

} // namespace appsl
