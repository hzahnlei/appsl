// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/runtime_factory.hpp"
#include "appsl/runtime/prelude.hpp"
#include "appsl/runtime/script_executor.hpp"

namespace appsl
{

	auto make_runtime() -> shared_ptr<Stack_Frame>
	{
		// The top level stack frame contains all predefined functions and constants.
		auto runtime{make_shared<Global_Stack_Frame>()};
		// Now add additional definitions from a Lisp "file" (embedded as a string).
		execute_script(PRELUDE, *runtime);
		return runtime;
	}

} // namespace appsl
