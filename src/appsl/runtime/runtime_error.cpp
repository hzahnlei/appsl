// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include "appsl/runtime/runtime_error.hpp"
#include "appsl/use_stl.hpp"

namespace appsl
{

	Runtime_Error::Runtime_Error(const string &message) noexcept : Base_Exception{message} {}

	auto Runtime_Error::pretty_message() const noexcept -> string
	{
		try
		{
			return "Runtime error: " + Base_Exception::message();
		}
		catch (...)
		{
			return "";
		}
	}

} // namespace appsl
