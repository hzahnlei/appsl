# app/SL - Application Scripting Language

A simple scripting language.
Can either be run as an interactive scripting shell from command line or embedded into your applications.

Originally this started as an exercise to write yet another Lisp interpreter (for learning and fun).
One day I had the idea to integrate a scripting engine into one of my other C++ projects ([Fonted](https://gitlab.com/hzahnlei/fonted)).
Which scripting engine should I choose?
So I decided to take my own, simply because I know this best.

2020-02-14, Holger Zahnleiter

## Introduction

Of cause there are plenty of fast, proven, mature and powerful scripting engines out there.
So why should you use this one?
app/SL comes with some amenities on the pro side to compensate its deficiencies:

1. It is packaged with Conan.
   As a result it is easy to add as a dependency to your C++ projects.
2. It is relatively simple and small.
3. app/SL is implemented as a direct interpreter.
   There is no virtual machine.
   The interpreter directly evaluates the abstract syntax tree (AST) which the parser constructs from the text input.
4. **Everything is an expression.**
   app/SL does not know statements, it only knows expressions.
   Every expression evaluates to some value, beeing it `nil`.
4. The language also features some nice additions to the usual Lisp recipe:
   - app/SL features **named arguments**.
     When applying a function, the parameter names to that function can be put infront of the respective arguments.
     This can increases readability.
     One trivial example is `(open filename: "tmp/test.txt")`.
     Here `open` is the function name, `filename` is the name of the parameter and `"tmp/test.txt"` is the argument passed to the function.
   - (Almost) **everything is a function**.
     - **Literals** such as `1`, or `3.14`, etc. are interpreted as constant functions, mapping no argument to their own value.
       Hence, they can be applied like functions: `(1)`, or `(1 ())`.
     - Even the names `true`, and `nil` are representing functions.
       app/SL does not have an `if` expression.
       Instead `true`/`nil` are functions with `then` and `else` parameters.
       The `true` function evaluates to the (evaluated) `then` argument, while the `nil` function evaluates to the (evaluated) `else` argument.
       The `true` and `nil` functions can be used like so: `((odd? n) then: "odd" else: "even")`.
       Here, `odd?` is a predicate function and evaluates to `true` or `nil`, depending on the value of `n`.
       Then we apply the `then` and `else` arguments to the resulting `true` or `nil` function.
       Say `n` = 11 is odd, then the above expression evaluates like so:
       1. `((odd? n) then: "odd" else: "even")`
       2. `((odd? 11) then: "odd" else: "even")`
       3. `(true then: "odd" else: "even")`
       4. `"odd"`
   - **Partial function application** is supported.
     For example `(partial > 5)` evaluates to a function that accepts one argument `n`: `(fun n (> 5 n))`.
     It evaluates to `true` if `5 > n`, it evaluats to `nil` else.
   - Function applications can be **composed** with the pipe (`'|'`) symbol.
     This semantic sugar increases readability compared to nested function applications.
     In this example
     ```
     (range 1 10) | (filter odd?) | (filter (partial > 5)) | (map sq)
     ```
     we produce a range of integers.
     We only accept odd numbers, that is 1, 3, 5, 7, and 9.
     Furthermore we discard every number that is greater than 5, leaving 1 and 3.
     Finaly we square all survivors resulting in 1 and 9.
     Without the ability to compose functions we would rewrite the above example like this:
     ```
     (map (filter (filter (range 1 10) odd?) (fun n (> 5 n))) sq)
     ```
     I like the piped version better.
   - Literal syntax for **lists** , **sets** and **maps**.
     Appart from `'(1 2 3)` lists can be written `[1 2 3]`.
     Sets are written `{1 2 3}`.
     In opposite to lists, each element in a set must be unique.
     Furthermore sets are sorted.
     Maps are writte `#{1 -> "one" 2 -> "two" 3 -> "three"}`.
     Maps may be used like functions with the key as an argument, returning the value.
     For example `(#{1 -> "one" 2 -> "two" 3 -> "three"} 2) // gives "two"`.
   - app/SL supporst **lazy** and **eager evaluation**.
     The built-in functions `map`, `filter` and `range` evaluate lazily.
     Under the hood they use generators to produce one element at a time.
     Therefore you can process huge lists/sets but use only as much memory as is required for the current element.
     Using lazy evaluation with `map`, `filter` and `range` is like stream processing.
     The pre-defined functions `map'`, `filter'` and `range'` on the other hand are producing lists as their result.
     The computation takes as long as it takes to compute the complete list of *n* elements.
     The memory computed ist equivalent for the sum of the memory required for all *n* elements.
   - **Records** are similar to tuples or pairs, or maybe structs in C.
     However, elements of a record are addressed by their name, rather their position/numeric index.
     (Records share this with maps.
     But maps allow arbitrary keys while records allow only names.
     Furthermore maps are sorted, records preserve the order given by the programmer.)
     Special syntax allows to define records like so `(x: 1 y: 5)`.
     Furthermore there is a syntax for accessing fields.
     We use dots `.` just like many other languages to access elements within a record.
     Here is an example: `(name: (last: "Mouse" first: "Mickey") city:"Mouseton").name.first // gives "Mickey"`
     Alternatively the elements of an record are accessed by name `((x: 1 y:5) y) // gives 5`.
     Again, a record can be seen as a function which accepts its field names as arguments.

     Be advised that accessing record fields in a function fashion results in an error when part of function bodies.
     Imagine `(def! len (fun coord2d (sqrt (+ (sq (coord2d x)) (sq (coord2d y))))))`.
     At "compile time" the interpreter checks the function definition for unknown names.
     Due to the dynamic type system of this Lisp variant, the interpreter has no chance to recognize that `x` and `y` are valid field names of the `coord2d` parameter unless the function gets applied to concrete arguments.
     Therefore the interpreter is issuing an error message something like `...unknown name x...`.
     The solution is to quote `x` and `y` like so `(def! len (fun coord2d (sqrt (+ (sq (coord2d 'x)) (sq (coord2d 'y))))))`.
     This `(len (x:1.5 y: 3.4))` gives `3.71618`.
     However, you should prefer the field accessor syntax: `(def! len (fun coord2d (sqrt (+ (sq coord2d.x) (sq coord2d.y)))))`


## Building from Source

This can easily be built from the source code contained herein.
This requires the following tools:

* A modern C++ compiler, that supports C++ 20 and onwards.
* CMake, version 3.20 or newer.
* Conan, 2.0.10 or newer.
* GNU Make 3.81 or newer.

On the shell run `make dependencies`.
This will instruct Conan to download the libraries which the font editor is depending on.
(For example, ImGui, Catch2 etc.)

Then execute `make build`.
This should build the executable, if CMake and Clang/GCC are set up properly.
The binary will be written to `./build/Debug/src`.
For running it type `./build/Debug/src/appsli`, or, by running `make run`.

You can also make a **release build** without debug symbols by typing `make release-build`.
The binary will be written to `./build/Release/src`.
For running it type `./build/Release/src/appsli`.

## User's Guide

### As a Library

You can add app/SL as a scripting engine to your project using Conan package manager by adding the dependency to your `conanfile.txt` file:

```
[requires]
...
appsl/1.0.0
...
```

Make the Conan dependencies available via
```
conan install . --build=missing \
                --settings build_type=Debug \
	        --settings compiler.cppstd=gnu20
```

In your `CMakeLists.txt` add the dependency too:

```
...
find_package (appsl CONFIG REQUIRED)
...
target_link_libraries (YOUR_PROJECT ... appsl::appsl ...)

```

Now you should be able to compile your project like so:

```
cmake --preset conan-debug
cmake --build --preset conan-debug
```

Also take a look at my project [Fonted](https://gitlab.com/hzahnlei/fonted).
It demonstrates how to integrate app/SL.

### As a Read Eval Print Loop

There comes a rudimentary REPL for the commandline with this project.
This is not a serious scripting console.
It is more a means of interactively learning and testing app/SL language features.

You run it by typing `build/Debug/src/appsli`, assuming your binary endet up in the `build` folder during compile.

```
AVAILABLE COMMANDS

	HELP - This short help message.

		appsli help

	VERSION - Version information.

		appsli version

	IF NO COMMAND SPECIFIED - Starts an interactive scripting shell or interprets a skript file (if given as an argument).

		appsli [<filename>]

		<filename>: Name of the skript file to be interpreted.
		            Optional, positional parameter.
		            Defaults to ''.
		            Where <filename> is a string that might be empty (may contain whitespace).
```

## Acknowledgements

I am thankfully using many free and/or open source libraries, tools, and services.

-  [Visual Studio Code](https://code.visualstudio.com)
-  [C++ Extension for VSC](https://github.com/Microsoft/vscode-cpptools.git)
-  [CMake Extension for VSC](https://github.com/microsoft/vscode-cmake-tools)
-  [Test Extension for VSC](https://github.com/matepek/vscode-catch2-test-adapter.git)
-  [CMake](https://cmake.org)
-  [GNU Make](https://www.gnu.org/software/make/)
-  [Conan](https://conan.io)
-  [GCC](https://gcc.gnu.org)
-  [Clang](https://clang.llvm.org)
-  [GitLab](https://gitlab.com)
-  [Catch2](https://github.com/catchorg/Catch2)
-  [{fmt}](https://github.com/fmtlib/fmt)
-  Some I may not even be aware of.


## Disclaimer

This is a private, free, open source and non-profit project (see LINCENSE file).
Use on your own risk.
I am not liable for any damage caused.
I am a private person, not associated with any companies, or organizations I may have mentioned herein.
