#pragma once

// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include <string>

namespace appsl {
	static constexpr unsigned int VERSION_MAJOR{$ENV{VERSION_MAJOR}};
	static constexpr unsigned int VERSION_MINOR{$ENV{VERSION_MINOR}};
	static constexpr unsigned int VERSION_PATCH{$ENV{VERSION_PATCH}};
	static const std::string VERSION_EXTENSION{"$ENV{VERSION_EXTENSION}"};
} // namespace appsl
