;; ****************************************************************************
;;
;; app/SL - Application Scripting Language
;;
;; (c) 2020, 2024, Holger Zahnleiter, All rights reserved
;;
;; ****************************************************************************

(def! /= (fun (m n) (not (= m n)))) 

(def! even? (fun n (= (mod n 2) 0))) 

(def! odd? (fun n (not (even? n)))) 

(def! =< (fun (m n) (not (< n m)))) 

(def! > (fun (m n) (< n m))) 

(def! >= (fun (m n) (not (< m n)))) 

(def! apply (fun (f args...) (f args))) 

(def! partial (fun ('f arg) (fun args... (f arg args)))) 

(def! cons (fun (lhs rhs) lhs , rhs)) 

(def! nil? (fun expr (= nil expr))) 

(def! sq (fun x (* x x))) 

(def! range' (fun [from to] 
	((=< from to) 
		then: (let [(inc-range (fun [from to] 
					((=< from to) 
						then: (cons from (inc-range (+ from 1) to)) 
						else: nil ) ))] 
			   (inc-range from to) ) 
		else: (let [(dec-range (fun [from to] 
					((>= from to) 
						then: (cons from (dec-range (- from 1) to)) 
						else: nil ) ))] 
			   (dec-range from to) ) ) )) 

(def! sequence (fun [f n c?] 
			((c? n) 
				then: nil 
				else: (cons (f n) (sequence f (+ n 1) c?)) ) )) 

(def! collatz (fun n 
			((odd? n) 
				then: (+ (* 3 n) 1) 
				else: (/ n 2) ) )) 

(def! rec-sequence (fun [f n c?] 
			((c? n) 
				then: n 
				else: (cons n (rec-sequence f (f n) c?)) ) )) 

(def! map' (fun [c f] 
		((nil? c) 
			then: nil 
			else: (cons (f (head c)) (map' (tail c) f)) ) )) 

(def! filter' (fun [c p?] 
			((nil? c) 
			then: nil 
			else: ((p? (head c)) 
				then: (cons (head c) (filter' (tail c) p?)) 
				else: (filter' (tail c) p?) ) ) )) 

(def! keep' filter')


(def! discard' (fun [c p?] 
			((nil? c) 
			then: nil 
			else: ((p? (head c)) 
				then: (discard' (tail c) p?) 
				else: (cons (head c) (discard' (tail c) p?)) ) ) )) 

;; app/SL does not really know  data types.  However, it is possible to tag ex-
;; pressions with a  type name.  Such type names are supposed to  start with an
;; upper-case letter to separate it from other names. Here, 'Error' can be seen
;; as a constructor function that constructs an object of the type 'Error'. And
;; 'Error?'  on the other hand is a  predicate function  that checks whether an
;; object is of type 'Error'.
(def! Error (fun (msg cause) (tagged (message: msg cause: cause) Error))) 
(def! Error? (fun e (tagged? e with: Error))) 

(def! id (fun x x)) 

(def! _enum_ (fun [col i] 
		((nil? col) 
			then: nil 
			else: (i, (head col)), (_enum_ (tail col) (+ i 1)) ) )) 
;; This is an equivalent function using the const function for the const opera-
;; tor. Decide for yourself which variant you consider more readable.
;; (def! _enum_ (fun [col i] 
;; 		((nil? col) 
;; 			then: nil 
;; 			else: (cons (cons i (head col))
;; 			            (_enum_ (tail col) (+ i 1)) ) ) ))


(def! enumerated' (fun c (_enum_ c 0)))

;; In case-expressions 'default' always makes the last/default case match.
(def! default true)
