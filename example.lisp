#! build/Debug/src/appsli

;; Please note that the parser allows a  hash-bang at the beginning of a source
;; file. This way app/SL programs can be executed like scripts.

;; Here we assume you have built the artifacts:
;;
;;     make build
;;
;; To be shure you can build it like this:
;;
;;     make clean dependencies build

;; Run these examples like so:
;;
;;     build/Debug/src/appsli --show-output true example.lisp
;;
;; Console output will be suppressed without the --show-output option.
;;
;; You may also invoke the intearctive REPL:
;;
;;     build/Debug/src/appsli
;;
;; You can copy the examples from here and paste them into the REPL and observe
;; the outputs produced.  Of cause you can experiment and type your own expres-
;; sions.

;; Compute the square numbers for the first ten natural numbers.
(map (range 1 10) sq)
;; This is equivalent but using the composition operator '|'.
(range 1 10) | (map sq)
;; Yet another equivalent way to compute square numbers.  However, this time we
;; do not compute lazily but eagerly.  There are eager equivalents for the lazy
;; functions with the same name, except the name ends with an apostroph.
(map' (range' 1 10) sq)

;; We can also filter numbers we are interested in.
(range 1 10) | (filter odd?) | (map sq)
;; This is equivalent, but 'keep' might be more intuitive sometimes.  'keep' is
;; just an alias for 'filter'. 
(range 1 10) | (keep odd?) | (map sq)
;; This filters for even instead of odd numbers.
(range 1 10) | (discard odd?) | (map sq)
;; Equivalent again.
(range 1 10) | (keep even?) | (map sq)

;; By the way: Names of predicate functions end with an question mark (?).

;; List can be written in two ways: Either as a list literal or using the cons
;; operator. Both are equivalent.
[1 2 3 4]
1, 2, 3, 4

;; There is a  syntax for maps.  Maps are  datastructures but  can be used like
;; functions.
(def! my_map #{1->"one" 2->"two" 3->"three"})
(my_map 2)

;; By the way: Destructive functions, that are functions with side-effects, end
;; with an exclamation mark (!).

;; app/SL knows records. These are essentially like C structs.  The dot syntax,
;; which is familiar from many other languages, is used to address fields with-
;; in a record
(def! my_record (first_name: "pete" last_name: "park"))
my_record.first_name

;; There is no if/then/else.  Instead 'true' is a  binary function which evalu-
;; ates to the first argument. 'nil' is also a binary function but evaluates to
;; the second argument. Predicate functions either return 'true' or 'nil'.
;; app/SL supports named arguments.  The parameters to 'true' and 'nil' have be-
;; en named 'then' and 'else' coincidentally.
((odd? 13) then: "odd" else: "even")
;; You do not have to  name your arguments.  The following is equivalent to the
;; above.
((odd? 13) "odd" "even")

;; There is another way of making decisions: Case expressions.
 (def! check10 (fun x
                    (case (> x 10) -> "greater than 10"
                          (= x 10) -> "equal to 10"
                                      "lesser than 10" ) ))
(check10 9)
(check10 10)
(check10 11)
;; Again, this is equivalent,  using the name  'default' to better indicate the
;; default case to the programmer. I think this is better communication the in-
;; tension.  Therefore I recommend to  overcome ones  own laziness and not skip
;; the additional 'default ->'.
(def! check10' (fun x
                    (case (> x 10) -> "greater than 10"
                          (= x 10) -> "equal to 10"
                          default  -> "lesser than 10" ) ))
(check10' 9)
(check10' 10)
(check10' 11)
