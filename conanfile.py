# *****************************************************************************
#
# app/SL - Application Scripting Language
#
# (c) 2020, 2024, Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

import os

from conan import ConanFile
from conan.tools.cmake import cmake_layout
from conan.tools.files import copy


class AppSLLibrary(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeDeps", "CMakeToolchain"
    requires = eval(os.environ["SOURCE_DEPENDENCIES"])
    test_requires = eval(os.environ["TEST_DEPENDENCIES"])

    def layout(self):
        cmake_layout(self)
