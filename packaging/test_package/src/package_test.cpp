// ****************************************************************************
//
// app/SL - Application Scripting Language
//
// (c) 2020, 2024, Holger Zahnleiter, All rights reserved
//
// ****************************************************************************

#include <appsl/appsl.hpp>
#include <catch2/catch_test_macros.hpp>
#include <junkbox/junkbox.hpp>
#include <yacl/yacl.hpp>

/**
 * @brief A view random samples to see whether the library can be included and linked.
 */
TEST_CASE("Verify package version", "[package-test]")
{
	REQUIRE(appsl::VERSION_MAJOR == 1);
	REQUIRE(appsl::VERSION_MINOR == 5);
	REQUIRE(appsl::VERSION_PATCH == 0);
	REQUIRE(appsl::VERSION_EXTENSION == "");
}

/**
 * @brief Access to trasitive dependency
 */
TEST_CASE("Access to trasitive dependency", "[package-test]")
{
	// YACL is a direct dependency...
	REQUIRE(yacl::VERSION_MAJOR == 1);
	REQUIRE(yacl::VERSION_MINOR == 8);
	REQUIRE(yacl::VERSION_PATCH == 0);
	REQUIRE(yacl::VERSION_EXTENSION == "");
	// ...that has itself a dependency on Junkbox.
	// Our package manager sould provide this transitive dependency.
	REQUIRE(junkbox::VERSION_MAJOR == 1);
	REQUIRE(junkbox::VERSION_MINOR == 7);
	REQUIRE(junkbox::VERSION_PATCH == 3);
	REQUIRE(junkbox::VERSION_EXTENSION == "");
	REQUIRE(junkbox::text::single_quoted("Hello, World!") == "'Hello, World!'");
	REQUIRE(junkbox::text::double_quoted("Hello, World!") == "\"Hello, World!\"");
}
