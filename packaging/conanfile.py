# *****************************************************************************
#
# app/SL - Application Scripting Language
#
# (c) 2020, 2024, Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

import os
from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps
from conan.tools.files import copy


class AppSLRecipe(ConanFile):
    name = "appsl"
    version = "1.5.0"
    package_type = "library"

    # Optional metadata
    license = "MIT"
    author = "Holger Zahnleiter opensource@holger.zahnleiter.org"
    url = "https://gitlab.com/hzahnlei/appsl"
    description = "app/SL - Application Scripting Language - Yet another C++ scripting engine to be embedded into your applications."
    topics = (
        "app/SL",
        "c++",
        "library",
        "scripting language",
        "scripting",
        "lisp",
        "interpreter",
    )

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*", "models/*", "templates/*"

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.rm_safe("fPIC")

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")

    def layout(self):
        cmake_layout(self)

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(
            self,
            pattern="*.hpp",
            src=os.path.join(self.source_folder, "include"),
            dst=os.path.join(self.package_folder, "include"),
        )
        copy(
            self,
            pattern="*.hpp",
            src=os.path.join(self.source_folder, "generated"),
            dst=os.path.join(self.package_folder, "include"),
        )
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["appsl"]

    # This way I can reuse the exact same dependencies from the source project.
    # However,  the dependencies will not be visible to the client of my libra-
    # ry.
    # requires = eval(os.environ["SOURCE_DEPENDENCIES"])

    def requirements(self):
        # Otherweise I could define the dependencies like below. The additional
        # flag makes sure the client of this  lib sees the dependencies and can
        # use them.
        #
        # self.requires("junkbox/1.7.3", transitive_headers=True)
        # self.requires("yacl/1.8.0", transitive_headers=True)
        # self.requires("fmt/10.1.1", transitive_headers=True)
        # self.requires("yaml-cpp/0.8.0", transitive_headers=True)
        #
        # But in this case,  the dependenies and the  versions in the  packaged
        # lib and the rest of  this project may differ as the  dependencies are
        # defined in a redundant way. Therefore, I do this:
        dependencies = eval(os.environ["SOURCE_DEPENDENCIES"])
        for dependency in dependencies:
            if not dependency.startswith("appsl"):  # Not depending on itself!
                self.requires(dependency, transitive_headers=True)
